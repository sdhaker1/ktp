package com.skymapglobal.ktp_citizen;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skymapglobal.ktp_citizen.helper.Utils;

import java.util.HashMap;
import java.util.Map;


public class LogInActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private AppCompatEditText user_name;
    private static View mLoginForm;
    private String mMobileNo="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_actvity);
        user_name = (AppCompatEditText) findViewById(R.id.editText_username);
        mLoginForm = findViewById(R.id.login_form);
        findViewById(R.id.btn_login).setOnClickListener(this);
        context = LogInActivity.this;
        mLoginForm.setVisibility(View.INVISIBLE);
        initLoginView();
    }

    private void initLoginView() {
        if (Utils.getBoolPreferences(context, "IS_LOGIN")) {
            Intent intent = new Intent(context, ActivityMain.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } else {
            mLoginForm.setVisibility(View.VISIBLE);
        }
    }


    private void SendOTP(final String otp, final String mobileno){
        final String smstext=otp+" IS YOUR OTP TO COMPLETE THE REGISTRATION PROCESS - KOLKATA POLICE";
        String REGISTER_URL="http://193.105.74.58/api/v3/sendsms/plain";
        final ProgressDialog progressDialog=new ProgressDialog(LogInActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        ShowOTPVerification(mobileno,otp);
                        //Toast.makeText(getActivity(),response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(LogInActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("user","Suhas");
                params.put("password","Sid@2017");
                params.put("sender", "KOLPOL");
                params.put("SMSText", smstext);
                params.put("GSM", mobileno);
                params.put("type", "longSMS");
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(LogInActivity.this);
        requestQueue.add(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                45000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                if ( user_name.getText().toString().trim().length()!=10) {
                    Toast.makeText(context, "Please enter 10 digit mobile no", Toast.LENGTH_LONG).show();
                } else {
                    mMobileNo=user_name.getText().toString().trim();
                    String otp=GenerateOTP();
                    SendOTP(otp,"91"+mMobileNo);
                }
                break;
            default:
                break;
        }

    }

    private String GenerateOTP(){
        String otp="";
        int randomPIN = (int)(Math.random()*9000)+1000;
        otp=""+randomPIN;
        return otp;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.exit(0);
        finishAffinity();
    }

    private void ShowOTPVerification(final String mobile_no,final String otp_no) {
        View view = this.getLayoutInflater().inflate(R.layout.dialog_otp_verification, null);
        final EditText edTextOTP = (EditText) view.findViewById(R.id.ed_otp);
        android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(this);
        dialog.setView(view);
        dialog.setCancelable(false);
        dialog.setTitle("Verification");
        dialog.setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                /*String entered_otp=edTextOTP.getText().toString().trim();
                if(entered_otp.equalsIgnoreCase(otp_no)){
                    dialog.dismiss();
                }else{
                    Toast.makeText(context,"Please provide valid OTP",Toast.LENGTH_SHORT).show();
                }*/
            }
        });
        dialog.setNegativeButton("RE SEND", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                /*String otp=GenerateOTP();
                SendOTP(otp,"91"+mobile_no);
                dialog.dismiss();*/
            }
        });


        final android.support.v7.app.AlertDialog alert = dialog.create();
        alert.show();
        alert.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                String entered_otp=edTextOTP.getText().toString().trim();
                if(entered_otp.equalsIgnoreCase(otp_no)){
                    alert.dismiss();
                    Utils.saveBoolPreferences(context,"IS_LOGIN",true);
                    Utils.saveStringPreferences(context,"MY_NUMBER",mMobileNo);
                    Intent intent = new Intent(context, ActivityMain.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(context,"Please provide valid OTP",Toast.LENGTH_SHORT).show();
                }

            }
        });

        alert.getButton(android.app.AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /*Boolean wantToCloseDialog = true;
                //Do stuff, possibly set wantToCloseDialog to true then...
                if(wantToCloseDialog)
                    alert.dismiss();
                //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.*/
                String otp=GenerateOTP();
                SendOTP(otp,mobile_no);
                alert.dismiss();
            }
        });
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.parseColor("#47A097"));
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#47A097"));
    }

}