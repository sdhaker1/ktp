package com.skymapglobal.ktp_citizen;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.ErrorDialogFragment;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.skymapglobal.ktp_citizen.fragments.AddPanicFragment;
import com.skymapglobal.ktp_citizen.fragments.CityParkingFragment;
import com.skymapglobal.ktp_citizen.fragments.ETAFragment;
import com.skymapglobal.ktp_citizen.fragments.MissingMobileFragment;
import com.skymapglobal.ktp_citizen.fragments.PronamRegisterFragment;
import com.skymapglobal.ktp_citizen.fragments.ReportCrimeFragment;
import com.skymapglobal.ktp_citizen.fragments.ReportViolationFragment;
import com.skymapglobal.ktp_citizen.fragments.resgister.DomesticHelpFragment;
import com.skymapglobal.ktp_citizen.fragments.resgister.SecurityGuardFragment;
import com.skymapglobal.ktp_citizen.fragments.resgister.ServiceProviderFragment;
import com.skymapglobal.ktp_citizen.fragments.resgister.TenantRegisterFragment;
import com.skymapglobal.ktp_citizen.helper.Utils;

import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Admin on 14-03-2017.
 */

public class MenuActivity extends AppCompatActivity
        implements EasyPermissions.PermissionCallbacks,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int REQUEST_READ_PHONE_STATE = 10;
    private static final int REQUEST_CALL_PHONE = 11;
    private static final int REQUEST_READ_CONTACTS = 12;
    private static final int REQUEST_WRITE_CONTACTS = 13;
    private static final int REQUEST_LOCATION_SERVICE = 21;
    private static final int REQUEST_RESOLVE_ERROR = 22;

    public static final String DIALOG_ERROR = "dialog_error";
    public static final String STATE_RESOLVING_ERROR = "resolving_error";

    private boolean mResolvingError = false;
    public GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;
    protected Location mLastLocation;
    private Fragment mFragment;

    boolean doubleBackToExitPressedOnce = false;

    private String mType    ="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_main);
        loadIMEI();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mType = getIntent().getStringExtra("KEY");

        mResolvingError = savedInstanceState != null
                && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);

        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getApplicationContext(), "Please update google play service", Toast.LENGTH_LONG).show();
            finish();
        } else if (!isDeviceOnline()) {
            Toast.makeText(getApplicationContext(), "Please connect to internet", Toast.LENGTH_LONG).show();
            finish();
        } else {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.show();
            setupAPI();
        }
    }




    private void setupAPI() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void getMyLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_SERVICE);
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 5);
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE}, 6);
            return;
        }
        if (isEnableGPS()) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getMyLocation();
        mProgressDialog.dismiss();



        if (mFragment == null) {
            if(mType.equalsIgnoreCase("ETA")){
                getSupportActionBar().setTitle("Know Your Travel Time");
                if (mFragment instanceof ETAFragment) {
                } else {
                    mFragment = ETAFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
            }
            if(mType.equalsIgnoreCase("PARKING")){
                getSupportActionBar().setTitle("City Parking (Park Street Pilot Project)");
                if (mFragment instanceof CityParkingFragment) {
                } else {
                    mFragment = CityParkingFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
            }
            if(mType.equalsIgnoreCase("MISSING_MOBILE")){
                getSupportActionBar().setTitle("Missing Mobile Status");
                if (mFragment instanceof MissingMobileFragment) {
                } else {
                    mFragment = MissingMobileFragment.newInstance();
                    startFragment(mFragment);
                }
            }

            if(mType.equalsIgnoreCase("PRONAM_REGISTER")){
                //Toast.makeText(MenuActivity.this,"Development on progress",Toast.LENGTH_LONG).show();
                getSupportActionBar().setTitle("Pronam Register");
                if (mFragment instanceof PronamRegisterFragment) {
                } else {
                    mFragment = PronamRegisterFragment.newInstance();
                    startFragment(mFragment);
                }
            }

            if(mType.equalsIgnoreCase("TENANT")){
                getSupportActionBar().setTitle("Tenant Registration");
                if (mFragment instanceof TenantRegisterFragment) {
                } else {
                    mFragment = TenantRegisterFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
            }
            if(mType.equalsIgnoreCase("BABY_SITTER")){
                getSupportActionBar().setTitle("Domestic Help/Baby Sitter");
                if (mFragment instanceof DomesticHelpFragment) {
                } else {
                    mFragment = DomesticHelpFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
            }
            if(mType.equalsIgnoreCase("SECURITY_GUARD")){
                getSupportActionBar().setTitle("Security Guard Registration");
                if (mFragment instanceof SecurityGuardFragment) {
                } else {
                    mFragment = SecurityGuardFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
            }
            if(mType.equalsIgnoreCase("SERVICE_PROVIDER")){
                getSupportActionBar().setTitle("Milkman etc Registration");
                if (mFragment instanceof ServiceProviderFragment) {
                } else {
                    mFragment = ServiceProviderFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
            }
            if(mType.equalsIgnoreCase("REPORT_CRIME")){
                getSupportActionBar().setTitle("Report Crime");
                if (mFragment instanceof ReportCrimeFragment) {
                } else {
                    mFragment = ReportCrimeFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
            }
            if(mType.equalsIgnoreCase("REPORT_TRAFFIC_VIOLATION")){
                getSupportActionBar().setTitle("Report Traffic Violation");
                if (mFragment instanceof ReportViolationFragment) {
                } else {
                    mFragment = ReportViolationFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
            }

            if(mType.equalsIgnoreCase("ADD_PANIC")){
                getSupportActionBar().setTitle("Register Emergency Contact");
                if (mFragment instanceof AddPanicFragment) {
                } else {
                    mFragment = AddPanicFragment.newInstance();
                    startFragment(mFragment);
                }
            }
            //For Durga Puja purpose only
            //mFragment = ViewTrafficFragment.newInstance(mGoogleApiClient, mLastLocation);
            //mFragment = ETAFragment.newInstance(mGoogleApiClient, mLastLocation);
            //startFragment(mFragment);

            /*mFragment = MissingMobileFragment.newInstance();
            startFragment(mFragment);*/
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    private void showErrorDialog(int errorCode) {
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getFragmentManager(), "Error dialog");
    }

    public void onDialogDismissed() {
        mResolvingError = false;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (mResolvingError) {
            return;
        } else if (connectionResult.hasResolution()) {
            try {
                mResolvingError = true;
                connectionResult.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GoogleApiAvailability.getErrorDialog()
            showErrorDialog(connectionResult.getErrorCode());
            mResolvingError = true;
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (requestCode == REQUEST_LOCATION_SERVICE) {
            getMyLocation();
        } else if (requestCode == 5) {
            getMyLocation();
        } else if (requestCode == 6) {
            getMyLocation();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @SuppressWarnings("StatementWithEmptyBody")

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        getMyLocation();
        switch (id) {
            /*
            R.id.nav_view_traffic is For Durga Puja Purpose
             */
            /*case R.id.nav_view_traffic:
                getSupportActionBar().setTitle("View Pujas");
                if (mFragment instanceof ViewTrafficFragment) {
                    break;
                } else {
                    mFragment = ViewTrafficFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;*/
            case R.id.nav_eta:
                getSupportActionBar().setTitle("ETA Source Destination");
                if (mFragment instanceof ETAFragment) {
                    break;
                } else {
                    mFragment = ETAFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;
            case R.id.nav_report_crime:
                getSupportActionBar().setTitle("Report Crime");

                if (mFragment instanceof ReportCrimeFragment) {
                    break;
                } else {
                    mFragment = ReportCrimeFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;
            case R.id.nav_report_traffic_violation:
                getSupportActionBar().setTitle("Report Traffic Violation");
                if (mFragment instanceof ReportViolationFragment) {
                    break;
                } else {
                    mFragment = ReportViolationFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;

//            case R.id.nav_help:
//                getSupportActionBar().setTitle("Register With Police");
//
//                if (mFragment instanceof RegisterFragment) {
//                    break;
//                } else {
//                    mFragment = RegisterFragment.newInstance(mGoogleApiClient, mLastLocation);
//                    startFragment(mFragment);
//                }
//                break;
            case R.id.nav_report_pending_traffic:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://kolkatatrafficpolice.net/"));
                startActivity(browserIntent);
                break;

            case R.id.nav_report_FIR:
                Intent browserIntentFIR = new Intent(Intent.ACTION_VIEW, Uri.parse("http://182.71.240.212/kpfirs"));
                startActivity(browserIntentFIR);
                break;

            case R.id.nav_report_missing_mobile:
                getSupportActionBar().setTitle("Missing Mobile Tracker");

                if (mFragment instanceof MissingMobileFragment) {
                    break;
                } else {
                    mFragment = MissingMobileFragment.newInstance();
                    startFragment(mFragment);
                }
                break;
            case R.id.nav_help:
                showContactUsDialog();
                break;
            case R.id.nav_parking:
                getSupportActionBar().setTitle("City Parking(Park St. Pilot Project)");

                if (mFragment instanceof CityParkingFragment) {
                    break;
                } else {
                    mFragment = CityParkingFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;

            case R.id.nav_karmameter:
                try {
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("org.ky21c.karmameter.passenger");
                    if (launchIntent != null) {
                        startActivity(launchIntent);//null pointer check in case package name was not found
                    }else{
                        final String appPackageName = "org.ky21c.karmameter.passenger"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(MenuActivity.this, "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.nav_tenant_register:
                getSupportActionBar().setTitle("Tenant Registration");

                if (mFragment instanceof TenantRegisterFragment) {
                    break;
                } else {
                    mFragment = TenantRegisterFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;
            case R.id.nav_security_guard_registration:
                getSupportActionBar().setTitle("Security Guard Registration");

                if (mFragment instanceof SecurityGuardFragment) {
                    break;
                } else {
                    mFragment = SecurityGuardFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;
            case R.id.nav_domestic_help:
                getSupportActionBar().setTitle("Domestic Help/Baby Sitter Registration");

                if (mFragment instanceof DomesticHelpFragment) {
                    break;
                } else {
                    mFragment = DomesticHelpFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;
            case R.id.nav_service_provider:
                getSupportActionBar().setTitle("Milkman etc. Registration");

                if (mFragment instanceof ServiceProviderFragment) {
                    break;
                } else {
                    mFragment = ServiceProviderFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;
            default:
                break;
        }

        return true;
    }

    private void startFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }


    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    public boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStop() {

        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    private boolean isEnableGPS() {
        final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showSettingsAlert();
            return false;
        }
        return true;
    }

    public void showSettingsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please enable location service.")
                .setCancelable(false)
                .setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        LocationManager service = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        boolean enabled = service
                                .isProviderEnabled(LocationManager.GPS_PROVIDER);

                        if (!enabled) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    }
                });

        AlertDialog alert = builder.create();
        alert.setTitle("Location Service Disabled");
        alert.show();
    }


    private void showContactUsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Connect with Us");
        //builder.setMessage("Please select type connection");
        View view = getLayoutInflater().inflate(R.layout.dialog_connect_us, null);
        builder.setView(view);
        view.findViewById(R.id.bt_sms).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = "9903588888";
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
            }
        });

        view.findViewById(R.id.bt_email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"octcr@kolkatatrafficpolice.gov.in"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Kolkata Police");
                startActivity(Intent.createChooser(intent, "Send Email"));
            }
        });

        view.findViewById(R.id.bt_whatsapp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PackageManager pm = getPackageManager();
                try {
//                    if (ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.READ_CONTACTS)
//                            != PackageManager.PERMISSION_GRANTED) {
//                        requestReadContactPermission();
//                        return;
//                    }
//
//                    if (ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.WRITE_CONTACTS)
//                            != PackageManager.PERMISSION_GRANTED) {
//                        requestWriteContactPermission();
//                        return;
//                    }
//
//
//                    Cursor c = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
//                            new String[]{ContactsContract.Contacts.Data._ID}, ContactsContract.Data.DATA1 + "=?",
//                            new String[]{"+919903588888"}, null);
//                    if (c == null || !c.isFirst()) {
//                        ContentResolver cr = MainActivity.this.getContentResolver();
//                        ContentValues cv = new ContentValues();
//                        cv.put(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, "KolkataPolice");
//                        cv.put(ContactsContract.CommonDataKinds.Phone.NUMBER, "+919903588888");
//                        cv.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
//                        cr.insert(ContactsContract.RawContacts.CONTENT_URI, cv);
//                        return;
//                    }

                    Intent waIntent = new Intent(Intent.ACTION_SEND, Uri.fromParts("smsto:", "+919903588888", null));
                    waIntent.setType("text/plain");
                    PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                    waIntent.setPackage(info.packageName);
                    startActivity(waIntent);

                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(MenuActivity.this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                            .show();
                }

            }
        });
        view.findViewById(R.id.bt_phone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestCallPhonePermission();
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "1073"));
                    startActivity(intent);
                }

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void loadIMEI() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            requestReadPhoneStatePermission();
        } else {
            doPermissionGrantedStuffs();
        }
    }

    public void doPermissionGrantedStuffs() {
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        Utils.IMEI_No = tm.getDeviceId();
    }

    private void requestReadPhoneStatePermission() {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.READ_PHONE_STATE)) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_PHONE_STATE},
                    REQUEST_READ_PHONE_STATE);
        }
    }

    private void requestCallPhonePermission() {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CALL_PHONE)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.CALL_PHONE},
                    REQUEST_CALL_PHONE);
        }
    }

    private void requestReadContactPermission() {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_CONTACTS)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    REQUEST_READ_CONTACTS);
        }
    }

    private void requestWriteContactPermission() {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_CONTACTS)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_CONTACTS},
                    REQUEST_WRITE_CONTACTS);
        }
    }
}
