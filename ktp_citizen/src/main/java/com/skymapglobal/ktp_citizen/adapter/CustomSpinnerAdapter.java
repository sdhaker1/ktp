package com.skymapglobal.ktp_citizen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skymapglobal.ktp_citizen.R;
import com.skymapglobal.ktp_citizen.models.PoliceStationDetails;

import java.util.ArrayList;

/**
 * Created by Admin on 02-03-2017.
 */

public class CustomSpinnerAdapter extends ArrayAdapter<PoliceStationDetails> {

    private final Context context;
    private final ArrayList<PoliceStationDetails> nameValues;
    private ViewHolder viewHolder;
    private final int resourceId;

    public CustomSpinnerAdapter(Context context, int resourceId,ArrayList<PoliceStationDetails> nameValues) {
        super(context,resourceId,nameValues);
        this.context = context;
        this.nameValues = nameValues;
        this.resourceId = resourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resourceId, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.txtView = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.txtView.setText(nameValues.get(position).getStationName());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resourceId, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.txtView = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.txtView.setText(nameValues.get(position).getStationName());
        return convertView;
    }


    public class ViewHolder {
        TextView txtView;
    }
}
