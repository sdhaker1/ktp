package com.skymapglobal.ktp_citizen.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by thaibui on 9/10/16.
 */
public class ServiceConnector {
    private static ServiceConnector ourInstance = new ServiceConnector();
    public final String TAG = this.getClass().getSimpleName();
    public RequestQueue mRequestQueue;

    public static ServiceConnector getInstance() {
        return ourInstance;
    }

    public void init(Context context) {
        if (mRequestQueue == null)
            mRequestQueue = Volley.newRequestQueue(context);
    }

    public void sendRequest(Context context, final String method, String param, final String type,final IServiceCallback callback) {
        String url="";
        if(type.equals("GET_MOBILE_MISSING_URL")){
            url = method + param;
            url = url.replace(" ", "%20");
        }else if(type.equals("PRONAM_SUBMISSION_URL")){
            url = method + param;
            url = url.replace(" ", "%20");
        }else if(type.equals("PARKING")){
            url = method + param;
            url = url.replace(" ", "%20");
        }else{
            url = method + param;
        }

        final ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.cancel();
                        if (response != null) {
                            callback.onReceiver(method, response);
                        } else {
                            callback.onError(method, "No data found!");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.cancel();
                        callback.onError(method, error.getMessage());
                    }
                }) {
        };

        addToRequestQueue(stringRequest);
    }


    private <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        mRequestQueue.add(req);
    }

    private <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        mRequestQueue.add(req);
    }

    private void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

}
