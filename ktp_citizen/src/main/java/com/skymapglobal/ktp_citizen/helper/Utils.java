package com.skymapglobal.ktp_citizen.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static String IMEI_No;
    public static String MOBILE_No;
    public static void saveBoolPreferences(Context context, String key,
                                           boolean value) {
        SharedPreferences faves = PreferenceManager
                .getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = faves.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getBoolPreferences(Context context, String key) {
        SharedPreferences myPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        boolean rtn = myPrefs.getBoolean(key, false);

        return rtn;
    }

    public static void saveIntPreferences(Context context, String key,
                                          Integer value) {
        SharedPreferences faves = PreferenceManager
                .getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = faves.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int getIntPreferences(Context context, String key,
                                        int defaultValue) {
        SharedPreferences myPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        int rtn = myPrefs.getInt(key, defaultValue);

        return rtn;
    }

    public static void saveFloatPreferences(Context context, String key,
                                            Float value) {
        SharedPreferences faves = PreferenceManager
                .getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = faves.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    public static float getFloatPreferences(Context context, String key,
                                            float defaultValue) {
        SharedPreferences myPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        float rtn = myPrefs.getFloat(key, defaultValue);

        return rtn;
    }

    public static void saveStringPreferences(Context context, String key,
                                             String value) {

        SharedPreferences faves = PreferenceManager
                .getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = faves.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getStringPreferences(Context context, String key,
                                              String defaultstr) {
        SharedPreferences myPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        String rtn = myPrefs.getString(key, defaultstr);

        return rtn;
    }

    public static String getMobileNumber(Context context){
        if(MOBILE_No==null) {
            TelephonyManager mngr = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
            try {

                if (mngr != null && mngr.getLine1Number().length() != 0) {
                    MOBILE_No = mngr.getLine1Number();
                } else {
//                    imei = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                }
            }catch (Exception e){
//                imei = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
        }
        if(MOBILE_No==null) return "0000000000";
        return MOBILE_No;
//        return "359453060708821";
    }


    public static String getIMEI(Context context){
        if(IMEI_No==null) {
            TelephonyManager mngr = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
            try {

                if (mngr != null && mngr.getDeviceId().length() != 0) {
                    IMEI_No = mngr.getDeviceId();
                } else {
//                    imei = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                }
            }catch (Exception e){
//                imei = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
        }
        if(IMEI_No==null) return "00000000000000000";
        return IMEI_No;
//        return "359453060708821";
    }
//
//    /**
//     * Called when the 'loadIMEI' function is triggered.
//     */
//    public void loadIMEI(Context context) {
//        // Check if the READ_PHONE_STATE permission is already available.
//        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE)
//                != PackageManager.PERMISSION_GRANTED) {
//            // READ_PHONE_STATE permission has not been granted.
//            requestReadPhoneStatePermission(context);
//        } else {
//            // READ_PHONE_STATE permission is already been granted.
//            doPermissionGrantedStuffs();
//        }
//    }
//
//
//
//    /**
//     * Requests the READ_PHONE_STATE permission.
//     * If the permission has been denied previously, a dialog will prompt the user to grant the
//     * permission, otherwise it is requested directly.
//     */
//    private void requestReadPhoneStatePermission(Context context) {
//        if (ActivityCompat.shouldShowRequestPermissionRationale(context,
//                Manifest.permission.READ_PHONE_STATE)) {
//            // Provide an additional rationale to the user if the permission was not granted
//            // and the user would benefit from additional context for the use of the permission.
//            // For example if the user has previously denied the permission.
//            //re-request
//            ActivityCompat.requestPermissions(context,
//                    new String[]{Manifest.permission.READ_PHONE_STATE},
//                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
//
//
//        } else {
//            // READ_PHONE_STATE permission has not been granted yet. Request it directly.
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE},
//                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
//        }
//    }
//
//    /**
//     * Callback received when a permissions request has been completed.
//     */
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//
//        if (requestCode == MY_PERMISSIONS_REQUEST_READ_PHONE_STATE) {
//            // Received permission result for READ_PHONE_STATE permission.est.");
//            // Check if the only required permission has been granted
//            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                // READ_PHONE_STATE permission has been granted, proceed with displaying IMEI Number
//                //alertAlert(getString(R.string.permision_available_read_phone_state));
//                doPermissionGrantedStuffs(co);
//            }
//        }
//    }
//    public void doPermissionGrantedStuffs() {
//        //Have an  object of TelephonyManager
//        TelephonyManager tm =(TelephonyManager)cgetSystemService(Context.TELEPHONY_SERVICE);
//        //Get IMEI Number of Phone  //////////////// for this example i only need the IMEI
//        String IMEINumber=tm.getDeviceId();
//        // Now read the desired content to a textview.
////        t = (TextView) findViewById(R.id.loading_tv2);
//        textView.setText(IMEINumber);
//    }
    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public static String convertDateToString(Date date, String format) {
        DateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(date);
    }
}
