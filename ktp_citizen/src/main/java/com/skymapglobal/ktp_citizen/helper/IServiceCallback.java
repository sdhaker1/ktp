package com.skymapglobal.ktp_citizen.helper;

/**
 * Created by thaibui on 9/10/16.
 */
public interface IServiceCallback {
    void onReceiver(String method, String msg);
    void onError(String method,String error);
}
