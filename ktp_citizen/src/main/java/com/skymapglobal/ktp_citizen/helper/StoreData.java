package com.skymapglobal.ktp_citizen.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.Permission;

import java.io.IOException;
import java.util.Collections;

/**
 * Created by thaibui on 8/24/16.
 */
public class StoreData {
    private static StoreData ourInstance;
    private Drive mService = null;
    private GoogleCredential mGoogleCredential;


    public static void init(Context context) {
        ourInstance = new StoreData(context);
    }

    public static StoreData getInstance() {
        return ourInstance;
    }

    private StoreData(Context context) {

        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        try {
            mGoogleCredential = GoogleCredential.fromStream(context.getAssets().open("KTP Citizen-35c66eace0ef.json"), transport, jsonFactory)
                    .createScoped(DriveScopes.all());
        } catch (Exception e) {
            e.printStackTrace();

        }
        mService = new com.google.api.services.drive.Drive.Builder(
                transport, jsonFactory, mGoogleCredential)
                .setApplicationName("My Drive")
                .build();

    }

    public void insertData(final Context context, final String folderName, final java.io.File imageFile, final java.io.File txtFile) {
        final ProgressDialog dialog = new ProgressDialog(context);
        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                dialog.setMessage("Please wait...");
                dialog.setCancelable(false);
                dialog.show();
                super.onPreExecute();
            }


            @Override
            protected String doInBackground(String... strings) {
                try {
                    File imageFolder = new File();
                    imageFolder.setName(folderName);
                    imageFolder.setMimeType("application/vnd.google-apps.folder");
                    File folder = mService.files().create(imageFolder).execute();
                    String folderId = folder.getId();

                    //image
                    File image = new File();
                    image.setName("image");
                    image.setDescription("Report Picture");
                    image.setMimeType("image/png");

                    if (folderId != null && folderId.length() > 0) {
                        image.setParents(Collections.singletonList(folderId));
                    }

                    FileContent mediaContent = new FileContent("image/pnt", imageFile);
                    mService.files().create(image, mediaContent).execute();

                    //text
                    File textBody = new File();
                    textBody.setName("text");
                    textBody.setDescription("Report information");
                    textBody.setMimeType("text/plain");

                    if (folderId != null && folderId.length() > 0) {
                        textBody.setParents(Collections.singletonList(folderId));
                    }
                    FileContent textContent = new FileContent("text/plain", txtFile);

                    mService.files().create(textBody, textContent).execute();
                    //
                    Permission newPermission = new Permission();
//                    newPermission.setEmailAddress("thai.bui@skymapglobal.com");
                    newPermission.setEmailAddress("vanthai5292@gmail.com");
                    newPermission.setType("user");
                    newPermission.setRole("reader");

                    mService.permissions().create(folderId, newPermission).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                dialog.dismiss();
                super.onPostExecute(s);
            }


        }.execute();

    }
}
