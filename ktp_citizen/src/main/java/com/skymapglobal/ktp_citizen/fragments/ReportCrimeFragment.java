package com.skymapglobal.ktp_citizen.fragments;


import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.skymapglobal.ktp_citizen.R;

public class ReportCrimeFragment extends BaseReportFragment {

    private static ReportCrimeFragment sInstance = new ReportCrimeFragment();


    public static ReportCrimeFragment newInstance(GoogleApiClient googleApiClient, Location location) {

        sInstance.mGoogleApiClient = googleApiClient;
        sInstance.mLocation = location;
        sInstance.tableName = "tblPublicCrime";
        sInstance.bucketName = "ktppublicreportcrime";
        sInstance.arrayTypeId = R.array.category_crime_array;
        return sInstance;
    }

}
