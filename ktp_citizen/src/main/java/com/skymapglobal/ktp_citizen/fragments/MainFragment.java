package com.skymapglobal.ktp_citizen.fragments;

import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.api.GoogleApiClient;

import com.skymapglobal.ktp_citizen.R;
import com.skymapglobal.ktp_citizen.models.TabPagerItem;
import com.skymapglobal.ktp_citizen.models.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tuanl on 20-Jul-16.
 */
public class MainFragment extends Fragment {

    private static MainFragment sInstance = new MainFragment();
    private List<TabPagerItem> mTabs = new ArrayList<>();
    private TabLayout mSlidingTabLayout;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;

    public static MainFragment newInstance(GoogleApiClient googleApiClient, Location location, int tab) {

        sInstance.mGoogleApiClient = googleApiClient;
        sInstance.mLocation = location;
        sInstance.selectTab(tab);

        return sInstance;
    }

    public MainFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createTabPagerItem();
    }

    private void createTabPagerItem() {
        mTabs.add(new TabPagerItem("View", ViewSiteFragment.newInstance(mGoogleApiClient, mLocation)));
        mTabs.add(new TabPagerItem("Report", ReportTrafficFragment.newInstance(mGoogleApiClient, mLocation)));
        mTabs.add(new TabPagerItem("ETA", ETAFragment.newInstance(mGoogleApiClient, mLocation)));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewpager);

        mViewPager.setOffscreenPageLimit(mTabs.size());
        mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), mTabs));
        mSlidingTabLayout = (TabLayout) view.findViewById(R.id.tabs);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mSlidingTabLayout.setElevation(15);
        }
        mSlidingTabLayout.setupWithViewPager(mViewPager);
        selectTab(0);
    }

    public void selectTab(int from) {
        if (mTabs.size() != 0) {
            TabLayout.Tab tab = mSlidingTabLayout.getTabAt(from);
            tab.select();
        }
    }

}
