package com.skymapglobal.ktp_citizen.fragments;


import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.skymapglobal.ktp_citizen.R;

public class ReportViolationFragment extends BaseReportFragment {
    private String TAG= "ReportViolationFragment";
    private static ReportViolationFragment sInstance = new ReportViolationFragment();

    public static ReportViolationFragment newInstance(GoogleApiClient googleApiClient, Location location) {

        sInstance.mGoogleApiClient = googleApiClient;
        sInstance.mLocation = location;
        sInstance.tableName = "tblPublicViolation";
        sInstance.bucketName = "ktppublicreportviolation";
        sInstance.arrayTypeId = R.array.category_violation_array;
        return sInstance;
    }
}
