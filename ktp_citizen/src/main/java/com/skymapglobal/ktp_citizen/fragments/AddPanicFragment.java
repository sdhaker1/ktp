package com.skymapglobal.ktp_citizen.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.skymapglobal.ktp_citizen.R;


public class AddPanicFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    public AddPanicFragment() {

    }


    public static AddPanicFragment newInstance() {
        AddPanicFragment fragment = new AddPanicFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    EditText mobileNo1 , mobileNo2 , mobileNo3 , mobileNo4 ;
    AppCompatButton submit ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_panic, container, false);
        mobileNo1 = (EditText) view.findViewById(R.id.panic_number_1);
        mobileNo2 = (EditText) view.findViewById(R.id.panic_number_2);
        mobileNo3 = (EditText) view.findViewById(R.id.panic_number_3);
        mobileNo4 = (EditText) view.findViewById(R.id.panic_number_4);
        submit = (AppCompatButton) view.findViewById(R.id.panic_submit);

        final SharedPreferences spfs = getActivity().getSharedPreferences("PanicNumbers" , Context.MODE_PRIVATE);

        String mobile1 = spfs.getString("mobile1" , null);
        if(mobile1 != null){
            mobileNo1.setText(mobile1);
        }else {
            mobileNo1.setHint("987XXXXXXX");
        }
        String mobile2 = spfs.getString("mobile2" , null);
        if(mobile2 != null){
            mobileNo2.setText(mobile2);
        }else {
            mobileNo2.setHint("987XXXXXXX");
        }


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number1 = mobileNo1.getText().toString();
                String number2 = mobileNo2.getText().toString();
                String number3 = mobileNo3.getText().toString();
                String number4 = mobileNo4.getText().toString();

                boolean isDataOk=true;

                if(number1.length()>0){
                    if(number1.length()!=10){
                        isDataOk=false;
                        mobileNo1.setError("Please enter 10 digit no");
                    }
                }
                if(number2.length()>0){
                    if(number2.length()!=10){
                        isDataOk=false;
                        mobileNo2.setError("Please enter 10 digit no");
                    }
                }
                if(number3.length()>0){
                    if(number3.length()!=10){
                        isDataOk=false;
                        mobileNo3.setError("Please enter 10 digit no");
                    }
                }
                if(number4.length()>0){
                    if(number4.length()!=10){
                        isDataOk=false;
                        mobileNo4.setError("Please enter 10 digit no");
                    }
                }

                if(isDataOk){
                    if(number1.length()==10 || number2.length()==10 || number3.length()==10 || number4.length()==10){
                        spfs.edit().putString("mobile1" , number1)
                                .putString("mobile2" , number2)
                                .putString("mobile3" , number3)
                                .putString("mobile4" , number4).commit();
                        Toast.makeText(getActivity(),"Emergency contact added successfully",Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();
                    }else{
                        Toast.makeText(getActivity(),"Please provide emergency contact no",Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
