package com.skymapglobal.ktp_citizen.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.skymapglobal.ktp_citizen.R;
import com.skymapglobal.ktp_citizen.helper.AppEngineService;
import com.skymapglobal.ktp_citizen.helper.CloudStorage;
import com.skymapglobal.ktp_citizen.helper.FileHelper;
import com.skymapglobal.ktp_citizen.models.PoliceRegisterModel;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class RegisterFragment extends BaseAPIFragment implements View.OnClickListener {

    private ImageView mAvatar;
    private EditText edt_tenant_name, edt_father_name, edt_card_no, edt_age, edt_occupation, edt_mobile, edt_tenant_address, edt_place_work, edt_details_person, edt_person_phone, edt_location, edt_house_address;
    private AppCompatSpinner spRegisterType, spMaritalStatus;

    private static RegisterFragment sInstance = new RegisterFragment();

    public static RegisterFragment newInstance(GoogleApiClient mGoogleApiClient, Location mLastLocation) {
        sInstance.location = mLastLocation;
        sInstance.mGoogleApiClient = mGoogleApiClient;
        return sInstance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        mAvatar = (ImageView) view.findViewById(R.id.rg_image);
        edt_tenant_name = (EditText) view.findViewById(R.id.rg_edt_tenant_name);
        edt_father_name = (EditText) view.findViewById(R.id.rg_edt_father_name);
        edt_card_no = (EditText) view.findViewById(R.id.rg_edt_card_no);
        edt_age = (EditText) view.findViewById(R.id.rg_edt_age);
        edt_occupation = (EditText) view.findViewById(R.id.rg_edt_occupation);
        edt_mobile = (EditText) view.findViewById(R.id.rg_edt_mobile);
        edt_tenant_address = (EditText) view.findViewById(R.id.rg_edt_tenant_address);
        edt_place_work = (EditText) view.findViewById(R.id.rg_edt_place_work);
        edt_details_person = (EditText) view.findViewById(R.id.rg_edt_details_person);
        edt_person_phone = (EditText) view.findViewById(R.id.rg_edt_person_phone);
        edt_location = (EditText) view.findViewById(R.id.rg_edt_area);
        edt_house_address = (EditText) view.findViewById(R.id.rg_edt_house_address);
        spRegisterType = (AppCompatSpinner) view.findViewById(R.id.rg_sp_register_type);
        spMaritalStatus = (AppCompatSpinner) view.findViewById(R.id.rg_sp_marital_status);
//        spHouseRented = (AppCompatSpinner) view.findViewById(R.id.rg_sp_house_rented);

        view.findViewById(R.id.rg_btn_submit).setOnClickListener(this);
        mAvatar.setOnClickListener(this);
//        updateLocation();

        return view;
    }

    private void updateLocation() {
        getMyLocation();
        List<Address> addresses;
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        if (location != null) {
            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                Address add = addresses.get(0);
                edt_location.setText(add.getAddressLine(0) + " - " + add.getAddressLine(1) + " - " + add.getAddressLine(2));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.rg_image:
                captureImage();
                break;
            case R.id.rg_btn_submit:
                attemptSubmit();
                break;
        }
    }

    private void attemptSubmit() {
        updateLocation();
        PoliceRegisterModel model = new PoliceRegisterModel();
        int typePos = spRegisterType.getSelectedItemPosition();
        int maritalPos = spMaritalStatus.getSelectedItemPosition();
//        int rentedPos = spHouseRented.getSelectedItemPosition();
        if (typePos == 0) {
            Toast.makeText(context, "Please select type", Toast.LENGTH_LONG).show();
            return;
        }

        if (imageFile == null || !imageFile.exists()) {
            Toast.makeText(context, "Please capture photo", Toast.LENGTH_LONG).show();
            return;
        }
        model.type = getResources().getStringArray(R.array.register_type_array)[typePos].trim();
        model.marital = getResources().getStringArray(R.array.marital_status_array)[maritalPos].trim();
//        model.rented = getResources().getStringArray(R.array.house_rented)[rentedPos].trim();

        model.tenantName = edt_tenant_name.getText().toString().trim();
        model.fatherName = edt_father_name.getText().toString().trim();
        model.cardNo = edt_card_no.getText().toString().trim();
        model.age = edt_age.getText().toString();
        model.occupation = edt_occupation.getText().toString().trim();
        model.mobile = edt_mobile.getText().toString().trim();
        model.tenantAddress = edt_tenant_address.getText().toString().trim();
        model.placeWork = edt_place_work.getText().toString();
        model.detailsPerson = edt_details_person.getText().toString().trim();
        model.personPhone = edt_person_phone.getText().toString().trim();
        model.currentLocation = edt_location.getText().toString();
        model.houseAddress = edt_house_address.getText().toString().trim();

        if (TextUtils.isEmpty(model.currentLocation)) {
            Toast.makeText(getActivity(), "Please wait load location!", Toast.LENGTH_LONG).show();

        }

        View view = null;
        boolean ready = true;
        if (TextUtils.isEmpty(model.houseAddress)) {
            edt_house_address.setError("Field Require");
            view = edt_house_address;
            ready = false;
        }
        if (TextUtils.isEmpty(model.personPhone)) {
            edt_person_phone.setError("Field Require");
            view = edt_person_phone;
            ready = false;
        }
        if (TextUtils.isEmpty(model.detailsPerson)) {
            edt_details_person.setError("Field Require");
            view = edt_details_person;
            ready = false;
        }
        if (TextUtils.isEmpty(model.placeWork)) {
            edt_place_work.setError("Field Require");
            view = edt_place_work;
            ready = false;
        }
        if (TextUtils.isEmpty(model.tenantAddress)) {
            edt_tenant_address.setError("Field Require");
            view = edt_tenant_address;
            ready = false;
        }
        if (TextUtils.isEmpty(model.mobile) || model.mobile.length() < 10) {
            edt_mobile.setError("Must be 10 digit");
            view = edt_mobile;
            ready = false;
        }
        if (TextUtils.isEmpty(model.occupation)) {
            edt_occupation.setError("Field Require");
            view = edt_occupation;
            ready = false;
        }
        if (TextUtils.isEmpty(model.age)) {
            edt_age.setError("Field Require");
            view = edt_age;
            ready = false;
        }
        if (TextUtils.isEmpty(model.cardNo)) {
            edt_card_no.setError("Field Require");
            view = edt_card_no;
            ready = false;
        }
        if (TextUtils.isEmpty(model.fatherName)) {
            edt_father_name.setError("Field Require");
            view = edt_father_name;
            ready = false;
        }
        if (TextUtils.isEmpty(model.tenantName)) {
            edt_tenant_name.setError("Field Require");
            view = edt_tenant_name;
            ready = false;
        }
        if (ready) {

            showAlertConfirmRegister(model, "", "Are You Register Now");
        } else {
            view.requestFocus();
        }

    }


    public void showAlertConfirmRegister(final PoliceRegisterModel model, final String title, final String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(msg)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        new CreateRegisterAsyncTask().execute(model);
                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle(title);
        alert.show();
    }

    private static String GenerateUniqueKey(){
        String key="";
        Date today;
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        today = new Date();
        key = formatter.format(today)+" "+new SimpleDateFormat("HHmmss").format(Calendar.getInstance().getTime());
        return key;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:

                if (resultCode == -1) {
                    String tag=GenerateUniqueKey();
                    Date dt=new Date();
                    if(location!=null){
                        tag+=location.getLatitude()+" Lng "+location.getLongitude();
                    }
                    FileHelper.getInstance().compressImage(imageFile.getPath(), imageFile.getPath(),tag);
                    mAvatar.setImageBitmap(bitmapFromFile(imageFile));
                } else if (resultCode == 0) {
                    Toast.makeText(context,
                            "User cancelled image capture", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Toast.makeText(context,
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();
                }
                break;


        }
    }

    File imageFile;

    private void captureImage() {
        FileHelper.initial(getContext());
        String image_name = "image_001.png";
        Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File imagesFolder = FileHelper.getInstance().getImageDir();
        imageFile = new File(imagesFolder, image_name);
        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
        startActivityForResult(imageIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }


    class CreateRegisterAsyncTask extends AsyncTask<PoliceRegisterModel, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress.setMessage("Please Wait...");
            mProgress.show();
        }

        @Override
        protected String doInBackground(PoliceRegisterModel... params) {
            try {
                PoliceRegisterModel models = params[0];
                return AppEngineService.getInstance().myApiService.registerPolice(new Gson().toJson(models)).execute().getData();
            } catch (IOException e) {
                return e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            mProgress.hide();
            try {
                Integer.parseInt(result);
                String caseNo = result;
                new UploadFileAsyncTask().execute(caseNo);
            } catch (Exception e) {
                Toast.makeText(context, "" + result, Toast.LENGTH_SHORT).show();
            }
        }
    }

    class UploadFileAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress.setMessage("Please Wait...");
            mProgress.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String caseNo = params[0];
                CloudStorage.shareInstance().uploadFileImage("registerwithpolice", imageFile, caseNo);
                return caseNo;

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();

            }
        }

        @Override
        protected void onPostExecute(String result) {
            mProgress.hide();
            if (result != null) {
                showAlertDialog("Information", "Your registration done successfully vide case no is " + result);
                imageFile.deleteOnExit();
            } else {
                showAlertDialog("Error", "Upload File Error");
            }

        }


    }

}
