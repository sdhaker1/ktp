package com.skymapglobal.ktp_citizen.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.skymapglobal.ktp_citizen.R;

import com.skymapglobal.ktp_citizen.helper.AppEngineService;
import com.skymapglobal.ktp_citizen.helper.CloudStorage;
import com.skymapglobal.ktp_citizen.helper.FileHelper;
import com.skymapglobal.ktp_citizen.helper.Utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public abstract class BaseReportFragment extends Fragment implements View.OnClickListener {

    private static final int REQUEST_CODE_CAPTURE_IMAGE = 1;

    private AppCompatSpinner spCategory, spCity;
    private EditText edtDescription, edtMobileNo;
    private AutoCompleteTextView edtPoliceStation;
    private ImageView imgPreview;
    private File mImageFile;
    private Context context;
    private ProgressDialog mProgress;

    private TextView textViewImageTypeTitle;

    protected GoogleApiClient mGoogleApiClient;
    protected Location mLocation;
    protected String tableName;
    protected String bucketName;
    protected int arrayTypeId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        mProgress = new ProgressDialog(context);
        String image_name = "image_001.png";
        File imagesFolder = FileHelper.getInstance().getImageDir();
        mImageFile = new File(imagesFolder, image_name);
        if (mImageFile.exists()) {
            mImageFile.delete();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report, container, false);

        spCategory = (AppCompatSpinner) view.findViewById(R.id.sp_category);
        spCity = (AppCompatSpinner) view.findViewById(R.id.sp_city);
        edtDescription = (EditText) view.findViewById(R.id.edt_description);
        edtMobileNo = (EditText) view.findViewById(R.id.edt_mobile);
        textViewImageTypeTitle=(TextView) view.findViewById(R.id.img_title);
        if(tableName.equalsIgnoreCase("tblPublicCrime")){
            textViewImageTypeTitle.setText("Take image of Crime Scene/Victim/Offender");
        }
        if(tableName.equalsIgnoreCase("tblPublicViolation")){
            textViewImageTypeTitle.setText("Take image of Traffic Violation");
        }
        edtPoliceStation = (AutoCompleteTextView) view.findViewById(R.id.edt_police_station);
        edtPoliceStation.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.police_station_array)));
        imgPreview = (ImageView) view.findViewById(R.id.img_preview);
        final ArrayAdapter<String> adapterCategory = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, getResources().getStringArray(arrayTypeId));
        final ArrayAdapter<String> adapterCity = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.city_array));
        spCategory.setAdapter(adapterCategory);
        spCity.setAdapter(adapterCity);
        view.findViewById(R.id.btn_take_picture).setOnClickListener(this);
        view.findViewById(R.id.btn_submit).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_take_picture:
                takePicture();

                break;
            case R.id.btn_submit:
                attemptSubmit();
                break;
            default:
                break;
        }
    }


    private void attemptSubmit() {
        try {
            int indexCategory = spCategory.getSelectedItemPosition();
            int indexCity = spCity.getSelectedItemPosition();
            edtDescription.setError(null);
            String category = getResources().getStringArray(arrayTypeId)[indexCategory];
            String city = getResources().getStringArray(R.array.city_array)[indexCity];
            String description = edtDescription.getText().toString().trim();
            String mobile = edtMobileNo.getText().toString().trim();
            String ps = edtPoliceStation.getText().toString().trim();

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());
            String mAdd = "";
            HashMap<String, String> params = new HashMap<>();

            if (mLocation != null) {
                mAdd="Lat "+ mLocation.getLatitude()+" Lng "+mLocation.getLongitude();
                params.put("lat", mLocation.getLatitude() + "");
                params.put("lng", mLocation.getLongitude() + "");
            } else {
                Toast.makeText(context, "Please enable location!", Toast.LENGTH_LONG).show();
                return;
            }
//            if (!mImageFile.exists()) {
//                Toast.makeText(context, "Please capture photo!", Toast.LENGTH_LONG).show();
//                return;
//            }

            if (TextUtils.isEmpty(mobile) || mobile.length() < 10) {
                edtMobileNo.setError("Must 10 digit");
                edtMobileNo.requestFocus();
                return;
            }

            if (TextUtils.isEmpty(ps)) {
                edtPoliceStation.setError("Field require");
                edtPoliceStation.requestFocus();
                return;
            }
            if (TextUtils.isEmpty(description)) {
                edtDescription.setError("Field require");
                edtDescription.requestFocus();
                return;
            }

            params.put("ps", ps + "");
            params.put("mobile", mobile + "");
            params.put("description", description + "");
            params.put("location", mAdd + "");
            params.put("deviceId", Utils.getIMEI(getActivity()));
            params.put("tableName", tableName);
            params.put("category", category);
            params.put("city", city);

            new CreateReportAsyncTask().execute(params);

        } catch (Exception e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }


    private void takePicture() {
        Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mImageFile));
        startActivityForResult(imageIntent, REQUEST_CODE_CAPTURE_IMAGE);
    }

    private static String GenerateUniqueKey(){
        String key="";
        Date today;
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        today = new Date();
        key = formatter.format(today)+" "+new SimpleDateFormat("HHmmss").format(Calendar.getInstance().getTime());
        return key;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case REQUEST_CODE_CAPTURE_IMAGE:
                if (resultCode == Activity.RESULT_OK) {
                    String tag=GenerateUniqueKey();
                    Date dt=new Date();
                    if(mLocation!=null){
                        tag+=mLocation.getLatitude()+" Lng "+mLocation.getLongitude();
                    }
                    FileHelper.getInstance().compressImage(mImageFile.getPath(), mImageFile.getPath(),tag);
                    imgPreview.setImageBitmap(bitmapFromUri(Uri.fromFile(mImageFile)));
                }
                break;
        }

    }

    public Bitmap bitmapFromUri(Uri uri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        return BitmapFactory.decodeFile(uri.getPath(), options);

    }


    class CreateReportAsyncTask extends AsyncTask<HashMap<String, String>, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress.setMessage("Please Wait...");
            mProgress.setCancelable(false);
            mProgress.show();
        }

        @Override
        protected String doInBackground(HashMap<String, String>... params) {
            try {
                HashMap<String, String> models = params[0];
                String description = models.get("description");
                String location = models.get("location");
                String deviceId = models.get("deviceId");
                String tableName = models.get("tableName");
                String city = models.get("city");
                String category = models.get("category");
                String lat = models.get("lat");
                String lng = models.get("lng");
                String mobile = models.get("mobile");
                String ps = models.get("ps");
                return AppEngineService.getInstance().myApiService.ktpPublishCreateReport(tableName, deviceId, description, location, category, city, lat, lng, mobile, ps).execute().getData();
            } catch (IOException e) {
                return e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            mProgress.hide();
            try {
                Integer.parseInt(result);
                String caseNo = result;
                new UploadFileAsyncTask().execute(caseNo);
            } catch (Exception e) {
                Toast.makeText(context, "" + result, Toast.LENGTH_SHORT).show();
            }
        }


    }

    class UploadFileAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress.setMessage("Please Wait...");
            mProgress.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String caseNo = params[0];
                if (mImageFile.exists())
                    CloudStorage.shareInstance().uploadFileImage(bucketName, mImageFile, caseNo);
                return caseNo;

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();

            }
        }

        @Override
        protected void onPostExecute(String result) {
            mProgress.hide();
            if (result != null) {
                if(result.equalsIgnoreCase("timeout")){
                    Toast.makeText(getActivity(),"Internet connection is slow. Please try again",Toast.LENGTH_LONG).show();
                }else{
                    String prefix="";
                    if(tableName.equalsIgnoreCase("tblPublicCrime")){
                        prefix="RC";
                    }
                    if(tableName.equalsIgnoreCase("tblPublicViolation")){
                        prefix="RTV";
                    }
                    showAlertDialog("Information", "Successfully vide case no is " + prefix+result);
                }
            } else {
                showAlertDialog("Error", "Upload File Error");
            }
        }
    }

    public void showAlertDialog(final String title, final String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        getActivity().onBackPressed();
                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle(title);
        alert.show();
    }

}
