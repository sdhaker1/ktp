package com.skymapglobal.ktp_citizen.fragments;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.skymapglobal.ktp_citizen.ActivityMain;
import com.skymapglobal.ktp_citizen.MenuActivity;
import com.skymapglobal.ktp_citizen.models.Utils;
import com.skymapglobal.ktp_citizen.MainActivity;
import com.skymapglobal.ktp_citizen.R;

import java.util.ArrayList;
import java.util.List;

public class ETAFragment extends Fragment implements OnMapReadyCallback,
        RoutingListener, GoogleMap.OnMyLocationButtonClickListener {

    private GoogleMap mGoogleMap;
    private Place mStartPoint;
    private Place mEndPoint;

    private List<Polyline> polylines = new ArrayList<>();

    private RelativeLayout mRelative;
    private static String TAG = "ETA";
    private List<Route> mRoutes = new ArrayList<>();

    private SupportPlaceAutocompleteFragment autocomplete_from_fragment;
    private SupportPlaceAutocompleteFragment autocomplete_to_fragment;

    private static ETAFragment sInstance;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private ProgressDialog mProgress;
    private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(new LatLng(23.63936, 68.14712), new LatLng(28.20453, 97.34466));


    public static ETAFragment newInstance(GoogleApiClient googleApiClient, Location location) {
        if (sInstance == null) {
            sInstance = new ETAFragment();
            sInstance.mGoogleApiClient = googleApiClient;
            sInstance.mLocation = location;
        }
        return sInstance;
    }


    public ETAFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProgress = new ProgressDialog(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_eta, container, false);
        mRelative = (RelativeLayout) view.findViewById(R.id.relative_eta);

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map_eta);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        ((MenuActivity) getActivity()).getMyLocation();
        AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES).build();
        autocomplete_from_fragment = (SupportPlaceAutocompleteFragment) this.getChildFragmentManager().findFragmentById(R.id.autocomplete_from_fragment_eta);
//        autocomplete_from_fragment.setFilter(autocompleteFilter);
        autocomplete_from_fragment.setBoundsBias(BOUNDS_INDIA);
        if (autocomplete_from_fragment != null) {
            autocomplete_from_fragment.setHint("Choose Starting Point");
            autocomplete_from_fragment.setText("Your location");
        }
        autocomplete_from_fragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(final Place place) {

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15);
                mGoogleMap.animateCamera(cameraUpdate, 1000, new GoogleMap.CancelableCallback() {
                    @Override
                    public void onFinish() {
                        // End marker
                        mGoogleMap.addMarker(new MarkerOptions().position(place.getLatLng()).title(place.getName().toString()));
                        mStartPoint = place;
                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }

            @Override
            public void onError(Status status) {

            }
        });


        autocomplete_to_fragment = (SupportPlaceAutocompleteFragment) this.getChildFragmentManager().findFragmentById(R.id.autocomplete_to_eta);
//        autocomplete_to_fragment.setFilter(autocompleteFilter);
        autocomplete_to_fragment.setBoundsBias(BOUNDS_INDIA);
        if (autocomplete_to_fragment != null) {
            autocomplete_to_fragment.setHint("Choose destination");
        }

        autocomplete_to_fragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(final Place place) {

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15);
                mGoogleMap.animateCamera(cameraUpdate, 1000, new GoogleMap.CancelableCallback() {
                    @Override
                    public void onFinish() {
                        // End marker
                        mGoogleMap.addMarker(new MarkerOptions().position(place.getLatLng()).title(place.getName().toString()));
                        mEndPoint = place;
                        query();
                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }

            @Override
            public void onError(Status status) {

            }
        });
        return view;
    }

    private void query() {
        if (mEndPoint == null) {
            Toast.makeText(getActivity(), "Please choose destination location", Toast.LENGTH_SHORT).show();
            return;
        }
        mGoogleMap.clear();
        if (mStartPoint != null) {
            mGoogleMap.addMarker(new MarkerOptions().position(mStartPoint.getLatLng()).title(mStartPoint.getName().toString()));
            mGoogleMap.addMarker(new MarkerOptions().position(mEndPoint.getLatLng()).title(mEndPoint.getName().toString()));
        } else {
            mGoogleMap.addMarker(new MarkerOptions().position(mEndPoint.getLatLng()).title(mEndPoint.getName().toString()));
        }


        LatLng start;
        if (mStartPoint == null) {
            if (mLocation == null) {
                mLocation = new Location("");
                mLocation.setLatitude(0.0d);
                mLocation.setLongitude(0.0d);
            }
            start = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());

        } else {
            start = mStartPoint.getLatLng();
        }

        showDialog("Please wait. Fetching route information");
        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .key("AIzaSyD8d12aW-kL_6XaBdIzey_PovBEvLREa1k")
                .waypoints(start, mEndPoint.getLatLng())
                .build();
        routing.execute();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (mGoogleMap == null) return;
        mGoogleMap.setTrafficEnabled(true);
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (mRelative.getVisibility() == View.GONE) {
                    mRelative.setVisibility(View.VISIBLE);
                } else {
                    mRelative.setVisibility(View.GONE);
                }
            }
        });
        LatLng latLng=null;
        if(mLocation!=null){
            latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        }else{
            latLng = new LatLng(22.572645, 88.363892);
        }
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
        mGoogleMap.animateCamera(cameraUpdate);
        setupLocationServices();
    }

    private void setupLocationServices() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
//        onMyLocationButtonClick();

    }


    @Override
    public boolean onMyLocationButtonClick() {
        if (mLocation != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()), 15);
            mGoogleMap.animateCamera(cameraUpdate, 1000, null);

        }
        return false;
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        hideDialog();
        if (e != null) {
            Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getActivity(), "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> arrayList, int i) {
        hideDialog();
        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines.clear();

        mRoutes = arrayList;

        for (int j = 0; j < mRoutes.size(); j++) {
            if (j != i) {
                PolylineOptions polyOptions = new PolylineOptions();
                polyOptions.width(10);
                polyOptions.addAll(mRoutes.get(j).getPoints());
                polyOptions.color(getResources().getColor(R.color.colorAccent));
                Polyline polyline = mGoogleMap.addPolyline(polyOptions);
                int k = mRoutes.get(j).getSegments().size() / 2;
                mGoogleMap.addMarker(new MarkerOptions()
                        .position(mRoutes.get(j).getSegments().get(k).startPoint())
                        .icon(BitmapDescriptorFactory.fromBitmap(Utils.makeBitmap(getActivity(), mRoutes.get(j), false))));
                polylines.add(polyline);
            }

        }
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.width(10);
        polyOptions.addAll(mRoutes.get(i).getPoints());
        polyOptions.color(getResources().getColor(R.color.colorPrimary));
        Polyline polyline = mGoogleMap.addPolyline(polyOptions);
        int k = mRoutes.get(i).getSegments().size() / 2;
        mGoogleMap.addMarker(new MarkerOptions()
                .position(mRoutes.get(i).getSegments().get(k).startPoint())
                .icon(BitmapDescriptorFactory.fromBitmap(Utils.makeBitmap(getActivity(), mRoutes.get(i), false))));
        polyline.setClickable(true);
        polylines.add(i, polyline);
    }

    @Override
    public void onRoutingCancelled() {

    }

    private void showDialog(String mess) {
        if (!mProgress.isShowing()) {
            mProgress.setMessage(mess);
            mProgress.show();
        }
    }

    private void hideDialog() {
        mProgress.dismiss();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            setupLocationServices();
        }
    }

}
