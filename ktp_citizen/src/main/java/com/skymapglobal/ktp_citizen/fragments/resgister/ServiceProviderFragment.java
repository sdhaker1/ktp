package com.skymapglobal.ktp_citizen.fragments.resgister;

import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.skymapglobal.ktp_citizen.fragments.BaseRegisterFragment;

/**
 * Created by thaibui on 9/17/16.
 */
public class ServiceProviderFragment extends BaseRegisterFragment {

    private static ServiceProviderFragment fragment = new ServiceProviderFragment();

    public static ServiceProviderFragment newInstance(GoogleApiClient mGoogleApiClient, Location mLastLocation) {
        fragment.mGoogleApiClient = mGoogleApiClient;
        fragment.location = mLastLocation;
        return fragment;
    }

    @Override
    public String getTableName() {
        return "ServiceProvider";
    }



}
