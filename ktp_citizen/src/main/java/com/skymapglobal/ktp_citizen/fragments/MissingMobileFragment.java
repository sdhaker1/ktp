package com.skymapglobal.ktp_citizen.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;
import com.skymapglobal.ktp_citizen.R;
import com.skymapglobal.ktp_citizen.adapter.CustomSpinnerAdapter;
import com.skymapglobal.ktp_citizen.helper.IServiceCallback;
import com.skymapglobal.ktp_citizen.helper.ServiceConnector;
import com.skymapglobal.ktp_citizen.models.MissingMobileDetails;
import com.skymapglobal.ktp_citizen.models.PoliceStationDetails;
import com.skymapglobal.ktp_citizen.models.ResponseParkingLocations;
import com.skymapglobal.ktp_citizen.models.entity.PackingDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by Admin on 02-03-2017.
 */

public class MissingMobileFragment extends Fragment implements IServiceCallback {


    private static MissingMobileFragment instance = new MissingMobileFragment();
    ArrayList<PoliceStationDetails> mPoliceStationDetailsList;
    CaldroidFragment dialogCaldroidFragment;
    CaldroidListener listener;
    String mGDDate="";
    SimpleDateFormat mFormatDate;
    EditText editTextGD,editTextReferenceNo;
    Spinner spinner;
    AppCompatButton appCompatButtonSubmit;
    PoliceStationDetails mPoliceStationDetails=null;
    MissingMobileDetails mMissingMobileDetails=null;
    TextView textViewContactNo,textViewOwnerName,textViewMobile1,textViewMobile2,textViewImei1,
            textViewImei2,textViewMissingDate,textViewManufacture,textViewModelDetails,textViewStatus;



    public static MissingMobileFragment newInstance() {
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_missing_mobile, container, false);
        spinner             = (Spinner) view.findViewById(R.id.spinner1);
        editTextGD          = (EditText) view.findViewById(R.id.editTextGDDate);
        editTextReferenceNo = (EditText) view.findViewById(R.id.editTextRefNo);
        appCompatButtonSubmit=(AppCompatButton) view.findViewById(R.id.appCompatButtonSubmit);
        editTextGD.setFocusable(false);
        editTextGD.setClickable(true);

        textViewContactNo   =(TextView) view.findViewById(R.id.textViewContactNo);
        textViewOwnerName   =(TextView) view.findViewById(R.id.textViewOwnerName);
        textViewMobile1     =(TextView) view.findViewById(R.id.textViewMobileNo1);
        textViewMobile2     =(TextView) view.findViewById(R.id.textViewMobileNo2);
        textViewImei1       =(TextView) view.findViewById(R.id.textViewIMEINo1);
        textViewImei2       =(TextView) view.findViewById(R.id.textViewIMEINo2);
        textViewMissingDate =(TextView) view.findViewById(R.id.textViewDateofMissing);
        textViewManufacture =(TextView) view.findViewById(R.id.textViewManufacture);
        textViewModelDetails=(TextView) view.findViewById(R.id.textViewModelDetails);
        textViewStatus      =(TextView) view.findViewById(R.id.textViewStatus);

        BuildPoliceStationList();
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(),R.layout.spinner_list_item,  mPoliceStationDetailsList);
        //adapter.setDropDownViewResource(R.layout.spinner_dropdown);
        spinner.setAdapter(adapter);
        mFormatDate = new SimpleDateFormat("yyyy-MM-dd");
        listener = new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {
                mGDDate = mFormatDate.format(date);
                editTextGD.setText(mGDDate);
                dialogCaldroidFragment.dismiss();
            }
            @Override
            public void onChangeMonth(int month, int year) {

            }
            @Override
            public void onLongClickDate(Date date, View view) {

            }
            @Override
            public void onCaldroidViewCreated() {

            }
        };

        editTextGD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseDateDialog();
            }
        });

        appCompatButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getWindow()
                        .setSoftInputMode(
                                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                ClearDetails();
                int position=spinner.getSelectedItemPosition();
                mPoliceStationDetails=mPoliceStationDetailsList.get(position);
                if(mPoliceStationDetails!=null){
                    String stationName=mPoliceStationDetails.getStationName();
                    String refno=editTextReferenceNo.getText().toString().trim();
                    if(false==stationName.equalsIgnoreCase("SELECT POLICE STATION")){
                        String pscode=mPoliceStationDetails.getCode();
                        if(mGDDate.trim().length()>0){
                            if(refno.length()>0){
                                requestMissingMobile(pscode,refno,mGDDate);
                            }else{
                                Toast.makeText(getActivity(),"Please enter the reference no",Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(getActivity(),"Please select the GD date",Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(getActivity(),"Please select police station",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getActivity(),"Please select police station",Toast.LENGTH_LONG).show();
                }
            }
        });
        return view;
    }

    public void ChooseDateDialog(){
        dialogCaldroidFragment = new CaldroidFragment();
        dialogCaldroidFragment.setCaldroidListener(listener);
        final String dialogTag = "CALDROID_DIALOG_FRAGMENT";
        Bundle bundle = new Bundle();
        bundle.putString(CaldroidFragment.DIALOG_TITLE,"Select a date");
        dialogCaldroidFragment.setArguments(bundle);
        dialogCaldroidFragment.show(getActivity().getSupportFragmentManager(),dialogTag);
    }

    private void BuildPoliceStationList() {
        mPoliceStationDetailsList = new ArrayList<PoliceStationDetails>();
        String[] section = getResources().getStringArray(R.array.police_station_code);
        if (section != null && section.length > 0) {
            for (String s : section) {
                if (s.contains("#")) {
                    String[] splitsection = s.split("#");
                    if (splitsection.length == 2) {
                        PoliceStationDetails obj = new PoliceStationDetails();
                        obj.setStationName(splitsection[0].trim());
                        obj.setCode(splitsection[1].trim());
                        mPoliceStationDetailsList.add(obj);
                        obj = null;
                    }
                }
            }
        }
    }

    @Override
    public void onReceiver(String method, String msg) {
        Log.e("Receiver", msg);
        if (method.equals(GET_MOBILE_MISSING_URL)) {

            try{
                JSONObject jsonObject = new JSONObject(msg);
                JSONObject jMessageObject=jsonObject.getJSONObject("Message");
                if (jMessageObject.getString("status").equals("1")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("Data");
                    //Iterate the jsonArray and print the info of JSONObjects
                    if(jsonArray!=null){
                        for(int i=0; i < jsonArray.length(); i++){
                            JSONObject obj = jsonArray.getJSONObject(i);
                            //mDeviceToken = jObject.optString("gcmId").toString();
                            mMissingMobileDetails=new MissingMobileDetails();
                            if(obj.optString("COMPLNTNAME").toString()!=null){
                                mMissingMobileDetails.setOwnerName(obj.optString("COMPLNTNAME").toString());
                            }
                            if(obj.optString("CONTACTNO").toString()!=null){
                                mMissingMobileDetails.setContactNo(obj.optString("CONTACTNO").toString());
                            }
                            if(obj.optString("DTMISSING").toString()!=null){
                                mMissingMobileDetails.setDateofMissing(obj.optString("DTMISSING").toString());
                            }
                            if(obj.optString("MOBILENO").toString()!=null){
                                mMissingMobileDetails.setMobileNo1(obj.optString("MOBILENO").toString());
                            }
                            if(obj.optString("MOBILENO2").toString()!=null){
                                mMissingMobileDetails.setMobileNo2(obj.optString("MOBILENO2").toString());
                            }
                            if(obj.optString("IMEI").toString()!=null){
                                mMissingMobileDetails.setImeiNo1(obj.optString("IMEI").toString());
                            }
                            if(obj.optString("IMEI2").toString()!=null){
                                mMissingMobileDetails.setImeiNo2(obj.optString("IMEI2").toString());
                            }
                            if(obj.optString("MAKE").toString()!=null){
                                mMissingMobileDetails.setManufacture(obj.optString("MAKE").toString());
                            }
                            if(obj.optString("MODEL").toString()!=null){
                                mMissingMobileDetails.setModelDetails(obj.optString("MODEL").toString());
                            }
                            if(obj.optString("PRSTATUS").toString()!=null){
                                mMissingMobileDetails.setStatus(obj.optString("PRSTATUS").toString());
                            }
                        }
                        SetDetails();
                    }else{
                        showAlert("Information", "No data found");
                    }
                }else{
                    showAlert("Information", "No data found");
                }

/*
                if (jsonObject.getString("Message").equals("1")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("Data");
                    //Iterate the jsonArray and print the info of JSONObjects
                    if(jsonArray!=null){
                        for(int i=0; i < jsonArray.length(); i++){
                            JSONObject obj = jsonArray.getJSONObject(i);
                            //mDeviceToken = jObject.optString("gcmId").toString();
                            mMissingMobileDetails=new MissingMobileDetails();
                            if(obj.optString("COMPLNTNAME").toString()!=null){
                                mMissingMobileDetails.setOwnerName(obj.optString("COMPLNTNAME").toString());
                            }
                            if(obj.optString("CONTACTNO").toString()!=null){
                                mMissingMobileDetails.setContactNo(obj.optString("CONTACTNO").toString());
                            }
                            if(obj.optString("DTMISSING").toString()!=null){
                                mMissingMobileDetails.setDateofMissing(obj.optString("DTMISSING").toString());
                            }
                            if(obj.optString("MOBILENO").toString()!=null){
                                mMissingMobileDetails.setMobileNo1(obj.optString("MOBILENO").toString());
                            }
                            if(obj.optString("MOBILENO2").toString()!=null){
                                mMissingMobileDetails.setMobileNo2(obj.optString("MOBILENO2").toString());
                            }
                            if(obj.optString("IMEI").toString()!=null){
                                mMissingMobileDetails.setImeiNo1(obj.optString("IMEI").toString());
                            }
                            if(obj.optString("IMEI2").toString()!=null){
                                mMissingMobileDetails.setImeiNo2(obj.optString("IMEI2").toString());
                            }
                            if(obj.optString("MAKE").toString()!=null){
                                mMissingMobileDetails.setManufacture(obj.optString("MAKE").toString());
                            }
                            if(obj.optString("MODEL").toString()!=null){
                                mMissingMobileDetails.setModelDetails(obj.optString("MODEL").toString());
                            }
                        }
                        SetDetails();

                    }else{
                        showAlert("Information", "No data found");
                    }
                }else{
                    showAlert("Information", "No data found");
                }*/
            }catch(JSONException je){

            }
        }
    }

    @Override
    public void onError(String method, String error) {
        showAlert("Error", error);
    }

    private void showAlert(String title, String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(msg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private final String GET_MOBILE_MISSING_URL ="http://182.71.240.212/crimebabu/epolicing/epolcing_case_details?";
    private void requestMissingMobile(String pscode,String caseref,String gdedate ) {
        /*
        Test Case
        pscode="S1";
        caseref="2138";
        gdedate="2017-02-24";*/

        String param = "ps_code=%s&case_ref=%s&gde_date=%s";
        param = String.format(param, pscode, caseref, gdedate);
//        param = "22.5800708/88.4359073";
        mMissingMobileDetails=null;
        ServiceConnector.getInstance().sendRequest(getActivity(), GET_MOBILE_MISSING_URL, param,"GET_MOBILE_MISSING_URL", this);

    }

    private void SetDetails(){
        textViewContactNo.setText(mMissingMobileDetails.getContactNo());
        textViewOwnerName.setText(mMissingMobileDetails.getOwnerName());
        textViewMobile1.setText(mMissingMobileDetails.getMobileNo1());
        textViewMobile2.setText(mMissingMobileDetails.getMobileNo2());
        textViewImei1.setText(mMissingMobileDetails.getImeiNo1());
        textViewImei2.setText(mMissingMobileDetails.getImeiNo2());
        textViewMissingDate.setText(mMissingMobileDetails.getDateofMissing());
        textViewManufacture.setText(mMissingMobileDetails.getManufacture());
        textViewModelDetails.setText(mMissingMobileDetails.getModelDetails());
        textViewStatus.setText("STATUS - "+mMissingMobileDetails.getStatus());
    }

    private void ClearDetails(){
        textViewContactNo.setText("");
        textViewOwnerName.setText("");
        textViewMobile1.setText("");
        textViewMobile2.setText("");
        textViewImei1.setText("");
        textViewImei2.setText("");
        textViewMissingDate.setText("");
        textViewManufacture.setText("");
        textViewModelDetails.setText("");
        textViewStatus.setText("");
    }
}
