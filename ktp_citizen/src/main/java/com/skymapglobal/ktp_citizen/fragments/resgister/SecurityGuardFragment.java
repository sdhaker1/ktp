package com.skymapglobal.ktp_citizen.fragments.resgister;

import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.skymapglobal.ktp_citizen.fragments.BaseRegisterFragment;

/**
 * Created by thaibui on 9/17/16.
 */
public class SecurityGuardFragment extends BaseRegisterFragment {

    private static SecurityGuardFragment fragment = new SecurityGuardFragment();

    public static SecurityGuardFragment newInstance(GoogleApiClient mGoogleApiClient, Location mLastLocation) {
        fragment.mGoogleApiClient = mGoogleApiClient;
        fragment.location = mLastLocation;
        return fragment;
    }

    @Override
    public String getTableName() {
        return "SecurityGuard";
    }



}
