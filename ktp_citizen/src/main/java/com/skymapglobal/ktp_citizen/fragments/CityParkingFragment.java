package com.skymapglobal.ktp_citizen.fragments;


import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;

import com.skymapglobal.ktp_citizen.R;
import com.skymapglobal.ktp_citizen.helper.IServiceCallback;
import com.skymapglobal.ktp_citizen.helper.ServiceConnector;
import com.skymapglobal.ktp_citizen.models.ResponseParkingLocations;
import com.skymapglobal.ktp_citizen.models.entity.PackingDetails;
import com.skymapglobal.ktp_citizen.models.entity.PackingLocation;

import java.io.FileNotFoundException;

/**
 * A simple {@link Fragment} subclass.
 */
public class CityParkingFragment extends Fragment implements OnMapReadyCallback, IServiceCallback, GoogleMap.OnMarkerClickListener {

    private Location mLocation;
    private GoogleApiClient mGoogleApiClient;
    private static CityParkingFragment instance = new CityParkingFragment();
    private GoogleMap mGoogleMap;

    public static CityParkingFragment newInstance(GoogleApiClient mGoogleApiClient, Location mLastLocation) {
        instance.mLocation = mLastLocation;
        instance.mGoogleApiClient = mGoogleApiClient;
        return instance;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_city_parking, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setOnMarkerClickListener(this);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        requestListPackingLocations();

    }

    //private final String GET_PRK_URL = "http://103.241.182.37:3093/getPrkLocations/";
    private final String GET_PRK_URL ="http://veiculu.com:8018/getPrkLocations/";
    private void requestListPackingLocations() {

        if (mLocation == null) return;
        mGoogleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()), 15);
                mGoogleMap.animateCamera(cameraUpdate, 1000, null);
                return false;
            }
        });
        String param = mLocation.getLatitude() + "/" + mLocation.getLongitude();
//        param = "22.5800708/88.4359073";
        ServiceConnector.getInstance().sendRequest(getActivity(), GET_PRK_URL, param,"PARKING", this);

    }

    @Override
    public void onReceiver(String method, String msg) {
        Log.e("Receiver", msg);
        if (method.equals(GET_PRK_URL)) {
            msg = Html.fromHtml(msg).toString();
            ResponseParkingLocations res = new Gson().fromJson(msg, ResponseParkingLocations.class);
            if (res != null && res.data != null && res.data.length > 0) {
                updateMapView(res.data);
            } else {
                showAlert("", "No data found");
            }
        } else if (method.equals(DETAILS_URL)) {
            PackingDetails res = new Gson().fromJson(msg, PackingDetails.class);
            if (res != null && res.lot_key != null) {
                updateMarker(res);
            } else {
                showAlert("", "No data found");
            }
        }
    }

    @Override
    public void onError(String method, String error) {
        showAlert("Error", error);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        String data = marker.getTag().toString();
        requestDetailsParking(data);
        return false;
    }

    private void updateMarker(PackingDetails res) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_parking_details, null);
        TextView name, type, open, close, available, present, owner, contact, ownerContact, remarks, street,
                twowheelerdayprice,twowheelernightprice,fourwheeledayprice,fourwheelernightprice,
                twowheelertotalspace,twowheeleravailablespace,fourwheeletotalspace,fourwheeleravailablespace;

        LinearLayout space =(LinearLayout) view.findViewById(R.id.layout_space);

        name = (TextView) view.findViewById(R.id.prk_name);
        type = (TextView) view.findViewById(R.id.prk_paid_type);
        open = (TextView) view.findViewById(R.id.prk_time_open);
        close = (TextView) view.findViewById(R.id.prk_time_close);
        owner = (TextView) view.findViewById(R.id.prk_owner_name);
        contact = (TextView) view.findViewById(R.id.prk_contact_no);
        ownerContact = (TextView) view.findViewById(R.id.prk_owner_ct_no);
        remarks = (TextView) view.findViewById(R.id.prk_remarks);
        street = (TextView) view.findViewById(R.id.prk_street);
        twowheelerdayprice = (TextView) view.findViewById(R.id.two_wheel_day_price);
        twowheelernightprice = (TextView) view.findViewById(R.id.two_wheel_night_price);

        fourwheeledayprice = (TextView) view.findViewById(R.id.four_wheel_day_price);
        fourwheelernightprice = (TextView) view.findViewById(R.id.four_wheel_night_price);

        twowheelertotalspace = (TextView) view.findViewById(R.id.two_wheel_total);
        twowheeleravailablespace = (TextView) view.findViewById(R.id.two_wheel_available);
        fourwheeletotalspace = (TextView) view.findViewById(R.id.four_wheel_total);
        fourwheeleravailablespace = (TextView) view.findViewById(R.id.four_wheel_available);

        if(res.active.trim().equalsIgnoreCase("N")){
            space.setVisibility(View.GONE);
        }
        name.setText(res.lot_name + "");
        type.setText(res.lot_paidType + "");
        open.setText(res.lot_opening_time + "");
        close.setText(res.lot_closing_time + "");
        owner.setText(res.lot_owner_name + "");
        contact.setText(res.lot_contact_no + "");
        ownerContact.setText(res.lot_owner_contact_no + "");
        remarks.setText(res.lot_remarks + "");
        street.setText(res.lot_street_name + "");
        twowheelerdayprice.setText(res.twoWheelerDayPrice + "");
        twowheelernightprice.setText(res.twoWheelerNightPrice + "");
        fourwheeledayprice.setText(res.fourWheelerDayPrice + "");
        fourwheelernightprice.setText(res.fourWheelerNightPrice + "");

        twowheelertotalspace.setText(res.twoWheelerTotal + "");
        twowheeleravailablespace.setText(res.twoWheelerAvailable + "");
        fourwheeletotalspace.setText(res.fourWheelerTotal + "");
        fourwheeleravailablespace.setText(res.fourWheelerAvailable + "");

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setView(view);
        dialog.setTitle(res.lot_name +"");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = dialog.create();
        alert.show();
        Button positivebutton=alert.getButton(DialogInterface.BUTTON_POSITIVE);
        positivebutton.setTextColor(Color.parseColor("#47A097"));
    }

    private void updateMapView(PackingLocation[] data) {
        mGoogleMap.clear();
        LatLngBounds.Builder latLngBounds = LatLngBounds.builder();
        /*mGoogleMap.addPolygon(new PolygonOptions()
                .add(new LatLng(22.562920521341468, 88.351248800754547), new LatLng(22.550551082083892, 88.354574739933014), new LatLng(22.552480004886149, 88.352691829204559), new LatLng(22.551884345413704, 88.35343211889267), new LatLng(22.562920521341468, 88.351248800754547))
                .strokeWidth(1)
                .strokeColor(Color.RED)
                .fillColor(Color.BLUE));*/
        for (PackingLocation packingLocation : data) {
            double lat = Double.parseDouble(packingLocation.lat);
            double lng = Double.parseDouble(packingLocation.lng);
            LatLng latLng = new LatLng(lat, lng);
            Marker marker = mGoogleMap.addMarker(new MarkerOptions().position(latLng).title(packingLocation.lot_name));
            marker.setTag(packingLocation.lot_key);
            if(packingLocation.avl.equalsIgnoreCase("Y")){
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.markera));
            }else{
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
            }
            marker.showInfoWindow();
            latLngBounds.include(latLng);
        }
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(latLngBounds.build(), 100);
        mGoogleMap.animateCamera(cameraUpdate);
    }

    private void showAlert(String title, String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(msg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    //final String DETAILS_URL = "http://103.241.182.37:8092/ParkingServer/rest/getParkingDetails/";
    final String DETAILS_URL = "http://veiculu.com:8017/ParkingServer/rest/getParkingDetails/";
    private void requestDetailsParking(String data) {
        ServiceConnector.getInstance().sendRequest(getActivity(), DETAILS_URL, data,"PARKING", this);
    }
}
