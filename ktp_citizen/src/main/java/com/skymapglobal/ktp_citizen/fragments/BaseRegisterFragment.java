package com.skymapglobal.ktp_citizen.fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.skymapglobal.ktp_citizen.R;
import com.skymapglobal.ktp_citizen.helper.AppEngineService;
import com.skymapglobal.ktp_citizen.helper.CloudStorage;
import com.skymapglobal.ktp_citizen.helper.FileHelper;
import com.skymapglobal.ktp_citizen.helper.Utils;
import com.skymapglobal.ktp_citizen.models.RegistrationModel;
import com.skymapglobal.ktp_citizen.models.entity.PackingDetails;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public abstract class BaseRegisterFragment extends BaseAPIFragment implements View.OnClickListener {

    private ImageView imagePreview;
    private EditText edt_name, edt_father_name, edt_card_no,edt_voter_id, edt_age, edt_present_address, edt_mobile, edt_permanent_address, edt_district, edt_ps, edt_emp_name, edt_emp_address, edt_emp_place_of_work, edt_emp_mobile, edt_emp_otp, edt_ref_name, edt_ref_mobile, edt_ref_address, edt_ref_ps, edt_ref_name_2, edt_ref_mobile_2, edt_ref_address_2, edt_ref_ps_2;
    private AppCompatSpinner spRegisterType, spMaritalStatus;
    private AutoCompleteTextView edt_police_station, edt_emp_police_station;
    RadioGroup radioGroupSame;
    boolean isOtpTaken=false;
    TextView textViewLable;
    TextView textViewTakePicture;
    TextView textEmployerDetails;
    public abstract String getTableName();

    private String mPrefix="";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String image_name = "image_001.png";
        File imagesFolder = FileHelper.getInstance().getImageDir();
        imageFile = new File(imagesFolder, image_name);
        if (imageFile.exists()) {
            imageFile.delete();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ArrayAdapter<String> adapterPS = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.police_station_array));

        View view = inflater.inflate(R.layout.fragment_base_register, container, false);
        view.findViewById(R.id.rg_btn_submit).setOnClickListener(this);
        view.findViewById(R.id.rg_btn_clear).setOnClickListener(this);
        view.findViewById(R.id.rg_btn_otp).setOnClickListener(this);
        imagePreview = (ImageView) view.findViewById(R.id.rg_image);
        imagePreview.setOnClickListener(this);

        edt_name = (EditText) view.findViewById(R.id.rg_edt_name);
        edt_father_name = (EditText) view.findViewById(R.id.rg_edt_father_name);
        edt_age = (EditText) view.findViewById(R.id.rg_edt_age);
        edt_card_no = (EditText) view.findViewById(R.id.rg_edt_card_no);
        edt_voter_id= (EditText) view.findViewById(R.id.rg_edt_voterCard);
        spMaritalStatus = (AppCompatSpinner) view.findViewById(R.id.rg_sp_marital_status);
        edt_present_address = (EditText) view.findViewById(R.id.rg_edt_present_address);
        edt_police_station = (AutoCompleteTextView) view.findViewById(R.id.rg_edt_police_station);
        edt_mobile = (EditText) view.findViewById(R.id.rg_edt_mobile);
        edt_permanent_address = (EditText) view.findViewById(R.id.rg_edt_permanent_address);
        edt_district = (EditText) view.findViewById(R.id.rg_edt_district);
        edt_ps = (EditText) view.findViewById(R.id.rg_edt_ps);
        edt_emp_name = (EditText) view.findViewById(R.id.rg_edt_emp_name);
        edt_emp_address = (EditText) view.findViewById(R.id.rg_edt_emp_address);
        edt_emp_police_station = (AutoCompleteTextView) view.findViewById(R.id.rg_edt_emp_police_station);
        edt_emp_place_of_work = (EditText) view.findViewById(R.id.rg_edt_emp_place_of_work);
        edt_emp_address = (EditText) view.findViewById(R.id.rg_edt_emp_address);
        edt_emp_address = (EditText) view.findViewById(R.id.rg_edt_emp_address);

        edt_ref_name = (EditText) view.findViewById(R.id.rg_edt_ref_name);
        edt_ref_mobile = (EditText) view.findViewById(R.id.rg_edt_ref_mobile);
        edt_ref_address = (EditText) view.findViewById(R.id.rg_edt_ref_address);
        edt_ref_ps = (EditText) view.findViewById(R.id.rg_edt_ref_ps);
        edt_ref_name_2 = (EditText) view.findViewById(R.id.rg_edt_ref_name_2);
        edt_ref_mobile_2 = (EditText) view.findViewById(R.id.rg_edt_ref_mobile_2);
        edt_ref_address_2 = (EditText) view.findViewById(R.id.rg_edt_ref_address_2);
        edt_ref_ps_2 = (EditText) view.findViewById(R.id.rg_edt_ref_ps_2);
        edt_emp_mobile= (EditText) view.findViewById(R.id.rg_edt_emp_mobile);
        radioGroupSame = (RadioGroup) view.findViewById(R.id.radio_group_same_add);

        textViewLable=(TextView)view.findViewById(R.id.textViewLable);
        textViewTakePicture=(TextView) view.findViewById(R.id.textViewTakePicture);

        textEmployerDetails=(TextView) view.findViewById(R.id.rg_edt_details_house_rented);

        String type=getTableName();
        if(type.equalsIgnoreCase("DomesticHelp")){
            textViewLable.setText("Domestic Help Details");
            textViewTakePicture.setText("TAKE PICTURE OF DOMESTIC HELP");
            mPrefix="D";
        }
        if(type.equalsIgnoreCase("SecurityGuard")){
            textViewLable.setText("Security Guard Details");
            textViewTakePicture.setText("TAKE PICTURE OF SECURITY GUARD");
            mPrefix="SG";
        }
        if(type.equalsIgnoreCase("ServiceProvider")){
            textViewLable.setText("Milkman, Newspaper etc. Details");
            textViewTakePicture.setText("TAKE PICTURE OF MILKMAN, NEWSPAPER ETC");
            mPrefix="SP";
        }
        if(type.equalsIgnoreCase("TenantRegister")){
            textViewLable.setText("Tenant Details");
            textViewTakePicture.setText("TAKE PICTURE OF TENANT");
            textEmployerDetails.setText("House Owner Details");
            edt_emp_name.setHint("* House Owner Name");
            mPrefix="TR";
        }


        final TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                edt_emp_place_of_work.setText(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        edt_emp_place_of_work.setEnabled(false);
        edt_emp_address.addTextChangedListener(textWatcher);

        radioGroupSame.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {


            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.radio_same:
                        edt_emp_place_of_work.setEnabled(false);
                        edt_emp_address.addTextChangedListener(textWatcher);
                        break;
                    case R.id.radio_not_same:
                        edt_emp_place_of_work.setEnabled(true);
                        edt_emp_place_of_work.requestFocus();
                        edt_emp_place_of_work.setText("");
                        edt_emp_address.removeTextChangedListener(textWatcher);
                        break;
                    default:
                        break;
                }
            }
        });
//        mAvatar.setOnClickListener(thi

        edt_police_station.setAdapter(adapterPS);
        edt_emp_police_station.setAdapter(adapterPS);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rg_btn_submit:
                attemptSubmit();
                break;
            case R.id.rg_btn_clear:
                attemptClear();
                break;
            case R.id.rg_image:
                captureImage();
                break;
            case R.id.rg_btn_otp:
                String mobile=edt_emp_mobile.getText().toString().trim();
                if (TextUtils.isEmpty(mobile) || mobile.length() < 10) {
                    edt_emp_mobile.setError("Mobile must be 10 digit");
                }else{
                    edt_emp_mobile.setEnabled(false);
                    String otp=GenerateOTP();
                    SendOTP(otp,"91"+mobile);
                }
                break;
        }
    }

    private void attemptSubmit() {
        updateLocation();
        RegistrationModel registrationModel = new RegistrationModel();
        int maritalPos = spMaritalStatus.getSelectedItemPosition();
        edt_name.setError(null);
        edt_father_name.setError(null);
        edt_age.setError(null);
        edt_card_no.setError(null);
        edt_present_address.setError(null);
        edt_police_station.setError(null);
        edt_mobile.setError(null);
        edt_permanent_address.setError(null);
        edt_district.setError(null);
        edt_ps.setError(null);
        edt_emp_name.setError(null);
        edt_emp_address.setError(null);
        edt_emp_police_station.setError(null);
        edt_emp_place_of_work.setError(null);
        edt_emp_mobile.setError(null);
       // edt_emp_otp.setError(null);
        edt_ref_name.setError(null);
        edt_ref_mobile.setError(null);
        edt_ref_address.setError(null);
        edt_ref_ps.setError(null);
        edt_ref_name_2.setError(null);
        edt_ref_mobile_2.setError(null);
        edt_ref_address_2.setError(null);
        edt_ref_ps_2.setError(null);


        registrationModel.marital = getResources().getStringArray(R.array.marital_status_array)[maritalPos].trim();
        registrationModel.name = edt_name.getText().toString().trim();
        registrationModel.fatherName = edt_father_name.getText().toString().trim();
        registrationModel.age = edt_age.getText().toString().trim();
        registrationModel.cardNo = edt_card_no.getText().toString().trim();
        registrationModel.voterID = edt_voter_id.getText().toString().trim();
        registrationModel.presentAddress = edt_present_address.getText().toString().trim();
        registrationModel.policeStation = edt_police_station.getText().toString().trim();
        registrationModel.mobile = edt_mobile.getText().toString().trim();
        registrationModel.permanentAddress = edt_permanent_address.getText().toString().trim();
        registrationModel.district = edt_district.getText().toString().trim();
        registrationModel.ps = edt_ps.getText().toString().trim();
        registrationModel.empName = edt_emp_name.getText().toString().trim();
        registrationModel.empAddress = edt_emp_address.getText().toString().trim();
        registrationModel.empPoliceStation = edt_emp_police_station.getText().toString().trim();
        registrationModel.empPlaceOfWork = edt_emp_place_of_work.getText().toString().trim();
        registrationModel.refName = edt_ref_name.getText().toString().trim() + "";
        registrationModel.refMobile = edt_ref_mobile.getText().toString().trim() + "";
        registrationModel.refAddress = edt_ref_address.getText().toString().trim() + "";
        registrationModel.refPs = edt_ref_ps.getText().toString().trim() + "";
        registrationModel.refName2 = edt_ref_name_2.getText().toString().trim() + "";
        registrationModel.refMobile2 = edt_ref_mobile_2.getText().toString().trim() + "";
        registrationModel.refAddress2 = edt_ref_address_2.getText().toString().trim() + "";
        registrationModel.refPs2 = edt_ref_ps_2.getText().toString().trim() + "";
        registrationModel.empMobile = edt_emp_mobile.getText().toString().trim() + "";
        //registrationModel.empOTP = edt_emp_otp.getText().toString().trim() + "";

        View view = null;
        boolean ready = true;
//        if (TextUtils.isEmpty(registrationModel.empPlaceOfWork)) {
//            edt_emp_place_of_work.setError("Field Require");
//            view = edt_emp_place_of_work;
//            ready = false;
//        }

        if(false==isOtpTaken){
            //edt_emp_mobile.setError("Mobile must be 10 digit");
            view = edt_emp_mobile;
            ready=false;
            Toast.makeText(context, "Please send OTP", Toast.LENGTH_LONG).show();
        }

        if (TextUtils.isEmpty(registrationModel.empPoliceStation)) {
            edt_emp_police_station.setError("Please provide the police station name");
            view = edt_emp_police_station;
            ready = false;
        }

        if (TextUtils.isEmpty(registrationModel.voterID)) {
            edt_voter_id.setError("Please provide the voter ID");
            view = edt_voter_id;
            ready = false;
        }

        if (TextUtils.isEmpty(registrationModel.empAddress)) {
            edt_emp_address.setError("Please provide the address");
            view = edt_emp_address;
            ready = false;
        }
        if (TextUtils.isEmpty(registrationModel.empName)) {
            edt_emp_name.setError("Please provide the name");
            view = edt_emp_name;
            ready = false;
        }
        if (TextUtils.isEmpty(registrationModel.ps)) {
            edt_ps.setError("Please provide the police station name");
            view = edt_ps;
            ready = false;
        }
        if (TextUtils.isEmpty(registrationModel.district)) {
            edt_district.setError("Please provide the district name");
            view = edt_district;
            ready = false;
        }
        if (TextUtils.isEmpty(registrationModel.permanentAddress)) {
            edt_permanent_address.setError("Please provide the address");
            view = edt_permanent_address;
            ready = false;
        }
        if (TextUtils.isEmpty(registrationModel.mobile) || registrationModel.mobile.length() < 10) {
            edt_mobile.setError("Must be 10 digit");
            view = edt_mobile;
            ready = false;
        }

        if (TextUtils.isEmpty(registrationModel.policeStation)) {
            edt_police_station.setError("Please provide the police station name");
            view = edt_police_station;
            ready = false;
        }
        if (TextUtils.isEmpty(registrationModel.presentAddress)) {
            edt_present_address.setError("Please provide the address");
            view = edt_present_address;
            ready = false;
        }
        if (TextUtils.isEmpty(registrationModel.age)) {
            edt_age.setError("Please provide the age");
            view = edt_age;
            ready = false;
        }
//        if (TextUtils.isEmpty(registrationModel.cardNo)) {
//            edt_card_no.setError("Field Require");
//            view = edt_card_no;
//            ready = false;
//        }
        if (TextUtils.isEmpty(registrationModel.fatherName)) {
            edt_father_name.setError("Please provide the father name");
            view = edt_father_name;
            ready = false;
        }
        if (TextUtils.isEmpty(registrationModel.name)) {
            edt_name.setError("Please provide the name");
            view = edt_name;
            ready = false;
        }


        if (ready) {
            if (imageFile.exists()) {
                showAlertConfirmRegister(registrationModel, "KTP", "Do you want registration now?");
            } else {
                Toast.makeText(context, "Please capture photo!", Toast.LENGTH_LONG).show();
            }
        } else {
            view.requestFocus();
        }
    }

    private void attemptClear() {
        isOtpTaken=false;

        if (imageFile.exists()) {
            imageFile.delete();
        }
        imagePreview.setImageResource(R.mipmap.ic_take_photo);
        edt_name.setText("");
        edt_father_name.setText("");
        edt_age.setText("");
        edt_card_no.setText("");
        edt_voter_id.setText("");
        edt_present_address.setText("");
        edt_police_station.setText("");
        edt_mobile.setText("");
        edt_permanent_address.setText("");
        edt_district.setText("");
        edt_ps.setText("");
        edt_emp_name.setText("");
        edt_emp_address.setText("");
        edt_emp_police_station.setText("");
        edt_emp_place_of_work.setText("");
        edt_ref_name.setText("");
        edt_ref_mobile.setText("");
        edt_ref_address.setText("");
        edt_ref_ps.setText("");
        edt_ref_name_2.setText("");
        edt_ref_mobile_2.setText("");
        edt_ref_address_2.setText("");
        edt_ref_ps_2.setText("");

        edt_name.setError(null);
        edt_father_name.setError(null);
        edt_age.setError(null);
        edt_card_no.setError(null);
        edt_present_address.setError(null);
        edt_police_station.setError(null);
        edt_mobile.setError(null);
        edt_permanent_address.setError(null);
        edt_district.setError(null);
        edt_ps.setError(null);
        edt_emp_name.setError(null);
        edt_emp_address.setError(null);
        edt_emp_police_station.setError(null);
        edt_emp_place_of_work.setError(null);
        edt_ref_name.setError(null);
        edt_ref_mobile.setError(null);
        edt_ref_address.setError(null);
        edt_ref_ps.setError(null);
        edt_ref_name_2.setError(null);
        edt_ref_mobile_2.setError(null);
        edt_ref_address_2.setError(null);
        edt_ref_ps_2.setError(null);
    }

    private String mPlace = "";

    private void updateLocation() {
        getMyLocation();
        List<Address> addresses;
        /*Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        if (location != null) {
            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                Address add = addresses.get(0);
                mPlace = add.getAddressLine(0) + " - " + add.getAddressLine(1) + " - " + add.getAddressLine(2);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/

        View view;
        if(edt_emp_police_station.getText().toString().length()>0){
            mPlace=edt_emp_police_station.getText().toString().trim();
            view=edt_emp_police_station;
        }else{
            view=edt_emp_police_station;
            edt_emp_police_station.setError("Please provide the police station name");
            view.requestFocus();
        }

    }


    public void showAlertConfirmRegister(final RegistrationModel model, final String title, final String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(msg)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        new CreateRegisterAsyncTask().execute(model);
                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle(title);
        alert.show();
    }

    private static String GenerateUniqueKey(){
        String key="";
        Date today;
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        today = new Date();
        key = formatter.format(today)+" "+new SimpleDateFormat("HHmmss").format(Calendar.getInstance().getTime());
        return key;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:

                if (resultCode == -1) {
                    String tag=GenerateUniqueKey();
                    Date dt=new Date();
                    if(location!=null){
                        tag+=location.getLatitude()+" Lng "+location.getLongitude();
                    }
                    FileHelper.getInstance().compressImage(imageFile.getPath(), imageFile.getPath(),tag);
                    imagePreview.setImageBitmap(bitmapFromFile(imageFile));
                } else if (resultCode == 0) {
                    Toast.makeText(context,
                            "User cancelled image capture", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Toast.makeText(context,
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
        }
    }

    File imageFile;

    private void captureImage() {
        Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
        startActivityForResult(imageIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private String GenerateOTP(){
        String otp="";
        int randomPIN = (int)(Math.random()*9000)+1000;
        otp=""+randomPIN;
        return otp;
    }

    private void ShowOTPVerification(final String mobile_no,final String otp_no) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_otp_verification, null);
        final EditText edTextOTP = (EditText) view.findViewById(R.id.ed_otp);
        android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(getActivity());
        dialog.setView(view);
        dialog.setCancelable(false);
        dialog.setTitle("Verification");
        dialog.setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                /*String entered_otp=edTextOTP.getText().toString().trim();
                if(entered_otp.equalsIgnoreCase(otp_no)){
                    dialog.dismiss();
                }else{
                    Toast.makeText(context,"Please provide valid OTP",Toast.LENGTH_SHORT).show();
                }*/
            }
        });
        dialog.setNegativeButton("RE SEND", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                /*String otp=GenerateOTP();
                SendOTP(otp,"91"+mobile_no);
                dialog.dismiss();*/
            }
        });


        final android.support.v7.app.AlertDialog alert = dialog.create();
        alert.show();
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               /* Boolean wantToCloseDialog = false;
                //Do stuff, possibly set wantToCloseDialog to true then...
                if(wantToCloseDialog)
                    alert.dismiss();
                //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
            */
                String entered_otp=edTextOTP.getText().toString().trim();
                if(entered_otp.equalsIgnoreCase(otp_no)){
                    alert.dismiss();
                }else{
                    Toast.makeText(context,"Please provide valid OTP",Toast.LENGTH_SHORT).show();
                }

            }
        });

        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /*Boolean wantToCloseDialog = true;
                //Do stuff, possibly set wantToCloseDialog to true then...
                if(wantToCloseDialog)
                    alert.dismiss();
                //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.*/
                String otp=GenerateOTP();
                SendOTP(otp,mobile_no);
                alert.dismiss();
            }
        });
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.parseColor("#47A097"));
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#47A097"));
    }


    private void SendOTP(final String otp, final String mobileno){
        final String smstext=otp+" IS YOUR OTP TO COMPLETE THE REGISTRATION PROCESS - KOLKATA POLICE";
        String REGISTER_URL="http://193.105.74.58/api/v3/sendsms/plain";
        final ProgressDialog progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        isOtpTaken=true;
                        ShowOTPVerification(mobileno,otp);
                        //Toast.makeText(getActivity(),response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(),error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("user","Suhas");
                params.put("password","Sid@2017");
                params.put("sender", "KOLPOL");
                params.put("SMSText", smstext);
                params.put("GSM", mobileno);
                params.put("type", "longSMS");
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                45000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }


    class CreateRegisterAsyncTask extends AsyncTask<RegistrationModel, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress.setMessage("Please Wait...");
            mProgress.setCancelable(false);
            mProgress.show();
        }
        @Override
        protected String doInBackground(RegistrationModel... params) {
            try {
                RegistrationModel models = params[0];
                String deviceId = Utils.getIMEI(context);
                String type = getTableName();
                if (location == null) {
                    return "Location not found!";
                }
                return AppEngineService.getInstance().myApiService.ktpPublishCreateRegistration(type, deviceId, new Gson().toJson(models), mPlace, location.getLatitude() + "", location.getLongitude() + "").execute().getData();
            } catch (IOException e) {
                return e.getMessage();
            }
        }
        @Override
        protected void onPostExecute(String result) {
            mProgress.hide();
            try {
                Integer.parseInt(result);
                String caseNo = result;
                new UploadFileAsyncTask().execute(caseNo);
            } catch (Exception e) {
                Toast.makeText(context, "" + result, Toast.LENGTH_SHORT).show();
            }
        }
    }

    class UploadFileAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress.setMessage("Please Wait...");
            mProgress.show();
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                String caseNo = params[0];
                CloudStorage.shareInstance().uploadFileImage("ktppublicregistration", imageFile, caseNo);
                return caseNo;
            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }
        }
        @Override
        protected void onPostExecute(String result) {
            mProgress.hide();
            if (result != null) {
                if(result.equalsIgnoreCase("timeout")){
                    Toast.makeText(getActivity(),"Internet connection is slow. Please try again",Toast.LENGTH_LONG).show();
                }else{
                    showAlertDialog("Information", "Your registration done successfully vide case no is " + mPrefix+result);
                    attemptClear();
                    imageFile.deleteOnExit();
                }

            } else {
                showAlertDialog("Error", "Upload File Error");
            }
        }
    }
}
