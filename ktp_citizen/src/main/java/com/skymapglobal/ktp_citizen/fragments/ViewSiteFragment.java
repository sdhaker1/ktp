package com.skymapglobal.ktp_citizen.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.geojson.GeoJsonFeature;
import com.google.maps.android.geojson.GeoJsonLayer;
import com.google.maps.android.geojson.GeoJsonPointStyle;
import com.skymapglobal.ktp_citizen.R;

import org.json.JSONException;

import java.io.IOException;

public class ViewSiteFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener {

    private GoogleMap mGoogleMap;

    private RelativeLayout mRelative;


    public ViewSiteFragment() {
    }

    private static ViewSiteFragment sInstance;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private ProgressDialog mProgress;

    public static ViewSiteFragment newInstance(GoogleApiClient googleApiClient, Location location) {
        if (sInstance == null) {
            sInstance = new ViewSiteFragment();
            sInstance.mGoogleApiClient = googleApiClient;
            sInstance.mLocation = location;
        }
        return sInstance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProgress = new ProgressDialog(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_site, container, false);
        mRelative = (RelativeLayout) view.findViewById(R.id.relative_view_traffic);

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (mGoogleMap == null) return;

        mGoogleMap.setTrafficEnabled(true);
        try {
//            GeoJsonLayer layer = new GeoJsonLayer(mGoogleMap, R.raw.site_boundary, getActivity().getApplicationContext());
//            layer.addLayerToMap();
            GeoJsonLayer layer1 = new GeoJsonLayer(mGoogleMap, R.raw.site_points, getActivity());
            layer1.addLayerToMap();
            addColorsToMarkers(layer1, mGoogleMap);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        mGoogleMap.moveCamera(new CameraUpdate();
        setupLocationServices();

    }

    private void setupLocationServices() {
        mGoogleMap.setOnMyLocationButtonClickListener(this);
        enableMyLocation();
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        } else if (mGoogleMap != null) {
            // Access to the location has been granted to the app.
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        if(mLocation!=null){
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()), 15);
            mGoogleMap.animateCamera(cameraUpdate, 1000, null);

        }
        return false;
    }

    private void addColorsToMarkers(GeoJsonLayer layer, GoogleMap map) {
        // Iterate over all the features stored in the layer
        LatLngBounds.Builder latLngBounds = new LatLngBounds.Builder();

        for (GeoJsonFeature feature : layer.getFeatures()) {
            // Check if the magnitude property exists
            if (feature.getProperty("x") != null && feature.hasProperty("y")) {
                double lat = Double.parseDouble(feature.getProperty("x"));
                double lng = Double.parseDouble(feature.getProperty("y"));
                latLngBounds.include(new LatLng(lng, lat));
            }


            if (feature.getProperty("FID_") != null && feature.hasProperty("Area")) {
                double FID_ = Double.parseDouble(feature.getProperty("Area"));

                // Get the icon for the feature
                BitmapDescriptor pointIcon = BitmapDescriptorFactory
                        .defaultMarker(getColor(FID_));

                // Create a new point style
                GeoJsonPointStyle pointStyle = new GeoJsonPointStyle();

                // Set options for the point style
                pointStyle.setIcon(pointIcon);
                pointStyle.setTitle("FID_: " + feature.getProperty("FID_"));
                pointStyle.setSnippet("Area: " + feature.getProperty("Area"));

                // Assign the point style to the feature
                feature.setPointStyle(pointStyle);
            }
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds.build(), 30));

        }
    }

    private static float getColor(double area) {
        if (area < 100.0) {
            return BitmapDescriptorFactory.HUE_CYAN;
        } else if (area < 500.0) {
            return BitmapDescriptorFactory.HUE_GREEN;
        } else if (area < 1000.0) {
            return BitmapDescriptorFactory.HUE_YELLOW;
        } else {
            return BitmapDescriptorFactory.HUE_RED;
        }
    }


}
