package com.skymapglobal.ktp_citizen.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.skymapglobal.ktp_citizen.R;
import com.skymapglobal.ktp_citizen.helper.StoreData;
import com.skymapglobal.ktp_citizen.helper.Utils;

import com.skymapglobal.ktp_citizen.helper.FileHelper;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ReportTrafficFragment extends Fragment {

    private static final int REQUEST_CODE_CAPTURE_IMAGE = 1;

    private static final int PICK_IMAGE = 0;

    private EditText mEditTextDescription;
    private Button mButtonChooseImage;
    private Button mButtonTakePhoto;
    private Button mButtonSubmit;
    private ImageView mImageView;
    private Uri mImageUri;

    private Calendar myCalendar = Calendar.getInstance();

    private static ReportTrafficFragment sInstance;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private ProgressDialog mProgress;

    private GoogleCredential mGoogleCredential;

    public ReportTrafficFragment() {
        // Required empty public constructor
    }

    public static ReportTrafficFragment newInstance(GoogleApiClient googleApiClient, Location location) {
        if (sInstance == null) {
            sInstance = new ReportTrafficFragment();
            sInstance.mGoogleApiClient = googleApiClient;
            sInstance.mLocation = location;

        }
        return sInstance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String image_name = "image_002.png";
        File imagesFolder = FileHelper.getInstance().getImageDir();
        File image = new File(imagesFolder, image_name);
        if(image.exists()){
            image.delete();
        }
        mImageUri = Uri.fromFile(image);
        mProgress = new ProgressDialog(getActivity());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_traffic, container, false);
        mButtonChooseImage = (Button) view.findViewById(R.id.bt_choose_images);
        mButtonTakePhoto = (Button) view.findViewById(R.id.bt_take_photo);
        mButtonSubmit = (Button) view.findViewById(R.id.bt_submit_report);
        mButtonSubmit.setEnabled(false);
        mEditTextDescription = (EditText) view.findViewById(R.id.et_description);
        mImageView = (ImageView) view.findViewById(R.id.iv_upload);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mButtonChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, PICK_IMAGE);
            }
        });
        mButtonTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                startActivityForResult(imageIntent,
                        REQUEST_CODE_CAPTURE_IMAGE);

            }
        });
        mButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                upload();
            }
        });
    }

    private static String GenerateUniqueKey(){
        String key="";
        Date today;
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        today = new Date();
        key = formatter.format(today)+" "+new SimpleDateFormat("HHmmss").format(Calendar.getInstance().getTime());
        return key;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_IMAGE:
                if (resultCode == getActivity().RESULT_OK) {
                    Uri uri = data.getData();
                    String[] filePath = { MediaStore.Images.Media.DATA };
                    Cursor cursor = getActivity().getContentResolver().query(uri, filePath, null, null, null);
                    cursor.moveToFirst();
                    String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
                    String tag=GenerateUniqueKey();
                    Date dt=new Date();
                    if(mLocation!=null){
                        tag+=mLocation.getLatitude()+" Lng "+mLocation.getLongitude();
                    }
                    FileHelper.getInstance().compressImage(imagePath, mImageUri.getPath(),tag);
                    mImageView.setImageBitmap(bitmapFromUri(mImageUri));
                    mButtonSubmit.setEnabled(true);
                }
                break;
            case REQUEST_CODE_CAPTURE_IMAGE:
                if (resultCode == Activity.RESULT_OK) {
                    String tag=GenerateUniqueKey();
                    Date dt=new Date();
                    if(mLocation!=null){
                        tag+=mLocation.getLatitude()+" Lng "+mLocation.getLongitude();
                    }
                    FileHelper.getInstance().compressImage(mImageUri.getPath(), mImageUri.getPath(),tag);
                                        mImageView.setImageBitmap(bitmapFromUri(mImageUri));
                    mButtonSubmit.setEnabled(true);
                }
                break;
        }
    }

    public Bitmap bitmapFromUri(Uri uri){
        BitmapFactory.Options options = new BitmapFactory.Options();
        return BitmapFactory.decodeFile(uri.getPath(), options);

    }


    private void upload() {
        try {
            File txtFile = new File(FileHelper.getTxtDir(), "doc2");
            FileWriter writer = new FileWriter(txtFile);
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());
            String mAdd = "";
            if (mLocation != null) {
                addresses = geocoder.getFromLocation(mLocation.getLatitude(), mLocation.getLongitude(), 1);
                Address add = addresses.get(0);
                mAdd = add.getAddressLine(0) + " - " + add.getAddressLine(1) + " - " + add.getAddressLine(2);
                mAdd = mAdd + " [" + mLocation.getLatitude() + "-" + mLocation.getLongitude() + "]";
            }

            writer.append("Description: "+mEditTextDescription.getText().toString());
            writer.append("\nPlace: "+mAdd);
            writer.flush();
            writer.close();
            Calendar calendar = Calendar.getInstance();
            String folderName = "TRAFFIC_"+ Utils.convertDateToString(calendar.getTime(),"yyyy-MM-dd HH:mm:ss");
            File imageFile = new File(mImageUri.getPath());
            if(imageFile.exists()){
                StoreData.getInstance().insertData(getActivity(), folderName, imageFile, txtFile);
            }else {
                Toast.makeText(getActivity(),"Please Capture Image",Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(),e.getMessage(),Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }

    private void showDialog(String mess) {
        if (!mProgress.isShowing()) {
            mProgress.setMessage(mess);
            mProgress.show();
        }
    }

    private void hideDialog() {
        mProgress.dismiss();
    }

}
