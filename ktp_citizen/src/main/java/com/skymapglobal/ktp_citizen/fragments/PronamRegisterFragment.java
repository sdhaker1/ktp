package com.skymapglobal.ktp_citizen.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;
import com.skymapglobal.ktp_citizen.ActivityMain;
import com.skymapglobal.ktp_citizen.CaldroidSampleActivity;
import com.skymapglobal.ktp_citizen.EGDActivity;
import com.skymapglobal.ktp_citizen.MenuActivity;
import com.skymapglobal.ktp_citizen.R;
import com.skymapglobal.ktp_citizen.adapter.CustomLostItemAdapter;
import com.skymapglobal.ktp_citizen.adapter.CustomSpinnerAdapter;
import com.skymapglobal.ktp_citizen.helper.FileHelper;
import com.skymapglobal.ktp_citizen.helper.IServiceCallback;
import com.skymapglobal.ktp_citizen.helper.ServiceConnector;
import com.skymapglobal.ktp_citizen.models.PoliceStationDetails;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Admin on 15-03-2017.
 */

public class PronamRegisterFragment extends BaseAPIFragment implements View.OnClickListener ,  IServiceCallback {


    private static PronamRegisterFragment instance = new PronamRegisterFragment();




    public static PronamRegisterFragment newInstance() {
        return instance;
    }


    private ImageView imagePreview;
    private EditText edt_name, edt_father_name,editTextDateofBirthDay,editTextDateofBirthMonth,editTextDateofBirthYear,editTextBloodGroup, rg_edt_email,
            rg_edt_spouse_name,editTextSpouseDateofBirthDay,editTextSpouseDateofBirthMonth,editTextSpouseDateofBirthYear,editTextBloodGroupSpouse,
            rg_edt_present_address,rg_edt_pin_number,rg_edt_phn_number,rg_edt_mobile_number,rg_edt_permanent_address,
            rg_edt_children_name,rg_edt_relationship,rg_edt_children_address,rg_edt_contact_number,

            edt_emergency_conatact_name, edt_emergency_conatact_no, edt_emergency_conatact_address, edt_emergency_conatact_email;

    private AppCompatSpinner spRegisterType, spMaritalStatus;
    private AutoCompleteTextView edt_police_station, edt_emp_police_station;
    RadioGroup radioGroupSex,radioGroupLiving;
    Spinner mSpinnerThana;

    //Mandatory Variable
    private String mApplicantName   ="";
    private String mSex             ="MALE";
    private String mDateOfBirth     ="";
    private String mPresentAddress  ="";
    private String mPostCode        ="";
    private String mPhoneNo         ="";
    private String mEmail           ="";

    private String mEmergencyName   ="";
    private String mEmergencyPhone  ="";


    private String mLivingAlone     ="No";

    private String mSpouseDateOfBirth     ="";

    File imageFile;

    private boolean isImageTaken=false;

    private boolean isOwnDateOfBirthDay=false;
    private boolean isOwnDateOfBirthMonth=false;
    private boolean isOwnDateOfBirthYear=false;

    private boolean isSpouseDateOfBirthDay=false;
    private boolean isSpouseDateOfBirthMonth=false;
    private boolean isspouseDateOfBirthYear=false;

    CaldroidFragment dialogCaldroidFragment;
    CaldroidListener listener;
    SimpleDateFormat mFormatDate;

    ArrayList<PoliceStationDetails> mPoliceStationDetailsList;
    ArrayAdapter<String> mDateAdapter=null;
    String[] mDayArray = null;
    String[] mMonthArray = null;
    String[] mYearArray = null;

    ArrayAdapter<String> mBloodAdapter=null;
    String[] mBloodGroup =null;

    private String errorToast="";



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String image_name = "pranam_applicant.png";
        File imagesFolder = FileHelper.getInstance().getImageDir();
        imageFile = new File(imagesFolder, image_name);
        if (imageFile.exists()) {
            imageFile.delete();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pronam_register, container, false);

        view.findViewById(R.id.rg_btn_submit).setOnClickListener(this);
        view.findViewById(R.id.rg_btn_clear).setOnClickListener(this);

        imagePreview = (ImageView) view.findViewById(R.id.rg_image);
        imagePreview.setOnClickListener(this);

        FillYear();
        mDayArray = getResources().getStringArray(R.array.day_array);
        mMonthArray = getResources().getStringArray(R.array.month_array);
        mBloodGroup= getResources().getStringArray(R.array.blood_group_array);

        //Personal Details
        edt_name                = (EditText) view.findViewById(R.id.edt_ApplicantName);
        radioGroupSex           = (RadioGroup) view.findViewById(R.id.radio_group_sex);
        editTextDateofBirthDay   = (EditText) view.findViewById(R.id.editTextDateofBirthDay);
        editTextDateofBirthMonth = (EditText) view.findViewById(R.id.editTextDateofBirthMonth);
        editTextDateofBirthYear  = (EditText) view.findViewById(R.id.editTextDateofBirthYear);
        editTextBloodGroup      = (EditText) view.findViewById(R.id.editTextBloodGroup);

        editTextBloodGroup.setFocusable(false);
        editTextBloodGroup.setClickable(true);

        rg_edt_email            = (EditText) view.findViewById(R.id.edt_email);

        radioGroupLiving        = (RadioGroup) view.findViewById(R.id.radio_group_living);

        //Spouse Details
        rg_edt_spouse_name              = (EditText) view.findViewById(R.id.rg_edt_spouse_name);
        editTextSpouseDateofBirthDay    = (EditText) view.findViewById(R.id.editTextSpouseDateofBirthDay);
        editTextSpouseDateofBirthMonth  = (EditText) view.findViewById(R.id.editTextSpouseDateofBirthMonth);
        editTextSpouseDateofBirthYear   = (EditText) view.findViewById(R.id.editTextSpouseDateofBirthYear);
        editTextBloodGroupSpouse        = (EditText) view.findViewById(R.id.editTextBloodGroupSpouse);

        editTextBloodGroupSpouse.setFocusable(false);
        editTextBloodGroupSpouse.setClickable(true);

        //Present Address
        rg_edt_present_address  =(EditText) view.findViewById(R.id.rg_edt_present_address);
        rg_edt_pin_number       =(EditText) view.findViewById(R.id.rg_edt_pin_number);
        rg_edt_phn_number       =(EditText) view.findViewById(R.id.rg_edt_phn_number);
        mSpinnerThana           =(Spinner)  view.findViewById(R.id.spinner_thana);
        rg_edt_mobile_number    =(EditText) view.findViewById(R.id.rg_edt_mobile_number);

        //Permanent Address
        rg_edt_permanent_address=(EditText) view.findViewById(R.id.rg_edt_permanent_address);

        //Details of family member
        rg_edt_children_name    =(EditText) view.findViewById(R.id.rg_edt_children_name);
        rg_edt_relationship     =(EditText) view.findViewById(R.id.rg_edt_relationship);
        rg_edt_children_address =(EditText) view.findViewById(R.id.rg_edt_children_address);
        rg_edt_contact_number   =(EditText) view.findViewById(R.id.rg_edt_contact_number);

        //Emergency contact details

        edt_emergency_conatact_name     = (EditText) view.findViewById(R.id.rg_edt_emergency_contact_name);
        edt_emergency_conatact_no       = (EditText) view.findViewById(R.id.rg_edt_emergency_contact_contact_number);
        edt_emergency_conatact_address  = (EditText) view.findViewById(R.id.rg_edt_emergency_contact_address);
        edt_emergency_conatact_email    = (EditText) view.findViewById(R.id.rg_edt_emergency_email);



        editTextDateofBirthDay.setFocusable(false);
        editTextDateofBirthDay.setClickable(true);

        editTextDateofBirthMonth.setFocusable(false);
        editTextDateofBirthMonth.setClickable(true);

        editTextDateofBirthYear.setFocusable(false);
        editTextDateofBirthYear.setClickable(true);

        editTextSpouseDateofBirthDay.setFocusable(false);
        editTextSpouseDateofBirthDay.setClickable(true);

        editTextSpouseDateofBirthMonth.setFocusable(false);
        editTextSpouseDateofBirthMonth.setClickable(true);

        editTextSpouseDateofBirthYear.setFocusable(false);
        editTextSpouseDateofBirthYear.setClickable(true);

        radioGroupSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.radio_male:
                        mSex="MALE";
                        break;
                    case R.id.radio_female:
                        mSex="FEMALE";
                        break;
                    default:
                        break;
                }
            }
        });

        radioGroupLiving.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup,int checkedId) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.radio_yes:
                        mLivingAlone="Yes";
                        rg_edt_spouse_name.setEnabled(false);
                        editTextSpouseDateofBirthDay.setEnabled(false);
                        editTextSpouseDateofBirthMonth.setEnabled(false);
                        editTextSpouseDateofBirthYear.setEnabled(false);
                        editTextBloodGroupSpouse.setEnabled(false);

                        rg_edt_spouse_name.setText("");
                        editTextSpouseDateofBirthDay.setText("");
                        editTextSpouseDateofBirthMonth.setText("");
                        editTextSpouseDateofBirthYear.setText("");
                        editTextBloodGroupSpouse.setText("");

                        break;
                    case R.id.radio_no:
                        mLivingAlone="No";
                        rg_edt_spouse_name.setEnabled(true);
                        editTextSpouseDateofBirthDay.setEnabled(true);
                        editTextSpouseDateofBirthMonth.setEnabled(true);
                        editTextSpouseDateofBirthYear.setEnabled(true);
                        editTextBloodGroupSpouse.setEnabled(true);
                        break;
                    default:
                        break;
                }
            }
        });

        editTextBloodGroupSpouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowBloodGroupDialog(2);
            }
        });

        editTextBloodGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowBloodGroupDialog(1);
            }
        });

        editTextDateofBirthDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isOwnDateOfBirthDay=true;
                //ChooseDateDialog();
                ShowDateDialog(1,1);

            }
        });
        editTextDateofBirthMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isOwnDateOfBirthMonth=true;
                ShowDateDialog(2,1);

            }
        });
        editTextDateofBirthYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isOwnDateOfBirthYear=true;
                ShowDateDialog(3,1);

            }
        });

        editTextSpouseDateofBirthDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSpouseDateOfBirthDay=true;
                ShowDateDialog(1,2);
            }
        });

        editTextSpouseDateofBirthMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSpouseDateOfBirthMonth=true;
                ShowDateDialog(2,2);
            }
        });

        editTextSpouseDateofBirthYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isspouseDateOfBirthYear=true;
                ShowDateDialog(3,2);
            }
        });

        mFormatDate = new SimpleDateFormat("dd-MM-yyyy");
        listener = new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {

                /*if(true==isOwnDateOfBirth){
                    mDateOfBirth = mFormatDate.format(date);
                    editTextDateofBirth.setText(mDateOfBirth);
                }else{
                    mSpouseDateOfBirth = mFormatDate.format(date);
                    editTextSpouseDateofBirth.setText(mSpouseDateOfBirth);
                }*/
                dialogCaldroidFragment.dismiss();
            }
            @Override
            public void onChangeMonth(int month, int year) {

            }
            @Override
            public void onLongClickDate(Date date, View view) {

            }
            @Override
            public void onCaldroidViewCreated() {

            }
        };

        BuildPoliceStationList();
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(),R.layout.spinner_list_item,  mPoliceStationDetailsList);
        //adapter.setDropDownViewResource(R.layout.spinner_dropdown);
        mSpinnerThana.setAdapter(adapter);

        return view;
    }

    private void FillYear(){
        mYearArray=new String[100];
        for(int count=0;count<100;count++){
            mYearArray[count]=(2017-count)+"";
        }
    }


    public void ShowDateDialog(final int datetype, final int birthdaytype) {


        if(1==datetype){
            mDateAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, android.R.id.text1, mDayArray);
        }
        if(2==datetype){
            mDateAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, android.R.id.text1, mMonthArray);
        }
        if(3==datetype){
            mDateAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, android.R.id.text1, mYearArray);
        }

        final Dialog mDialogCustomer = new Dialog(getActivity(), R.style.PauseDialog);
        mDialogCustomer.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogCustomer.setContentView(R.layout.dialog_date);
        mDialogCustomer.setCancelable(true);
        TextView title=(TextView) mDialogCustomer.findViewById(R.id.title);
        if(1==datetype){
            title.setText("     Day     ");
        }
        if(2==datetype){
            title.setText("    Month    ");
        }
        if(3==datetype){
            title.setText("    Year    ");
        }
        ListView dialogList = (ListView) mDialogCustomer.findViewById(R.id.LostItemList);
        dialogList.setAdapter(mDateAdapter);
        dialogList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch(birthdaytype){
                    case 1:
                        switch (datetype){
                            case 1:
                                editTextDateofBirthDay.setText(mDayArray[position]);
                                break;
                            case 2:
                                editTextDateofBirthMonth.setText(mMonthArray[position]);
                                break;
                            case 3:
                                editTextDateofBirthYear.setText(mYearArray[position]);
                                break;
                        }
                        break;
                    case 2:
                        switch (datetype){
                            case 1:
                                editTextSpouseDateofBirthDay.setText(mDayArray[position]);
                                break;
                            case 2:
                                editTextSpouseDateofBirthMonth.setText(mMonthArray[position]);
                                break;
                            case 3:
                                editTextSpouseDateofBirthYear.setText(mYearArray[position]);
                                break;
                        }
                        break;
                }
                mDialogCustomer.cancel();
            }
        });
        mDialogCustomer.show();
    }


    public void ShowBloodGroupDialog(final int whosebloodgroup) {
        mBloodAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, android.R.id.text1, mBloodGroup);

        final Dialog mDialogCustomer = new Dialog(getActivity(), R.style.PauseDialog);
        mDialogCustomer.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogCustomer.setContentView(R.layout.dialog_date);
        mDialogCustomer.setCancelable(true);
        TextView title=(TextView) mDialogCustomer.findViewById(R.id.title);
        title.setText("  Blood Group  ");

        ListView dialogList = (ListView) mDialogCustomer.findViewById(R.id.LostItemList);
        dialogList.setAdapter(mBloodAdapter);
        dialogList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (whosebloodgroup){
                    case 1:
                        editTextBloodGroup.setText(mBloodGroup[position]);
                        break;
                    case 2:
                        editTextBloodGroupSpouse.setText(mBloodGroup[position]);
                        break;
                }
                mDialogCustomer.cancel();
            }
        });
        mDialogCustomer.show();
    }

    private boolean DateValidation(String day,String month,int year,String months){
        boolean isOk=true;
        errorToast="";
        if (day.equals("31") &&
                (month.equals("4") || month .equals("6") || month.equals("9") ||
                        month.equals("11") || month.equals("04") || month .equals("06") ||
                        month.equals("09"))) {
            isOk= false; // only 1,3,5,7,8,10,12 has 31 days
            errorToast+=months+" don't have 31 day";
        }else if (month.equals("2") || month.equals("02")) {
            //leap year
            if(year % 4==0){
                if(Integer.parseInt(day)>29){
                    isOk= false;
                    errorToast+="Selected year is leap year february month should not greater than 29 days";
                }
                else{
                    isOk= true;
                }
            }else{
                if(Integer.parseInt(day)>28){
                    isOk= false;
                    errorToast+="Selected year is not leap year february month should not greater than 28 days";
                }
                else{
                    isOk= true;
                }
            }
        } else if(Integer.parseInt(day)>31 || Integer.parseInt(day)< 1){
            isOk = false;
        } else if(Integer.parseInt(month)>12){
            isOk = false;
        } else{
            isOk = true;
        }
        return isOk;
    }

    private void BuildPoliceStationList() {
        mPoliceStationDetailsList = new ArrayList<PoliceStationDetails>();
        String[] section = getResources().getStringArray(R.array.police_station_code);
        if (section != null && section.length > 0) {
            for (String s : section) {
                if (s.contains("#")) {
                    String[] splitsection = s.split("#");
                    if (splitsection.length == 2) {
                        PoliceStationDetails obj = new PoliceStationDetails();
                        obj.setStationName(splitsection[0].trim());
                        obj.setCode(splitsection[1].trim());
                        mPoliceStationDetailsList.add(obj);
                        obj = null;
                    }
                }
            }
        }
    }

    public void ChooseDateDialog(){
        dialogCaldroidFragment = new CaldroidFragment();
        dialogCaldroidFragment.setCaldroidListener(listener);
        final String dialogTag = "CALDROID_DIALOG_FRAGMENT";
        Bundle bundle = new Bundle();
        bundle.putString(CaldroidFragment.DIALOG_TITLE,"Select a date");
        dialogCaldroidFragment.setArguments(bundle);
        dialogCaldroidFragment.show(getActivity().getSupportFragmentManager(),dialogTag);

        //DateDialog();
    }

   /* public void DateDialog(){
        //CaldroidFragment caldroidFragment = new CaldroidFragment();
        dialogCaldroidFragment = new CaldroidFragment();
        dialogCaldroidFragment.setCaldroidListener(listener);
        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        dialogCaldroidFragment.setArguments(args);

        android.support.v4.app.FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
        t.replace(R.id.cale, dialogCaldroidFragment);
        t.commit();
    }*/



    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.rg_btn_submit:
                AttemptSubmit();
                break;

            case R.id.rg_btn_clear:
                ClearData();
                break;

            case R.id.rg_image:
                captureImage();
                break;

            default:
                break;
        }
    }

    private void captureImage() {
        Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
        startActivityForResult(imageIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:

                if (resultCode == -1) {
                    FileHelper.getInstance().compressImage(imageFile.getPath(), imageFile.getPath(),"");
                    imagePreview.setImageBitmap(bitmapFromFile(imageFile));
                    isImageTaken=true;
                } else if (resultCode == 0) {
                    isImageTaken=false;
                    Toast.makeText(context,"User cancelled image capture", Toast.LENGTH_SHORT).show();
                } else {
                    isImageTaken=false;
                    Toast.makeText(context,"Sorry! Failed to capture image", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void ClearData(){

        imagePreview.setImageResource(R.mipmap.ic_take_photo);

        edt_name.setText("");
        editTextDateofBirthDay.setText("");
        editTextDateofBirthMonth.setText("");
        editTextDateofBirthYear.setText("");
        editTextBloodGroup.setText("");
        rg_edt_email.setText("");


        //Spouse Details
        rg_edt_spouse_name.setText("");
        editTextSpouseDateofBirthDay.setText("");
        editTextSpouseDateofBirthMonth.setText("");
        editTextSpouseDateofBirthYear.setText("");
        editTextBloodGroupSpouse.setText("");

        //Present Address
        rg_edt_present_address.setText("");
        rg_edt_pin_number.setText("");
        rg_edt_phn_number.setText("");
        rg_edt_mobile_number.setText("");

        //Permanent Address
        rg_edt_permanent_address.setText("");

        //Details of family member
        rg_edt_children_name.setText("");
        rg_edt_relationship.setText("");
        rg_edt_children_address.setText("");
        rg_edt_contact_number.setText("");

        //Emergency contact details

        edt_emergency_conatact_name.setText("");
        edt_emergency_conatact_no.setText("");
        edt_emergency_conatact_address.setText("");
        edt_emergency_conatact_email.setText("");




    }


    private void AttemptSubmit(){

        boolean isDataOk=true;

        mApplicantName      =edt_name.getText().toString().trim();
        mPresentAddress     =rg_edt_present_address.getText().toString().trim();
        mPostCode           =rg_edt_pin_number.getText().toString().trim();
        mPhoneNo            =rg_edt_mobile_number.getText().toString().trim();
        mEmail              =rg_edt_email.getText().toString().trim();

        mEmergencyName      =edt_emergency_conatact_name.getText().toString().trim();
        mEmergencyPhone     =edt_emergency_conatact_no.getText().toString().trim();



        String day=editTextDateofBirthDay.getText().toString().trim();
        String month=editTextDateofBirthMonth.getText().toString().trim();
        String year=editTextDateofBirthYear.getText().toString().trim();

        if(day.length()<=0){
            isDataOk=false;
            Toast.makeText(getActivity(),"Please select valid Date in Date of Birth",Toast.LENGTH_LONG).show();
        }
        if(month.length()<=0){
            isDataOk=false;
            Toast.makeText(getActivity(),"Please select valid Month in Date of Birth",Toast.LENGTH_LONG).show();
        }
        if(year.length()<=0){
            isDataOk=false;
            Toast.makeText(getActivity(),"Please select valid Year in Date of Birth",Toast.LENGTH_LONG).show();
        }
        if(true==isDataOk){
            String monthnumber=MonthToNumber(month);
            int yearno=Integer.parseInt(year);
            if((2017-yearno)>=60){
                boolean isOk=DateValidation(day,monthnumber,yearno,month);
                if(true==isOk){
                    mDateOfBirth=day+"-"+monthnumber+"-"+year;
                }else{
                    isDataOk=false;
                    Toast.makeText(getActivity(),errorToast,Toast.LENGTH_LONG).show();
                }
            }else{
                isDataOk=false;
                Toast.makeText(getActivity(),"You are not Senior Citizen",Toast.LENGTH_LONG).show();
            }
        }


        if(mApplicantName.length()<=0){
            isDataOk=false;
            edt_name.setError("Please provide name");
        }

        if(mPresentAddress.length()<=0){
            isDataOk=false;
            rg_edt_present_address.setError("Please provide valid address");
        }

        if(mPostCode.length()<=0){
            isDataOk=false;
            rg_edt_pin_number.setError("Please provide 6 digit pin");
        }

        if(mPhoneNo.length()!=10){
            isDataOk=false;
            rg_edt_mobile_number.setError("Please provide 10 digit mobile no");
        }

        if(mEmail.length()<=0){
            isDataOk=false;
            rg_edt_email.setError("Please provide mail id");
        }

        if(mDateOfBirth.length()<=0){
            isDataOk=false;
            //editTextDateofBirth.setError("Please provide date of birth");
        }

        if(false==isImageTaken){
            isDataOk=false;
            Toast.makeText(getActivity(),"Please take image",Toast.LENGTH_SHORT).show();
        }

        if(mEmergencyName.length()<=0){
            isDataOk=false;
            edt_emergency_conatact_name.setError("Please provide valid emergency contact name");
        }

        if(mEmergencyPhone.length()!=10){
            isDataOk=false;
            edt_emergency_conatact_no.setError("Please provide 10 digit emergency contact");
        }


        if(true==isDataOk){
            SubMissionofPronamRegistration(mApplicantName,
                    mSex,
                    mDateOfBirth,
                    mEmail,
                    mPresentAddress,
                    mPostCode,
                    mPhoneNo,
                    mEmergencyName,
                    mEmergencyPhone,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "");

        }else{
            Toast.makeText(getActivity(),"Please provide valid input in mandatory field",Toast.LENGTH_SHORT).show();
        }
    }

    private String MonthToNumber(String month){
        String number="00";

        switch (month){
            case "Jan":
                number="01";
                break;
            case "Feb":
                number="02";
                break;
            case "Mar":
                number="03";
                break;
            case "Apr":
                number="04";
                break;
            case "May":
                number="05";
                break;
            case "Jun":
                number="06";
                break;
            case "Jul":
                number="07";
                break;
            case "Aug":
                number="08";
                break;
            case "Sept":
                number="09";
                break;
            case "Oct":
                number="10";
                break;
            case "Nov":
                number="11";
                break;
            case "Dec":
                number="12";
                break;

        }

        return  number;
    }



    private final String PRONAM_SUBMISSION_URL ="http://pronam.co.in/policeform/API/senddata.php?";

    private void SubMissionofPronamRegistration(String name, String sex, String dob, String email, String present_address,
                                                String postcode, String phone_no, String contacted_person_name, String contacted_person_email,
                                                String blood, String spousename, String spouse_dob, String living_alone,
                                                String dobproof1, String spouse_blood, String paddress1, String phno1,
                                                String thana, String peradd, String name2, String relationship,
                                                String address2, String contactno, String checkagrement, String no,
                                                String spousename2, String dob3, String dobproof3, String blood3,
                                                String address3, String email2, String phno2, String dobproof2) {


        //String param = "ps_code=%s&case_ref=%s&gde_date=%s";

        String param="api_id=hA5d6hG05dfkM7Fp9gfR&"+
                "name=%s&" +
                "sex=%s&" +
                "dob=%s&" +
                "email=%s&" +
                "present_address=%s&" +
                "postcode=%s&" +
                "phone_no=%s&" +
                "contacted_person_name=%s&" +
                "contacted_person_email=%s&" +
                "blood=%s&" +
                "spousename=%s&" +
                "spouse_dob=%s&" +
                "living_alone=%s&" +
                "dobproof1=%s&" +
                "spouse_blood=%s&" +
                "paddress1=%s&" +
                "phno1=%s&" +
                "thana=%s&" +
                "peradd=%s&" +
                "name2=%s&" +
                "relationship=%s&" +
                "address2=%s&" +
                "contactno=%s&" +
                "checkagrement=%s&" +
                "no=%s&" +
                "spousename2=%s&" +
                "dob3=%s&" +
                "dobproof3=%s&" +
                "blood3=%s&" +
                "address3=%s&" +
                "email=%s&" +
                "phno2=%s&" +
                "dobproof2=%s";


        param = String.format(param,
                name,
                sex,
                dob,
                email,
                present_address,
                postcode,
                phone_no,
                contacted_person_name,
                contacted_person_email,
                blood,
                spousename,
                spouse_dob,
                living_alone,
                dobproof1,
                spouse_blood,
                paddress1,
                phno1,
                thana,
                peradd,
                name2,
                relationship,
                address2,
                contactno,
                checkagrement,
                no,
                spousename2,
                dob3,
                dobproof3,
                blood3,
                address3,
                email2,
                phno2,
                dobproof2);

        ServiceConnector.getInstance().sendRequest(getActivity(), PRONAM_SUBMISSION_URL, param,"PRONAM_SUBMISSION_URL", this);

    }

    @Override
    public void onReceiver(String method, String msg) {
        if (method.equals(PRONAM_SUBMISSION_URL)) {
            if(msg.contains("successfully")){
                ClearData();
                Toast.makeText(getActivity(),"Record inserted sucessfully",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onError(String method, String error) {
        if (method.equals(PRONAM_SUBMISSION_URL)) {
            Toast.makeText(getActivity(),error,Toast.LENGTH_LONG).show();
        }

    }
}
