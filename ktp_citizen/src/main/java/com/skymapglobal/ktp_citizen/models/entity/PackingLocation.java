package com.skymapglobal.ktp_citizen.models.entity;

/**
 * Created by thaibui on 9/10/16.
 */
public class PackingLocation {
    public String lot_key;
    public String lot_name;
    public String lng;
    public String lat;
    public String avl;
}
