package com.skymapglobal.ktp_citizen.models;

/**
 * Created by Admin on 30-04-2017.
 */

public class EGdComplaintDetails {
    public String Name          ="";
    public String Gender        ="";
    public String Address       ="";
    public String MobileNo      ="";
    public String AlternateNo   ="";
    public String ComplaintType ="";
    public String PlaceOfOccurence  ="";
    public String DateOfOccurrence  ="";
    public String PoliceStationName ="";

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getAlternateNo() {
        return AlternateNo;
    }

    public void setAlternateNo(String alternateNo) {
        AlternateNo = alternateNo;
    }

    public String getComplaintType() {
        return ComplaintType;
    }

    public void setComplaintType(String complaintType) {
        ComplaintType = complaintType;
    }

    public String getPlaceOfOccurence() {
        return PlaceOfOccurence;
    }

    public void setPlaceOfOccurence(String placeOfOccurence) {
        PlaceOfOccurence = placeOfOccurence;
    }

    public String getDateOfOccurrence() {
        return DateOfOccurrence;
    }

    public void setDateOfOccurrence(String dateOfOccurrence) {
        DateOfOccurrence = dateOfOccurrence;
    }

    public String getPoliceStationName() {
        return PoliceStationName;
    }

    public void setPoliceStationName(String policeStationName) {
        PoliceStationName = policeStationName;
    }

}
