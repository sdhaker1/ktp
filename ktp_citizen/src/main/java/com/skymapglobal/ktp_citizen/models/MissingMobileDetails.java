package com.skymapglobal.ktp_citizen.models;

/**
 * Created by Admin on 03-03-2017.
 */

public class MissingMobileDetails {
    String ownerName   ="";
    String mobileNo1    ="";
    String mobileNo2    ="";
    String imeiNo1      ="";
    String imeiNo2      ="";
    String dateofMissing="";
    String manufacture  ="";
    String modelDetails ="";
    String contactNo    ="";
    String status       ="";


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }



    public String getImeiNo2() {
        return imeiNo2;
    }

    public void setImeiNo2(String imeiNo2) {
        this.imeiNo2 = imeiNo2;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownaerName) {
        this.ownerName = ownaerName;
    }

    public String getMobileNo1() {
        return mobileNo1;
    }

    public void setMobileNo1(String mobileNo1) {
        this.mobileNo1 = mobileNo1;
    }

    public String getMobileNo2() {
        return mobileNo2;
    }

    public void setMobileNo2(String mobileNo2) {
        this.mobileNo2 = mobileNo2;
    }

    public String getImeiNo1() {
        return imeiNo1;
    }

    public void setImeiNo1(String imeiNo1) {
        this.imeiNo1 = imeiNo1;
    }

    public String getDateofMissing() {
        return dateofMissing;
    }

    public void setDateofMissing(String dateofMissing) {
        this.dateofMissing = dateofMissing;
    }

    public String getManufacture() {
        return manufacture;
    }

    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }

    public String getModelDetails() {
        return modelDetails;
    }

    public void setModelDetails(String modelDetails) {
        this.modelDetails = modelDetails;
    }
}
