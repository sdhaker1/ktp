package com.skymapglobal.ktp_citizen.models;

/**
 * Created by thaibui on 9/19/16.
 */
public class RegistrationModel {
    public String name;
    public String fatherName;
    public String age;
    public String cardNo;
    public String voterID;
    public String presentAddress;
    public String policeStation;
    public String mobile;
    public String marital;
    public String permanentAddress;
    public String district;
    public String ps;
    public String empName;
    public String empAddress;
    public String empPoliceStation;
    public String empPlaceOfWork;
    public String refName;
    public String refMobile;
    public String refAddress;
    public String refPs;
    public String refName2;
    public String refMobile2;
    public String refAddress2;
    public String refPs2;

    public String empOTP;
    public String empMobile;
}
