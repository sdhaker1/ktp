package com.skymapglobal.ktp_citizen.models;

/**
 * Created by thaibui on 8/27/16.
 */
public class PoliceRegisterModel{

    public String type;
    public String marital;
    public String rented;
    public String tenantName;
    public String fatherName;
    public String cardNo;
    public String age;
    public String occupation;
    public String mobile;
    public String tenantAddress;
    public String placeWork;
    public String detailsPerson;
    public String personPhone;
    public String area;
    public String currentLocation;
    public String houseAddress;

}
