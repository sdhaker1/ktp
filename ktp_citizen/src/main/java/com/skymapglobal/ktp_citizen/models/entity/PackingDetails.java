package com.skymapglobal.ktp_citizen.models.entity;

/**
 * Created by thaibui on 9/10/16.
 */
public class PackingDetails {
    public String lot_name;
    public String lot_paidType;
    public String lot_opening_time;
    public String lot_no;
    public String lot_contact_no;
    public String lot_owner_name;
    public String lot_key;
    public String active;
    public String lot_remarks;
    public String lot_owner_contact_no;
    public String lot_street_name;
    public String lot_closing_time;
    public String totalAvialable;
    public String totalPresent;


    public String twoWheelerDayPrice;
    public String fourWheelerDayPrice;
    public String twoWheelerNightPrice;
    public String fourWheelerNightPrice;
    public String twoWheelerTotal;
    public String twoWheelerAvailable;
    public String fourWheelerTotal;
    public String fourWheelerAvailable;

}//"lot_name":"Abhikshan Bhavan",
//        "lot_paidType":"Free",
//        "lot_opening_time":"08:00 AM",
//        "lot_no":"NA",
//        "lot_contact_no":"8888812347",
//        "lot_owner_name":"A. Kumar",
//        "lot_key":"314",
//        "lot_remarks":"Overhead WaterTank",
//        "lot_owner_contact_no":"9999912347",
//        "lot_street_name":"Sector-V",
//        "lot_closing_time":"10:00 PM",
//        "totalAvialable":"100",
//        "totalPresent":"100"