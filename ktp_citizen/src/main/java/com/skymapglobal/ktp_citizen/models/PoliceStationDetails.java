package com.skymapglobal.ktp_citizen.models;

/**
 * Created by Admin on 02-03-2017.
 */

public class PoliceStationDetails {
    String mStationName     ="";
    String mCode            ="";

    public String getCode() {
        return mCode;
    }

    public void setCode(String mCode) {
        this.mCode = mCode;
    }

    public String getStationName() {
        return mStationName;
    }

    public void setStationName(String mStationName) {
        this.mStationName = mStationName;
    }



}
