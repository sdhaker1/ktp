package com.skymapglobal.ktp_citizen.models;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

/**
 * Created by Admin on 29-04-2017.
 */

public class ComplaintRegisterDetails implements KvmSerializable {

    public String Name             ="";
    public String DateOfBirth      ="";
    public String Gender           ="";
    public String MobileNo         ="";
    public String AlternateNo      ="";
    public String Address          ="";
    public String PSID             ="";
    public String PSName           ="";
    public String PSAddress        ="";
    public String PlaceOfOccurance ="";
    public String DateOfOccurance  ="";
    public String ComplaintType    ="";

    public ComplaintRegisterDetails(){

    }

    @Override
    public Object getProperty(int i) {

        switch(i)
        {
            case 0:
                return Name;
            case 1:
                return DateOfBirth;
            case 2:
                return Gender;
            case 3:
                return MobileNo;
            case 4:
                return AlternateNo;
            case 5:
                return Address;
            case 6:
                return PSID;
            case 7:
                return PSName;
            case 8:
                return PSAddress;
            case 9:
                return PlaceOfOccurance;
            case 10:
                return DateOfOccurance;
            case 11:
                return ComplaintType;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 12;
    }

    @Override
    public void setProperty(int i, Object value) {

        switch(i)
        {
            case 0:
                Name = value.toString();
                break;
            case 1:
                DateOfBirth= value.toString();
                break;
            case 2:
                Gender= value.toString();
                break;
            case 3:
                MobileNo= value.toString();
                break;
            case 4:
                AlternateNo= value.toString();
                break;
            case 5:
                Address= value.toString();
                break;
            case 6:
                PSID= value.toString();
                break;
            case 7:
                PSName= value.toString();
                break;
            case 8:
                PSAddress= value.toString();
                break;
            case 9:
                PlaceOfOccurance= value.toString();
                break;
            case 10:
                DateOfOccurance= value.toString();
                break;
            case 11:
                ComplaintType= value.toString();
                break;
            default:
                break;
        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo info) {

        switch(i)
        {
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Name";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "DateOfBirth";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Gender";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "MobileNo";
                break;
            case 4:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "AlternateNo";
                break;
            case 5:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Address";
                break;
            case 6:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "PSID";
                break;
            case 7:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "PSName";
                break;
            case 8:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "PSAddress";
                break;
            case 9:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "PlaceOfOccurance";
                break;
            case 10:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "DateOfOccurance";
                break;
            case 11:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ComplaintType";
                break;
            default:
                break;
        }

    }
}
