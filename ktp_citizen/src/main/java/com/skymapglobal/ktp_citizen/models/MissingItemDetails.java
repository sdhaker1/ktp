package com.skymapglobal.ktp_citizen.models;

/**
 * Created by Admin on 30-04-2017.
 */

public class MissingItemDetails {

    public String ID                ="";
    public String Order             ="";
    public String ItemName          ="";

    public String Field1Type        ="";
    public String Field1Mandatory   ="";
    public String Field1Show        ="";

    public String Field2Type        ="";
    public String Field2Mandatory   ="";
    public String Field2Show        ="";

    public String Field3Type        ="";
    public String Field3Mandatory   ="";
    public String Field3Show        ="";

    public String Type              ="";

    public String Field1Value       ="";
    public String Field2Value       ="";
    public String Field3Value       ="";

    public String getField1Value() {
        return Field1Value;
    }

    public void setField1Value(String field1Value) {
        Field1Value = field1Value;
    }

    public String getField2Value() {
        return Field2Value;
    }

    public void setField2Value(String field2Value) {
        Field2Value = field2Value;
    }

    public String getField3Value() {
        return Field3Value;
    }

    public void setField3Value(String field3Value) {
        Field3Value = field3Value;
    }




    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getOrder() {
        return Order;
    }

    public void setOrder(String order) {
        Order = order;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getField1Type() {
        return Field1Type;
    }

    public void setField1Type(String field1Type) {
        Field1Type = field1Type;
    }

    public String getField1Mandatory() {
        return Field1Mandatory;
    }

    public void setField1Mandatory(String field1Mandatory) {
        Field1Mandatory = field1Mandatory;
    }

    public String getField1Show() {
        return Field1Show;
    }

    public void setField1Show(String field1Show) {
        Field1Show = field1Show;
    }

    public String getField2Type() {
        return Field2Type;
    }

    public void setField2Type(String field2Type) {
        Field2Type = field2Type;
    }

    public String getField2Mandatory() {
        return Field2Mandatory;
    }

    public void setField2Mandatory(String field2Mandatory) {
        Field2Mandatory = field2Mandatory;
    }

    public String getField2Show() {
        return Field2Show;
    }

    public void setField2Show(String field2Show) {
        Field2Show = field2Show;
    }

    public String getField3Type() {
        return Field3Type;
    }

    public void setField3Type(String field3Type) {
        Field3Type = field3Type;
    }

    public String getField3Mandatory() {
        return Field3Mandatory;
    }

    public void setField3Mandatory(String field3Mandatory) {
        Field3Mandatory = field3Mandatory;
    }

    public String getField3Show() {
        return Field3Show;
    }

    public void setField3Show(String field3Show) {
        Field3Show = field3Show;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }






}
