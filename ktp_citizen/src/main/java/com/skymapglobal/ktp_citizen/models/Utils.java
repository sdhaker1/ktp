package com.skymapglobal.ktp_citizen.models;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.directions.route.Route;
import com.google.maps.android.ui.IconGenerator;
import com.skymapglobal.ktp_citizen.R;

/**
 * Created by tuanl on 25-Jul-16.
 */
public class Utils {
    public static void showMessage(Context context, String mess){
        Toast.makeText(context, mess, Toast.LENGTH_SHORT).show();
    }
    public static Bitmap makeBitmap(Context context, Route route, boolean fastest) {
        IconGenerator iconGenerator = new IconGenerator(context);
        View view = ((Activity)context).getLayoutInflater().inflate(R.layout.content_marker_view, null);
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        TextView tvAuto = (TextView) view.findViewById(R.id.tv_auto);

        tvTitle.setText(route.getDurationText() + "(" + route.getDistanceText() +")");
        tvAuto.setText("fastest");
        if (!fastest){
            tvAuto.setVisibility(View.GONE);
        }
        iconGenerator.setContentView(view);
        return iconGenerator.makeIcon();
    }
}
