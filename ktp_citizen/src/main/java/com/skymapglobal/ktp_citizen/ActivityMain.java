package com.skymapglobal.ktp_citizen;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.ErrorDialogFragment;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.gson.Gson;
import com.skymapglobal.ktp_citizen.fragments.BaseRegisterFragment;
import com.skymapglobal.ktp_citizen.fragments.CityParkingFragment;
import com.skymapglobal.ktp_citizen.fragments.ETAFragment;
import com.skymapglobal.ktp_citizen.fragments.MissingMobileFragment;
import com.skymapglobal.ktp_citizen.fragments.ReportCrimeFragment;
import com.skymapglobal.ktp_citizen.fragments.ReportViolationFragment;
import com.skymapglobal.ktp_citizen.fragments.resgister.DomesticHelpFragment;
import com.skymapglobal.ktp_citizen.fragments.resgister.SecurityGuardFragment;
import com.skymapglobal.ktp_citizen.fragments.resgister.ServiceProviderFragment;
import com.skymapglobal.ktp_citizen.fragments.resgister.TenantRegisterFragment;
import com.skymapglobal.ktp_citizen.helper.AppEngineService;
import com.skymapglobal.ktp_citizen.helper.Utils;
import com.skymapglobal.ktp_citizen.models.RegistrationModel;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import pub.devrel.easypermissions.EasyPermissions;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by Admin on 14-03-2017.
 */

public class ActivityMain extends AppCompatActivity
        implements EasyPermissions.PermissionCallbacks,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int REQUEST_READ_PHONE_STATE = 10;
    private static final int REQUEST_CALL_PHONE = 11;
    private static final int REQUEST_READ_CONTACTS = 12;
    private static final int REQUEST_WRITE_CONTACTS = 13;
    private static final int REQUEST_LOCATION_SERVICE = 21;
    private static final int REQUEST_RESOLVE_ERROR = 22;
    private static final int REQUEST_SEND_SMS = 22;


    public static final String DIALOG_ERROR = "dialog_error";
    public static final String STATE_RESOLVING_ERROR = "resolving_error";

    private boolean mResolvingError = false;
    public GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;
    protected Location mLastLocation;

    private Fragment mFragment;

    boolean doubleBackToExitPressedOnce = false;

    public static ImageButton mImageButtonETA = null;
    public static ImageButton mImageButtonParking = null;

    public static ImageButton mImageButtonPendingTrafficcase = null;
    public static ImageButton mImageButtonFIR = null;

    public static ImageButton mImageButtonMissingMobile = null;
    public static ImageButton mImageButtonEGD = null;

    public static ImageButton mImageButtonPronamRegister = null;
    public static ImageButton mImageButtonPronamApplication = null;

    public static ImageButton mImageButtonKarmometer = null;
    public static ImageButton mImageButtonContactUs = null;

    public static ImageButton mImageButtonTenant = null;
    public static ImageButton mImageButtonBabySitter = null;
    public static ImageButton mImageButtonSecurityGuard = null;
    public static ImageButton mImageButtonService = null;

    public static ImageButton mImageButtonReportCrime = null;
    public static ImageButton mImageButtonReportTrafficViolation = null;
    public static ImageButton mImageEmergencyButton = null;

    public static ImageButton mImagePanicButton = null;



    public FloatingActionButton actionHelp;

    private String mDate="";
    SimpleDateFormat mFormatDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_menu);
        loadIMEI();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mFormatDate = new SimpleDateFormat("yyyy-MM-dd");
        mDate=mFormatDate.format(new Date());

        InitializeView();
        //Package();

        mResolvingError = savedInstanceState != null
                && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);

        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getApplicationContext(), "Please update google play service", Toast.LENGTH_LONG).show();
            finish();
        } else if (!isDeviceOnline()) {
            Toast.makeText(getApplicationContext(), "Please connect to internet", Toast.LENGTH_LONG).show();
            finish();
        } else {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.show();
            setupAPI();
        }

        mImageButtonETA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityMain.this, MenuActivity.class);
                i.putExtra("KEY", "ETA");
                startActivity(i);
            }
        });
        mImageButtonParking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityMain.this, MenuActivity.class);
                i.putExtra("KEY", "PARKING");
                startActivity(i);
            }
        });

        mImageButtonPendingTrafficcase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://kolkatatrafficpolice.net/"));
                startActivity(browserIntent);
            }
        });

        mImageButtonFIR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntentFIR = new Intent(Intent.ACTION_VIEW, Uri.parse("http://fir.kolkatapolice.org"));
                startActivity(browserIntentFIR);
            }
        });

        mImageButtonMissingMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityMain.this, MenuActivity.class);
                i.putExtra("KEY", "MISSING_MOBILE");
                startActivity(i);
            }
        });

        mImageButtonEGD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://kpcomplaint.betterplace.co.in/kiosk"));
                startActivity(browserIntent);*/
                Intent i = new Intent(ActivityMain.this, EGDActivity.class);
                //i.putExtra("KEY", "TENANT");
                startActivity(i);
            }
        });


        mImageButtonPronamApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog alertDialog = (new AlertDialog.Builder(ActivityMain.this)).setIcon(R.drawable.fir_icon).setPositiveButton("New User", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent i2 = new Intent(ActivityMain.this, MenuActivity.class);
                        i2.putExtra("KEY", "PRONAM_REGISTER");
                        startActivity(i2);
                        // alertDialog.dismiss();
                    }
                }).setNegativeButton("Existing User", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        try {
                            Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.vipin12ph.pronam");
                            if (launchIntent != null) {
                                startActivity(launchIntent);//null pointer check in case package name was not found
                            } else {
                                final String appPackageName = "com.vipin12ph.pronam"; // getPackageName() from Context or Activity object
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                }
                            }
                        } catch (Exception e) {
                            Toast.makeText(ActivityMain.this, "Karmameter not installed", Toast.LENGTH_SHORT).show();
                        }

                    }
                }).create();
                alertDialog.show();

            }
        });


        mImageButtonKarmometer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("org.ky21c.karmameter.passenger");
                    if (launchIntent != null) {
                        startActivity(launchIntent);//null pointer check in case package name was not found
                    } else {
                        final String appPackageName = "org.ky21c.karmameter.passenger"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(ActivityMain.this, "Karmameter not installed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mImageButtonContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showContactUsDialog();
            }
        });
        mImageButtonTenant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityMain.this, MenuActivity.class);
                i.putExtra("KEY", "TENANT");
                startActivity(i);
            }
        });
        mImageButtonBabySitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityMain.this, MenuActivity.class);
                i.putExtra("KEY", "BABY_SITTER");
                startActivity(i);
            }
        });
        mImageButtonSecurityGuard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityMain.this, MenuActivity.class);
                i.putExtra("KEY", "SECURITY_GUARD");
                startActivity(i);
            }
        });
        mImageButtonService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityMain.this, MenuActivity.class);
                i.putExtra("KEY", "SERVICE_PROVIDER");
                startActivity(i);
            }
        });
        mImageButtonReportCrime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityMain.this, MenuActivity.class);
                i.putExtra("KEY", "REPORT_CRIME");
                startActivity(i);
            }
        });
        mImageButtonReportTrafficViolation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityMain.this, MenuActivity.class);
                i.putExtra("KEY", "REPORT_TRAFFIC_VIOLATION");
                startActivity(i);
            }
        });

        mImageEmergencyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityMain.this, MenuActivity.class);
                i.putExtra("KEY", "ADD_PANIC");
                startActivity(i);
            }
        });

        actionHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hitPanic();
            }
        });

        mImagePanicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitPanic();
            }
        });

    }

    public void hitPanic() {

        if (requestEmergencyPermission()) {
            tryGettingLocation(this);
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "100"));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestEmergencyPermission();
            }
            startActivity(intent);
        }
    }

    Subscription subscription;

    public void tryGettingLocation(Context context) {
        LocationRequest request = LocationRequest.create() //standard GMS LocationRequest
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setNumUpdates(2)
                .setInterval(1000);

        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(context);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //should not happen
        }
        subscription = locationProvider.getUpdatedLocation(request)
                .subscribe(new Action1<Location>() {
                    @Override
                    public void call(Location location) {
                        final SharedPreferences spfs = getSharedPreferences("PanicNumbers", Context.MODE_PRIVATE);

                        String mobile1 = spfs.getString("mobile1", null);
                        String mobile2 = spfs.getString("mobile2", null);

                        String mobile3 = spfs.getString("mobile3", null);
                        String mobile4 = spfs.getString("mobile4", null);

                        new CreateRegisterAsyncTask().execute();

                        sendSMS("9432624365", "", location);

                        if (mobile1 != null){
                            if(mobile1.length()==10){
                                sendSMS(mobile1, "", location);
                            }
                        }

                        if (mobile2 != null){
                            if(mobile2.length()==10){
                                sendSMS(mobile2, "", location);
                            }
                        }

                        if (mobile3 != null){
                            if(mobile3.length()==10){
                                sendSMS(mobile3, "", location);
                            }
                        }

                        if (mobile4 != null){
                            if(mobile4.length()==10){
                                sendSMS(mobile4, "", location);
                            }
                        }

                    }

                });
    }

    ProgressDialog mProgress;//=new ProgressDialog(ActivityMain.this);


    class CreateRegisterAsyncTask extends AsyncTask<RegistrationModel, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress=new ProgressDialog(ActivityMain.this);
            mProgress.setMessage("Please Wait...");
            mProgress.setCancelable(false);
            mProgress.show();
        }
        @Override
        protected String doInBackground(RegistrationModel... params) {
            try {
                String deviceId = Utils.getStringPreferences(ActivityMain.this,"MY_NUMBER",null);
                if(deviceId==null){
                    deviceId=Utils.getIMEI(ActivityMain.this);
                }
                if (mLastLocation == null) {
                    return "Location not found!";
                }
                return AppEngineService.getInstance().myApiService.insertPanic(mDate, deviceId,  mLastLocation.getLatitude() + "", mLastLocation.getLongitude() + "").execute().getData();
            } catch (IOException e) {
                return e.getMessage();
            }
        }
        @Override
        protected void onPostExecute(String result) {
            mProgress.hide();
            try {
                Integer.parseInt(result);
                String caseNo = result;
                //new BaseRegisterFragment.UploadFileAsyncTask().execute(caseNo);
            } catch (Exception e) {
                Toast.makeText(ActivityMain.this, "" + result, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void sendSMS(String phoneNo, String msg, Location mLocation) {
        if (!subscription.isUnsubscribed()) subscription.unsubscribe();
        if (isEnableGPS()) {
            if (mLastLocation != null) {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNo, null, "Please help me. Now I am near to this location  : http://maps.google.com/maps?f=d&daddr=" + ("" + mLocation.getLatitude()).trim() + "%2C" + ("" + mLocation.getLongitude()).trim(), null, null);
            }
        }
    }


    private void InitializeView() {
        mImageButtonETA = (ImageButton) findViewById(R.id.imageButtonETA);
        mImageButtonParking = (ImageButton) findViewById(R.id.imageButtonParking);

        mImageButtonPendingTrafficcase = (ImageButton) findViewById(R.id.imageButtonKnowYourPendingTraffic);
        mImageButtonFIR = (ImageButton) findViewById(R.id.imageButtonFIR);
        mImageButtonMissingMobile = (ImageButton) findViewById(R.id.imageButtonMissingMobile);
        mImageButtonEGD = (ImageButton) findViewById(R.id.imageButtonEGD);

        mImageButtonPronamApplication = (ImageButton) findViewById(R.id.imageButtonPronam);
//        mImageButtonPronamRegister = (ImageButton) findViewById(R.id.imageButtonPronamRegister);

        mImageButtonKarmometer = (ImageButton) findViewById(R.id.imageButtonKarmaMeter);
        mImageButtonContactUs = (ImageButton) findViewById(R.id.imageButtonConnect);

        mImageButtonTenant = (ImageButton) findViewById(R.id.imageButtonTenant);
        mImageButtonBabySitter = (ImageButton) findViewById(R.id.imageButtonDomestic);
        mImageButtonSecurityGuard = (ImageButton) findViewById(R.id.imageButtonSecurity);
        mImageButtonService = (ImageButton) findViewById(R.id.imageButtonServiceProvider);

        mImageButtonReportCrime = (ImageButton) findViewById(R.id.imageButtonReportCrime);
        mImageButtonReportTrafficViolation = (ImageButton) findViewById(R.id.imageButtonReportTrafficViolation);
        mImageEmergencyButton = (ImageButton) findViewById(R.id.imageButtonAddPanic);

        mImagePanicButton= (ImageButton) findViewById(R.id.imageButtonPanic);

        actionHelp = (FloatingActionButton) findViewById(R.id.action_help);
    }

    private void setupAPI() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void getMyLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_SERVICE);
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 5);
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE}, 6);
            return;
        }


        if (isEnableGPS()) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getMyLocation();
        mProgressDialog.dismiss();
        if (mFragment == null) {
            //For Durga Puja purpose only
            //mFragment = ViewTrafficFragment.newInstance(mGoogleApiClient, mLastLocation);
            //mFragment = ETAFragment.newInstance(mGoogleApiClient, mLastLocation);
            //startFragment(mFragment);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    private void showErrorDialog(int errorCode) {
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getFragmentManager(), "Error dialog");
    }

    public void onDialogDismissed() {
        mResolvingError = false;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (mResolvingError) {
            return;
        } else if (connectionResult.hasResolution()) {
            try {
                mResolvingError = true;
                connectionResult.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GoogleApiAvailability.getErrorDialog()
            showErrorDialog(connectionResult.getErrorCode());
            mResolvingError = true;
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (requestCode == REQUEST_LOCATION_SERVICE) {
            getMyLocation();
        } else if (requestCode == 5) {
            getMyLocation();
        } else if (requestCode == 6) {
            getMyLocation();
        } else if (requestCode == 7) {
            getMyLocation();
        } else if (requestCode == REQUEST_SEND_SMS) {
            hitPanic();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }
        if (requestCode == REQUEST_SEND_SMS) {
            hitPanic();
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click back button again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        getMyLocation();

        switch (id) {

            case R.id.nav_eta:
                getSupportActionBar().setTitle("ETA Source Destination");
                if (mFragment instanceof ETAFragment) {
                    break;
                } else {
                    mFragment = ETAFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;
            case R.id.nav_report_crime:
                getSupportActionBar().setTitle("Report Crime");

                if (mFragment instanceof ReportCrimeFragment) {
                    break;
                } else {
                    mFragment = ReportCrimeFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;
            case R.id.nav_report_traffic_violation:
                getSupportActionBar().setTitle("Report Traffic Violation");

                if (mFragment instanceof ReportViolationFragment) {
                    break;
                } else {
                    mFragment = ReportViolationFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;

//            case R.id.nav_help:
//                getSupportActionBar().setTitle("Register With Police");
//
//                if (mFragment instanceof RegisterFragment) {
//                    break;
//                } else {
//                    mFragment = RegisterFragment.newInstance(mGoogleApiClient, mLastLocation);
//                    startFragment(mFragment);
//                }
//                break;
            case R.id.nav_report_pending_traffic:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://kolkatatrafficpolice.net/"));
                startActivity(browserIntent);
                break;

            case R.id.nav_report_FIR:
                Intent browserIntentFIR = new Intent(Intent.ACTION_VIEW, Uri.parse("http://182.71.240.212/kpfirs"));
                startActivity(browserIntentFIR);
                break;

            case R.id.nav_report_missing_mobile:
                getSupportActionBar().setTitle("Missing Mobile Status");

                if (mFragment instanceof MissingMobileFragment) {
                    break;
                } else {
                    mFragment = MissingMobileFragment.newInstance();
                    startFragment(mFragment);
                }
                break;
            case R.id.nav_help:
                showContactUsDialog();
                break;
            case R.id.nav_parking:
                getSupportActionBar().setTitle("City Parking");

                if (mFragment instanceof CityParkingFragment) {
                    break;
                } else {
                    mFragment = CityParkingFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;

            case R.id.nav_karmameter:
                try {
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("org.ky21c.karmameter.passenger");
                    if (launchIntent != null) {
                        startActivity(launchIntent);//null pointer check in case package name was not found
                    } else {
                        final String appPackageName = "org.ky21c.karmameter.passenger"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(ActivityMain.this, "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.nav_tenant_register:
                getSupportActionBar().setTitle("Tenant Registration");

                if (mFragment instanceof TenantRegisterFragment) {
                    break;
                } else {
                    mFragment = TenantRegisterFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;
            case R.id.nav_security_guard_registration:
                getSupportActionBar().setTitle("Security Guard Registration");

                if (mFragment instanceof SecurityGuardFragment) {
                    break;
                } else {
                    mFragment = SecurityGuardFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;
            case R.id.nav_domestic_help:
                getSupportActionBar().setTitle("Domestic Help/Baby Sitter");

                if (mFragment instanceof DomesticHelpFragment) {
                    break;
                } else {
                    mFragment = DomesticHelpFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;
            case R.id.nav_service_provider:
                getSupportActionBar().setTitle("Service Providers");

                if (mFragment instanceof ServiceProviderFragment) {
                    break;
                } else {
                    mFragment = ServiceProviderFragment.newInstance(mGoogleApiClient, mLastLocation);
                    startFragment(mFragment);
                }
                break;
            default:
                break;
        }

        return true;
    }


    private void startFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }


    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    public boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStop() {

        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    private boolean isEnableGPS() {
        final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showSettingsAlert();
            return false;
        }
        return true;
    }

    public void showSettingsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please enable location service.")
                .setCancelable(false)
                .setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        LocationManager service = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        boolean enabled = service
                                .isProviderEnabled(LocationManager.GPS_PROVIDER);

                        if (!enabled) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    }
                });

        AlertDialog alert = builder.create();
        alert.setTitle("Location Service Disabled");
        alert.show();
    }


    private void showContactUsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Connect with Us");
        //builder.setMessage("Please select type connection");
        View view = getLayoutInflater().inflate(R.layout.dialog_connect_us, null);
        builder.setView(view);
        view.findViewById(R.id.bt_sms).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //String number1 = "9903588888";
                String number2 = "9432624365";
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number2, null)));
            }
        });

        view.findViewById(R.id.bt_email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"ccrdd@kolkatapolice.gov.in","kpolcontrolroom@gmail.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Kolkata Police");
                startActivity(Intent.createChooser(intent, "Send Email"));
            }
        });

        view.findViewById(R.id.bt_whatsapp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PackageManager pm = getPackageManager();
                try {
//                    if (ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.READ_CONTACTS)
//                            != PackageManager.PERMISSION_GRANTED) {
//                        requestReadContactPermission();
//                        return;
//                    }
//
//                    if (ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.WRITE_CONTACTS)
//                            != PackageManager.PERMISSION_GRANTED) {
//                        requestWriteContactPermission();
//                        return;
//                    }
//
//
//                    Cursor c = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
//                            new String[]{ContactsContract.Contacts.Data._ID}, ContactsContract.Data.DATA1 + "=?",
//                            new String[]{"+919903588888"}, null);
//                    if (c == null || !c.isFirst()) {
//                        ContentResolver cr = MainActivity.this.getContentResolver();
//                        ContentValues cv = new ContentValues();
//                        cv.put(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, "KolkataPolice");
//                        cv.put(ContactsContract.CommonDataKinds.Phone.NUMBER, "+919903588888");
//                        cv.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
//                        cr.insert(ContactsContract.RawContacts.CONTENT_URI, cv);
//                        return;
//                    }

                    /*PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                    Uri uri=Uri.parse("smsto: +919903588888");
                    Intent waIntent = new Intent(Intent.ACTION_SENDTO, uri);
                    waIntent.setPackage("com.whatsapp");
                    startActivity(Intent.createChooser(waIntent,""));*/

                    /*waIntent.setType("text/plain");
                    PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                    waIntent.setPackage(info.packageName);
                    startActivity(waIntent);*/

                    Intent waIntent = new Intent(Intent.ACTION_SEND, Uri.fromParts("smsto:", "+919903588888", null));
                    waIntent.setType("text/plain");
                    PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                    waIntent.setPackage(info.packageName);
                    startActivity(waIntent);

                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(ActivityMain.this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                            .show();
                }

            }
        });
        view.findViewById(R.id.bt_phone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestCallPhonePermission();
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "1073"));
                    startActivity(intent);
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void loadIMEI() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            requestReadPhoneStatePermission();
        } else {
            doPermissionGrantedStuffs();
        }
    }

    public void doPermissionGrantedStuffs() {
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        Utils.IMEI_No = tm.getDeviceId();
    }

    private void requestReadPhoneStatePermission() {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.READ_PHONE_STATE)) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_PHONE_STATE},
                    REQUEST_READ_PHONE_STATE);
        }
    }

    private void requestCallPhonePermission() {

        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.CALL_PHONE},
                REQUEST_CALL_PHONE);
        // }
    }

    private void requestReadContactPermission() {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_CONTACTS)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    REQUEST_READ_CONTACTS);
        }
    }

    private void requestWriteContactPermission() {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_CONTACTS)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_CONTACTS},
                    REQUEST_WRITE_CONTACTS);
        }
    }

    private boolean requestEmergencyPermission() {

        ArrayList<String> permission = new ArrayList<String>();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permission.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permission.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            permission.add(Manifest.permission.SEND_SMS);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            permission.add(Manifest.permission.CALL_PHONE);
        }


        if (permission.size() > 0) {
            String[] permisssions = permission.toArray(new String[permission.size()]);
            ActivityCompat.requestPermissions(this,
                    permisssions,
                    REQUEST_SEND_SMS);
            return false;
        }
        return true;

    }

    private void Package() {
        final PackageManager pm = getPackageManager();
        //get a list of installed apps.
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        int UID;
        String packname = "";
        //loop through the list of installed packages and see if the selected
        //app is in the list
        for (ApplicationInfo packageInfo : packages) {


            /*UID = packageInfo.uid;

            if(UID==10134){
                String []files=packageInfo.sharedLibraryFiles;
                Bundle b=packageInfo.metaData;
                String str=bundle2string(b);
                int c=0;
                int x=c+1;
                Toast.makeText(StackService.this, " Input Service Running method", Toast.LENGTH_SHORT).show();
            }*/


            packname += packageInfo.packageName + "  \n";
            /*if(packageInfo.packageName.equals(app_selected)){
                //get the UID for the selected app

                break; //found a match, don't need to search anymore
            }*/

        }

        packname += "END";
    }
}
