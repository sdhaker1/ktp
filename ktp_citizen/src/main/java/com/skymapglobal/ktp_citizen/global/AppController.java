package com.skymapglobal.ktp_citizen.global;

import android.app.Application;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.skymapglobal.ktp_citizen.helper.CloudStorage;
import com.skymapglobal.ktp_citizen.helper.FileHelper;
import com.skymapglobal.ktp_citizen.helper.ServiceConnector;
import com.skymapglobal.ktp_citizen.helper.StoreData;

/**
 * Created by thaibui on 8/24/16.
 */
public class AppController extends Application {
    @Override
    public void onCreate() {
        ServiceConnector.getInstance().init(getApplicationContext());
        FileHelper.initial(getApplicationContext());
        StoreData.init(getApplicationContext());
        CloudStorage.shareInstance().init(getApplicationContext());
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);
        super.onCreate();
    }
}
