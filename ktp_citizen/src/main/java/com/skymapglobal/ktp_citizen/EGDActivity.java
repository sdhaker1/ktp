package com.skymapglobal.ktp_citizen;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.style.TextAppearanceSpan;
import android.util.Base64;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;
import com.skymapglobal.ktp_citizen.adapter.CustomLostItemAdapter;
import com.skymapglobal.ktp_citizen.adapter.CustomSpinnerAdapter;
import com.skymapglobal.ktp_citizen.helper.FileHelper;
import com.skymapglobal.ktp_citizen.models.ComplaintRegisterDetails;
import com.skymapglobal.ktp_citizen.models.EGdComplaintDetails;
import com.skymapglobal.ktp_citizen.models.MissingItemDetails;
import com.skymapglobal.ktp_citizen.models.PoliceStationDetails;
import com.skymapglobal.ktp_citizen.models.entity.PackingDetails;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Admin on 28-04-2017.
 */



public class EGDActivity extends AppCompatActivity
        implements EasyPermissions.PermissionCallbacks,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener {

/*private static String SOAP_ACTION = "http://tempuri.org/";
    private static String NAMESPACE = "http://tempuri.org/";
    private static String URL = "http://parktest.mine.nu:81/Service.asmx";
    private static String METHOD_SETMOBILEAUTHENTICATION    = "SetMobileAuthentication";
    private static String METHOD_GETMOBILEAUTHENTICATION    = "GetMobileAuthentication";
    private static String METHOD_GETCOMPLAINITEM            = "GetComplaintItem";

    private static String METHOD_FILECOMPLAINT              = "SetFileComplaint";
    private static String METHOD_FILECOMPLAINTITEMS         = "SetFileComplaintItems";
    private static String METHOD_FILECOMPLAINTIMAGE         = "SetFileComplaintImage";*/

    private static final int REQUEST_WRITE_STORAGE = 5;
    private static final int REQUEST_CAMERA = 0;
    private final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    private int mRowID=0;

    public static TextView mTextViewHeader                      = null;

    public static LinearLayout mllayoutMobileAuthentication     = null;
    public static LinearLayout mllayoutComplainantDetails1      = null;
    public static LinearLayout mllayoutComplainantDetails2      = null;
    public static LinearLayout mllayoutComplainDetails          = null;
    public static LinearLayout mllayoutLostItemDetails          = null;
    public static LinearLayout mllayoutPreview                  = null;
    public static LinearLayout mllayoutFinalEgd                 = null;

    private static LinearLayout mParentLayout 		            = null;

    private static LinearLayout mParentLayoutItem 		        = null;

    public static Button mButtonNext                            = null;
    public static Button mButtonPrevious                        = null;
    public static Button mButtonCancel                          = null;

    public static Button mButtonAdd                             = null;

    public static EditText mEditTextMobile              =null;

    public static EditText mEditTextComplaintNamtName   =null;
    public static Spinner mSpinnerSex                   =null;

    public static EditText mEditTextComplaintMobileNo   =null;
    public static EditText mEditTextComplaintMobileNoCopy   =null;
    public static EditText mEditTextComplaintAlterNateNo=null;
    public static EditText mEditTextAddress             =null;


    public static EditText mEditTextComplaintType       =null;
    public static EditText mEditTextPlaceofOccurence    =null;
    public static EditText mEditTextDateofOccurrence    =null;
    public static Spinner mSpinnerThana                 =null;

    public static Button mButtonLost                             = null;

    //public static Spinner mSpinnerLostItemList          =null;

    public static TextView mTextName                    =null;
    public static TextView mTextGender                  =null;
    public static TextView mTextAddress                 =null;
    public static TextView mTextMobileNo                =null;
    public static TextView mTextAlternateMobileNo       =null;
    public static TextView mTextComplainType            =null;
    public static TextView mTextPlaceofOccurr           =null;
    public static TextView mTextDateofOccurr            =null;
    public static TextView mTextPoliceStation           =null;

    public static TextView mTextReceiptNo           =null;

    private Bitmap mUserBitmap=null;


    private Context mContext;
    private int mStartIndex=0;

    private String mMobieNo="";
    private String mOtpCode="";
    private String mComplaintName="";
    private String mComplaintSex="";

    private String mAlterNateMobileNo="";
    private String mAddress="";

    private String mComplaintType="";
    private String mPlaceOfOccurrence="";
    private String mDateOfOccurrence="";
    private String mPoliceStationName="";
    private String mPSID="";

    private String mLostItemType="";

    public ProgressDialog mProgressDialog;
    public Handler mReportHandler;

    private String mobileAuthenticationResult   ="";
    private String otpVerificationResult        ="";
    private String setAuthenticationresult      ="";
    private String receiptNo                    ="";

    private String mEgdNo="";

    private boolean isImagetake=false;

    SoapObject request;
    SoapSerializationEnvelope envelope;
    HttpTransportSE androidHttpTransport;
    SoapObject result;

    ArrayAdapter<String> genderadapter;

    ArrayList<MissingItemDetails> mMissingItemDetailsList;
    //ArrayList<MissingItemDetails> mCartMissingItemDetailsList;

    ArrayList<MissingItemDetails> mFinalMissingItemDetailsList;

    MissingItemDetails mMissingItemDetails=null;

    ArrayList<PoliceStationDetails> mPoliceStationDetailsList;
    PoliceStationDetails mPoliceStationDetails=null;

    CustomSpinnerAdapter mPoliceStationAdapter=null;

    CustomLostItemAdapter mCustomLostItemAdapter=null;

    CaldroidFragment dialogCaldroidFragment;
    CaldroidListener listener;

    SimpleDateFormat mFormatDate;

    private List<EditText> mEditTextList = new ArrayList<EditText>();

    EGdComplaintDetails mEGdComplaintDetails;

    private boolean isItemSelected  =false;
    private boolean isItemAdded     =false;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_egd);
        //loadIMEI();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Lost Items eGD");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext=EGDActivity.this;

        mFinalMissingItemDetailsList=new ArrayList<MissingItemDetails>();

        genderadapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.gender_array));

        BuildPoliceStationList();
        mPoliceStationAdapter = new CustomSpinnerAdapter(this,R.layout.spinner_list_item,  mPoliceStationDetailsList);

        //mCartMissingItemDetailsList=new ArrayList<MissingItemDetails>();

        InitializeView();


        mFormatDate = new SimpleDateFormat("yyyy-MM-dd");
        final Date today=new Date();

        listener = new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {
                if(date.after(today)){
                    Toast.makeText(mContext,"Future date cannot be selected",Toast.LENGTH_SHORT).show();
                }else{
                    mDateOfOccurrence = mFormatDate.format(date);
                    mEditTextDateofOccurrence.setText(mDateOfOccurrence);
                    dialogCaldroidFragment.dismiss();
                }
            }
            @Override
            public void onChangeMonth(int month, int year) {

            }
            @Override
            public void onLongClickDate(Date date, View view) {

            }
            @Override
            public void onCaldroidViewCreated() {

            }
        };



        mButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IndexWiseControl(mStartIndex);
            }
        });

        mButtonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mStartIndex-=1;
                IndexWiseView(mStartIndex);

            }
        });

        mButtonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isOk=FillItemData();
                if(false==isOk){
                    Toast.makeText(mContext,"Please provide mandatory input in * marked field",Toast.LENGTH_LONG).show();
                }else{
                    mButtonAdd.setVisibility(View.INVISIBLE);
                    DrawItemSummary(1,0);
                }
            }
        });

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mStartIndex==6){
                    finish();
                }else{
                    mStartIndex=0;
                    ShowExitConformationDialog();
                }
            }
        });

        mEditTextDateofOccurrence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseDateDialog();
            }
        });

        mButtonLost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mMissingItemDetailsList!=null){
                    ShowLostItemDetails();
                }
            }
        });


        mReportHandler = new Handler() {
            public void handleMessage(Message msg) {
                mProgressDialog.cancel();
                final int job = msg.getData().getInt("JOBDONE");
                EGDActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        switch (job) {
                            case 1:
                                if(mobileAuthenticationResult!=null){
                                    if(mobileAuthenticationResult.trim().equalsIgnoreCase("true")){
                                        ShowOTPVerification();
                                    }else{
                                        Toast.makeText(mContext,"Mobile authentication error. Please try again",Toast.LENGTH_SHORT).show();
                                    }
                                }else{
                                    Toast.makeText(mContext,"Mobile authentication error. Please try again",Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case 2:
                                if(otpVerificationResult!=null){
                                    if(otpVerificationResult.trim().equalsIgnoreCase("true")){
                                        mStartIndex+=1;
                                        IndexWiseView(mStartIndex);
                                    }else{
                                        ShowOTPVerification();
                                        Toast.makeText(mContext,"Please provide valid OTP",Toast.LENGTH_SHORT).show();
                                    }
                                }else{
                                    ShowOTPVerification();
                                    Toast.makeText(mContext,"Please provide valid OTP",Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case 3:

                                if(mMissingItemDetailsList!=null){
                                    if(mMissingItemDetailsList.size()>0){
                                    /*mCustomLostItemAdapter = new CustomLostItemAdapter(EGDActivity.this,R.layout.spinner_list_item,  mMissingItemDetailsList);
                                    mSpinnerLostItemList.setAdapter(mCustomLostItemAdapter);*/
                                        mStartIndex+=1;
                                        IndexWiseView(mStartIndex);
                                    }else{

                                    }
                                }else{
                                    Toast.makeText(mContext,"Please press Next to try again",Toast.LENGTH_SHORT).show();
                                }
                                break;

                            case 4:
                                if(receiptNo.length()>0){
                                    mTextReceiptNo.setText("e-GD Number\n"+receiptNo);
                                    mStartIndex+=1;
                                    IndexWiseView(mStartIndex);
                                }
                                break;
                        }
                    }
                });
            }
        };
    }



    private void InitializeView(){

        mTextViewHeader=(TextView) findViewById(R.id.textViewheadertitle);

        mllayoutMobileAuthentication    =(LinearLayout) findViewById(R.id.linearLayoutMobileAuthentication);
        mllayoutComplainantDetails1     =(LinearLayout) findViewById(R.id.linearLayoutComplainnantDetails1);
        mllayoutComplainantDetails2     =(LinearLayout) findViewById(R.id.linearLayoutComplainnantDetails2);
        mllayoutComplainDetails         =(LinearLayout) findViewById(R.id.linearLayoutComplainDetails);
        mllayoutLostItemDetails         =(LinearLayout) findViewById(R.id.linearLayoutLostItemDetailsList);
        mllayoutPreview                 =(LinearLayout) findViewById(R.id.linearLayoutSummary);
        mllayoutFinalEgd                =(LinearLayout) findViewById(R.id.linearLayoutEGDFinalReceipt);

        mParentLayout = (LinearLayout) findViewById(R.id.linearLayoutParent);

        mParentLayoutItem= (LinearLayout) findViewById(R.id.linearLayoutParentItem);

        mButtonNext                     =(Button) findViewById(R.id.buttonNext);
        mButtonPrevious                 =(Button) findViewById(R.id.buttonPrevious);
        mButtonCancel                   =(Button) findViewById(R.id.buttonCancel);
        mButtonAdd                      =(Button) findViewById(R.id.buttonAdd);

        mEditTextMobile                 =(EditText) findViewById(R.id.egd_mobileNo);

        mllayoutMobileAuthentication.setVisibility(View.VISIBLE);
        mllayoutComplainantDetails1.setVisibility(View.GONE);
        mllayoutComplainantDetails2.setVisibility(View.GONE);
        mllayoutComplainDetails.setVisibility(View.GONE);
        mllayoutLostItemDetails.setVisibility(View.GONE);
        mllayoutPreview.setVisibility(View.GONE);
        mllayoutFinalEgd.setVisibility(View.GONE);
        mStartIndex=0;
        mButtonNext.setVisibility(View.VISIBLE);
        mButtonPrevious.setVisibility(View.GONE);
        mButtonCancel.setVisibility(View.VISIBLE);

        mButtonNext.setText("NEXT");

        mEditTextComplaintNamtName      =(EditText)findViewById(R.id.egd_ComplainantName);
        mSpinnerSex                     =(Spinner)findViewById(R.id.spinner_gender);
        mSpinnerSex.setAdapter(genderadapter);

        mEditTextComplaintMobileNo          =(EditText)findViewById(R.id.egd_ComplainantName);
        mEditTextComplaintMobileNoCopy      =(EditText)findViewById(R.id.egd_mobilenocopy);
        mEditTextComplaintAlterNateNo       =(EditText)findViewById(R.id.egd_altermobile);
        mEditTextAddress                    =(EditText)findViewById(R.id.egd_address);

        mEditTextComplaintType       =(EditText)findViewById(R.id.egd_complaintype);
        mEditTextPlaceofOccurence    =(EditText)findViewById(R.id.egd_placeofoccurrence);
        mEditTextDateofOccurrence    =(EditText)findViewById(R.id.egd_dateofoccurrence);
        mSpinnerThana                =(Spinner)findViewById(R.id.spinner_thana);

        mButtonLost         =(Button) findViewById(R.id.buttonItem);

        mEditTextComplaintType.setEnabled(false);
        mEditTextDateofOccurrence.setFocusable(false);
        mEditTextDateofOccurrence.setClickable(true);

        mSpinnerThana.setAdapter(mPoliceStationAdapter);

        mTextName                    =(TextView)findViewById(R.id.textViewName);
        mTextGender                  =(TextView)findViewById(R.id.textViewGender);
        mTextAddress                 =(TextView)findViewById(R.id.textViewAddress);
        mTextMobileNo                =(TextView)findViewById(R.id.textViewMobileNo);
        mTextAlternateMobileNo       =(TextView)findViewById(R.id.textViewAlternateMobileNo);
        mTextComplainType            =(TextView)findViewById(R.id.textViewcomplaintType);
        mTextPlaceofOccurr           =(TextView)findViewById(R.id.textViewplaceofoccur);
        mTextDateofOccurr            =(TextView)findViewById(R.id.textViewDateofOccurrence);
        mTextPoliceStation           =(TextView)findViewById(R.id.textViewPsName);

        mTextReceiptNo               =(TextView)findViewById(R.id.textViewegdReceiptNo);

    }

    @Override
    public void onClick(View v) {
        String tag = (String) v.getTag();
        if(tag.equalsIgnoreCase("date")){
            //Toast.makeText(mContext,"Date Clicked",Toast.LENGTH_SHORT).show();
            ShowDateDetails();
        }

    }

    private void SetEditTextText(String tag,String text){
        if(mEditTextList!=null){
            for (int count=0;count<mEditTextList.size();count++){
                if(mEditTextList.get(count).getTag().toString().equalsIgnoreCase(tag)){
                    mEditTextList.get(count).setText(text);
                    break;
                }
            }
        }
    }

    private void DrawLayout(){

        mParentLayout.removeAllViews();
        mEditTextList = new ArrayList<EditText>();

        LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        LinearLayout childlayout = new LinearLayout(this);
        childlayout.setLayoutParams(Params);
        childlayout.setPadding(0, 0, 0, 5);
        childlayout.setOrientation(LinearLayout.VERTICAL);


        LinearLayout.LayoutParams Params1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,100f);
        LinearLayout subchildlayout = new LinearLayout(this);
        subchildlayout.setLayoutParams(Params1);
        subchildlayout.setPadding(0, 0, 0, 5);
        subchildlayout.setOrientation(LinearLayout.HORIZONTAL);


        LinearLayout subchildlayout1 = new LinearLayout(this);
        subchildlayout1.setLayoutParams(Params1);
        subchildlayout1.setPadding(0, 0, 0, 0);
        subchildlayout1.setOrientation(LinearLayout.HORIZONTAL);

        childlayout.addView(NewtextView(mMissingItemDetails.getItemName(),false));

        subchildlayout1.addView(AddTextView(1,mMissingItemDetails.getField1Mandatory().trim(),mMissingItemDetails.getField1Show(),1,0));
        subchildlayout1.addView(AddTextView(1,mMissingItemDetails.getField2Mandatory().trim(),mMissingItemDetails.getField2Show(),1,0));
        subchildlayout1.addView(AddTextView(1,mMissingItemDetails.getField3Mandatory().trim(),mMissingItemDetails.getField3Show(),1,0));

        childlayout.addView(subchildlayout1);

        subchildlayout.addView(AddEditText(mRowID,mMissingItemDetails.getField1Type().trim(),mMissingItemDetails.getField1Show(),1));
        mRowID+=1;
        subchildlayout.addView(AddEditText(mRowID,mMissingItemDetails.getField2Type().trim(),mMissingItemDetails.getField2Show(),1));
        mRowID+=1;
        subchildlayout.addView(AddEditText(mRowID,mMissingItemDetails.getField3Type().trim(),mMissingItemDetails.getField3Show(),1));
        mRowID+=1;

        /*ImageView imageView = new ImageView(this);
        imageView.setTag("");
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT ,10f);
        imageView.setLayoutParams(params);
        imageView.setOnClickListener(this);
        imageView.setImageResource(R.drawable.cross);
        subchildlayout.addView(imageView);*/

        childlayout.addView(subchildlayout);
        mParentLayout.addView(childlayout);
        DrawItemSummary(0,0);
        //mCartMissingItemDetailsList.add(mMissingItemDetails);

    }


    private TextView NewtextView(String displayname,boolean isHeader) {
        LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
        TextView textView = new TextView(this);
        if(true==isHeader){
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        textView.setLayoutParams(Params);
        textView.setText(displayname);
        textView.setTextColor(Color.WHITE);
        textView.setTypeface(null, Typeface.BOLD);
        return textView;
    }

    private EditText AddEditText(int tag, String edtype,String hinttext, int param) {
        EditText editText = new EditText(this);
        if(param==0){
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,90f);
            editText.setLayoutParams(params);
        }else{
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,30f);
            editText.setLayoutParams(params);
        }
        if(edtype.equalsIgnoreCase("2")){
            editText.setInputType(InputType.TYPE_CLASS_NUMBER );
            editText.setTag(tag);
        }else if(edtype.equalsIgnoreCase("3")){
            editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS );
            editText.setFocusable(false);
            editText.setClickable(true);
            editText.setOnClickListener(this);
            editText.setTag("date");
        }else{
            editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS | InputType.TYPE_TEXT_FLAG_MULTI_LINE|InputType.TYPE_TEXT_FLAG_AUTO_CORRECT);
            editText.setTag(tag);
        }
        editText.setSingleLine(true);
        editText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        editText.setTextColor(Color.GRAY);
        editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        editText.setTypeface(null, Typeface.NORMAL);
        if(false==hinttext.contains("anyType")){
            editText.setHint(hinttext);
        }
        //editText.setText(value);
        editText.setBackgroundDrawable(getResources().getDrawable(R.drawable.spinner_bkg));
        editText.setHintTextColor(Color.LTGRAY);
        mEditTextList.add(editText);
        return editText;
    }



    private TextView AddTextView(int tag, String mandatory,String hinttext, int param,int iotype) {
        TextView textView = new TextView(this);
        textView.setTag(tag);
        if(param==0){
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,90f);
            textView.setLayoutParams(params);
        }else{
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,30f);
            textView.setLayoutParams(params);
        }

        if(1==iotype){//Output
            if(false==hinttext.contains("anyType")){
                textView.setText(hinttext);
                textView.setTypeface(null, Typeface.BOLD);
            }else{
                textView.setText("");
                textView.setTypeface(null, Typeface.BOLD);
            }

        }else{//input
            if(mandatory.equalsIgnoreCase("true")){
                textView.setText(" * "+hinttext);
            }else{
                if(false==hinttext.contains("anyType")) {
                    textView.setText(hinttext);
                }else{
                    textView.setText("");
                }
            }
            textView.setTypeface(null, Typeface.BOLD);
        }

        textView.setTextColor(Color.WHITE);

        return textView;
    }

    private void FillData(){
        mEGdComplaintDetails=new EGdComplaintDetails();
        mEGdComplaintDetails.setName(mComplaintName);
        mEGdComplaintDetails.setGender(mComplaintSex);
        mEGdComplaintDetails.setAddress(mAddress);
        mEGdComplaintDetails.setMobileNo(mMobieNo);

        mEGdComplaintDetails.setAlternateNo(mAlterNateMobileNo);
        mEGdComplaintDetails.setComplaintType(mComplaintType);
        mEGdComplaintDetails.setPlaceOfOccurence(mPlaceOfOccurrence);
        mEGdComplaintDetails.setDateOfOccurrence(mDateOfOccurrence);
        mEGdComplaintDetails.setPoliceStationName(mPoliceStationName);

    }

    private void DrawSummary(){

        if(mEGdComplaintDetails!=null){
            mTextName.setText(mEGdComplaintDetails.getName());
            mTextGender.setText(mEGdComplaintDetails.getGender());
            mTextAddress.setText(mEGdComplaintDetails.getAddress());
            mTextMobileNo.setText(mEGdComplaintDetails.getMobileNo());
            mTextAlternateMobileNo.setText(mEGdComplaintDetails.getAlternateNo());
            mTextComplainType.setText(mEGdComplaintDetails.getComplaintType());
            mTextPlaceofOccurr.setText(mEGdComplaintDetails.getPlaceOfOccurence());
            mTextDateofOccurr.setText(mEGdComplaintDetails.getDateOfOccurrence());
            mTextPoliceStation.setText(mEGdComplaintDetails.getPoliceStationName());
        }
    }

    private boolean FillItemData(){
        boolean isAllRight=true;

        if(mEditTextList.size()>0) {
            for(int count=0;count<mEditTextList.size();count+=3){
                EditText editText1=mEditTextList.get(count);
                EditText editText2=mEditTextList.get(count+1);
                EditText editText3=mEditTextList.get(count+2);
                boolean isok=SetItemDate(editText1.getText().toString().trim()
                        ,editText2.getText().toString().trim()
                        ,editText3.getText().toString().trim());
                if(false==isok){
                    isAllRight=false;
                    return isAllRight;
                }
            }
        }
        return isAllRight;
    }

    private boolean SetItemDate(String field1,String field2,String field3){
        boolean isOK=true;
        String mandatory1="";
        String mandatory2="";
        String mandatory3="";

        mandatory1=mMissingItemDetails.getField1Mandatory().trim();
        mandatory2=mMissingItemDetails.getField2Mandatory().trim();
        mandatory3=mMissingItemDetails.getField3Mandatory().trim();
        if(mandatory1.equalsIgnoreCase("true")){
            if(field1.length()<=0){
                isOK=false;
            }
        }
        if(mandatory2.equalsIgnoreCase("true")){
            if(field1.length()<=0){
                isOK=false;
            }
        }
        if(mandatory3.equalsIgnoreCase("true")){
            if(field1.length()<=0){
                isOK=false;
            }
        }

        if(true==isOK){
            MissingItemDetails obj=new MissingItemDetails();
            obj.setID(mMissingItemDetails.getID());
            obj.setOrder(mMissingItemDetails.getOrder());
            obj.setItemName(mMissingItemDetails.getItemName());

            obj.setField1Type(mMissingItemDetails.getField1Type());
            obj.setField1Mandatory(mandatory1);
            obj.setField1Show(mMissingItemDetails.getField1Show());

            obj.setField2Type(mMissingItemDetails.getField2Type());
            obj.setField2Mandatory(mandatory2);
            obj.setField2Show(mMissingItemDetails.getField2Show());

            obj.setField3Type(mMissingItemDetails.getField2Type());
            obj.setField3Mandatory(mandatory1);
            obj.setField3Show(mMissingItemDetails.getField3Show());

            obj.setField1Value(field1);
            obj.setField2Value(field2);
            obj.setField3Value(field3);

            mFinalMissingItemDetailsList.add(obj);
        }
        return isOK;
    }

    private void DrawItemSummary(int type,int showin){
        if(1==type){
            mParentLayout.removeAllViews();
        }
        if(1==showin){
            mParentLayoutItem.removeAllViews();
        }
        if(mFinalMissingItemDetailsList.size()>0){
            LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            for(int count=0;count<mFinalMissingItemDetailsList.size();count++){
                MissingItemDetails obj=mFinalMissingItemDetailsList.get(count);
                LinearLayout childlayout = new LinearLayout(this);
                childlayout.setLayoutParams(Params);
                childlayout.setPadding(2, 2, 2, 5);
                childlayout.setBackgroundColor(Color.parseColor("#130044"));
                childlayout.setOrientation(LinearLayout.VERTICAL);
                if(0==count){
                    childlayout.addView(NewtextView("Your Lost Item List",true));
                }

                LinearLayout.LayoutParams Params1 = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,100f);
                LinearLayout subchildlayout = new LinearLayout(this);
                subchildlayout.setLayoutParams(Params1);
                subchildlayout.setPadding(0, 0, 0, 5);
                subchildlayout.setOrientation(LinearLayout.HORIZONTAL);
                childlayout.addView(NewtextView(obj.getItemName(),false));
                subchildlayout.addView(AddTextView(1,obj.getField1Type().trim(),obj.getField1Show()+"\n"+obj.getField1Value(),1,1));
                subchildlayout.addView(AddTextView(1,obj.getField2Type().trim(),obj.getField2Show()+"\n"+obj.getField2Value(),1,1));
                subchildlayout.addView(AddTextView(1,obj.getField3Type().trim(),obj.getField3Show()+"\n"+obj.getField3Value(),1,1));
                childlayout.addView(subchildlayout);
                if(1==showin){
                    mParentLayoutItem.addView(childlayout);
                }else{
                    mParentLayout.addView(childlayout);
                }
            }
        }
    }

    private void IndexWiseView(int control){

        switch (control){
            case 0:

                mTextViewHeader.setText("Mobile Authentication");
                mllayoutMobileAuthentication.setVisibility(View.VISIBLE);
                mllayoutComplainantDetails1.setVisibility(View.GONE);
                mllayoutComplainantDetails2.setVisibility(View.GONE);
                mllayoutComplainDetails.setVisibility(View.GONE);
                mllayoutLostItemDetails.setVisibility(View.GONE);
                mllayoutPreview.setVisibility(View.GONE);
                mllayoutFinalEgd.setVisibility(View.GONE);

                mButtonNext.setVisibility(View.VISIBLE);
                mButtonPrevious.setVisibility(View.GONE);
                mButtonCancel.setVisibility(View.VISIBLE);
                mButtonNext.setText("NEXT");

                break;

            case 1:

                mTextViewHeader.setText("Complainant Details");
                mllayoutMobileAuthentication.setVisibility(View.GONE);
                mllayoutComplainantDetails1.setVisibility(View.VISIBLE);
                mllayoutComplainantDetails2.setVisibility(View.GONE);
                mllayoutComplainDetails.setVisibility(View.GONE);
                mllayoutLostItemDetails.setVisibility(View.GONE);
                mllayoutPreview.setVisibility(View.GONE);
                mllayoutFinalEgd.setVisibility(View.GONE);

                mButtonNext.setVisibility(View.VISIBLE);
                mButtonPrevious.setVisibility(View.GONE);
                mButtonCancel.setVisibility(View.VISIBLE);

                mButtonNext.setText("NEXT");

                break;
            case 2:

                mTextViewHeader.setText("Complainant Details");
                mllayoutMobileAuthentication.setVisibility(View.GONE);
                mllayoutComplainantDetails1.setVisibility(View.GONE);
                mllayoutComplainantDetails2.setVisibility(View.VISIBLE);
                mllayoutComplainDetails.setVisibility(View.GONE);
                mllayoutLostItemDetails.setVisibility(View.GONE);
                mllayoutPreview.setVisibility(View.GONE);
                mllayoutFinalEgd.setVisibility(View.GONE);

                mButtonNext.setVisibility(View.VISIBLE);
                mButtonPrevious.setVisibility(View.VISIBLE);
                mButtonCancel.setVisibility(View.VISIBLE);

                mEditTextComplaintMobileNoCopy.setText(mMobieNo);
                mEditTextComplaintMobileNoCopy.setEnabled(false);

                mButtonNext.setText("NEXT");

                break;
            case 3:

                mTextViewHeader.setText("Complain Details");
                mllayoutMobileAuthentication.setVisibility(View.GONE);
                mllayoutComplainantDetails1.setVisibility(View.GONE);
                mllayoutComplainantDetails2.setVisibility(View.GONE);
                mllayoutComplainDetails.setVisibility(View.VISIBLE);
                mllayoutLostItemDetails.setVisibility(View.GONE);
                mllayoutPreview.setVisibility(View.GONE);
                mllayoutFinalEgd.setVisibility(View.GONE);

                mButtonNext.setVisibility(View.VISIBLE);
                mButtonPrevious.setVisibility(View.VISIBLE);
                mButtonCancel.setVisibility(View.VISIBLE);

                mButtonNext.setText("NEXT");

                break;
            case 4:

                mTextViewHeader.setText("Lost Item Details");
                mllayoutMobileAuthentication.setVisibility(View.GONE);
                mllayoutComplainantDetails1.setVisibility(View.GONE);
                mllayoutComplainantDetails2.setVisibility(View.GONE);
                mllayoutComplainDetails.setVisibility(View.GONE);
                mllayoutLostItemDetails.setVisibility(View.VISIBLE);
                mllayoutPreview.setVisibility(View.GONE);
                mllayoutFinalEgd.setVisibility(View.GONE);

                mButtonNext.setVisibility(View.VISIBLE);
                mButtonPrevious.setVisibility(View.VISIBLE);
                mButtonCancel.setVisibility(View.VISIBLE);

                mButtonNext.setText("NEXT");
                mParentLayout.removeAllViews();
                mEditTextList =new ArrayList<EditText>();
                mFinalMissingItemDetailsList =new ArrayList<MissingItemDetails>();
                //mCartMissingItemDetailsList=new ArrayList<MissingItemDetails>();

                break;
            case 5:

                mTextViewHeader.setText("Complaint Summary");
                mllayoutMobileAuthentication.setVisibility(View.GONE);
                mllayoutComplainantDetails1.setVisibility(View.GONE);
                mllayoutComplainantDetails2.setVisibility(View.GONE);
                mllayoutComplainDetails.setVisibility(View.GONE);
                mllayoutLostItemDetails.setVisibility(View.GONE);
                mllayoutPreview.setVisibility(View.VISIBLE);
                mllayoutFinalEgd.setVisibility(View.GONE);

                mButtonNext.setVisibility(View.VISIBLE);
                mButtonNext.setText("SUBMIT");
                mButtonPrevious.setVisibility(View.VISIBLE);
                mButtonCancel.setVisibility(View.VISIBLE);

                mParentLayoutItem.removeAllViews();
                DrawItemSummary(0,1);
                DrawSummary();

                break;
            case 6:

                mTextViewHeader.setText("EGD Receipt Details");
                mllayoutMobileAuthentication.setVisibility(View.GONE);
                mllayoutComplainantDetails1.setVisibility(View.GONE);
                mllayoutComplainantDetails2.setVisibility(View.GONE);
                mllayoutComplainDetails.setVisibility(View.GONE);
                mllayoutLostItemDetails.setVisibility(View.GONE);
                mllayoutPreview.setVisibility(View.GONE);
                mllayoutFinalEgd.setVisibility(View.VISIBLE);


                mButtonNext.setVisibility(View.GONE);
                mButtonPrevious.setVisibility(View.GONE);
                mButtonCancel.setVisibility(View.VISIBLE);
                mButtonCancel.setText("CLOSE");


                break;

            default:

                mTextViewHeader.setText("Mobile Authentication");
                mllayoutMobileAuthentication.setVisibility(View.VISIBLE);
                mllayoutComplainantDetails1.setVisibility(View.GONE);
                mllayoutComplainantDetails2.setVisibility(View.GONE);
                mllayoutComplainDetails.setVisibility(View.GONE);
                mllayoutLostItemDetails.setVisibility(View.GONE);
                mllayoutPreview.setVisibility(View.GONE);
                mllayoutFinalEgd.setVisibility(View.GONE);
                mStartIndex=0;

                mButtonNext.setVisibility(View.VISIBLE);
                mButtonPrevious.setVisibility(View.GONE);
                mButtonCancel.setVisibility(View.VISIBLE);


                break;
        }

    }

    private void IndexWiseControl(int control){

        switch (control){
            case 0:
                mMobieNo=mEditTextMobile.getText().toString();
                if(mMobieNo.length()>0){
                    if(mMobieNo.length()==10){
                        FetchAndSaveData(1);
                    }else{
                        Toast.makeText(mContext,"Please provide 10 digit mobile no",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(mContext,"Please provide 10 digit mobile no",Toast.LENGTH_LONG).show();
                }
                break;
            case 1:

                mComplaintName=mEditTextComplaintNamtName.getText().toString();
                mComplaintSex=genderadapter.getItem(mSpinnerSex.getSelectedItemPosition());
                if(mComplaintName.length()>0 && mComplaintSex.length()>0){
                    mStartIndex+=1;
                    IndexWiseView(mStartIndex);
                }else{
                    if(mComplaintName.length()<=0){
                        Toast.makeText(mContext,"Please provide complainant name",Toast.LENGTH_LONG).show();
                    }
                    if(mComplaintSex.length()<=0){
                        Toast.makeText(mContext,"Please select sex",Toast.LENGTH_LONG).show();
                    }
                }

                break;
            case 2:
                mAlterNateMobileNo=mEditTextComplaintAlterNateNo.getText().toString().trim();
                mAddress=mEditTextAddress.getText().toString().trim();
                if(mAddress.length()>0){
                    if(mAlterNateMobileNo.length()>0){
                        if(10==mAlterNateMobileNo.length()|| 8 ==mAlterNateMobileNo.length()){
                            mStartIndex+=1;
                            IndexWiseView(mStartIndex);
                        }else{
                            Toast.makeText(mContext,"Please provide valid 8 or 10 gigit alternate no",Toast.LENGTH_LONG).show();
                        }
                    }else{
                        mStartIndex+=1;
                        IndexWiseView(mStartIndex);
                    }
                }else{
                    Toast.makeText(mContext,"Please provide address",Toast.LENGTH_LONG).show();
                }

                break;
            case 3:

                mComplaintType=mEditTextComplaintType.getText().toString();
                mPlaceOfOccurrence=mEditTextPlaceofOccurence.getText().toString();
                mPoliceStationName="";

                int position=mSpinnerThana.getSelectedItemPosition();
                mPoliceStationDetails=mPoliceStationDetailsList.get(position);
                if(mPoliceStationDetails!=null){
                    mPoliceStationName=mPoliceStationDetails.getStationName();
                    if(false==mPoliceStationName.equalsIgnoreCase("SELECT POLICE STATION")){
                        mPSID=mPoliceStationDetails.getCode();
                        if(mDateOfOccurrence.trim().length()>0){
                            if(mPlaceOfOccurrence.length()>0){

                                FetchAndSaveData(3);
                                /*mStartIndex+=1;
                                IndexWiseView(mStartIndex);*/
                            }else{
                                Toast.makeText(this,"Please enter the place of occurrence",Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(this,"Please select the date of occurrence",Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(this,"Please select police station",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(this,"Please select police station",Toast.LENGTH_LONG).show();
                }

                /*mAlterNateMobileNo=mEditTextComplaintAlterNateNo.getText().toString();
                mAddress=mEditTextAddress.getText().toString();
                if(mAddress.length()>0){
                    if(mAlterNateMobileNo.length()>0){
                        if(10==mAlterNateMobileNo.length()|| 8 ==mAlterNateMobileNo.length()){
                            mStartIndex+=1;
                            IndexWiseView(mStartIndex);
                        }else{
                            Toast.makeText(mContext,"Please provide valid 8 or 10 gigit alternate no",Toast.LENGTH_LONG).show();
                        }
                    }else{
                        mStartIndex+=1;
                        IndexWiseView(mStartIndex);
                    }
                }else{
                    Toast.makeText(mContext,"Please provide address",Toast.LENGTH_LONG).show();
                }*/
                break;
            case 4:
                if(mFinalMissingItemDetailsList.size()>0){
                    mStartIndex+=1;
                    FillData();
                    IndexWiseView(mStartIndex);
                }else{
                    Toast.makeText(mContext,"Please add at least one lost item",Toast.LENGTH_LONG).show();
                }
                break;
            case 5:
                //FetchAndSaveData(4);
                PermissionForImageCapture();
                break;
            case 6:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        ShowExitConformationDialog();
    }

    public void ShowExitConformationDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(EGDActivity.this);
        builder.setMessage("Do you want to exit")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setTitle("Alert");
        alert.show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (requestCode == REQUEST_WRITE_STORAGE) {
            requestCameraPermission();
        } else if (requestCode == REQUEST_CAMERA) {
            captureImage();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        finish();
    }







   /* private void Mobile(){
        //Initialize soap request + add parameters



        Runnable r =new Runnable() {
            @Override
            public void run() {


                    Integer       iRetVal                = 0;
                    SoapPrimitive SoapResult             = null;
                    SoapObject    SoapRequest            = null;
                    String        SOAP_ACTION            = "http://tempuri.org/SetMobileAuthentication";
                    String        OPERATION_NAME         = "SetMobileAuthentication";
                    String        WSDL_TARGET_NAMESPACE  = "http://tempuri.org/";
                    String        SOAP_ADDRESS           = "http://166.62.41.225:81/Service.asmx";

                    SoapRequest = new SoapObject(WSDL_TARGET_NAMESPACE,OPERATION_NAME);

                    PropertyInfo propertyInfo = new PropertyInfo();
                    propertyInfo.setName("MobileNo");
                    propertyInfo.setValue("9038244639");
                    propertyInfo.setType(String.class);
                    SoapRequest.addProperty(propertyInfo);

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.implicitTypes = true;
                    envelope.setOutputSoapObject(SoapRequest);

                    HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                    httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding = \"utf-8\"?>");
                    httpTransport.debug = true;

                    try
                    {
                        // Call remote web service.
                        httpTransport.call(SOAP_ACTION,envelope);
                        // Receive response from remote web service.
                        SoapResult = (SoapPrimitive)envelope.getResponse();

                        // Check if response was received properly.
                        if( null != SoapResult ) {
                            // Get return value from service.
                            String strRetVal = SoapResult.toString();
                            // Check return value from service.
                            if(strRetVal.equals("true")) {
                                iRetVal = 1;
                            }
                            else{
                                iRetVal = 0;
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        iRetVal = 0;
                        ex.printStackTrace();
                    }
            }
        };

        Thread t=new Thread(r);
        t.start();



        //Use this to add parameters
        //request.addProperty("Parameter","Value");

        //Declare the version of the SOAP request


        // Get the SoapResult from the envelope body.


    }*/


    public void FetchAndSaveData(final int whattodo) {
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage("Please wait..");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        new Thread() {
            public void run() {
                switch (whattodo) {
                    case 1:
                        //GENERATE OTP
                        int result=SetmobileAuthentication();
                        if(1==result){
                            mobileAuthenticationResult="true";
                        }else{
                            mobileAuthenticationResult="false";
                        }

                        break;
                    case 2:
                        //VALIDATEOTP
                        int result1=GetmobileAuthentication();
                        if(1==result1){
                            otpVerificationResult="true";
                        }else{
                            otpVerificationResult="false";
                        }
                        break;
                    case 3:
                        //GETCOMPLAINTITEM
                        int result3=GetComplaintListItem();
                        if(1==result3){
                            otpVerificationResult="true";
                        }else{
                            otpVerificationResult="false";
                        }
                        break;

                    case 4:
                        //SETFILECOMPLAINT
                        int result4=SubmitUserInfoDatatoCloud();
                        if(1==result4){
                            for(int count=0;count<mFinalMissingItemDetailsList.size();count++){
                                String mobile=mEGdComplaintDetails.getMobileNo();
                                String whatlost=mFinalMissingItemDetailsList.get(count).getID();
                                String field1=mFinalMissingItemDetailsList.get(count).getField1Value();
                                String field2=mFinalMissingItemDetailsList.get(count).getField2Value();
                                String field3=mFinalMissingItemDetailsList.get(count).getField3Value();


                                int result5=SubmitLostItemDatatoCloud(setAuthenticationresult,mobile,whatlost,field1,field2,field3);

                            }
                            otpVerificationResult="true";
                            if(true==isImagetake){
                                if(mUserBitmap!=null){
                                    SetFileComplaintImage(setAuthenticationresult);
                                }
                            }
                        }else{
                            otpVerificationResult="false";
                        }
                        break;

                    case 5:
                        //SETFILECOMPLAINTITEM
                        break;

                    case 6:
                        //SETFILECOMPLAINTIMAGE
                        break;
                }
                Message msg = mReportHandler.obtainMessage();
                Bundle b = new Bundle();
                b.putInt("JOBDONE", whattodo);
                msg.setData(b);
                mReportHandler.sendMessage(msg);
            }
        }.start();
    }

    private void ShowOTPVerification() {
        View view = EGDActivity.this.getLayoutInflater().inflate(R.layout.dialog_otp_verification, null);
        final EditText edTextOTP = (EditText) view.findViewById(R.id.ed_otp);
        android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(EGDActivity.this);
        dialog.setView(view);
        dialog.setCancelable(true);
        dialog.setTitle("Verification");
        dialog.setPositiveButton("VALIDATE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                /*String entered_otp=edTextOTP.getText().toString().trim();
                if(entered_otp.equalsIgnoreCase(otp_no)){
                    dialog.dismiss();
                }else{
                    Toast.makeText(context,"Please provide valid OTP",Toast.LENGTH_SHORT).show();
                }*/
            }
        });
        dialog.setNegativeButton("RE SEND", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                /*String otp=GenerateOTP();
                SendOTP(otp,"91"+mobile_no);
                dialog.dismiss();*/
            }
        });


        final android.support.v7.app.AlertDialog alert = dialog.create();
        alert.show();
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mOtpCode=edTextOTP.getText().toString().trim();
                FetchAndSaveData(2);
                alert.dismiss();
            }
        });

        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FetchAndSaveData(1);
                alert.dismiss();
            }
        });
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.parseColor("#47A097"));
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#47A097"));
    }


    private int SetmobileAuthentication(){
        Integer       iRetVal                = 0;
        SoapPrimitive SoapResult             = null;
        SoapObject    SoapRequest            = null;
        String        SOAP_ACTION            = "http://tempuri.org/SetMobileAuthentication";
        String        OPERATION_NAME         = "SetMobileAuthentication";
        String        WSDL_TARGET_NAMESPACE  = "http://tempuri.org/";
        String        SOAP_ADDRESS           = "http://166.62.41.225:81/Service.asmx";

        SoapRequest = new SoapObject(WSDL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo propertyInfo = new PropertyInfo();
        propertyInfo.setName("MobileNo");
        propertyInfo.setValue(mMobieNo);
        propertyInfo.setType(String.class);
        SoapRequest.addProperty(propertyInfo);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.implicitTypes = true;
        envelope.setOutputSoapObject(SoapRequest);

        HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
        httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding = \"utf-8\"?>");
        httpTransport.debug = true;

        try
        {
            // Call remote web service.
            httpTransport.call(SOAP_ACTION,envelope);
            // Receive response from remote web service.
            SoapResult = (SoapPrimitive)envelope.getResponse();

            // Check if response was received properly.
            if( null != SoapResult ) {
                // Get return value from service.
                mobileAuthenticationResult = SoapResult.toString();
                // Check return value from service.
                if(mobileAuthenticationResult.equals("true")) {
                    iRetVal = 1;
                }
                else{
                    iRetVal = 0;
                }
            }
        }
        catch(Exception ex)
        {
            iRetVal = 0;
            ex.printStackTrace();
        }
        return iRetVal;

    }

    private int GetmobileAuthentication(){
        Integer       iRetVal                = 0;
        SoapPrimitive SoapResult             = null;
        SoapObject    SoapRequest            = null;
        String        SOAP_ACTION            = "http://tempuri.org/GetMobileAuthentication";
        String        OPERATION_NAME         = "GetMobileAuthentication";
        String        WSDL_TARGET_NAMESPACE  = "http://tempuri.org/";
        String        SOAP_ADDRESS           = "http://166.62.41.225:81/Service.asmx";

        SoapRequest = new SoapObject(WSDL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo propertyInfo = new PropertyInfo();
        propertyInfo.setName("MobileNo");
        propertyInfo.setValue(mMobieNo);
        propertyInfo.setType(String.class);
        SoapRequest.addProperty(propertyInfo);

        PropertyInfo propertyInfo2 = new PropertyInfo();
        propertyInfo2.setName("OTP");
        propertyInfo2.setValue(mOtpCode);
        propertyInfo2.setType(String.class);
        SoapRequest.addProperty(propertyInfo2);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.implicitTypes = true;
        envelope.setOutputSoapObject(SoapRequest);

        HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
        httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding = \"utf-8\"?>");
        httpTransport.debug = true;

        try
        {
            // Call remote web service.
            httpTransport.call(SOAP_ACTION,envelope);
            // Receive response from remote web service.
            SoapResult = (SoapPrimitive)envelope.getResponse();

            // Check if response was received properly.
            if( null != SoapResult ) {
                // Get return value from service.
                mobileAuthenticationResult = SoapResult.toString();
                // Check return value from service.
                if(mobileAuthenticationResult.equals("true")) {
                    iRetVal = 1;
                }
                else{
                    iRetVal = 0;
                }
            }
        }
        catch(Exception ex)
        {
            iRetVal = 0;
            ex.printStackTrace();
        }
        return iRetVal;

    }

    private int SubmitUserInfoDatatoCloud(){
        Integer       iRetVal                = 0;
        SoapPrimitive SoapResult             = null;
        SoapObject    SoapRequest            = null;
        String        SOAP_ACTION            = "http://tempuri.org/SetFileComplaint2";
        String        OPERATION_NAME         = "SetFileComplaint2";
        String        WSDL_TARGET_NAMESPACE  = "http://tempuri.org/";
        String        SOAP_ADDRESS           = "http://166.62.41.225:81/Service.asmx";

        SoapRequest = new SoapObject(WSDL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo propertyInfo1 = new PropertyInfo();
        propertyInfo1.setName("MobileNo");
        propertyInfo1.setValue(mEGdComplaintDetails.getMobileNo());
        propertyInfo1.setType(String.class);
        SoapRequest.addProperty(propertyInfo1);

        PropertyInfo propertyInfo2 = new PropertyInfo();
        propertyInfo2.setName("AlternativeMobileNo");
        propertyInfo2.setValue(mEGdComplaintDetails.getAlternateNo());
        propertyInfo2.setType(String.class);
        SoapRequest.addProperty(propertyInfo2);

        PropertyInfo propertyInfo3 = new PropertyInfo();
        propertyInfo3.setName("DateofBirth");
        propertyInfo3.setValue("");
        propertyInfo3.setType(Date.class);
        SoapRequest.addProperty(propertyInfo3);

        PropertyInfo propertyInfo4 = new PropertyInfo();
        propertyInfo4.setName("Name");
        propertyInfo4.setValue(mEGdComplaintDetails.getName());
        propertyInfo4.setType(String.class);
        SoapRequest.addProperty(propertyInfo4);

        PropertyInfo propertyInfo5 = new PropertyInfo();
        propertyInfo5.setName("Gender");
        propertyInfo5.setValue(mEGdComplaintDetails.getGender());
        propertyInfo5.setType(String.class);
        SoapRequest.addProperty(propertyInfo5);

        PropertyInfo propertyInfo6 = new PropertyInfo();
        propertyInfo6.setName("Address");
        propertyInfo6.setValue(mEGdComplaintDetails.getAddress());
        propertyInfo6.setType(String.class);
        SoapRequest.addProperty(propertyInfo6);

        PropertyInfo propertyInfo7 = new PropertyInfo();
        propertyInfo7.setName("ComplaintType");
        propertyInfo7.setValue(mEGdComplaintDetails.getComplaintType());
        propertyInfo7.setType(String.class);
        SoapRequest.addProperty(propertyInfo7);

        PropertyInfo propertyInfo8 = new PropertyInfo();
        propertyInfo8.setName("PlaceOfOccurance");
        propertyInfo8.setValue(mEGdComplaintDetails.getPlaceOfOccurence());
        propertyInfo8.setType(String.class);
        SoapRequest.addProperty(propertyInfo8);

        PropertyInfo propertyInfo9 = new PropertyInfo();
        propertyInfo9.setName("DateOfOccurance");
        propertyInfo9.setValue(mEGdComplaintDetails.getDateOfOccurrence());
        propertyInfo9.setType(Date.class);
        SoapRequest.addProperty(propertyInfo9);

        PropertyInfo propertyInfo10 = new PropertyInfo();
        propertyInfo10.setName("PSName");
        propertyInfo10.setValue(mEGdComplaintDetails.getPoliceStationName());
        propertyInfo10.setType(String.class);
        SoapRequest.addProperty(propertyInfo10);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.implicitTypes = true;
        envelope.setOutputSoapObject(SoapRequest);

        HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
        httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding = \"utf-8\"?>");
        httpTransport.debug = true;

        try
        {
            // Call remote web service.
            httpTransport.call(SOAP_ACTION,envelope);
            // Receive response from remote web service.
            SoapResult = (SoapPrimitive)envelope.getResponse();

            // Check if response was received properly.
            if( null != SoapResult ) {
                // Get return value from service.
                String strRetVal = SoapResult.toString();
                setAuthenticationresult=strRetVal;
                // Check return value from service.
                if(false==strRetVal.equalsIgnoreCase("0")) {
                    iRetVal = 1;
                }
                else{
                    iRetVal = 0;
                }
            }
        }
        catch(Exception ex)
        {
            iRetVal = 0;
            ex.printStackTrace();
        }

        return iRetVal;
    }

    private int SubmitLostItemDatatoCloud(String RowID,String MobileNo,String WhatLost,String Field1,String Field2,String Field3){
        Integer       iRetVal                = 0;
        SoapPrimitive SoapResult             = null;
        SoapObject    SoapRequest            = null;
        String        SOAP_ACTION            = "http://tempuri.org/SetFileComplaintItems";
        String        OPERATION_NAME         = "SetFileComplaintItems";
        String        WSDL_TARGET_NAMESPACE  = "http://tempuri.org/";
        String        SOAP_ADDRESS           = "http://166.62.41.225:81/Service.asmx";

        SoapRequest = new SoapObject(WSDL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo propertyInfo1 = new PropertyInfo();
        propertyInfo1.setName("RowID");
        propertyInfo1.setValue(RowID);
        propertyInfo1.setType(Integer.class);
        SoapRequest.addProperty(propertyInfo1);

        PropertyInfo propertyInfo2 = new PropertyInfo();
        propertyInfo2.setName("MobileNo");
        propertyInfo2.setValue(MobileNo);
        propertyInfo2.setType(String.class);
        SoapRequest.addProperty(propertyInfo2);

        PropertyInfo propertyInfo3 = new PropertyInfo();
        propertyInfo3.setName("WhatLost");
        propertyInfo3.setValue(WhatLost);
        propertyInfo3.setType(String.class);
        SoapRequest.addProperty(propertyInfo3);

        PropertyInfo propertyInfo4 = new PropertyInfo();
        propertyInfo4.setName("Field1");
        propertyInfo4.setValue(Field1);
        propertyInfo4.setType(String.class);
        SoapRequest.addProperty(propertyInfo4);

        PropertyInfo propertyInfo5 = new PropertyInfo();
        propertyInfo5.setName("Field2");
        propertyInfo5.setValue(Field2);
        propertyInfo5.setType(String.class);
        SoapRequest.addProperty(propertyInfo5);

        PropertyInfo propertyInfo6 = new PropertyInfo();
        propertyInfo6.setName("Field3");
        propertyInfo6.setValue(Field3);
        propertyInfo6.setType(String.class);
        SoapRequest.addProperty(propertyInfo6);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.implicitTypes = true;
        envelope.setOutputSoapObject(SoapRequest);

        HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
        httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding = \"utf-8\"?>");
        httpTransport.debug = true;

        try
        {
            // Call remote web service.
            httpTransport.call(SOAP_ACTION,envelope);
            // Receive response from remote web service.
            SoapResult = (SoapPrimitive)envelope.getResponse();

            // Check if response was received properly.
            if( null != SoapResult ) {
                // Get return value from service.
                String strRetVal = SoapResult.toString();
                receiptNo=strRetVal;
                // Check return value from service.
                if(false==strRetVal.equalsIgnoreCase("0")) {
                    iRetVal = 1;
                }
                else{
                    iRetVal = 0;
                }
            }
        }
        catch(Exception ex)
        {
            iRetVal = 0;
            ex.printStackTrace();
        }

        return iRetVal;
    }

    private int GetComplaintListItem(){
        Integer       iRetVal                = 0;
        SoapObject    SoapResult            = null;
        SoapObject    SoapRequest            = null;
        String        SOAP_ACTION            = "http://tempuri.org/GetComplaintItem";
        String        OPERATION_NAME         = "GetComplaintItem";
        String        WSDL_TARGET_NAMESPACE  = "http://tempuri.org/";
        String        SOAP_ADDRESS           = "http://166.62.41.225:81/Service.asmx";

        SoapRequest = new SoapObject(WSDL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo propertyInfo = new PropertyInfo();
        propertyInfo.setName("ComplaintType");
        propertyInfo.setValue("Lost");
        propertyInfo.setType(String.class);
        SoapRequest.addProperty(propertyInfo);

        /*PropertyInfo propertyInfo2 = new PropertyInfo();
        propertyInfo2.setName("OTP");
        propertyInfo2.setValue(mOtpCode);
        propertyInfo2.setType(String.class);
        SoapRequest.addProperty(propertyInfo2);*/

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.implicitTypes = true;
        envelope.setOutputSoapObject(SoapRequest);

        HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
        httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding = \"utf-8\"?>");
        httpTransport.debug = true;

        try
        {
            // Call remote web service.
            httpTransport.call(SOAP_ACTION,envelope);
            // Receive response from remote web service.
            SoapResult = (SoapObject) envelope.getResponse();

            // Check if response was received properly.

            if( null != SoapResult ) {
                mMissingItemDetailsList=new ArrayList<MissingItemDetails>();
                // Get return value from service.
                for(int count=0;count<SoapResult.getPropertyCount();count++){
                    SoapObject so=(SoapObject)SoapResult.getProperty(count);
                    MissingItemDetails obj=new MissingItemDetails();
                    obj.setID(so.getPropertyAsString(0));
                    obj.setOrder(so.getPropertyAsString(1));
                    obj.setItemName(so.getPropertyAsString(2));

                    obj.setField1Type(so.getPropertyAsString(3));
                    obj.setField1Mandatory(so.getPropertyAsString(4));
                    obj.setField1Show(so.getPropertyAsString(5));

                    obj.setField2Type(so.getPropertyAsString(6));
                    obj.setField2Mandatory(so.getPropertyAsString(7));
                    obj.setField2Show(so.getPropertyAsString(8));

                    obj.setField3Type(so.getPropertyAsString(9));
                    obj.setField3Mandatory(so.getPropertyAsString(10));
                    obj.setField3Show(so.getPropertyAsString(11));

                    obj.setType(so.getPropertyAsString(12));

                    mMissingItemDetailsList.add(obj);
                    obj=null;
                }
                // Check return value from service.
                if(mMissingItemDetailsList.size()>0) {
                    iRetVal = 1;
                }
                else{
                    iRetVal = 0;
                }
            }
        }
        catch(Exception ex)
        {
            iRetVal = 0;
            ex.printStackTrace();
        }
        return iRetVal;

    }


    private int SetFileComplaintImage(String RowID){

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        mUserBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.NO_WRAP);

        Integer       iRetVal                = 0;
        SoapPrimitive SoapResult             = null;
        SoapObject    SoapRequest            = null;
        String        SOAP_ACTION            = "http://tempuri.org/SetFileComplaintImage";
        String        OPERATION_NAME         = "SetFileComplaintImage";
        String        WSDL_TARGET_NAMESPACE  = "http://tempuri.org/";
        String        SOAP_ADDRESS           = "http://166.62.41.225:81/Service.asmx";

        SoapRequest = new SoapObject(WSDL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo propertyInfo1 = new PropertyInfo();
        propertyInfo1.setName("RowID");
        propertyInfo1.setValue(RowID);
        propertyInfo1.setType(Integer.class);
        SoapRequest.addProperty(propertyInfo1);




        PropertyInfo propertyInfo2 = new PropertyInfo();
        propertyInfo2.setName("Image1");
        propertyInfo2.setValue(encodedImage);
        propertyInfo2.setType(String.class);
        SoapRequest.addProperty(propertyInfo2);

        PropertyInfo propertyInfo3 = new PropertyInfo();
        propertyInfo3.setName("Image2");
        propertyInfo3.setValue(encodedImage);
        propertyInfo3.setType(String.class);
        SoapRequest.addProperty(propertyInfo3);

        PropertyInfo propertyInfo4 = new PropertyInfo();
        propertyInfo4.setName("Image3");
        propertyInfo4.setValue(encodedImage);
        propertyInfo4.setType(String.class);
        SoapRequest.addProperty(propertyInfo4);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        //envelope.implicitTypes = true;
        envelope.setOutputSoapObject(SoapRequest);

        HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
        httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding = \"utf-8\"?>");
        httpTransport.debug = true;

        try
        {
            // Call remote web service.
            httpTransport.call(SOAP_ACTION,envelope);
            // Receive response from remote web service.

            String response=httpTransport.responseDump;

            SoapResult = (SoapPrimitive)envelope.getResponse();

            // Check if response was received properly.
            if( null != SoapResult ) {
                // Get return value from service.
                String imageresult = SoapResult.toString();
                // Check return value from service.
                if(imageresult.equals("true")) {
                     iRetVal = 1;
                }
                else{
                    iRetVal = 0;
                }
            }
        }
        catch(Exception ex)
        {
            iRetVal = 0;
            ex.printStackTrace();
        }
        return iRetVal;

    }


    private void BuildPoliceStationList() {
        mPoliceStationDetailsList = new ArrayList<PoliceStationDetails>();
        String[] section = getResources().getStringArray(R.array.police_station_code);
        if (section != null && section.length > 0) {
            for (String s : section) {
                if (s.contains("#")) {
                    String[] splitsection = s.split("#");
                    if (splitsection.length == 2) {
                        PoliceStationDetails obj = new PoliceStationDetails();
                        obj.setStationName(splitsection[0].trim());
                        obj.setCode(splitsection[1].trim());
                        mPoliceStationDetailsList.add(obj);
                        obj = null;
                    }
                }
            }
        }
    }

    public void ChooseDateDialog(){
        dialogCaldroidFragment = new CaldroidFragment();
        dialogCaldroidFragment.setCaldroidListener(listener);
        final String dialogTag = "CALDROID_DIALOG_FRAGMENT";
        Bundle bundle = new Bundle();
        bundle.putString(CaldroidFragment.DIALOG_TITLE,"Select a date");
        dialogCaldroidFragment.setArguments(bundle);
        dialogCaldroidFragment.setMaxDate(new Date());
        dialogCaldroidFragment.show(this.getSupportFragmentManager(),dialogTag);
    }

    public void ShowLostItemDetails() {
        mCustomLostItemAdapter = new CustomLostItemAdapter(EGDActivity.this,R.layout.spinner_list_item,  mMissingItemDetailsList);
        final Dialog mDialogCustomer = new Dialog(mContext, R.style.PauseDialog);
        mDialogCustomer.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogCustomer.setContentView(R.layout.dialog_lost_itemlist);
        mDialogCustomer.setCancelable(true);
        ListView dialogList = (ListView) mDialogCustomer.findViewById(R.id.LostItemList);
        dialogList.setAdapter(mCustomLostItemAdapter);
        dialogList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mMissingItemDetails=mMissingItemDetailsList.get(position);
                isItemSelected=true;
                mButtonAdd.setVisibility(View.VISIBLE);
                DrawLayout();
                mDialogCustomer.cancel();
            }
        });
        mDialogCustomer.show();
    }

    private void FillYear(){
        mYearArray=new String[100];
        for(int count=0;count<100;count++){
            mYearArray[count]=(2017-count)+"";
        }
    }
    String[] mYearArray = null;
    String dateofbirth="";

    public void ShowDateDetails() {
        dateofbirth="";
        FillYear();
        final String[] mDayArray = getResources().getStringArray(R.array.day_array);
        final String[] mMonthArray = getResources().getStringArray(R.array.month_array);

        //mCustomLostItemAdapter = new CustomLostItemAdapter(EGDActivity.this,R.layout.spinner_list_item,  mMissingItemDetailsList);
        final ArrayAdapter mDayAdapter = new ArrayAdapter<String>(this,R.layout.spinner_list_item, R.id.title, mDayArray);
        final ArrayAdapter mMonthAdapter = new ArrayAdapter<String>(this,R.layout.spinner_list_item, R.id.title, mMonthArray);
        final ArrayAdapter mYearAdapter = new ArrayAdapter<String>(this,R.layout.spinner_list_item, R.id.title, mYearArray);



        final Dialog mDialogCustomer = new Dialog(mContext, R.style.PauseDialog);
        mDialogCustomer.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogCustomer.setContentView(R.layout.date_dialog);
        mDialogCustomer.setCancelable(true);

        final Spinner sday = (Spinner) mDialogCustomer.findViewById(R.id.spinner_day);
        final Spinner smonth = (Spinner) mDialogCustomer.findViewById(R.id.spinner_month);
        final Spinner syear = (Spinner) mDialogCustomer.findViewById(R.id.spinner_year);

        sday.setAdapter(mDayAdapter);
        smonth.setAdapter(mMonthAdapter);
        syear.setAdapter(mYearAdapter);
        Button submit = (Button) mDialogCustomer.findViewById(R.id.buttonDone);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int iday = sday.getSelectedItemPosition();
                int imonth = smonth.getSelectedItemPosition();
                int iyear=syear.getSelectedItemPosition();

                String sday     =mDayArray[iday];
                String smonth   =mMonthArray[imonth];
                String syear    =mYearArray[iyear];

                String monthnumber=MonthToNumber(smonth);
                boolean isOk=DateValidation(sday,monthnumber,iyear,smonth);
                if(true==isOk){
                    dateofbirth=sday+"-"+smonth+"-"+syear;
                    SetEditTextText("date",dateofbirth);
                    mDialogCustomer.cancel();
                }else{
                    Toast.makeText(mContext,errorToast,Toast.LENGTH_SHORT).show();
                }
            }
        });
        mDialogCustomer.show();
    }

    private String MonthToNumber(String month){
        String number="00";

        switch (month){
            case "Jan":
                number="01";
                break;
            case "Feb":
                number="02";
                break;
            case "Mar":
                number="03";
                break;
            case "Apr":
                number="04";
                break;
            case "May":
                number="05";
                break;
            case "Jun":
                number="06";
                break;
            case "Jul":
                number="07";
                break;
            case "Aug":
                number="08";
                break;
            case "Sept":
                number="09";
                break;
            case "Oct":
                number="10";
                break;
            case "Nov":
                number="11";
                break;
            case "Dec":
                number="12";
                break;

        }

        return  number;
    }

    private String errorToast="";

    private boolean DateValidation(String day,String month,int year,String months){
        boolean isOk=true;
        errorToast="";
        if (day.equals("31") &&
                (month.equals("4") || month .equals("6") || month.equals("9") ||
                        month.equals("11") || month.equals("04") || month .equals("06") ||
                        month.equals("09"))) {
            isOk= false; // only 1,3,5,7,8,10,12 has 31 days
            errorToast+=months+" don't have 31 day";
        }else if (month.equals("2") || month.equals("02")) {
            //leap year
            if(year % 4==0){
                if(Integer.parseInt(day)>29){
                    isOk= false;
                    errorToast+="Selected year is leap year february month should not greater than 29 days";
                }
                else{
                    isOk= true;
                }
            }else{
                if(Integer.parseInt(day)>28){
                    isOk= false;
                    errorToast+="Selected year is not leap year february month should not greater than 28 days";
                }
                else{
                    isOk= true;
                }
            }
        } else if(Integer.parseInt(day)>31 || Integer.parseInt(day)< 1){
            isOk = false;
        } else if(Integer.parseInt(month)>12){
            isOk = false;
        } else{
            isOk = true;
        }
        return isOk;
    }

    public void PermissionForImageCapture() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Do you want to take picture?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                        if (ActivityCompat.checkSelfPermission(EGDActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestWriteStoragePermission();
                            return;
                        }
                        if (ActivityCompat.checkSelfPermission(EGDActivity.this, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestCameraPermission();
                        } else {
                            captureImage();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        isImagetake=false;
                        FetchAndSaveData(4);
                        dialog.dismiss();

                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Information");
        alert.show();
    }


    private void requestWriteStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        }
    }


    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA);

        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        }
    }

    File imageFile;

    private void captureImage() {
        Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String image_name = "image_egd.png";
        File imagesFolder = FileHelper.getInstance().getImageDir();
        imageFile = new File(imagesFolder, image_name);
        if (imageFile.exists()) {
            imageFile.delete();
        }
        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
        startActivityForResult(imageIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
                if (resultCode == -1) {
                    FileHelper.getInstance().compressImage(imageFile.getPath(), imageFile.getPath(),"");
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    mUserBitmap = BitmapFactory.decodeFile(imageFile.getPath(),options);
                    isImagetake=true;
                    FetchAndSaveData(4);
                } else if (resultCode == 0) {
                    isImagetake=false;
                    FetchAndSaveData(4);
                    Toast.makeText(mContext,"User cancelled image capture", Toast.LENGTH_SHORT).show();
                } else {
                    isImagetake=false;
                    FetchAndSaveData(4);
                    Toast.makeText(mContext,"Sorry! Failed to capture image", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
