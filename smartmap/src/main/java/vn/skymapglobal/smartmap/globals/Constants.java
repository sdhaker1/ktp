package vn.skymapglobal.smartmap.globals;

/**
 * Created by thaibui on 10/5/16.
 */

public class Constants {
    public static final String HOT_SPOTS_METHOD = "hotspots";
    public static final String LOGIN_METHOD = "authenticate";
    public static final String REGISTER_METHOD = "register";
    public static final String REPORT_METHOD = "hotspotreports";
    public static final String GET_ADDRESS_METHOD = "address";

    public static final String GET_POLYGON = "POLYGON";

    public static final String PREFERENCE_USER = "userId";
    public static final String PREFERENCE_AUTH = "isAuthenticate";
    public static final String PREFERENCE_FILTER = "currentFilter";
}
