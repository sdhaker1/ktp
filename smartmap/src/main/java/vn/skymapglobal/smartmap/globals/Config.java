package vn.skymapglobal.smartmap.globals;

/**
 * Created by thaibui on 10/5/16.
 */

public class Config {
    public static final String SERVICE_URL = "http://smartmap.skymapglobal.vn/api/v1.0/";
    public static final String GET_POLYGON_URL = "http://nominatim.openstreetmap.org/search?countrycodes=%s&state=%s&format=json&addressdetails=1&polygon_geojson=0";
    public static final String GET_POLYGON_URL_2 = "http://nominatim.openstreetmap.org/search?q=%s&countrycodes=%s&format=json&addressdetails=1&polygon_geojson=0";

}
