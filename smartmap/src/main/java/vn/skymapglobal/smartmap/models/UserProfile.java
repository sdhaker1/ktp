package vn.skymapglobal.smartmap.models;

/**
 * Created by thaibui on 10/5/16.
 */

public class UserProfile {
    public int id;
    public String name;
    public String email;
    public String mobile;
    public String created_at;
    public String updated_at;
}
