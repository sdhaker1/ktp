package vn.skymapglobal.smartmap.models;

/**
 * Created by thaibui on 10/5/16.
 */

public class State {
    public int id;
    public int country_id;
    public String code;
    public String name;
    public double latitude;
    public double longitude;
    public int zoom_level;
}
