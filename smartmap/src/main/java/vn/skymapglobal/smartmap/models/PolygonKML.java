package vn.skymapglobal.smartmap.models;

/**
 * Created by thaibui on 10/6/16.
 */

public class PolygonKML {
    public String place_id;
    public String[] boundingbox;
    public Object geojson;
}
