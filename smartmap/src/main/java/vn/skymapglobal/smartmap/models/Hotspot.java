package vn.skymapglobal.smartmap.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by thaibui on 10/6/16.
 */

public class Hotspot implements Parcelable{

    public int id;
    public String source;
    public double longitude;
    public double latitude;
    public long acq_date;
    public long acq_time;
    public String acquired;
    public String satellite;
    public int confidence;
    public String country;
    public String state;
    public String note;

    protected Hotspot(Parcel in) {
        id = in.readInt();
        source = in.readString();
        longitude = in.readDouble();
        latitude = in.readDouble();
        acq_date = in.readLong();
        acq_time = in.readLong();
        acquired = in.readString();
        satellite = in.readString();
        confidence = in.readInt();
        country = in.readString();
        state = in.readString();
        note = in.readString();
    }

    public static final Creator<Hotspot> CREATOR = new Creator<Hotspot>() {
        @Override
        public Hotspot createFromParcel(Parcel in) {
            return new Hotspot(in);
        }

        @Override
        public Hotspot[] newArray(int size) {
            return new Hotspot[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeInt(id);
        parcel.writeString(source);
        parcel.writeDouble(longitude);
        parcel.writeDouble(latitude);
        parcel.writeLong(acq_date);
        parcel.writeLong(acq_time);
        parcel.writeString(acquired);
        parcel.writeString(satellite);
        parcel.writeInt(confidence);
        parcel.writeString(country);
        parcel.writeString(state);
        parcel.writeString(note);
    }


//    "id": 341920
//            "source": "VIIRS"
//            "longitude": 26.78419
//            "latitude": 94.67185
//            "acq_date": 20161003
//            "acq_time": 1940
//            "acquired": "2016-10-03 00:00:00"
//            "satellite": "N"
//            "confidence": 70
//            "country": "India"
//            "state": "Assam"
//            "note": null
}
