package vn.skymapglobal.smartmap;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import vn.skymapglobal.smartmap.common.BaseActivity;
import vn.skymapglobal.smartmap.helpers.ServiceAsyncTask;
import vn.skymapglobal.smartmap.models.Country;
import vn.skymapglobal.smartmap.models.ResponseAddress;
import vn.skymapglobal.smartmap.models.UserProfile;

import static vn.skymapglobal.smartmap.globals.Constants.GET_ADDRESS_METHOD;
import static vn.skymapglobal.smartmap.globals.Constants.REGISTER_METHOD;

public class RegisterActivity extends BaseActivity {
    private EditText edtName, edtMobile, edtEmail, edtPass, edtConfirmPass, edtOrganization;
    private AutoCompleteTextView edtCountry;
    private TextInputLayout viewEmail, viewName, viewMobile, viewPass, viewOrganization, viewCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        viewEmail = (TextInputLayout) findViewById(R.id.register_view_email);
        viewName = (TextInputLayout) findViewById(R.id.register_view_name);
        viewMobile = (TextInputLayout) findViewById(R.id.register_view_mobile);
        viewPass = (TextInputLayout) findViewById(R.id.register_view_pass);
        viewOrganization = (TextInputLayout) findViewById(R.id.register_view_organization);
        viewCountry = (TextInputLayout) findViewById(R.id.register_view_country);
        edtName = (EditText) findViewById(R.id.register_edt_name);
        edtMobile = (EditText) findViewById(R.id.register_edt_mobile);
        edtEmail = (EditText) findViewById(R.id.register_edt_email);
        edtOrganization = (EditText) findViewById(R.id.register_edt_organization);
        edtCountry = (AutoCompleteTextView) findViewById(R.id.register_edt_country);
        edtPass = (EditText) findViewById(R.id.register_edt_password);
        edtConfirmPass = (EditText) findViewById(R.id.register_edt_password_confirmation);
        findViewById(R.id.register_btn_create_account).setOnClickListener(this);
        requestCountries();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.register_btn_create_account:
                attemptRegister();
                break;
        }
    }

    private void attemptRegister() {
        HashMap<String, String> params = new HashMap<>();

        params.put("name", edtName.getText().toString());
        params.put("mobile", edtMobile.getText().toString());
        params.put("password", edtPass.getText().toString());
        params.put("password_confirmation", edtConfirmPass.getText().toString());
        params.put("email", edtEmail.getText().toString());
        params.put("country", edtCountry.getText().toString());
        params.put("organization", edtOrganization.getText().toString());

        ServiceAsyncTask serviceAsyncTask = new ServiceAsyncTask(this);
        serviceAsyncTask.setListener(this);
        serviceAsyncTask.sendPost(REGISTER_METHOD, params);
    }


    @Override
    public void onReceiver(String method, String data) {
        switch (method) {
            case GET_ADDRESS_METHOD:
                if (data != null) {
                    Country[] mCountries = new Gson().fromJson(data, ResponseAddress.class).countries;
                    ArrayList<String> countries = new ArrayList<>();
                    for (Country country : mCountries) {
                        countries.add(country.name);
                    }
                    edtCountry.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, countries));
                }
                break;
            case REGISTER_METHOD:
                UserProfile user = new Gson().fromJson(data, UserProfile.class);
                finishLogin(user);
                break;
        }
    }

    @Override
    public void onReceiverObject(String method, JSONObject data) {

    }

    private void finishLogin(UserProfile user) {
//        PreferencesUtils.putPreferences(this, PREFERENCE_USER, user.id);
//        PreferencesUtils.putPreferences(this, PREFERENCE_AUTH, true);
        Toast.makeText(this, "Register successfully, Please wait administrator confirm", Toast.LENGTH_LONG).show();
        finish();
    }


    @Override
    public void onError(String method, int code, String error) {
        switch (code) {
            case 422:
                if (method.equals(REGISTER_METHOD)) {
                    try {
                        JSONObject objError = new JSONObject(error);
                        detectRegisterError(objError);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;

            default:
                showAlert("Error", error);
                break;
        }
    }

    private void detectRegisterError(JSONObject objError) {
        try {
            Log.e(TAG, objError.toString());
            View focus = null;
            viewName.setError(null);
            viewMobile.setError(null);
            viewPass.setError(null);
            viewEmail.setError(null);
            viewCountry.setError(null);
            viewOrganization.setError(null);

            if (objError.has("password")) {
                JSONArray errors = objError.getJSONArray("password");
                focus = edtPass;
                viewPass.setError(errors.get(0).toString());
            }


            if (objError.has("country")) {
                JSONArray errors = objError.getJSONArray("country");
                focus = edtCountry;
                viewCountry.setError(errors.get(0).toString());

            }

            if (objError.has("organization")) {
                JSONArray errors = objError.getJSONArray("organization");
                focus = edtOrganization;
                viewOrganization.setError(errors.get(0).toString());

            }
            if (objError.has("email")) {
                JSONArray errors = objError.getJSONArray("email");
                focus = edtEmail;
                viewEmail.setError(errors.get(0).toString());

            }
            if (objError.has("mobile")) {
                JSONArray errors = objError.getJSONArray("mobile");
                focus = edtMobile;
                viewMobile.setError(errors.get(0).toString());

            }

            if (objError.has("name")) {
                JSONArray errors = objError.getJSONArray("name");
                focus = edtName;
                viewName.setError(errors.get(0).toString());
            }

            if (focus != null) {
                focus.requestFocus();
            } else {
                showAlert("Error", objError.toString());
            }


        } catch (JSONException e) {
            showAlert("Error", objError.toString());
            e.printStackTrace();
        }
    }

    private void requestCountries() {
        ServiceAsyncTask serviceAsyncTask = new ServiceAsyncTask(this);
        serviceAsyncTask.setListener(this);
        serviceAsyncTask.sendGet(GET_ADDRESS_METHOD, "");
    }
}
