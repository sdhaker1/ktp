package vn.skymapglobal.smartmap;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.maps.android.geojson.GeoJsonLayer;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import vn.skymapglobal.smartmap.common.BaseActivity;
import vn.skymapglobal.smartmap.helpers.FileHelper;
import vn.skymapglobal.smartmap.helpers.PreferencesUtils;
import vn.skymapglobal.smartmap.helpers.ServiceAsyncTask;
import vn.skymapglobal.smartmap.helpers.Utils;
import vn.skymapglobal.smartmap.models.Country;
import vn.skymapglobal.smartmap.models.Hotspot;
import vn.skymapglobal.smartmap.models.HotspotFilter;
import vn.skymapglobal.smartmap.models.PolygonKML;
import vn.skymapglobal.smartmap.models.ResponseAddress;
import vn.skymapglobal.smartmap.models.State;

import static vn.skymapglobal.smartmap.globals.Constants.GET_ADDRESS_METHOD;
import static vn.skymapglobal.smartmap.globals.Constants.GET_POLYGON;
import static vn.skymapglobal.smartmap.globals.Constants.HOT_SPOTS_METHOD;
import static vn.skymapglobal.smartmap.globals.Constants.PREFERENCE_AUTH;
import static vn.skymapglobal.smartmap.globals.Constants.PREFERENCE_FILTER;
import static vn.skymapglobal.smartmap.globals.Constants.PREFERENCE_USER;

public class MainActivity extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, RoutingListener {
    private GoogleMap mGoogleMap;
    private HotspotFilter mHotspotFilter;
    private Country[] mCountries;
    private AlertDialog dialogFilter;
    private String[] sources = new String[]{"Viirs", "Modis"};
    private State mCurrentState;
    private Country mCurrentCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        String currentFilter = PreferencesUtils.getPreferences(this, PREFERENCE_FILTER, null);
        if (currentFilter != null) {
            mHotspotFilter = new Gson().fromJson(currentFilter, HotspotFilter.class);
        }
        if (isGrantPermission())
            FileHelper.initial(this);
        setupAPI();

    }

    private void requestCountries() {
        ServiceAsyncTask serviceAsyncTask = new ServiceAsyncTask(this);
        serviceAsyncTask.setListener(this);
        serviceAsyncTask.sendGet(GET_ADDRESS_METHOD, "");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_about:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://skymapglobal.com/"));
                startActivity(browserIntent);
                break;
            case R.id.action_logout:
                logout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (mGoogleMap == null) {
            showAlert("Error", "Can't load map!");
            return;
        }
//        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
        mGoogleMap.setOnMarkerClickListener(this);

        FloatingActionButton button = (FloatingActionButton) findViewById(R.id.fab);
        button.setOnClickListener(this);
        requestCountries();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
    }

    private void logout() {
        PreferencesUtils.putPreferences(this, PREFERENCE_AUTH, false);
        PreferencesUtils.putPreferences(this, PREFERENCE_USER, null);
        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("animation", false);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                openFilter();
                break;
        }
    }

    @Override
    public void onReceiver(String method, String data) {
        switch (method) {
            case GET_ADDRESS_METHOD:
                if (data != null) {
                    mCountries = new Gson().fromJson(data, ResponseAddress.class).countries;
                    openFilter();
                } else {
                    showAlert("", "Data not found!");
                }
                break;
            case HOT_SPOTS_METHOD:
                Log.e(TAG, "RECEIVER");
                Hotspot[] hotspots = new Gson().fromJson(data, Hotspot[].class);

                updateMapView(hotspots);
                break;
            case GET_POLYGON:
                PolygonKML[] polygonKMLs = new Gson().fromJson(data, PolygonKML[].class);
                if (polygonKMLs != null && polygonKMLs.length > 0) {
                    moveCamera(polygonKMLs[0]);
                }
                break;
        }
    }

    private void updateMapView(Hotspot[] hotspots) {
        if (mGoogleMap != null) {
            mGoogleMap.clear();
            getStateData(mHotspotFilter.statePos);

            for (Hotspot hotspot : hotspots) {
                LatLng latLng = new LatLng(hotspot.latitude, hotspot.longitude);
                Marker marker = mGoogleMap.addMarker(new MarkerOptions().position(latLng));
                marker.setTitle(hotspot.acquired);
                marker.setTag(hotspot);
                if (hotspot.confidence < 50) {
                    marker.setIcon(BitmapDescriptorFactory.fromAsset("icon_fire_white.png"));
                } else if (hotspot.confidence < 80) {
                    marker.setIcon(BitmapDescriptorFactory.fromAsset("icon_fire_yellow.png"));
                } else {
                    marker.setIcon(BitmapDescriptorFactory.fromAsset("icon_fire_red.png"));
                }

            }
        }

    }

    private void getStateData(int statePos) {
        ServiceAsyncTask serviceAsyncTask = new ServiceAsyncTask(this);
        serviceAsyncTask.setListener(this);
        if (statePos > 0)
            serviceAsyncTask.sendGetPolygon(GET_POLYGON, mCurrentCountry.code, mCurrentState.name, null);
        else
            serviceAsyncTask.sendGetPolygon(GET_POLYGON, mCurrentCountry.code, null, mCurrentCountry.name);
    }

    private void moveCamera(PolygonKML polygonKML) {
        LatLng ll1 = new LatLng(Double.parseDouble(polygonKML.boundingbox[0]), Double.parseDouble(polygonKML.boundingbox[2]));
        LatLng ll2 = new LatLng(Double.parseDouble(polygonKML.boundingbox[1]), Double.parseDouble(polygonKML.boundingbox[3]));
        LatLngBounds latLngBounds = new LatLngBounds(ll1, ll2);
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 100));
        try {
            if (polygonKML.geojson == null) return;
            GeoJsonLayer geoJsonLayer = new GeoJsonLayer(mGoogleMap, new JSONObject(new Gson().toJson(polygonKML.geojson)));
            geoJsonLayer.getDefaultPolygonStyle().setStrokeColor(Color.argb(50, 0, 0, 255));
            geoJsonLayer.addLayerToMap();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openFilter() {
        if (mCountries == null) {
            requestCountries();
            return;
        }
        if (mCountries.length == 0) {
            showAlert("", "Coming soon!");
        } else {

            showDialogFilter();
        }
    }


    private void showDialogFilter() {
        if (dialogFilter == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Hotspot Filter");
            final View view = getLayoutInflater().inflate(R.layout.dialog_filter, null);
            builder.setView(view);
            ArrayList<String> countries = new ArrayList<>();
            for (Country country : mCountries) {
                countries.add(country.name);
            }
            RadioButton radio0 = (RadioButton) view.findViewById(R.id.radio_bt_day);
            RadioButton radio1 = (RadioButton) view.findViewById(R.id.radio_bt_week);

            final AppCompatSpinner spCountry = (AppCompatSpinner) view.findViewById(R.id.filter_sp_country);
            final AppCompatSpinner spState = (AppCompatSpinner) view.findViewById(R.id.filter_sp_state);
            final AppCompatSpinner spSource = (AppCompatSpinner) view.findViewById(R.id.filter_sp_source);

            spSource.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, sources));
            spCountry.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, countries));

            if (mHotspotFilter != null) {
                spSource.setSelection(mHotspotFilter.sourcePos);
                spCountry.setSelection(mHotspotFilter.countryPos);
                if (mHotspotFilter.timePos == 0) {
                    radio0.setChecked(true);
                    radio1.setChecked(false);
                } else {
                    radio0.setChecked(false);
                    radio1.setChecked(true);
                }
            }
            spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    ArrayList<String> states = new ArrayList<>();
                    states.add("All");
                    for (State state : mCountries[i].states) {
                        states.add(state.name);

                    }
                    spState.setAdapter(new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, states));
                    if (mHotspotFilter != null && mHotspotFilter.countryPos == i)
                        spState.setSelection(mHotspotFilter.statePos);
                    else {
                        spState.setSelection(0);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            ArrayList<String> states = new ArrayList<>();
            for (State state : mCountries[0].states) {
                states.add(state.name);
            }

            spState.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, states));
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    RadioButton radio = (RadioButton) view.findViewById(R.id.radio_bt_day);
                    int countDay = radio.isChecked() ? 1 : 7;
                    Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
                    String to = Utils.convertDateToString(calendar.getTime(), "yyyy-MM-dd'T'HH:mm:ss.u'Z'");
                    calendar.add(Calendar.DAY_OF_MONTH, -countDay);
                    String from = Utils.convertDateToString(calendar.getTime(), "yyyy-MM-dd'T'HH:mm:ss.u'Z'");
                    String source = sources[spSource.getSelectedItemPosition()].toLowerCase();
                    query(source, spCountry.getSelectedItemPosition(), spState.getSelectedItemPosition(), from, to);
                    mHotspotFilter = new HotspotFilter();
                    mHotspotFilter.countryPos = spCountry.getSelectedItemPosition();
                    mHotspotFilter.statePos = spState.getSelectedItemPosition();
                    mHotspotFilter.sourcePos = spSource.getSelectedItemPosition();
                    mHotspotFilter.timePos = radio.isChecked() ? 0 : 1;
                    PreferencesUtils.putPreferences(MainActivity.this, PREFERENCE_FILTER, new Gson().toJson(mHotspotFilter));
                    dialog.dismiss();

                }
            });
            dialogFilter = builder.create();
        }
        dialogFilter.show();

    }


    private void query(String source, int countryPos, int statePos, String fromDate, String toDate) {

        mCurrentCountry = mCountries[countryPos];

        HashMap<String, String> params = new HashMap<>();
        params.put("source", source);
        params.put("country", mCurrentCountry.name);
        if (statePos > 0) {
            mCurrentState = mCountries[countryPos].states[statePos - 1];
            String state = mCurrentState.name;
            params.put("state", state);
        }
        params.put("from_date", fromDate);
        params.put("to_date", toDate);

        Log.e(TAG, new Gson().toJson(params));
        ServiceAsyncTask serviceAsyncTask = new ServiceAsyncTask(this);
        serviceAsyncTask.setListener(this);
        serviceAsyncTask.sendPost(HOT_SPOTS_METHOD, params);
    }

    @Override
    public void onReceiverObject(String method, JSONObject data) {

    }

    @Override
    public void onError(String method, int errorCode, String errorData) {
        switch (errorCode) {
            case 422:
                showAlert("Error Details", errorData);
                break;

            default:
                showAlert("Error", errorData);
                break;
        }
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Hotspot Details");

        final Hotspot hotspot = (Hotspot) marker.getTag();
        if (hotspot == null) {
            return true;
        }
        String msg = "Type: " + hotspot.source;
        msg += "\nDate time: " + hotspot.acquired;
        msg += "\nConfidence: " + hotspot.confidence;
        msg += "\nLocation: [" + hotspot.latitude + " - " + hotspot.longitude + "]";
        msg += "\nSatellite: " + hotspot.satellite;
        msg += "\nNote: " + hotspot.note;
        builder.setMessage(msg);
        builder.setPositiveButton("Zoom", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 15), 3000, null);
                dialog.dismiss();
            }
        });
        builder.setNeutralButton("Report", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                Intent intent = new Intent(MainActivity.this, ReportActivity.class);
                intent.putExtra(ReportActivity.ARG_HOTSPOT, hotspot);
                startActivity(intent);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Direction", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (location != null) {
                    viewDirection(new LatLng(hotspot.latitude, hotspot.longitude));
                    dialog.dismiss();
                } else {
                    Toast.makeText(MainActivity.this, "Wait load location", Toast.LENGTH_SHORT).show();
                }

            }
        });

        builder.create().show();
        return true;
    }

    private ProgressDialog mProgressDialog;

    private void viewDirection(LatLng hotspotLatlng) {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait. Fetching route information");
        mProgressDialog.show();
        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .key("AIzaSyCvMT_PtMDkIEiv402skjb9vBJqvNYhSRc")
                .waypoints(new LatLng(location.getLatitude(), location.getLongitude()), hotspotLatlng)
                .build();
        routing.execute();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION && grantResults[0] == 0 && permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            FileHelper.initial(this);
        }

    }

    @Override
    public void onRoutingFailure(RouteException e) {

    }

    @Override
    public void onRoutingStart() {

    }

    private ArrayList<Polyline> mPolylines = new ArrayList<>();

    private ArrayList<Marker> markers = new ArrayList<>();

    @Override
    public void onRoutingSuccess(ArrayList<Route> routes, int i) {
        for (Marker marker : markers) {
            marker.setVisible(false);
        }
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        if (mPolylines.size() > 0) {
            for (Polyline poly : mPolylines) {
                poly.remove();

            }
        }
        mPolylines.clear();
        for (int j = 0; j < routes.size(); j++) {
            if (j != i) {
                PolylineOptions polyOptions = new PolylineOptions();
                polyOptions.width(10);
                polyOptions.addAll(routes.get(j).getPoints());
                polyOptions.color(getResources().getColor(R.color.colorAccent));
                Polyline polyline = mGoogleMap.addPolyline(polyOptions);
                int k = routes.get(j).getSegments().size() / 2;
                markers.add(mGoogleMap.addMarker(new MarkerOptions()
                        .position(routes.get(j).getSegments().get(k).startPoint())
                        .icon(BitmapDescriptorFactory.fromBitmap(makeBitmap(routes.get(j), false)))));
                mPolylines.add(polyline);
            }

        }

        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.width(10);
        polyOptions.addAll(routes.get(i).getPoints());
        polyOptions.color(getResources().getColor(R.color.colorPrimary));
        Polyline polyline = mGoogleMap.addPolyline(polyOptions);
        int k = routes.get(i).getSegments().size() / 2;
        markers.add(mGoogleMap.addMarker(new MarkerOptions()
                .position(routes.get(i).getSegments().get(k).startPoint())
                .icon(BitmapDescriptorFactory.fromBitmap(makeBitmap(routes.get(i), false)))));
        polyline.setClickable(false);
        mPolylines.add(i, polyline);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latLng : polyline.getPoints()) {
            builder.include(latLng);
        }
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 100));

        AlertDialog.Builder builderAlert = new AlertDialog.Builder(this);
        builderAlert.setTitle("Smart Map");
        builderAlert.setMessage("Do you want open Google Driving ?");
        final LatLng end = polyline.getPoints().get(polyline.getPoints().size() - 1);
        builderAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + end.latitude + "," + end.longitude);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
                dialog.dismiss();
            }
        });
        builderAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        builderAlert.create().show();

    }

    public Bitmap makeBitmap(Route route, boolean fastest) {
        IconGenerator iconGenerator = new IconGenerator(this);
        View view = getLayoutInflater().inflate(R.layout.content_marker_view, null);
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        TextView tvAuto = (TextView) view.findViewById(R.id.tv_auto);

        tvTitle.setText(route.getDurationText() + "(" + route.getDistanceText() + ")");
        tvAuto.setText("fastest");
        if (!fastest) {
            tvAuto.setVisibility(View.GONE);
        }
        iconGenerator.setContentView(view);
        return iconGenerator.makeIcon();
    }

    @Override
    public void onRoutingCancelled() {

    }
}
