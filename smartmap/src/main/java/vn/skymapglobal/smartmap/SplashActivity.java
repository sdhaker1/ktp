package vn.skymapglobal.smartmap;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;

import vn.skymapglobal.smartmap.common.BaseActivity;
import vn.skymapglobal.smartmap.helpers.PreferencesUtils;
import vn.skymapglobal.smartmap.helpers.ServiceAsyncTask;
import vn.skymapglobal.smartmap.models.UserProfile;

import static vn.skymapglobal.smartmap.globals.Constants.LOGIN_METHOD;
import static vn.skymapglobal.smartmap.globals.Constants.PREFERENCE_AUTH;
import static vn.skymapglobal.smartmap.globals.Constants.PREFERENCE_USER;

public class SplashActivity extends BaseActivity {
    private ImageView imgLogo, imgSmartMap, imgPlatform;
    private EditText edtUser, edtPassword;
    private View loginForm, btnSignIn, btnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        imgLogo = (ImageView) findViewById(R.id.splash_img_logo);
        imgSmartMap = (ImageView) findViewById(R.id.splash_img_smartmap);
        imgPlatform = (ImageView) findViewById(R.id.splash_img_platform);
        loginForm = findViewById(R.id.splash_login_form);
        btnSignUp = findViewById(R.id.btn_sign_in);
        btnSignIn = findViewById(R.id.btn_sign_up);
        edtUser = (EditText) findViewById(R.id.edt_username);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        isGrantPermission();
        initial();
    }

    private void initial() {
        boolean isAnimation = getIntent().getBooleanExtra("animation", true);
        if (isAnimation) {
            loginForm.setVisibility(View.GONE);
            Animation rotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_rotate);
            imgLogo.setAnimation(rotate);
//        Animator anim = AnimatorInflater.loadAnimator(getApplicationContext(), R.animator.clockwise);
//        anim.setTarget(imgLogo);
//        anim.setDuration(10000);
//        anim.start();

            Animation scale = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_translate);
            scale.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    boolean isLogin = PreferencesUtils.getPreferences(SplashActivity.this, PREFERENCE_AUTH, false);
                    if (isLogin) {
                        navigateMain();
                    } else {
                        loginForm.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            imgSmartMap.setAnimation(scale);
            Animation alpha = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_alpha);
            imgPlatform.setAnimation(alpha);
        } else {
            Animation rotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_rotate);
            imgLogo.setAnimation(rotate);
            loginForm.setVisibility(View.VISIBLE);
        }

        btnSignIn.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_in:
                attemptLogin();
                break;
            case R.id.btn_sign_up:
                navigateRegister();
                break;
        }
    }

    private void attemptLogin() {
        String user = edtUser.getText().toString();
        String pass = edtPassword.getText().toString();

        View editView = null;
        boolean cancel = false;

        if (TextUtils.isEmpty(pass)) {
            cancel = true;
            edtPassword.setError("Field require");
            editView = edtPassword;
        }

        if (TextUtils.isEmpty(user)) {
            cancel = true;
            edtUser.setError("Field require");
            editView = edtUser;
        }

        if (cancel) {
            if (editView != null)
                editView.requestFocus();
        } else {
            HashMap<String, String> params = new HashMap<>();
            params.put("mobile", user);
            params.put("password", pass);
            ServiceAsyncTask serviceAsyncTask = new ServiceAsyncTask(this);
            serviceAsyncTask.setListener(this);
            serviceAsyncTask.sendPost(LOGIN_METHOD, params);
        }


    }

    @Override
    public void onReceiver(String method, String data) {
        if (method.equals(LOGIN_METHOD)) {
            UserProfile user = new Gson().fromJson(data, UserProfile.class);
            finishLogin(user);
        }

    }

    @Override
    public void onReceiverObject(String method, JSONObject data) {

    }

    @Override
    public void onError(String method, int code, String error) {
        switch (code) {
            case 422:
                showAlert("ErrorDetails", error);
                break;

            default:
                showAlert("Error", error);
                break;
        }

    }


    private void finishLogin(UserProfile user) {
        PreferencesUtils.putPreferences(this, PREFERENCE_USER, user.id);
        PreferencesUtils.putPreferences(this, PREFERENCE_AUTH, true);
        Toast.makeText(this, "Login success", Toast.LENGTH_SHORT).show();
        navigateMain();
    }

    private void navigateMain() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void navigateRegister() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
//        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
