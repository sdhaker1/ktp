package vn.skymapglobal.smartmap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import vn.skymapglobal.smartmap.common.BaseActivity;
import vn.skymapglobal.smartmap.helpers.CloudStorage;
import vn.skymapglobal.smartmap.helpers.FileHelper;
import vn.skymapglobal.smartmap.helpers.PreferencesUtils;
import vn.skymapglobal.smartmap.helpers.ServiceAsyncTask;
import vn.skymapglobal.smartmap.helpers.Utils;
import vn.skymapglobal.smartmap.models.Hotspot;

import static vn.skymapglobal.smartmap.globals.Constants.PREFERENCE_USER;
import static vn.skymapglobal.smartmap.globals.Constants.REPORT_METHOD;

public class ReportActivity extends BaseActivity {
    public static final String ARG_HOTSPOT = "HOTSPOT";
    private Hotspot hotspot;
    private EditText edtDescription, edtHotspotAdd;
    private ImageView imgPreview;
    private File mImageFile;
    private final int REQUEST_CODE_CAPTURE_IMAGE = 10;
    public boolean isPermission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hotspot = getIntent().getParcelableExtra(ARG_HOTSPOT);
        setContentView(R.layout.activity_report);
        edtDescription = (EditText) findViewById(R.id.report_edt_description);
        edtHotspotAdd = (EditText) findViewById(R.id.report_edt_location);
        imgPreview = (ImageView) findViewById(R.id.report_img_preview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initial();
        if (isGrantPermission()) {
            createFile();
        }
        setupAPI();

    }

    private void createFile() {
        mImageFile = new File(FileHelper.getInstance().getImageDir(), "image");
        if (mImageFile.exists()) {
            mImageFile.deleteOnExit();
        }
        isPermission = true;
    }

    private void initial() {

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.ENGLISH);
        String address;
        try {
            addresses = geocoder.getFromLocation(hotspot.latitude, hotspot.longitude, 1);
            if (addresses.size() > 0) {
                Address add = addresses.get(0);
                address = add.getAddressLine(0) + " - " + add.getAddressLine(1) + "\n";
            } else {
                address = "";
            }
        } catch (IOException e) {
            address = "";
            e.printStackTrace();
        }
        address += " [" + hotspot.latitude + " - " + hotspot.longitude + "]";
        edtHotspotAdd.setText(address);
        findViewById(R.id.report_btn_submit).setOnClickListener(this);
        findViewById(R.id.report_btn_take_picture).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.report_btn_submit:
                attemptCreateReport();
                break;
            case R.id.report_btn_take_picture:
                captureImage();
                break;
        }
    }

    private void attemptCreateReport() {
        if (mImageFile == null || !mImageFile.exists()) {
            Toast.makeText(this, "Please capture image!", Toast.LENGTH_SHORT).show();
            return;
        }
        String description = edtDescription.getText().toString();
        int userId = PreferencesUtils.getPreferences(this, PREFERENCE_USER, 0);
        int hotspotId = hotspot.id;
        Location currentLocation = getLocation();

        if (currentLocation != null) {
            HashMap<String, String> params = new HashMap<>();
            params.put("description", description);
            params.put("user_id", String.valueOf(userId));
            params.put("hotspot_id", String.valueOf(hotspotId));
            params.put("latitude", currentLocation.getLatitude() + "");
            params.put("longitude", currentLocation.getLongitude() + "");
            params.put("image_url", Utils.generateImageUrl(userId));
            new CreateReportAsyncTask().execute(params);
        } else {
            Toast.makeText(this, "Please wait load location!", Toast.LENGTH_SHORT).show();
        }

    }

    private void captureImage() {
        if (isGrantPermission()) {
            if (isPermission) {
                createFile();
            }
            Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mImageFile));
            startActivityForResult(imageIntent, REQUEST_CODE_CAPTURE_IMAGE);
        } else {
            Toast.makeText(this, "Please allow Permission", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case REQUEST_CODE_CAPTURE_IMAGE:
                if (resultCode == Activity.RESULT_OK) {
                    if (mImageFile.exists()) {
                        FileHelper.getInstance().compressImage(mImageFile.getPath(), mImageFile.getPath());
                        imgPreview.setImageBitmap(bitmapFromUri(Uri.fromFile(mImageFile)));
                    } else {
                        Toast.makeText(this, "Capture image false, please retry", Toast.LENGTH_SHORT).show();
                    }

                }
                break;
        }

    }

    @Override
    public void onReceiver(String method, String data) {
        if (method.equals(REPORT_METHOD)) {
            showAlert("", "Successfully create report!");
        }
    }

    @Override
    public void onReceiverObject(String method, JSONObject data) {

    }

    @Override
    public void onError(String method, int errorCode, String errorData) {
        showAlert("", errorData);
    }


    private class CreateReportAsyncTask extends AsyncTask<HashMap<String, String>, Void, HashMap<String, String>> {
        ProgressDialog mProgress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress = new ProgressDialog(ReportActivity.this);
            mProgress.setMessage("Please Wait...");
            mProgress.show();
        }

        @SafeVarargs
        @Override
        protected final HashMap<String, String> doInBackground(HashMap<String, String>... data) {
            try {
                HashMap<String, String> params = data[0];
                String imageName = params.get("image_url");
                CloudStorage.shareInstance().uploadFileImage("smartmaphotspot", mImageFile, imageName);
                params.put("image_url", "https://storage.googleapis.com/smartmaphotspot/" + imageName);
                params.put("active","true");
                return params;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(HashMap<String, String> result) {
            mProgress.dismiss();
            if (result == null) {
                Toast.makeText(ReportActivity.this, "Error Upload file", Toast.LENGTH_LONG).show();
            } else {
                ServiceAsyncTask serviceAsyncTask = new ServiceAsyncTask(ReportActivity.this);
                serviceAsyncTask.setListener(ReportActivity.this);
                serviceAsyncTask.sendPost(REPORT_METHOD, result);
            }

        }


    }
}
