package vn.skymapglobal.smartmap.helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import vn.skymapglobal.smartmap.globals.AppController;
import vn.skymapglobal.smartmap.globals.Config;


public class ServiceAsyncTask {

    private ProgressDialog progressDialog;
    private IServiceReceiver mCallback;

    public void setListener(IServiceReceiver callBack) {
        mCallback = callBack;
    }

    public ServiceAsyncTask(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait....");


    }

    public void sendPost(final String method, final HashMap<String, String> params) {
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.SERVICE_URL + method,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.e("onResponse", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has("data"))
                                mCallback.onReceiver(method, jsonObject.getString("data"));
                            else
                                mCallback.onReceiver(method, response);
                        } catch (JSONException e) {
                            mCallback.onError(method, -3, "Invalid response!");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("onResponse", error.toString());
                        progressDialog.dismiss();
                        try {
                            NetworkResponse networkResponse = error.networkResponse;
                            if (networkResponse != null && networkResponse.data != null) {
                                String json = new String(networkResponse.data);
                                JSONObject jsonObject = new JSONObject(json);
                                switch (networkResponse.statusCode) {

                                    case 401:
                                        mCallback.onError(method, networkResponse.statusCode, jsonObject.getString("message"));
                                        break;
                                    case 422:
                                        mCallback.onError(method, networkResponse.statusCode, jsonObject.getString("errors"));
                                        break;
                                    case 500:
                                        mCallback.onError(method, networkResponse.statusCode, jsonObject.getString("message"));
                                        break;
                                    default:
                                        if (jsonObject.has("message"))
                                            mCallback.onError(method, networkResponse.statusCode, jsonObject.getString("message"));
                                        else {
                                            mCallback.onError(method, -2, "Server Invalid");

                                        }
                                        break;
                                }

                            } else {
                                mCallback.onError(method, -1, "Can't connect to server!");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mCallback.onError(method, -1, "Connect error, retry later!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void sendGet(final String method, final String params) {

        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.SERVICE_URL + method,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.e("onResponse", response);
                        mCallback.onReceiver(method, response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        mCallback.onError(method, -1, "No data response, Please retry!");

                    }
                }) {

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(stringRequest);

    }

    public void sendGetPolygon(final String method, final String countryId, final String state, String countryName) {
        progressDialog.show();
        try {
            String URL;
            if (state != null)
                URL = String.format(Config.GET_POLYGON_URL, URLEncoder.encode(countryId, "UTF-8"), URLEncoder.encode(state, "UTF-8"));
            else
                URL = String.format(Config.GET_POLYGON_URL_2, URLEncoder.encode(countryName, "UTF-8"), URLEncoder.encode(countryId, "UTF-8"));

            StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.e("onResponse", response);
                            mCallback.onReceiver(method, response);

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            mCallback.onError(method, -2, "Can't load state data!");

                        }
                    }) {

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    15000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppController.getInstance().addToRequestQueue(stringRequest);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


}
