package vn.skymapglobal.smartmap.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by thaibui on 10/6/16.
 */

public class Utils {
    public static String convertDateToString(Date date, String format) {
        DateFormat formatter = new SimpleDateFormat(format, Locale.ENGLISH);
        formatter.setTimeZone(TimeZone.getTimeZone("GMT0"));
        return formatter.format(date);
    }

    public static String generateImageUrl(int userId) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT0"), Locale.ENGLISH);
        long time = calendar.getTimeInMillis();
        return String.valueOf(userId) + String.valueOf(time);
    }
}
