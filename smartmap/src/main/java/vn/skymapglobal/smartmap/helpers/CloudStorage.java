package vn.skymapglobal.smartmap.helpers;


import android.content.Context;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.StorageScopes;
import com.google.api.services.storage.model.StorageObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SKYMAP GLOBAL on 8/2/2016.
 */
public class CloudStorage {
    public Storage storage;

    private static CloudStorage instance = new CloudStorage();


    public void init(Context context) {
        HttpTransport httpTransport = new NetHttpTransport();
        JsonFactory jsonFactory = new JacksonFactory();
        List<String> scopes = new ArrayList<String>();
        scopes.add(StorageScopes.DEVSTORAGE_FULL_CONTROL);

        Credential credential = null;
        try {
            credential = new GoogleCredential.Builder()
                    .setTransport(httpTransport)
                    .setJsonFactory(jsonFactory)
                    .setServiceAccountId("ktpcloud-1470216325260@appspot.gserviceaccount.com") //Email
                    .setServiceAccountPrivateKeyFromP12File(getTempPkc12File(context))
                    .setServiceAccountScopes(scopes).build();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        storage = new Storage.Builder(httpTransport, jsonFactory,
                credential).setApplicationName("ktpcloud")
                .build();
    }

    public static CloudStorage shareInstance() {
        return instance;
    }

    @SuppressWarnings("deprecation")
    private static File getTempPkc12File(Context context) throws IOException {
        // xxx.p12 export from google API console
        InputStream pkc12Stream = context.getResources().getAssets()
                .open("ktpcloud-0e90f08f8fcb.p12", Context.MODE_WORLD_WRITEABLE);// AppData.getInstance().getAssets().open("My First Project-723631384e6a.p12");
        File tempPkc12File = File.createTempFile("temp_pkc12_file", "p12");
        OutputStream tempFileStream = new FileOutputStream(tempPkc12File);

        int read = 0;
        byte[] bytes = new byte[1024];
        while ((read = pkc12Stream.read(bytes)) != -1) {
            tempFileStream.write(bytes, 0, read);
        }
        return tempPkc12File;
    }

    public void uploadFilePDF(String bucketName, File file, String case_no) throws Exception {
        StorageObject object = new StorageObject();
        object.setBucket(bucketName);
        InputStream stream = new FileInputStream(file);

        try {
            InputStreamContent content = new InputStreamContent("application/pdf", stream);
            Storage.Objects.Insert insert = storage.objects().insert(bucketName, null, content);
            String[] parts = case_no.split(",");
            insert.setName(parts[0]);
            insert.execute();
        } finally {
            stream.close();
        }
    }

    public void uploadFileImage(String bucketName, File file, String case_no) throws Exception {
        StorageObject object = new StorageObject();
        object.setBucket(bucketName);
        InputStream stream = new FileInputStream(file);

        try {
            InputStreamContent content = new InputStreamContent("image/png", stream);
            Storage.Objects.Insert insert = storage.objects().insert(bucketName, null, content);
            String[] parts = case_no.split(",");
            insert.setName(parts[0]);
            insert.execute();
        } finally {
            stream.close();
        }
    }

}
