package vn.skymapglobal.smartmap.helpers;

import org.json.JSONObject;

/**
 * Created by thaibui on 10/5/16.
 */

public interface IServiceReceiver {
    void onReceiver(String method, String data);

    void onReceiverObject(String method, JSONObject data);

    void onError(String method, int errorCode, String errorData);
}
