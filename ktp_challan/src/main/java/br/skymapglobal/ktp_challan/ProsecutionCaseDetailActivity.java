package br.skymapglobal.ktp_challan;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.Calendar;

import br.skymapglobal.ktp_challan.adapter.DividerItemDecoration;
import br.skymapglobal.ktp_challan.adapter.ItemModel;
import br.skymapglobal.ktp_challan.adapter.ProsecutionDetailRecyclerViewAdapter;
import br.skymapglobal.ktp_challan.adapter.RecyclerViewType;
import br.skymapglobal.ktp_challan.asynctasks.CallWebServiceAsyncTask;
import br.skymapglobal.ktp_challan.helpers.ApplicationConstants;
import br.skymapglobal.ktp_challan.helpers.CallbackInterface;
import br.skymapglobal.ktp_challan.helpers.IProsecutionDetailListener;
import br.skymapglobal.ktp_challan.helpers.Utils;
import br.skymapglobal.ktp_challan.models.ResponseGetCaseList;
import br.skymapglobal.ktp_challan.models.entity.CaseDetailInfo;
import br.skymapglobal.ktp_challan.models.entity.ProsecutionInfo;

public class ProsecutionCaseDetailActivity extends AppCompatActivity implements IProsecutionDetailListener, CallbackInterface {
    public static final String ARG_TITLE = "title";
    public static final String ARG_CASE = "case";

    private RecyclerView recyclerView;
    private FloatingActionButton fab;
    private ArrayList<ItemModel> listModels;
    private ProsecutionDetailRecyclerViewAdapter adapter;
    private IProsecutionDetailListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listener = this;
        setContentView(R.layout.activity_spot_case_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String title = getIntent().getStringExtra(ARG_TITLE);
        getSupportActionBar().setTitle(title);
        recyclerView = (RecyclerView) findViewById(R.id.list_detail);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.setVisibility(View.GONE);
        listModels = new ArrayList<>();
        listModels.add(new ItemModel(RecyclerViewType.HEADER, null, null));
        adapter = new ProsecutionDetailRecyclerViewAdapter(listModels, listener);
        recyclerView.setAdapter(adapter);
        getSummeryRequest();
    }


    private void getSummeryRequest() {
        String param = "&reg_device_type=%s&reg_device_id=%s&PP_CD=%s&category=%s&comp_case_dt=%s";
//        String reg_device_id = "359453060708821";
//        String reg_device_type = "A";
//        String PP_CD = "CAL/WB9560";
//        String comp_case_dt = "05-AUG-2016";

        String reg_device_id = br.skymapglobal.ktp_challan.helpers.Utils.getIMEI(this);
        String reg_device_type = "A";
        String PP_CD = Utils.getStringPreferences(this, "USERNAME", "");
        String comp_case_dt = br.skymapglobal.ktp_challan.helpers.Utils.convertDateToString(Calendar.getInstance().getTime(), "dd-MMM-yyyy").toUpperCase();
        String category = getIntent().getStringExtra(ARG_CASE);
        param = String.format(param, reg_device_type, reg_device_id, PP_CD, category, comp_case_dt);
        new CallWebServiceAsyncTask(this, ApplicationConstants.GET_CASE_LIST_METHOD, param, 0).setListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        listener = null;
    }

    @Override
    public void onItemClick(ProsecutionInfo item) {

    }

    @Override
    public void refreshList(String response, String method, int type) {
        if (type == 0 && method.equals(ApplicationConstants.GET_CASE_LIST_METHOD)) {
            ResponseGetCaseList responseGetCaseList = ResponseGetCaseList.parseFromJSON(response);
            if (responseGetCaseList == null) {
                showAlert("", "Data Empty!");
                listModels = new ArrayList<>();
                listModels.add(new ItemModel(RecyclerViewType.HEADER, null, null));
                adapter.setItems(listModels);
                adapter.notifyDataSetChanged();
                return;
            }
            if (responseGetCaseList.status == 1) {
                if (responseGetCaseList.data != null) {
                    listModels = new ArrayList<>();
                    listModels.add(new ItemModel(RecyclerViewType.HEADER, null, null));
                    for (CaseDetailInfo caseDetailInfo : responseGetCaseList.data.CaseListCollection) {
                        ItemModel item = new ItemModel(RecyclerViewType.ITEM_1, null, caseDetailInfo);
                        listModels.add(item);
                    }
                    adapter.setItems(listModels);
                    adapter.notifyDataSetChanged();
                }

            } else {
                showAlert("Error", responseGetCaseList.msg);
            }
        }
    }

    void showAlert(String title, String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(msg);
        alertDialog.setTitle(title);
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alertDialog.create().show();
    }

}
