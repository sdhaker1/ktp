package br.skymapglobal.ktp_challan.adapter;

import br.skymapglobal.ktp_challan.models.BaseModel;

/**
 * Created by thaibui on 8/17/16.
 */
public class ItemModel {
    private RecyclerViewType type;
    private String name;
    private BaseModel object;

    public RecyclerViewType getType() {
        return type;
    }

    public void setType(RecyclerViewType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BaseModel getObject() {
        return object;
    }

    public void setObject(BaseModel object) {
        this.object = object;
    }

    public ItemModel(RecyclerViewType type, String name, BaseModel object) {

        this.type = type;
        this.name = name;
        this.object = object;
    }
}
