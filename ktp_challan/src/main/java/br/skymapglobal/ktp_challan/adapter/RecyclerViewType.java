package br.skymapglobal.ktp_challan.adapter;

/**
 * Created by thaibui on 8/17/16.
 */
public enum RecyclerViewType {
    HEADER(1),
    GROUP(2),
    FOOTER(11),
    ITEM_1(3),
    ITEM_2(4);

    private final int value;

    private RecyclerViewType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
