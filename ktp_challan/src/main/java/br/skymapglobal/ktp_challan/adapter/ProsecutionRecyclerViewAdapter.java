package br.skymapglobal.ktp_challan.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.skymapglobal.ktp_challan.R;
import br.skymapglobal.ktp_challan.helpers.IProsecutionListener;
import br.skymapglobal.ktp_challan.models.entity.ProsecutionInfo;


public class ProsecutionRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemModel> items;
    private final IProsecutionListener mListener;

    public ProsecutionRecyclerViewAdapter(List<ItemModel> items, IProsecutionListener listener) {
        this.items = items;
        mListener = listener;
    }

    public void setItems(List<ItemModel> items) {
        this.items = items;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType().getValue();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == RecyclerViewType.HEADER.getValue()) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.header_prosecution, parent, false);
            return new ProsecutionHeaderViewHolder(view);

        } else if (viewType == RecyclerViewType.FOOTER.getValue()) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.footer_prosecution, parent, false);
            return new FooterViewHolder(view);
        } else if (viewType == RecyclerViewType.ITEM_1.getValue()) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_prosecution, parent, false);
            return new ItemViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemModel itemModel = items.get(position);
        if (holder instanceof ProsecutionHeaderViewHolder) {
            return;
        } else if (holder instanceof ItemViewHolder) {
            ItemViewHolder mViewHolder = (ItemViewHolder) holder;
            final ProsecutionInfo prosecutionInfo = (ProsecutionInfo) itemModel.getObject();
            mViewHolder.mCaseType.setText(prosecutionInfo.type + "");
            if (TextUtils.isEmpty(prosecutionInfo.TOTAL_CASE)) {
                mViewHolder.mCaseCount.setText("-");

            } else {
                mViewHolder.mCaseCount.setText(prosecutionInfo.TOTAL_CASE + "");

            }
            mViewHolder.mAmount.setText(prosecutionInfo.TOTAL_FINE + "");
            mViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClick(prosecutionInfo);
                }
            });
        } else if (holder instanceof FooterViewHolder) {
            FooterViewHolder mViewHolder = (FooterViewHolder) holder;
            mViewHolder.mTotal.setText(itemModel.getName());
        }

    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mCaseType;
        public final TextView mCaseCount;
        public final TextView mAmount;

        public ItemViewHolder(View view) {
            super(view);
            mView = view;
            mCaseType = (TextView) view.findViewById(R.id.case_type);
            mCaseCount = (TextView) view.findViewById(R.id.case_count);
            mAmount = (TextView) view.findViewById(R.id.amount);

        }

    }

    private class ProsecutionHeaderViewHolder extends RecyclerView.ViewHolder {
        public final View mView;


        public ProsecutionHeaderViewHolder(View view) {
            super(view);
            mView = view;

        }

    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTotal;

        public FooterViewHolder(View view) {
            super(view);
            mView = view;
            mTotal = (TextView) view.findViewById(R.id.sum);
        }

    }
}
