package br.skymapglobal.ktp_challan.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.skymapglobal.ktp_challan.R;
import br.skymapglobal.ktp_challan.helpers.IVehicleWisePendingListener;
import br.skymapglobal.ktp_challan.models.entity.CaseCountInfo;


public class VehicleWisePendingRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemModel> items;
    private final IVehicleWisePendingListener mListener;

    public VehicleWisePendingRecyclerViewAdapter(List<ItemModel> items, IVehicleWisePendingListener listener) {
        this.items = items;
        mListener = listener;
    }

    public void setItems(List<ItemModel> items) {
        this.items = items;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType().getValue();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == RecyclerViewType.HEADER.getValue()) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.header_vehicle_wise_pending, parent, false);
            return new HeaderViewHolder(view);

        } else if (viewType == RecyclerViewType.ITEM_1.getValue()) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_vehicle_wise_pending, parent, false);
            return new ItemViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemModel itemModel = items.get(position);
        if (holder instanceof HeaderViewHolder) {
            return;
        } else if (holder instanceof ItemViewHolder) {
            ItemViewHolder mViewHolder = (ItemViewHolder) holder;
            final CaseCountInfo caseDetailInfo = (CaseCountInfo) itemModel.getObject();
            mViewHolder.mType.setText(caseDetailInfo.type + "");
            if (TextUtils.isEmpty(caseDetailInfo.pending)) {
                mViewHolder.mPending.setText("-");
            } else {
                mViewHolder.mPending.setText(caseDetailInfo.pending);
            }
            if (TextUtils.isEmpty(caseDetailInfo.court)) {
                mViewHolder.mCount.setText("-");
            } else {
                mViewHolder.mCount.setText(caseDetailInfo.court);
            }

        }

    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mType;
        public final TextView mPending;
        public final TextView mCount;


        public ItemViewHolder(View view) {
            super(view);
            mView = view;
            mType = (TextView) view.findViewById(R.id.vehicle_wise_pending_column_1);
            mPending = (TextView) view.findViewById(R.id.vehicle_wise_pending_column_2);
            mCount = (TextView) view.findViewById(R.id.vehicle_wise_pending_column_3);

        }

    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        public final View mView;


        public HeaderViewHolder(View view) {
            super(view);
            mView = view;

        }

    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTotal;

        public FooterViewHolder(View view) {
            super(view);
            mView = view;
            mTotal = (TextView) view.findViewById(R.id.sum);
        }

    }
}
