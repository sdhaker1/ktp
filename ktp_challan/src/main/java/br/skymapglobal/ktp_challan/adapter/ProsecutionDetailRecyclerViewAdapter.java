package br.skymapglobal.ktp_challan.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.skymapglobal.ktp_challan.R;
import br.skymapglobal.ktp_challan.helpers.IProsecutionDetailListener;
import br.skymapglobal.ktp_challan.models.entity.CaseDetailInfo;


public class ProsecutionDetailRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemModel> items;
    private final IProsecutionDetailListener mListener;

    public ProsecutionDetailRecyclerViewAdapter(List<ItemModel> items, IProsecutionDetailListener listener) {
        this.items = items;
        mListener = listener;
    }

    public void setItems(List<ItemModel> items) {
        this.items = items;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType().getValue();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == RecyclerViewType.HEADER.getValue()) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.header_prosecution_detail, parent, false);
            return new HeaderViewHolder(view);

        } else if (viewType == RecyclerViewType.ITEM_1.getValue()) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_prosecution_detail, parent, false);
            return new ItemViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemModel itemModel = items.get(position);
        if (holder instanceof HeaderViewHolder) {
            return;
        } else if (holder instanceof ItemViewHolder) {
            ItemViewHolder mViewHolder = (ItemViewHolder) holder;
            final CaseDetailInfo caseDetailInfo = (CaseDetailInfo) itemModel.getObject();
            mViewHolder.mCaseNo.setText(caseDetailInfo.case_no + "");
            mViewHolder.mVehicleNo.setText(caseDetailInfo.vech_no + "");
            mViewHolder.mVehicleType.setText(caseDetailInfo.vech_type_cd + "");
            mViewHolder.mOffenceType.setText(caseDetailInfo.vio_us_cd + "");
            mViewHolder.mAmount.setText(caseDetailInfo.fine_amt + "");
        }

    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mCaseNo;
        public final TextView mVehicleType;
        public final TextView mVehicleNo;
        public final TextView mOffenceType;
        public final TextView mAmount;


        public ItemViewHolder(View view) {
            super(view);
            mView = view;
            mCaseNo = (TextView) view.findViewById(R.id.detail_case_column_1);
            mVehicleNo = (TextView) view.findViewById(R.id.detail_case_column_2);
            mVehicleType = (TextView) view.findViewById(R.id.detail_case_column_3);
            mOffenceType = (TextView) view.findViewById(R.id.detail_case_column_4);
            mAmount = (TextView) view.findViewById(R.id.detail_case_column_5);
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public final View mView;


        public HeaderViewHolder(View view) {
            super(view);
            mView = view;
        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTotal;

        public FooterViewHolder(View view) {
            super(view);
            mView = view;
            mTotal = (TextView) view.findViewById(R.id.sum);
        }
    }
}
