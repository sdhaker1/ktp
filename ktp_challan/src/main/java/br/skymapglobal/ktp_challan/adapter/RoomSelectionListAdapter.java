package br.skymapglobal.ktp_challan.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import br.skymapglobal.ktp_challan.R;

/**
 * Created by aa on 12/19/2015.
 */
public class RoomSelectionListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> roomArrayList;
    public static ArrayList<String> roomListIds = new ArrayList<String>();


    private static LayoutInflater inflater = null;


    public RoomSelectionListAdapter(Context context, ArrayList<String> roomArrayList) {
            this.context=context;
            this.  roomArrayList=roomArrayList;
//            this.selected=selected;
//           checkbox=new boolean[cart_data1.length];


    }

    @Override
    public int getCount() {
        return roomArrayList.size();
    }


    @Override
    public Object getItem(int i) {
        return i;
    }



    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        final String roomList=roomArrayList.get(i);
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);


        View view1;
        if (convertView == null) {

            view1 = new View(context);

            view1 = inflater.inflate(R.layout.selected_room_list_layout, null);


        } else {
            view1 = (View) convertView;
        }


        TextView tvTitle = (TextView) view1.findViewById(R.id.room_row1);
        tvTitle.setText(roomArrayList.get(i).toString());

        CheckBox cb = (CheckBox) view1.findViewById(R.id.check_row1);


        cb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (((CheckBox) v).isChecked()) {
//                    String checkRoomId = roomList.getId();
//                    roomListIds.add(checkRoomId);

                    v.setSelected(true);
                } else {

                    for (int i=0; i<roomListIds.size(); i++) {
//                        if (roomListIds.get(i) == roomList.getId()) {
//                            roomListIds.remove(i);
//                        }
                    }
                    v.setSelected(false);

                }
            }
        });

        Log.d("ValueOFIds,",""+roomListIds);
        return view1;


    }

}