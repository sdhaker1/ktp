package br.skymapglobal.ktp_challan.models;

import com.google.gson.Gson;

import br.skymapglobal.ktp_challan.models.entity.ProsecutionInfo;

/**
 * Created by thaibui on 8/17/16.
 */
public class ResponseGetSummery extends BaseModel {
    public ProsecutionInfo[] data;
    public int status;
    public String msg;

    public static ResponseGetSummery parseFromJSON(String json){
        return new Gson().fromJson(json,ResponseGetSummery.class);
    }
}
