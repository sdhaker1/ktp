package br.skymapglobal.ktp_challan.models;

import com.google.gson.Gson;

import br.skymapglobal.ktp_challan.models.entity.CaseDetailInfo;
import br.skymapglobal.ktp_challan.models.entity.ProsecutionInfo;

/**
 * Created by thaibui on 8/17/16.
 */
public class ResponseGetCaseList extends BaseModel {
    public CaseList data;
    public int status;
    public String msg;
    public int total_no_case;

    public static ResponseGetCaseList parseFromJSON(String json) {
        return new Gson().fromJson(json, ResponseGetCaseList.class);
    }

    public class CaseList{
      public  CaseDetailInfo[] CaseListCollection;
    }
}
