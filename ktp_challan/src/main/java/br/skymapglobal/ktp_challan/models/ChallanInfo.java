package br.skymapglobal.ktp_challan.models;

/**
 * Created by SKYMAP GLOBAL on 7/5/2016.
 */
public class ChallanInfo {


    private String status;
    private String msg;
    private String vech_type;
    private String total_no_case;
    private String offence_no;
    private String vehicle_rta;
    private String owner_name;
    private String ownerf_name;
    private String owner_address;
    private String case_no;
    private String case_date;
    private String case_status;
    private String vio_us_cd;
    private String fine_amt;
    private String disposed_date;
    private String challan_no;
    private String challan_dt;
    private String court;
    private String grd_off_cd;
    private String AmountListCollection;

}
