package br.skymapglobal.ktp_challan.models;

import java.util.Calendar;

import br.skymapglobal.ktp_challan.helpers.Utils;

public class SpotChallanInfo {

    public String MOBILE_NO ;
    public String case_no;
    public String vehicle_no;
    public String offence_type;
    public String spot_fine_amt;
    public String date;
    public String courtName;
    public String case_id;
    public String fineAmountStr;
    public String Acused_prsn_name = "...................";

    public String oname;
    public String guard;
}
