package br.skymapglobal.ktp_challan.models;

import br.skymapglobal.ktp_challan.models.entity.CaseHistory;

/**
 * Created by thaibui on 8/25/16.
 */
public class CaseHistoryResponse {
    public int status;
    public String msg;
    public String vech_type;
    public int total_no_case;
    public int offence_no;
    public String vehicle_rta;
    public String owner_name;
    public String ownerf_name;
    public String owner_address;
    public CaseHistory data;


}
