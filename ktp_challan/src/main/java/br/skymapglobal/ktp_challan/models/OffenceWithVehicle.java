package br.skymapglobal.ktp_challan.models;

/**
 * Created by Admin on 05-01-2017.
 */

public class OffenceWithVehicle {

    private String vehicltype="";
    private String ofencetype="";
    private String status="";

    public String getOfencetype() {
        return ofencetype;
    }
    public void setOfencetype(String ofencetype) {
        this.ofencetype = ofencetype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVehicltype() {
        return vehicltype;
    }

    public void setVehicltype(String vehicltype) {
        this.vehicltype = vehicltype;
    }

}
