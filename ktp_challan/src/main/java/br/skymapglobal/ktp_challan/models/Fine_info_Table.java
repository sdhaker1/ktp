package br.skymapglobal.ktp_challan.models;

/**
 * Created by sl5 on 12/6/16.
 */
public class Fine_info_Table {

    private String DT;
    private String VIO_US_CD;
    private String FINE_AMT_1;
    private String FINE_AMT_2;
    private String FINE_AMT_3;
    private String FINE_AMT_4;
    private String VECH_TYPE_CD;

    public String getVIO_US_CD() {
        return VIO_US_CD;
    }

    public void setVIO_US_CD(String VIO_US_CD) {
        this.VIO_US_CD = VIO_US_CD;
    }

    public String getVECH_TYPE_CD() {
        return VECH_TYPE_CD;
    }

    public void setVECH_TYPE_CD(String VECH_TYPE_CD) {
        this.VECH_TYPE_CD = VECH_TYPE_CD;
    }

    public String getDT() {
        return DT;
    }

    public void setDT(String DT) {
        this.DT = DT;
    }

    public String getFINE_AMT_1() {
        return FINE_AMT_1;
    }

    public void setFINE_AMT_1(String FINE_AMT_1) {
        this.FINE_AMT_1 = FINE_AMT_1;
    }

    public String getFINE_AMT_2() {
        return FINE_AMT_2;
    }

    public void setFINE_AMT_2(String FINE_AMT_2) {
        this.FINE_AMT_2 = FINE_AMT_2;
    }

    public String getFINE_AMT_3() {
        return FINE_AMT_3;
    }

    public void setFINE_AMT_3(String FINE_AMT_3) {
        this.FINE_AMT_3 = FINE_AMT_3;
    }

    public String getFINE_AMT_4() {
        return FINE_AMT_4;
    }

    public void setFINE_AMT_4(String FINE_AMT_4) {
        this.FINE_AMT_4 = FINE_AMT_4;
    }
}
