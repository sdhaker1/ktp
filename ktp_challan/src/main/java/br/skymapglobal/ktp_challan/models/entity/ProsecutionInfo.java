package br.skymapglobal.ktp_challan.models.entity;

import br.skymapglobal.ktp_challan.adapter.RecyclerViewType;
import br.skymapglobal.ktp_challan.models.BaseModel;

/**
 * Created by thaibui on 8/17/16.
 */
public class ProsecutionInfo extends BaseModel {
    public String type;
    public String TOTAL_CASE;
    public String TOTAL_FINE;
}
