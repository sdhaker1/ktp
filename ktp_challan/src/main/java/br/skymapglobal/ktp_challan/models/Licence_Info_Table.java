package br.skymapglobal.ktp_challan.models;

/**
 * Created by sl5 on 9/6/16.
 */
public class Licence_Info_Table {

    private String ID="id";
    private String STATUS="status";
    private String ADDRESS="address";
    private String OWNER_NAME="owner_name";
    private String ISSUE_DATE="issue_date";
    private String EXPIRY_DATE="expiry_type";
    private String LICENCE_NUMBER="licence_no";
    private String VEHICAL_CLASS="vehicle_class";

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getLICENCE_NUMBER() {
        return LICENCE_NUMBER;
    }

    public void setLICENCE_NUMBER(String LICENCE_NUMBER) {
        this.LICENCE_NUMBER = LICENCE_NUMBER;
    }

    public String getOWNER_NAME() {
        return OWNER_NAME;
    }

    public void setOWNER_NAME(String OWNER_NAME) {
        this.OWNER_NAME = OWNER_NAME;
    }

    public String getISSUE_DATE() {
        return ISSUE_DATE;
    }

    public void setISSUE_DATE(String ISSUE_DATE) {
        this.ISSUE_DATE = ISSUE_DATE;
    }

    public String getEXPIRY_DATE() {
        return EXPIRY_DATE;
    }

    public void setEXPIRY_DATE(String EXPIRY_DATE) {
        this.EXPIRY_DATE = EXPIRY_DATE;
    }

    public String getVEHICAL_CLASS() {
        return VEHICAL_CLASS;
    }

    public void setVEHICAL_CLASS(String VEHICAL_CLASS) {
        this.VEHICAL_CLASS = VEHICAL_CLASS;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
}
