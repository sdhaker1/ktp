package br.skymapglobal.ktp_challan.models;

import com.google.gson.Gson;

/**
 * Created by thaibui on 8/17/16.
 */
public class BaseModel {
    public String toJSON(){
        return new Gson().toJson(this);
    }

}
