package br.skymapglobal.ktp_challan.models;

import android.graphics.Bitmap;

import java.util.Calendar;

import br.skymapglobal.ktp_challan.helpers.Utils;

/**
 * Created by thaibui on 8/31/16.
 */
public class SeizureChallanInfo {
    public String MOBILE_NO;
    public String Acused_prsn_name;
    public String case_id;
    public String urlImage;
    public String offenceType;
    public String vehicle_no;
    public String date = Utils.convertDateToString(Calendar.getInstance().getTime(),"dd-MMM-yyyy");
    public String time = Utils.convertDateToString(Calendar.getInstance().getTime(),"HH:mm");
    public String place;
    public String fineAmount;
    public String fineAmountStr;
    public Bitmap bitmap;

    public String oname;
    public String userName;
    public int itemCheck;
}
