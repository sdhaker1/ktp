package br.skymapglobal.ktp_challan.models;

import com.google.gson.Gson;

import br.skymapglobal.ktp_challan.models.entity.CaseCountInfo;
import br.skymapglobal.ktp_challan.models.entity.ProsecutionInfo;

/**
 * Created by thaibui on 8/17/16.
 */
public class ResponseCaseCountList extends BaseModel {
    public CaseCountInfo[] data;
    public int status;
    public String msg;
    public String vehicle_rta;
    public String vehicle_type_cd;


    public static ResponseCaseCountList parseFromJSON(String json){
        return new Gson().fromJson(json,ResponseCaseCountList.class);
    }
}
