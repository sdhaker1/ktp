package br.skymapglobal.ktp_challan.models;

import br.skymapglobal.ktp_challan.R;

/**
 * Created by thaibui on 8/27/16.
 */
public class PoliceRegisterModel extends BaseModel{

    public String type;
    public String marital;
    public String rented;
    public String tenantName;
    public String fatherName;
    public String cardNo;
    public String age;
    public String occupation;
    public String mobile;
    public String tenantAddress;
    public String placeWork;
    public String detailsPerson;
    public String personPhone;
    public String area;
    public String currentLocation;
    public String houseAddress;

}
