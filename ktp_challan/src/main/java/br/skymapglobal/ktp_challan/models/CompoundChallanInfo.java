package br.skymapglobal.ktp_challan.models;

import java.util.Calendar;
import java.util.Date;

import br.skymapglobal.ktp_challan.helpers.Utils;

/**
 * Created by thaibui on 8/10/16.
 */
public class CompoundChallanInfo {
    public String imageUrl;
    public String case_no;
    public String v_no;
    public String reg_device_id;
    public String reg_device_type;
    public String vehicle_type;
    public String Acused_prsn_name;
    public String Acused_prsn_addr;
    public String Beat_No;
    public String Road_Name;
    public String us01;
    public int on01;
    public String us02;
    public int on02;
    public String us03;
    public int on03;
    public String us04;
    public int on04;
    public  String us05;
    public int on05;
    public String Fine_AMT;
    public String DOC_NAME;
    public String SEIZED_DOC_ID;
    public String MOBILE_NO;
    public String omobile;
    public String PP_CD ;
    public String guard_code;

    public String court = "XXXXX";
    public String date = Utils.convertDateToString(Calendar.getInstance().getTime(),"dd-MMM-yyyy");
    public String time = Utils.convertDateToString(Calendar.getInstance().getTime(),"HH:mm");
    public String oname;
    public String courtName;
}
