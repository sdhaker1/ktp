package br.skymapglobal.ktp_challan.models;

/**
 * Created by thaibui on 8/15/16.
 */
public class ReportModels {
    public String tableName;
    public String description;
    public String guardCode;
    public String userId;
    public String beatNo;
    public String status;
    public boolean active;
    public String longitude;
    public String latitude;
    public String type;
    public String place;
}
