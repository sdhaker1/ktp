package br.skymapglobal.ktp_challan.models;

/**
 * Created by SKYMAP GLOBAL on 7/1/2016.
 */
public class Vehicle_info {//Variables that are in our json
    private String status;
    private String msg;
    private String vech_type="";
    private String total_no_case;
    private String offence_no;
    private String vehicle_rta;
    private String owner_name;
    private String ownerf_name;
    private String owner_address;
    private String case_no;
    private String case_date;
    private String case_status;
    private String vio_us_cd;
    private String fine_amt;
    private String disposed_date;
    private String challan_no;
    private String challan_dt;
    private String court;
    private String grd_off_cd;
    private String AmountListCollection;
    //Getters and setters

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getVech_type() {
        return vech_type;
    }

    public void setVech_type(String vech_type) {
        this.vech_type = vech_type;
    }

    public String getTotal_no_case() {
        return total_no_case;
    }

    public void setTotal_no_case(String total_no_case) {
        this.total_no_case = total_no_case;
    }

    public String getOffence_no() {
        return offence_no;
    }

    public void setOffence_no(String offence_no) {
        this.offence_no = offence_no;
    }

    public String getVehicle_rta() {
        return vehicle_rta;
    }

    public void setVehicle_rta(String vehicle_rta) {
        this.vehicle_rta = vehicle_rta;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getOwnerf_name() {
        return ownerf_name;
    }

    public void setOwnerf_name(String ownerf_name) {
        this.ownerf_name = ownerf_name;
    }

    public String getOwner_address() {
        return owner_address;
    }

    public void setOwner_address(String owner_address) {
        this.owner_address = owner_address;
    }

    public String getCase_no() {
        return case_no;
    }

    public void setCase_no(String case_no) {
        this.case_no = case_no;
    }

    public String getCase_date() {
        return case_date;
    }

    public void setCase_date(String case_date) {
        this.case_date = case_date;
    }

    public String getCase_status() {
        return case_status;
    }

    public void setCase_status(String case_status) {
        this.case_status = case_status;
    }

    public String getVio_us_cd() {
        return vio_us_cd;
    }

    public void setVio_us_cd(String vio_us_cd) {
        this.vio_us_cd = vio_us_cd;
    }

    public String getFine_amt() {
        return fine_amt;
    }

    public void setFine_amt(String fine_amt) {
        this.fine_amt = fine_amt;
    }

    public String getDisposed_date() {
        return disposed_date;
    }

    public void setDisposed_date(String disposed_date) {
        this.disposed_date = disposed_date;
    }

    public String getChallan_no() {
        return challan_no;
    }

    public void setChallan_no(String challan_no) {
        this.challan_no = challan_no;
    }

    public String getChallan_dt() {
        return challan_dt;
    }

    public void setChallan_dt(String challan_dt) {
        this.challan_dt = challan_dt;
    }

    public String getCourt() {
        return court;
    }

    public void setCourt(String court) {
        this.court = court;
    }

    public String getGrd_off_cd() {
        return grd_off_cd;
    }

    public void setGrd_off_cd(String grd_off_cd) {
        this.grd_off_cd = grd_off_cd;
    }

    public String getAmountListCollection() {
        return AmountListCollection;
    }

    public void setAmountListCollection(String amountListCollection) {
        AmountListCollection = amountListCollection;
    }
}