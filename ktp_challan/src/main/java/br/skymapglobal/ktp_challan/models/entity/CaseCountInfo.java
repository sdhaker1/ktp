package br.skymapglobal.ktp_challan.models.entity;

import br.skymapglobal.ktp_challan.models.BaseModel;

/**
 * Created by thaibui on 8/20/16.
 */
public class CaseCountInfo extends BaseModel{
    public String type;
    public String pending;
    public String court;
}
