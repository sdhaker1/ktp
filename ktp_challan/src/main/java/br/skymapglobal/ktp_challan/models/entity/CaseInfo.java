package br.skymapglobal.ktp_challan.models.entity;

/**
 * Created by thaibui on 8/25/16.
 */
public class CaseInfo {
    public String case_no;
    public String case_date;
    public String case_status;
    public String vio_us_cd;
    public String fine_amt;
    public String disposed_date;
    public String challan_no;
    public String challan_dt;
    public String court;
    public String grd_off_cd;
}