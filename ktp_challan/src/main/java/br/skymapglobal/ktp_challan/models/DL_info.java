package br.skymapglobal.ktp_challan.models;

/**
 * Created by SKYMAP GLOBAL on 7/3/2016.
 */
public class DL_info {
    private String owner_name;
    private String issu_dt;
    private String lcns_cat;
    private String valid_upto;
    private String owner_address;

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getIssu_dt() {
        return issu_dt;
    }

    public void setIssu_dt(String issu_dt) {
        this.issu_dt = issu_dt;
    }

    public String getLcns_cat() {
        return lcns_cat;
    }

    public void setLcns_cat(String lcns_cat) {
        this.lcns_cat = lcns_cat;
    }

    public String getValid_upto() {
        return valid_upto;
    }

    public void setValid_upto(String valid_upto) {
        this.valid_upto = valid_upto;
    }

    public String getOwner_address() {
        return owner_address;
    }

    public void setOwner_address(String owner_address) {
        this.owner_address = owner_address;
    }
}
