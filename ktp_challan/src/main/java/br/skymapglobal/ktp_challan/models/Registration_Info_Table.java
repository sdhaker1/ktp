package br.skymapglobal.ktp_challan.models;

/**
 * Created by sl5 on 9/6/16.
 */
public class Registration_Info_Table {

    private String ID="id";
    private String UID="uid";
    private String COLOR="color";
    private String ADDRESS="address";
    private String FUEL_TYPE="fuel_type";
    private String OWNER_NAME="owner_name";
    private String MAKER_MODEL="maker_model";
    private String VEHICLE_TYPE="vehicle_type";
    private String CHASSIS_NUMBER="chassis_no";
    private String REGISTRATION_NUMBER="reg_no";
    private String REGISTRATION_DATE="registration_date";

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getREGISTRATION_NUMBER() {
        return REGISTRATION_NUMBER;
    }

    public void setREGISTRATION_NUMBER(String REGISTRATION_NUMBER) {
        this.REGISTRATION_NUMBER = REGISTRATION_NUMBER;
    }

    public String getREGISTRATION_DATE() {
        return REGISTRATION_DATE;
    }

    public void setREGISTRATION_DATE(String REGISTRATION_DATE) {
        this.REGISTRATION_DATE = REGISTRATION_DATE;
    }

    public String getOWNER_NAME() {
        return OWNER_NAME;
    }

    public void setOWNER_NAME(String OWNER_NAME) {
        this.OWNER_NAME = OWNER_NAME;
    }

    public String getFUEL_TYPE() {
        return FUEL_TYPE;
    }

    public void setFUEL_TYPE(String FUEL_TYPE) {
        this.FUEL_TYPE = FUEL_TYPE;
    }

    public String getCHASSIS_NUMBER() {
        return CHASSIS_NUMBER;
    }

    public void setCHASSIS_NUMBER(String CHASSIS_NUMBER) {
        this.CHASSIS_NUMBER = CHASSIS_NUMBER;
    }

    public String getMAKER_MODEL() {
        return MAKER_MODEL;
    }

    public void setMAKER_MODEL(String MAKER_MODEL) {
        this.MAKER_MODEL = MAKER_MODEL;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public String getCOLOR() {
        return COLOR;
    }

    public void setCOLOR(String COLOR) {
        this.COLOR = COLOR;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getVEHICLE_TYPE() {
        return VEHICLE_TYPE;
    }

    public void setVEHICLE_TYPE(String VEHICLE_TYPE) {
        this.VEHICLE_TYPE = VEHICLE_TYPE;
    }
}
