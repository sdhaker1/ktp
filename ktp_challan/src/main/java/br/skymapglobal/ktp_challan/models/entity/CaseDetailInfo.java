package br.skymapglobal.ktp_challan.models.entity;

import br.skymapglobal.ktp_challan.models.BaseModel;

/**
 * Created by thaibui on 8/19/16.
 */
public class CaseDetailInfo extends BaseModel{

    public String case_no;
    public String vech_no;
    public String fine_amt;
    public String vio_us_cd;
    public String vech_type_cd;
}
