package br.skymapglobal.ktp_challan.helpers;

import java.util.Calendar;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class HeaderFooterPageEvent extends PdfPageEventHelper {
	private static Font SMALL_FONT = new Font(Font.FontFamily.HELVETICA, 9,
			Font.ITALIC);
	private String caseNo;
	private String baseUrl;

	public static HeaderFooterPageEvent getInstance(String caseNo, String baseUrl) {
		HeaderFooterPageEvent instance = new HeaderFooterPageEvent();
		instance.caseNo = caseNo;
		instance.baseUrl = baseUrl;
		return instance;
	}

	@Override
	public void onStartPage(PdfWriter writer, Document document) {
		Rectangle rect = writer.getBoxSize("art");
		Calendar calendar = Calendar.getInstance();
		ColumnText.showTextAligned(writer.getDirectContent(),
				Element.ALIGN_LEFT,
				new Phrase(calendar.getTime().toString(), SMALL_FONT), rect.getLeft(),
				rect.getTop(), 0);
		ColumnText.showTextAligned(writer.getDirectContent(),
				Element.ALIGN_RIGHT, new Phrase(baseUrl + caseNo, SMALL_FONT),
				rect.getRight(), rect.getTop(), 0);
	}

	@Override
	public void onEndPage(PdfWriter writer, Document document) {
		Rectangle rect = writer.getBoxSize("art");
		ColumnText.showTextAligned(writer.getDirectContent(),
				Element.ALIGN_LEFT, new Phrase(baseUrl + caseNo, SMALL_FONT),
				rect.getLeft(), rect.getBottom(), 0);
		ColumnText.showTextAligned(writer.getDirectContent(),
				Element.ALIGN_RIGHT, new Phrase("1/1", SMALL_FONT),
				rect.getRight(), rect.getBottom(), 0);
	}
}
