package br.skymapglobal.ktp_challan.helpers;

import android.content.Context;
import android.graphics.Bitmap;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import br.skymapglobal.ktp_challan.models.CompoundChallanInfo;
import br.skymapglobal.ktp_challan.models.SeizureChallanInfo;
import br.skymapglobal.ktp_challan.models.SpotChallanInfo;

/**
 * Created by thaibui on 8/10/16.
 */
public class PDFHelper {

    private static PDFHelper ourInstance;
    private static Context context;

    private final static Font TITLE_FONT = new Font(Font.FontFamily.HELVETICA, 12,
            Font.BOLD);
    private final static Font BOLD_FONT = new Font(Font.FontFamily.HELVETICA, 10,
            Font.BOLD);
    private final static Font NORMAL_FONT = new Font(Font.FontFamily.HELVETICA, 10,
            Font.NORMAL);
    private final static Font BOLD_ITALIC_FONT = new Font(Font.FontFamily.HELVETICA,
            10, Font.BOLDITALIC);
    private final static Font ITALIC_FONT = new Font(Font.FontFamily.HELVETICA, 10,
            Font.ITALIC);
    private final static Font SMALL_FONT = new Font(Font.FontFamily.HELVETICA, 9,
            Font.NORMAL);

    public static void initial(Context context) {
        if (ourInstance == null)
            ourInstance = new PDFHelper(context);
    }

    public static PDFHelper getInstance() {
        return ourInstance;
    }

    private PDFHelper(Context context) {
        this.context = context;
    }

    public boolean generateSeizureFile(final SeizureChallanInfo seizureChallanInfo) {
        try {

            String fileName = "seizure.pdf";
            /*File file = new File(FileHelper.getInstance().getDocumentDir().getPath(), fileName);
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream(file.getPath()));*/
            //File file = new File(FileHelper.getInstance().getDocumentDir().getPath(), fileName);
            ByteArrayOutputStream ba =new ByteArrayOutputStream();
            Document document = new Document();
            //PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream(file.getPath()));
            PdfWriter writer = PdfWriter.getInstance(document,ba);

            writer.setEncryption(seizureChallanInfo.MOBILE_NO.getBytes(), seizureChallanInfo.MOBILE_NO.getBytes(), PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128);
            writer.createXmpMetadata();

            document.setPageSize(PageSize.A4);
            document.setMargins(50, 50, 70, 70);

            Rectangle rect = new Rectangle(40, 40, 556, 803);
            writer.setBoxSize("art", rect);
            document.open();
            addMetaData(document, "Seizure Document");
            addBorder(document, new Rectangle(40, 220, 696, 783));
            addContentSeizurePage(document, seizureChallanInfo);
            document.close();
            InputStream is=new ByteArrayInputStream(ba.toByteArray());
            CloudStorage.shareInstance().uploadFilePDF1("seizurechallan", is, seizureChallanInfo.case_id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }


    public boolean generateSpotFile(final SpotChallanInfo spotChallanInfo) {
        try {
            String fileName = "spot.pdf";
            /*File file = new File(FileHelper.getInstance().getDocumentDir().getPath(), fileName);
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file.getPath()));*/
            //File file = new File(FileHelper.getInstance().getDocumentDir().getPath(), fileName);
            ByteArrayOutputStream ba =new ByteArrayOutputStream();
            Document document = new Document();
            //PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream(file.getPath()));
            PdfWriter writer = PdfWriter.getInstance(document,ba);

            writer.setEncryption(spotChallanInfo.MOBILE_NO.getBytes(), spotChallanInfo.MOBILE_NO.getBytes(), PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128);
            writer.createXmpMetadata();

            document.setPageSize(PageSize.A4);
            document.setMargins(90, 90, 70, 70);


            Rectangle rect = new Rectangle(50, 50, 556, 803);
            writer.setBoxSize("art", rect);
            document.open();
            addMetaData(document, "Spot Document");
            addBorder(document, new Rectangle(70, 330, 526, 783));
            addContentSpotPage(document, spotChallanInfo);
            document.close();
            InputStream is=new ByteArrayInputStream(ba.toByteArray());
            CloudStorage.shareInstance().uploadFilePDF1("spotpayment", is, spotChallanInfo.case_no);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean generateCompoundFile(final CompoundChallanInfo challanInfo) {
        try {
            String fileName = "compound.pdf";


            //File file = new File(FileHelper.getInstance().getDocumentDir().getPath(), fileName);
            ByteArrayOutputStream ba =new ByteArrayOutputStream();
            Document document = new Document();
            //PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream(file.getPath()));
            PdfWriter writer = PdfWriter.getInstance(document,ba);
            writer.setEncryption(challanInfo.MOBILE_NO.getBytes(), challanInfo.MOBILE_NO.getBytes(), PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128);
            writer.createXmpMetadata();

            document.setPageSize(PageSize.A4);
            document.setMargins(50, 50, 70, 70);

            Rectangle rect = new Rectangle(40, 40, 556, 803);
            writer.setBoxSize("art", rect);
//                    HeaderFooterPageEvent event = HeaderFooterPageEvent.getInstance(
//                            "AR16001393", PDFCompound.BASE_URL);
//                    writer.setPageEvent(event);
            document.open();
            addMetaData(document, "Compound Document");
            addBorder(document, new Rectangle(40, 60, 556, 783));
            addContentCompoundPage(document, challanInfo);
            document.close();
            InputStream is=new ByteArrayInputStream(ba.toByteArray());
            CloudStorage.shareInstance().uploadFilePDF1("compoundchallan", is, challanInfo.case_no);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void addContentSeizurePage(Document document, SeizureChallanInfo seizureChallanInfo) throws DocumentException, IOException {
        Paragraph preface = new Paragraph();
        // We add one empty line
        // Lets write a big header
        preface.add(new Paragraph(String.format(PDFSeizure.LEFT_HEADER, seizureChallanInfo.case_id), BOLD_FONT));

        addEmptyLine(preface, 1);
        // Will create: Report generated by: _name, _date
        Paragraph title = new Paragraph(PDFSeizure.TITLE, TITLE_FONT);
        title.setAlignment(Element.ALIGN_CENTER);
        preface.add(title);
        addEmptyLine(preface, 1);

        Paragraph subTile = new Paragraph(PDFSeizure.SUB_TITLE_1, SMALL_FONT);
        subTile.setAlignment(Element.ALIGN_CENTER);
        preface.add(subTile);
        Paragraph subTile2 = new Paragraph(PDFSeizure.SUB_TITLE_2, BOLD_FONT);
        subTile2.setAlignment(Element.ALIGN_CENTER);
        preface.add(subTile2);
        addEmptyLine(preface, 1);

        Paragraph content1 = new Paragraph(String.format(PDFSeizure.CONTENT_1, seizureChallanInfo.vehicle_no), NORMAL_FONT);
        content1.setAlignment(Element.ALIGN_JUSTIFIED);
        preface.add(content1);

        document.add(preface);
        addEmptyLine(preface, 1);
        Phrase[] phrases = new Phrase[8];
        phrases[0] = new Phrase(String.format(PDFSeizure.ITEM_LIST_1, new Object[]{seizureChallanInfo.Acused_prsn_name}), NORMAL_FONT);
        phrases[1] = new Phrase(PDFSeizure.ITEM_LIST_2, NORMAL_FONT);
        phrases[2] = new Phrase(PDFSeizure.ITEM_LIST_3, NORMAL_FONT);
        phrases[3] = new Phrase(PDFSeizure.ITEM_LIST_4, NORMAL_FONT);
        phrases[4] = new Phrase(PDFSeizure.ITEM_LIST_5, NORMAL_FONT);
        phrases[5] = new Phrase(PDFSeizure.ITEM_LIST_6, NORMAL_FONT);
        phrases[6] = new Phrase(PDFSeizure.ITEM_LIST_7, NORMAL_FONT);
        phrases[7] = new Phrase(new Chunk(String.format(PDFSeizure.ITEM_LIST_8, new Object[]{seizureChallanInfo.offenceType}), NORMAL_FONT));
        PdfPTable pdfPTable = new PdfPTable(20);
        pdfPTable.setTotalWidth(document.getPageSize().getWidth() - 100);
        pdfPTable.setLockedWidth(true);
        PdfPCell pdfPCell0, pdfPCell1;
        for (int i = 0; i < 8; i++) {
            if (i == seizureChallanInfo.itemCheck) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                seizureChallanInfo.bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                Image image = Image.getInstance((byteArrayOutputStream).toByteArray());
                image.scaleAbsolute(12, 12);
                pdfPCell0 = new PdfPCell(new PdfPCell(image, true));
            } else {
                pdfPCell0 = new PdfPCell(new Phrase("  ", NORMAL_FONT));
            }
            pdfPCell0.setColspan(1);
            pdfPCell0.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfPCell0.setBorder(Rectangle.NO_BORDER);

            pdfPCell1 = new PdfPCell(phrases[i]);
            pdfPCell1.setColspan(19);
            pdfPCell1.setBorder(Rectangle.NO_BORDER);
            pdfPCell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            pdfPTable.addCell(pdfPCell0);
            pdfPTable.addCell(pdfPCell1);
        }

        document.add(pdfPTable);

        Object[] param = new Object[3];
        param[0] = seizureChallanInfo.date;
        param[1] = seizureChallanInfo.time;
        param[2] = seizureChallanInfo.place;

        Paragraph content2 = new Paragraph(String.format(PDFSeizure.CONTENT_2, param), NORMAL_FONT);
        document.add(content2);

        Chunk chunk = new Chunk("Signature of driver/owner\n   a. Sd/- " + seizureChallanInfo.Acused_prsn_name + "\nSeized Document\n", NORMAL_FONT);

        List list;
        ListItem item;

        list = new List(true);
        item = new ListItem(chunk);
        list.add(item);
        if (seizureChallanInfo.urlImage != null) {
            Image image1 = Image.getInstance(seizureChallanInfo.urlImage);
            image1.scaleAbsolute(100, 80);
            item.add(image1);
        }
        document.add(list);
        addEmptyLine(preface, 1);

        PdfPTable table = new PdfPTable(1);
        PdfPCell c1 = new PdfPCell(
                new Phrase(new Phrase("Sd/- " + seizureChallanInfo.oname + " - " + seizureChallanInfo.userName + "\nFull Signature with Address & Designation", NORMAL_FONT)));
        c1.setBorder(Rectangle.NO_BORDER);
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        document.add(table);
    }

    private void addContentSpotPage(Document document, SpotChallanInfo spotChallanInfo) throws DocumentException {
        Paragraph preface = new Paragraph();
        preface.add(new Paragraph(String.format(PDFSpot.HEADER_1, spotChallanInfo.case_id.trim()), BOLD_FONT));
        preface.add(new Paragraph(String.format(PDFSpot.HEADER_2, spotChallanInfo.case_no.trim()), BOLD_FONT));
        addEmptyLine(preface, 1);
        Paragraph title = new Paragraph(PDFSpot.TITLE, BOLD_FONT);
        title.setAlignment(Element.ALIGN_CENTER);
        preface.add(title);

        Paragraph subTile = new Paragraph(PDFSpot.SUB_TITLE_1, SMALL_FONT);
        subTile.setAlignment(Element.ALIGN_CENTER);
        preface.add(subTile);

        Paragraph subTile2 = new Paragraph(String.format(PDFSpot.SUB_TITLE_2,spotChallanInfo.guard), BOLD_FONT);
        subTile2.setAlignment(Element.ALIGN_CENTER);
        preface.add(subTile2);

        Paragraph date = new Paragraph(String.format(PDFSpot.SUB_TITLE_3, spotChallanInfo.date), SMALL_FONT);
        date.setAlignment(Element.ALIGN_RIGHT);
        preface.add(date);

        addEmptyLine(preface, 1);
        Object[] contentParam = new Object[5];
        contentParam[0] = spotChallanInfo.Acused_prsn_name;
        contentParam[1] = spotChallanInfo.fineAmountStr;
        contentParam[2] = spotChallanInfo.vehicle_no;
        contentParam[3] = spotChallanInfo.offence_type;
        contentParam[4] = spotChallanInfo.spot_fine_amt;
        Paragraph content1 = new Paragraph(String.format(PDFSpot.CONTENT_1, contentParam), BOLD_FONT);

        content1.setAlignment(Element.ALIGN_JUSTIFIED);
        preface.add(content1);
        addEmptyLine(preface, 1);

        addEmptyLine(preface, 2);
        PdfPTable table = new PdfPTable(2);
        PdfPCell c0 = new PdfPCell(new Phrase(""));
        String signature = String.format(PDFSpot.SIGNATURE,spotChallanInfo.oname);
        PdfPCell c1 = new PdfPCell(new Phrase(signature, BOLD_FONT));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c0.setBorder(Rectangle.NO_BORDER);
        c1.setBorder(Rectangle.NO_BORDER);
        table.addCell(c0);
        table.addCell(c1);

        preface.add(table);

        document.add(preface);
    }

    private void addContentCompoundPage(Document document, CompoundChallanInfo compoundChallanInfo)
            throws DocumentException, IOException {

        Paragraph preface = new Paragraph();
        preface.add(new Paragraph(
                PDFCompound.LEFT_HEADER + compoundChallanInfo.case_no, BOLD_FONT));
        addEmptyLine(preface, 1);
        Paragraph title = new Paragraph(PDFCompound.TITLE, TITLE_FONT);
        title.setAlignment(Element.ALIGN_CENTER);
        preface.add(title);

        addEmptyLine(preface, 1);
        Paragraph subTile = new Paragraph(PDFCompound.SUB_TITLE_1, SMALL_FONT);
        subTile.setAlignment(Element.ALIGN_CENTER);
        preface.add(subTile);

        Paragraph subTile2 = new Paragraph(PDFCompound.SUB_TITLE_2, BOLD_FONT);
        subTile2.setAlignment(Element.ALIGN_CENTER);
        preface.add(subTile2);
        addEmptyLine(preface, 1);
        Object[] param = new Object[10];
        param[0] = compoundChallanInfo.v_no;
        param[1] = compoundChallanInfo.vehicle_type;
        param[2] = compoundChallanInfo.Acused_prsn_name;
        param[3] = compoundChallanInfo.Road_Name;
        param[4] = compoundChallanInfo.time;
        param[5] = compoundChallanInfo.Acused_prsn_name;
        param[6] = compoundChallanInfo.us01;
        param[7] = compoundChallanInfo.Fine_AMT;
        param[8] = compoundChallanInfo.Fine_AMT;
        param[9] = compoundChallanInfo.Fine_AMT;

        Paragraph content1 = new Paragraph(String.format(PDFCompound.CONTENT_1,
                param), NORMAL_FONT);
        content1.setAlignment(Element.ALIGN_JUSTIFIED);
        preface.add(content1);

        Paragraph content2 = new Paragraph(PDFCompound.CONTENT_2, NORMAL_FONT);
        content2.setAlignment(Element.ALIGN_JUSTIFIED);
        preface.add(content2);

        addEmptyLine(preface, 2);
        PdfPTable table = new PdfPTable(2);
        PdfPCell c0 = new PdfPCell(new Phrase(""));
        PdfPCell c1 = new PdfPCell(new Phrase(String.format(PDFCompound.SIGNATURE_1, compoundChallanInfo.oname),
                NORMAL_FONT));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c0.setBorder(Rectangle.NO_BORDER);
        c1.setBorder(Rectangle.NO_BORDER);
        table.addCell(c0);
        table.addCell(c1);

        preface.add(table);
        addEmptyLine(preface, 1);

        Object[] param3 = new Object[5];
        param3[0] = compoundChallanInfo.Fine_AMT;
        param3[1] = compoundChallanInfo.Acused_prsn_name;
        param3[2] = compoundChallanInfo.courtName;
        param3[3] = compoundChallanInfo.date;
        param3[4] = compoundChallanInfo.Road_Name;
        Paragraph content3 = new Paragraph(String.format(PDFCompound.CONTENT_3,
                param3), NORMAL_FONT);
        content3.setAlignment(Element.ALIGN_JUSTIFIED);
        preface.add(content3);

        String seized_doc_id="";
        if(compoundChallanInfo.DOC_NAME.trim().equalsIgnoreCase("DL")){
            seized_doc_id=compoundChallanInfo.SEIZED_DOC_ID;
        }

        Chunk chunk = new Chunk(compoundChallanInfo.DOC_NAME +" "+seized_doc_id+ "\n", NORMAL_FONT);
        List list = new List(true);
        ListItem item = new ListItem(chunk);
        list.add(item);

        if (compoundChallanInfo.imageUrl != null) {
            Image image1 = Image.getInstance(compoundChallanInfo.imageUrl);
            image1.scaleAbsolute(100, 80);
            item.add(image1);
        }

        document.add(preface);
        document.add(list);

        // document.add(image1);
        addEmptyLine(preface, 1);

        table.deleteLastRow();
        c1 = new PdfPCell(new Phrase(String.format(PDFCompound.SIGNATURE_2, compoundChallanInfo.Acused_prsn_name), NORMAL_FONT));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setBorder(Rectangle.NO_BORDER);

        table.addCell(c0);
        table.addCell(c1);
        document.add(table);
    }


    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private void addMetaData(Document document, String subject) {
        document.addTitle("KOLKATA - POLICE");
        document.addSubject(subject);
        document.addKeywords("PDF");
        document.addAuthor("SkyMap");
        document.addCreator("SkyMap");
    }

    private void addBorder(Document document, Rectangle rect) throws DocumentException {
        rect.setBorder(Rectangle.BOX);
        rect.setBorderWidth(1);
        rect.setBorderColor(BaseColor.BLACK);
        document.add(rect);
    }

    class PDFSeizure {
        private static final String CONTENT_1 = "The following documents in respect of vehicle No %s have been seized by me for taking further action:\n";
        private static final String CONTENT_2 = "\nThis document authorizes the plying of the vehicle by the owner and driving of vehicle by the driver named above till 15days  only.\nThe owner/driver is hereby is asked to appear before the Court of- Ld. Alipore  Court.\nTaxing officer ...... or to the undersigned__by_.\nDate and Time of Seizure %s %s Place %s";
        private static final String ITEM_LIST_1 = "1. Registration Certificate in the name of %s of showing U.L.W.___ Seating capacity__G.V.W. __ C.F. being valid up to __ .";
        private static final String ITEM_LIST_2 = "2. Permit (Permanent/Temporary) No .......... for the year ...... route ...... valid up to ......";
        private static final String ITEM_LIST_3 = "3. Insurance Certificate No ...... valid up to .......";
        private static final String ITEM_LIST_4 = "4. Tax Token/Treasury Challan/D.C.R. No. ..... valid up to .......";
        private static final String ITEM_LIST_5 = "5. Manifest/Challan/ Cash memo No. ..... dated. ...... for the goods weighing ........";
        private static final String ITEM_LIST_6 = "6. Particulars of any other document ............";
        private static final String ITEM_LIST_7 = "7. Driving Licence No. of Shri ..... son of ..... residing at ..... valid up to authorised to drive ..... (type of vehicle).";
        private static final String ITEM_LIST_8 = "8. Nature of offences detected %s";
        private static final String LEFT_HEADER = "Seizure No. %s";
        private static final String SUB_TITLE_1 = "[See rule 123 of the West Bengal Motor Vehicles Rules. 1989]";
        private static final String SUB_TITLE_2 = "TRANSPORT DEPARTMENT\nChecking of Motor Vehicles and Siezure List";
        private static final String TITLE = "FORM VIII";

    }


    class PDFCompound {
        private static final String LEFT_HEADER = "Compound No. ";
        private static final String TITLE = "FORM COMP.PENAL";
        private static final String SUB_TITLE_1 = "[See rule 349 of the West Bengal Motor Vehicles Rules, 1989]";
        private static final String SUB_TITLE_2 = "Government of West Bengal\nOffice of the District Magistrate/Sub-Divisional Officer\nMotor Vehicles Department";
        private static final String CONTENT_1 = ""
                + "Whereas the Vehicle No. %s Type/Make/Model %s Owned / Driven by %s"
                + " was found plying on %s at %s hours, in violation"
                + " of the provisions of section/s, Rule/s of the MVA 1988, WBMVR, 1989, CMVR, 1989 and whereas"
                + " you %s driver/owner of the vehicle at the time of detection of the offence have"
                + " prayed to be excused and opted for compounding of the offence under sub-section (1) of section 200 of"
                + " the MVA, 1988, and whereas the maximum penalty imposable for the offence/s committed under"
                + " section/s %s of the MVA 1988 in total is Rs.%s/- .  Now, therefore, in"
                + " exercise of the powers conferred upon me under sub-section (1) of section 200 of the MVA, 1988."
                + " I do hereby compound the offence/s under section 200 of the MVA , 1988 for the payment"
                + " of Rs.%s/- (Rupees %s only) by you and direct you to make payment of the fines"
                + " through www.kolkatatrafficpolice.gov.in /GRIPS /concerned TP Guard /Lalbazar Counter failing"
                + " which prosecution proceeding will be initiated in appropriate court of Law.";

        private static final String CONTENT_2 = "The seized documents will be returned to you on production of the receipt before the officer-in-charge of this Traffic Guard showing deposit of compounded amount before the officer who remains in charge of the concerned Guard.";
        private static final String SIGNATURE_1 = "Sd/- %s\nFull Signature of the compounding\nofficer with designation";
        private static final String CONTENT_3 = ""
                + "I have committed the offence/s as mentioned in paragraph. I am agreeable to compound the offence/s"
                + " and to pay Rs.%s/- as directed. I %s do hereby declare that I was driving the"
                + " above vehicle. I do hereby bind myself to compound the above offences within the date stipulated"
                + " by law or to remain present before the court %s for contesting/ awaiting the outcome"
                + " of the case. I hereby declare that I shall inform the registered owner (if not present) accordingly."
                + "\nDate %s Place %s\nDocument Seized";
        private static final String SIGNATURE_2 = "Sd/- %s\nSignature or thumb impression of Driver/owner of the vehicle";

    }

    class PDFSpot {
        private static final String HEADER_1 = "West Bengal Form No. 4705.                                                               No- %s";
        private static final String HEADER_2 = "Financial Rule Form No. ID.                                                         Case No- %s";
        private static final String TITLE = "Receipt for Payments to Government.";
        private static final String SUB_TITLE_1 = "[See Rule 2B, W.B.F.R.]";
        private static final String SUB_TITLE_2 = "GOVERNMENT OF WEST BENGAL"
                + "\n%s Department";
        private static final String SUB_TITLE_3 = "Dated the %s";
        private static final String CONTENT_1 = ""
                + "\nReceived from - %s"
                + "\nThe sum of Rupees - %s Only."
                + "\n\nOn account of - %s."
                + "\nU/S- %s"
                + "\nSpot Fine."
                + "\n\nRs. %s /-\n";

        private static final String SIGNATURE = "Sd/- %s\n"
                + "\nSignature of the officer";
    }

}
