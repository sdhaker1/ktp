package br.skymapglobal.ktp_challan.helpers;

import br.skymapglobal.ktp_challan.models.entity.ProsecutionInfo;

/**
 * Created by thaibui on 8/17/16.
 */
public interface IProsecutionListener {
    void onItemClick(ProsecutionInfo item);

}
