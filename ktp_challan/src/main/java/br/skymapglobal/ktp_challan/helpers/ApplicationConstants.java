package br.skymapglobal.ktp_challan.helpers;

/**
 * Created by sl5 on 9/6/16.
 */
public class ApplicationConstants {

    public static final String SUCCESS = "Success";
    public static String TIME="Time";
    public static String TIME_ANALYST = "http://54.88.119.188:8000/ktp_analytics?";
    public static String BASE_URL = "https://kolkatatrafficpolice.net/subsequentoffence/api.aspx?action=";
    public static String BASE_URL_SKYMAP = "https://kolkatatrafficpolice.net/SubsequentOffence/Skymap_Api.aspx?action=";
    public static String LOGIN_METHOD = "LoginCheck";
    public static String REGISTER_METHOD = "Login";
    public static String DEVICE_STATUS = "DeviceStatus";
    public static String SUBEQUENT_LIST_METHOD = "GetVehicle";
    public static String SubsequentList_Skymap_METHOD = "SubsequentList_Skymap";
    public static String US_LIST_METHOD = "USList";
    public static String CASE_COUNT_LIST_METHOD = "CaseCountList";
    public static String DL_LIST_METHOD = "DLList";
    public static String GET_SPOT_FINE_METHOD = "getspotfine";
    public static String GET_PENDING_CASE_METHOD = "GetPendingCase";
    public static String GET_GUARD_METHOD = "GetGUard";
    public static String GET_FINE_AMT_METHOD = "GetFineAMT";
    public static String GET_SUMMERY_METHOD = "Getsummery";
    public static String GET_CASE_LIST_METHOD = "CaseList";
    public static String GET_CASE_COUNT_METHOD = "CaseCountList";

    public static String BEAT_CHECKING_METHOD = "Beatchecking";
    public static String GET_PLACE_METHOD = "GetPlace";
    public static String GENRT_COMPOUND_METHOD = "GenrtCompound";
    public static String GENRT_MERGED_COMPOUND_METHOD = "Genrt_SPOT_Compound";
    public static String GENRT_SEIZURE_METHOD = "GenrtSeizure";
    public static String GENERATE_PDF_METHOD = "GeneratePdf";
    public static String INSERT_SPOT_METHOD = "insertspot";
    public static String CASE_HISTORY = "SubsequentList";
    public static String SENDER = "sender";
    public static String CASE_NO = "caseno";
    public static String COMP_CASE_DT = "comp_case_dt";
    public static String SPOT_FINE_AMT = "spot_fine_amt";
    public static String OMOBILE = "omobile";
    public static String GUARD_CODE = "GRD_OFF_CD";
    public static String SRAID_GUARD_CODE = "sraid_guard";
    public static String CATEGORY = "category";
    public static String VEHICLE_NO = "vehicle_no";
    public static String ACUSED_PRSN_NAME = "Acused_prsn_name";
    public static String ACUSED_PRSN_ADDR = "Acused_prsn_addr";
    public static String BEAT_NO = "Beat_No";
    public static String ROAD_NAME = "Road_Name";
    public static String US_01 = "us01";
    public static String ON_01 = "on01";
    public static String US_02 = "us02";
    public static String ON_02 = "on02";
    public static String US_03 = "us03";
    public static String ON_03 = "on03";
    public static String US_04 = "us04";
    public static String ON_04 = "on04";
    public static String US_05 = "us05";
    public static String ON_05 = "on05";
    public static String Fine_AMT = "Fine_AMT";
    public static String DOC_NAME = "DOC_NAME";
    public static String SEIZED_DOC_ID = "SEIZED_DOC_ID";
    public static String MOBILE_NO = "MOBILE_NO";
    public static String PP_CD = "PP_CD";
    public static String BEAT_NO_KEY = "beatno";
    public static String DL_NO = "DL_No";
    public static String VECH_NO = "vech_no";
    public static String VIO_US_CD_KEY = "vio_us_cd";
    public static String COMP_CASE_NO = "comp_case_no";
    public static String REG_DEVICE_ID = "reg_device_id";
    public static String REG_MOBILE_NO = "reg_mob_no";
    public static String REG_DEVICE_TOKEN = "reg_device_token";
    public static String REG_EMAIL = "reg_email";
    public static String GPF = "mem_gpf_no";
    public static String VERSION = "version";
    public static String REG_DEVICE_TYPE = "reg_device_type";
    public static String ID = "id";
    public static String REGISTRATION_NUMBER = "reg_no";
    public static String REGISTRATION_DATE = "registration_date";
    public static String OWNER_NAME = "owner_name";
    public static String FUEL_TYPE = "fuel_type";
    public static String VEHICLE_TYPE = "vehicle_type";
    public static String MAKER_MODEL = "maker_model";
    public static String CHASSIS_NUMBER = "chassis_no";

    public static String LICENCE_NUMBER = "licence_no";
    public static String ISSUE_DATE = "issue_date";
    public static String EXPIRY_DATE = "expiry_type";
    public static String VEHICAL_CLASS = "vehicle_class";
    public static String ADDRESS = "address";
    public static String STATUS = "status";
    public static String COLOR = "color";
    public static String UID = "uid";

    public static String TABLE_REGISTRATION_INFO = "registration_info";
    public static final String CREATE_TABLE_REGISTRATION_INFO = "CREATE TABLE IF NOT EXISTS " + TABLE_REGISTRATION_INFO + "("
            + ID + " Text,"
            + REGISTRATION_NUMBER + " Text,"
            + REGISTRATION_DATE + " Text,"
            + OWNER_NAME + " Text,"
            + ADDRESS + " Text,"
            + FUEL_TYPE + " Text,"
            + VEHICLE_TYPE + " Text,"
            + MAKER_MODEL + " Text,"
            + COLOR + " Text,"
            + UID + " Text,"
            + CHASSIS_NUMBER + " Text" + ")";
    public static String TABLE_LICENECE_INFO = "licence_info";
    public static final String CREATE_TABLE_LICENCE_INFO = "CREATE TABLE IF NOT EXISTS " + TABLE_LICENECE_INFO + "("
            + ID + " Text,"
            + LICENCE_NUMBER + " Text,"
            + OWNER_NAME + " Text,"
            + ADDRESS + " Text,"
            + ISSUE_DATE + " Text,"
            + EXPIRY_DATE + " Text,"
            + VEHICAL_CLASS + " Text,"
            + STATUS + " Text" + ")";
    public static String VIO_US_CD = "VIO_US_CD";
    public static String VECH_TYPE_CD = "VECH_TYPE_CD";
    public static String DT = "DT";
    public static String FINE_AMT_1 = "FINE_AMT_1";
    public static String FINE_AMT_2 = "FINE_AMT_2";
    public static String FINE_AMT_3 = "FINE_AMT_3";
    public static String FINE_AMT_4 = "FINE_AMT_4";

    //current location code is commented as told by rupesh sir
//    public static String getCurrentLocation(Context mContext) {
//        String location_str = null;
//        GPSTracker gps = new GPSTracker(mContext);
//        double latitude = gps.getLatitude();
//        double longitude = gps.getLongitude();
//        Geocoder geocoder;
//        List<Address> addresses = null;
//        geocoder = new Geocoder(mContext, Locale.getDefault());
//
//        try {
//            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        if (addresses != null && addresses.size() > 0) {
//            String address1 = addresses.get(0).getThoroughfare();
//            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//            String city = addresses.get(0).getLocality();
//            String subLocality = addresses.get(0).getSubLocality();
//            String state = addresses.get(0).getAdminArea();
//            String country = addresses.get(0).getCountryName();
//            String postalCode = addresses.get(0).getPostalCode();
//            String knownName = addresses.get(0).getFeatureName();
////        location_str =address+" "+subLocality + " " + city + " " + state + " " + country + " " + postalCode;
//            if (address1 != null) {
//                location_str = address1;
//            }
//        }
//        return location_str;
//    }


}
