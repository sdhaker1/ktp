package br.skymapglobal.ktp_challan.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {

    public static String IMEI_No;

    public static void saveBoolPreferences(Context context, String key,boolean value) {
        SharedPreferences faves = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = faves.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getBoolPreferences(Context context, String key) {
        SharedPreferences myPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean rtn = myPrefs.getBoolean(key, false);
        return rtn;
    }

    public static void saveIntPreferences(Context context, String key,Integer value) {
        SharedPreferences faves = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = faves.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int getIntPreferences(Context context, String key,int defaultValue) {
        SharedPreferences myPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        int rtn = myPrefs.getInt(key, defaultValue);
        return rtn;
    }

    public static void saveFloatPreferences(Context context, String key,Float value) {
        SharedPreferences faves = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = faves.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    public static float getFloatPreferences(Context context, String key,float defaultValue) {
        SharedPreferences myPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return myPrefs.getFloat(key, defaultValue);
    }

    public static void saveStringPreferences(Context context, String key,String value) {
        SharedPreferences faves = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = faves.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getStringPreferences(Context context, String key,String defaultstr) {
        SharedPreferences myPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        String rtn = myPrefs.getString(key, defaultstr);
        return rtn;
    }

    public static String getDevice() {
        return Build.DEVICE;
    }
    public static String getModel() {
        return Build.MODEL;
    }
    public static String getProduct() {
        return Build.PRODUCT;
    }
    public static String getSdkversion() {
        return Build.VERSION.SDK_INT+"";
    }

    public static String getNetworkClass(Context context) {
        TelephonyManager mTelephonyManager = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = mTelephonyManager.getNetworkType();
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "2G";
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "3G";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "4G";
            default:
                return "Unknown";
        }
    }

    public static String getIMEI(Context context) {
        if (IMEI_No == null) {
            TelephonyManager mngr = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
            try {
                if (mngr != null && mngr.getDeviceId().length() != 0) {
                    IMEI_No = mngr.getDeviceId();
                } else {
//                    imei = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                }
            } catch (Exception e) {
//                imei = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
        }
        if (Utils.IMEI_No == null  || Utils.IMEI_No.equals("000000000000000")) {
            Utils.IMEI_No = "359453060700820";
        }
        return IMEI_No;
    }


    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public static String convertDateToString(Date date, String format) {
        DateFormat formatter = new SimpleDateFormat(format, Locale.ENGLISH);
        return formatter.format(date);
    }
}
