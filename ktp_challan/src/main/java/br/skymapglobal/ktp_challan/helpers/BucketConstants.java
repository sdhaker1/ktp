package br.skymapglobal.ktp_challan.helpers;

/**
 * Created by thaibui on 8/14/16.
 */
public class BucketConstants {
    public static final String REPORT_TRAFFIC_CONGESTION = "reporttrafficcongestion";
    public static final String REPORT_SIGNAL = "reportsignal";
    public static final String REPORT_VOILATION = "reportviolation";
    public static final String REPORT_MARKING = "reportmarking";
    public static final String REPORT_FURNITURE = "reportfurniture";
    public static final String REPORT_CONDITION = "reportcondition";
    public static final String REPORT_OTHERS = "reportothers";

}
