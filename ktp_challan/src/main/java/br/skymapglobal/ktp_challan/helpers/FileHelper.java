package br.skymapglobal.ktp_challan.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by thaibui on 8/10/16.
 */
public class FileHelper {
    private static FileHelper ourInstance;
    private static Context context;
    private final static String KTP_FOLDER = "KTP";
    private final static String COMPOUND_FOLDER = "Compound";
    private final static String SEIZURE_FOLDER = "Seizure";
    private final static String DOC_FOLDER = "Document";
    private final static String INTERNAL_REPORT_FOLDER = "InternalReport";

    private static File ktpFolder, seizureFolder, compoundFolder, docFolder, internalReportFolder;

    public static void initial(Context context) {
        if (ourInstance == null)
            ourInstance = new FileHelper(context);

        ktpFolder = new File(Environment.getExternalStorageDirectory(), KTP_FOLDER);
        if (!ktpFolder.exists()){
            ktpFolder.mkdirs();
        }

        compoundFolder = new File(ktpFolder, COMPOUND_FOLDER);
        if (!compoundFolder.exists()){
            compoundFolder.mkdirs();
        }

        seizureFolder = new File(ktpFolder, SEIZURE_FOLDER);

        if (!seizureFolder.exists()){
            seizureFolder.mkdirs();

        }

        docFolder = new File(ktpFolder, DOC_FOLDER);
        if (!docFolder.exists()){
            docFolder.mkdirs();

        }

        internalReportFolder = new File(ktpFolder, INTERNAL_REPORT_FOLDER);
        if (!internalReportFolder.exists()){
            internalReportFolder.mkdirs();
        }

    }

    public static FileHelper getInstance() {
        return ourInstance;
    }

    private FileHelper(Context context) {
        this.context = context;
    }

    public File getCompoundDir() {
        if (!compoundFolder.exists()){
            initial(context);
        }
        return compoundFolder;
    }

    public File getSeizureDir() {
        if (!seizureFolder.exists()){
            initial(context);
        }
        return seizureFolder;
    }

    public File getDocumentDir() {
        if (!docFolder.exists()){
            initial(context);
        }
        return docFolder;
    }

    public File getInternalReportDir() {
        if (!internalReportFolder.exists()){
            initial(context);
        }
        return internalReportFolder;
    }


    public String compressImage(String filePath) {

        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float maxHeight = 512.0f;
        float maxWidth = 512.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filePath);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filePath;

    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


}
