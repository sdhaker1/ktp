package br.skymapglobal.ktp_challan.asynctasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

import br.skymapglobal.ktp_challan.MainActivity;
import br.skymapglobal.ktp_challan.VolleySingleton;
import br.skymapglobal.ktp_challan.helpers.ApplicationConstants;
import br.skymapglobal.ktp_challan.helpers.CallbackInterface;

/**
 * Created by saurabh on 5/12/14.
 */
public class CallWebServiceAsyncTask {
    Context context;
    String wsMethod;
    String urlParams;
    long starttime=0;
    long endtime=0;
    int type;
    ProgressDialog progressDialog;
    private CallbackInterface mCallback;

    public void setListener(CallbackInterface callBack) {
        mCallback = callBack;
    }

    public CallWebServiceAsyncTask(Context context, String wsMethod, String urlParams, int type) {
        this.context = context;
        this.wsMethod = wsMethod;
        if(wsMethod.equalsIgnoreCase(ApplicationConstants.TIME)) {
           // ApplicationConstants.TIME_ANALYST= "http://54.88.119.188:8000/ktp_analytics?";
        }else{
            starttime=(new Date()).getTime();
            ApplicationConstants.TIME_ANALYST+=("&"+this.wsMethod+"_start_time="+starttime+"");
        }
        this.urlParams = urlParams;
        this.type = type;
        getData();
    }

    public void getData() {

        HttpsTrustManager.allowAllSSL();
        String url = null;
        try {
            urlParams = urlParams.replace("(", URLEncoder.encode("(", "UTF-8"));
            urlParams = urlParams.replace(")", URLEncoder.encode(")", "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (wsMethod.equals(ApplicationConstants.DL_LIST_METHOD)) {
            url = ApplicationConstants.BASE_URL_SKYMAP + "" + wsMethod + "" + urlParams;
        } else if(wsMethod.equalsIgnoreCase(ApplicationConstants.TIME)){
            url=ApplicationConstants.TIME_ANALYST;
        }else {
            url = ApplicationConstants.BASE_URL + "" + wsMethod + "" + urlParams;
        }
        url = url.replace(" ", "%20");
        Log.e("URL==", "curr=" + url);

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait....");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            if(progressDialog!=null){
                                progressDialog.dismiss();
                            }
                            if(wsMethod.equalsIgnoreCase(ApplicationConstants.TIME)) {
                                ApplicationConstants.TIME_ANALYST= "http://54.88.119.188:8000/ktp_analytics?";
                            }else{
                                endtime=(new Date()).getTime();
                                long executintime=endtime-starttime;
                                ApplicationConstants.TIME_ANALYST+=("&"+wsMethod+"_end_time="+endtime+"&"+wsMethod+"_exc_time="+executintime);
                            }
                            if(mCallback!=null){
                                mCallback.refreshList(response, wsMethod, type);
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (!wsMethod.equals(ApplicationConstants.SubsequentList_Skymap_METHOD)) {
                            progressDialog.dismiss();
                        }
                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        Log.v("error", "" + error);
                    }
                }) {

        };

//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
//                45000,
//                0,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, 0, 0));

        // Adding request to request queue
        //AppController.getInstance().addToRequestQueue(stringRequest);
        VolleySingleton.getInstance(context).add(stringRequest);
    }

}
