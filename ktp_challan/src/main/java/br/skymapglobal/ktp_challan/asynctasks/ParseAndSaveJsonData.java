package br.skymapglobal.ktp_challan.asynctasks;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;

import br.skymapglobal.ktp_challan.models.Vehicle_info;
import br.skymapglobal.ktp_challan.helpers.ApplicationConstants;

/**
 * Created by saurabh on 6/12/14.
 */
public class ParseAndSaveJsonData {

    private Context context;
    private String parseString;
    private String returnString = "";
    public static ArrayList<Vehicle_info> roomLists = new ArrayList<>();

    public ParseAndSaveJsonData(Context context, String parseString) {

        this.context = context;
        this.parseString = parseString;
    }

    public ArrayList<Object> parseRoomList() {
        roomLists.clear();
        ArrayList<Object> objectsArrayList = new ArrayList<Object>();
        try {

//            JSONArray jsonArray = new JSONArray(this.parseString);
//            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = new JSONObject(this.parseString);
                Vehicle_info roomList = new Vehicle_info();
                roomList.setVech_type(jsonObject.getString("vech_type"));
                roomList.setTotal_no_case(jsonObject.getString("total_no_case"));
                roomList.setOffence_no(jsonObject.getString("offence_no"));
                roomList.setVehicle_rta(jsonObject.getString("vehicle_rta"));
                roomList.setOwner_name(jsonObject.getString("owner_name"));
                roomList.setOwnerf_name(jsonObject.getString("ownerf_name"));
                roomList.setOwner_address(jsonObject.getString("owner_address"));

                roomLists.add(roomList);
                objectsArrayList.add(roomList);
//            }
            returnString = ApplicationConstants.SUCCESS + "~Get Room list Successfully !";
            objectsArrayList.add(0, returnString);

        } catch (Exception e) {
            Log.e("ParseAndSaveJsonData parseRoomList", e.toString());
            returnString = "Failed to request Room List, retry again!";
            objectsArrayList.add(returnString);
        }
        return objectsArrayList;
    }



}

