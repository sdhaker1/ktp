package br.skymapglobal.ktp_challan.fragment.internalreport;

import android.support.annotation.NonNull;
import android.view.View;

import br.skymapglobal.ktp_challan.R;
import br.skymapglobal.ktp_challan.helpers.BucketConstants;


public class SignalFragment extends BaseInternalReport {

    @NonNull
    @Override
    String getBucketName() {
        return BucketConstants.REPORT_SIGNAL;
    }

    @NonNull
    @Override
    String getTableName() {
        return "tblSignal";
    }

    @Override
    void initialView(View view) {
        view.findViewById(R.id.select_type_view).setVisibility(View.GONE);
        view.findViewById(R.id.title_view).setVisibility(View.GONE);
    }

    @Override
    String getTypeSelected() {
        return null;
    }


}
