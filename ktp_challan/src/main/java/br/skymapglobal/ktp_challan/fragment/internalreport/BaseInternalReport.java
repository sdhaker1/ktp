package br.skymapglobal.ktp_challan.fragment.internalreport;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import br.skymapglobal.ktp_challan.R;
import br.skymapglobal.ktp_challan.asynctasks.CallWebServiceAsyncTask;
import br.skymapglobal.ktp_challan.fragment.BaseFragment;
import br.skymapglobal.ktp_challan.helpers.AppEngineService;
import br.skymapglobal.ktp_challan.helpers.ApplicationConstants;
import br.skymapglobal.ktp_challan.helpers.BucketConstants;
import br.skymapglobal.ktp_challan.helpers.CallbackInterface;
import br.skymapglobal.ktp_challan.helpers.CloudStorage;
import br.skymapglobal.ktp_challan.helpers.FileHelper;
import br.skymapglobal.ktp_challan.helpers.Utils;
import br.skymapglobal.ktp_challan.models.ReportModels;

public abstract class BaseInternalReport extends BaseFragment implements View.OnClickListener, CallbackInterface {

    private ImageView previewImage;
    private AutoCompleteTextView gaurd_code_ac;
    private EditText beat_et;
    private EditText current_loc_et;
    private AppCompatSpinner sp_selectType;
    private File fileUri;
    private Uri dirUri;
    private String bucketName;
    private EditText edtDescription;
    public ReportModels reportModels;


    @NonNull
    abstract String getBucketName();

    @NonNull
    abstract String getTableName();

    abstract void initialView(View view);

    abstract String getTypeSelected();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bucketName = getBucketName();
        reportModels = new ReportModels();
        reportModels.tableName = getTableName();
        reportModels.userId = Utils.getStringPreferences(context, "USERNAME", "");
        reportModels.status = "pending";
        reportModels.active = true;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_internal_report, container,
                false);
        gaurd_code_ac = (AutoCompleteTextView) view.findViewById(R.id.gaurd_code_ac);
        beat_et = (EditText) view.findViewById(R.id.beat_et);
        edtDescription = (EditText) view.findViewById(R.id.edt_desciption);
        current_loc_et = (EditText) view.findViewById(R.id.edt_location);
        previewImage = (ImageView) view.findViewById(R.id.image_preview);
        sp_selectType = (AppCompatSpinner) view.findViewById(R.id.sp_select_type);

        view.findViewById(R.id.ivSearchBeatInfo).setOnClickListener(this);
        view.findViewById(R.id.btn_upload_photo).setOnClickListener(this);
        view.findViewById(R.id.btn_submit).setOnClickListener(this);
        view.findViewById(R.id.btn_cancel).setOnClickListener(this);

        gaurd_code_ac.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        final ArrayAdapter<String> guard_adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.guard_type_array));


        gaurd_code_ac.setAdapter(guard_adapter);

        if (!Utils.getStringPreferences(context, "Guard_Code", "").equals("")) {
            gaurd_code_ac.setText(Utils.getStringPreferences(context, "Guard_Code", ""));
        }


        if (gaurd_code_ac.getText().toString().equals("")) {
            request_For_Guard_code();
        }

        if (!Utils.getStringPreferences(context, "BEAT_NO", "").equals("")) {
            beat_et.setText(Utils.getStringPreferences(context, "BEAT_NO", ""));
        }
        gaurd_code_ac.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showConformationGuardChangedDialog(position);
            }
        });

        if (!gaurd_code_ac.getText().toString().equals("") && !beat_et.getText().toString().equals("")) {
            get_Location_From_Server();
        }

        initialView(view);
        return view;
    }

    private void captureImage() {
        FileHelper.initial(getContext());
        String image_name = "image_001.png";

        Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File imagesFolder = FileHelper.getInstance().getInternalReportDir();

        dirUri = Uri.fromFile(imagesFolder);

        fileUri = new File(imagesFolder, image_name);

        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileUri));
        startActivityForResult(imageIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:

                if (resultCode == -1) {
                    previewCapturedImage(0);

                } else if (resultCode == 0) {
                    Toast.makeText(context,
                            "User cancelled image capture", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Toast.makeText(context,
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();
                }
                break;


        }
    }

    private void previewCapturedImage(int type) {
        if (type == 0) {
            //Capture
            if (fileUri == null) {
                Toast.makeText(context, "Error save image, retry", Toast.LENGTH_LONG).show();
                return;
            }

            FileHelper.getInstance().compressImage(fileUri.getPath());

        } else if (type == 1) {
            return;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                options);
        previewImage.setImageBitmap(bitmap);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivSearchBeatInfo:
                if (!beat_et.getText().toString().equals("")) {
                    request_Checking_Beat_No();
                }
                break;
            case R.id.btn_upload_photo:
                if (isGrantedPermission())
                    captureImage();
                break;
            case R.id.btn_submit:
                if (enableGPS() && canGetLocation()) {
                    attemptSubmitProcess();
                }


                break;
            case R.id.btn_cancel:
//                beat_et.setText("");
                if (fileUri != null) {
                    File file = new File(fileUri.getPath());
                    if (file.exists()) {
                        file.deleteOnExit();
                    }
                    fileUri = null;
                }

                edtDescription.setText("");
//                current_loc_et.setText("");

                previewImage.setImageDrawable(getResources().getDrawable(R.drawable.no_image));
                break;
        }
    }

    private void attemptSubmitProcess() {
        boolean isReady = true;
        if (location == null) {
            Toast.makeText(getActivity(), "Please wait load location", Toast.LENGTH_SHORT).show();
            return;
        }
        if (bucketName == null)
            return;

        if (TextUtils.isEmpty(edtDescription.getText().toString())) {
            edtDescription.setError("Field require!");
            isReady = false;
        }
        if (TextUtils.isEmpty(edtDescription.getText().toString())) {
            edtDescription.setError("Field require!");
            isReady = false;
        }
        if (TextUtils.isEmpty(gaurd_code_ac.getText().toString())) {
            gaurd_code_ac.setError("Field require!");
            isReady = false;
        }

        if (TextUtils.isEmpty(beat_et.getText().toString())) {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());
            if (location != null) {
                try {
                    addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    Address add = addresses.get(0);
                    current_loc_et.setText(add.getAddressLine(0) + " - " + add.getAddressLine(1) + " - " + add.getAddressLine(2));
                } catch (IOException e) {
                    isReady = false;
                    e.printStackTrace();
                }

            } else {
                isReady = false;
                beat_et.setError("Field require!");

            }
        }

        if (TextUtils.isEmpty(current_loc_et.getText().toString())) {
            Toast.makeText(getActivity(), "Beat code invalid!", Toast.LENGTH_SHORT).show();
            isReady = false;
        }
        if (fileUri == null) {
            Toast.makeText(getActivity(), "Please capture image!", Toast.LENGTH_SHORT).show();
            isReady = false;
        }

        if (isReady) {
            Utils.saveStringPreferences(context, "BEAT_NO", beat_et.getText().toString());
            reportModels.description = edtDescription.getText().toString();
            reportModels.guardCode = gaurd_code_ac.getText().toString();
            reportModels.beatNo = beat_et.getText().toString();
            reportModels.longitude = "" + location.getLongitude();
            reportModels.latitude = "" + location.getLatitude();
            reportModels.type = getTypeSelected();
            reportModels.place = current_loc_et.getText().toString();
            if (TextUtils.isEmpty(reportModels.beatNo)) {
                reportModels.beatNo = "null";
            }
            if (TextUtils.isEmpty(reportModels.type)) {
                if (bucketName.equals(BucketConstants.REPORT_OTHERS)) {
                    return;
                } else {
                    reportModels.type = "null";
                }
            }

            new CreateReportAsyncTask().execute(reportModels);
        }


    }

    @Override
    public void refreshList(String response, String method, int type) {
        if (method.equals(ApplicationConstants.GET_GUARD_METHOD)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1) && (jsonObject.getString("msg").equals("Success"))) {
                    gaurd_code_ac.setText(jsonObject.getString("Guard_Code"));
                    Utils.saveStringPreferences(context, "Guard_Code", jsonObject.getString("Guard_Code"));
                }
            } catch (Exception e) {
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }

        if (method.equals(ApplicationConstants.BEAT_CHECKING_METHOD)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1) && (jsonObject.getString("msg").equals("Success"))) {
//                    if(current_loc_et.getText().equals("")) {
                    get_Location_From_Server();
//                    }
                } else {
                    beat_et.setText("");
                    Toast.makeText(context, "You have entered wrong beat number", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (method.equals(ApplicationConstants.GET_PLACE_METHOD)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1) && (jsonObject.getString("msg").equals("Success"))) {
                    String PLACE_NAME = jsonObject.getString("PLACE_NAME");
                    current_loc_et.setText(PLACE_NAME);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void request_Checking_Beat_No() {
        String v_no = beat_et.getText().toString();
        String reg_device_id = br.skymapglobal.ktp_challan.helpers.Utils.getIMEI(context);
        String reg_device_type = "A";
        new CallWebServiceAsyncTask(context, ApplicationConstants.BEAT_CHECKING_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.BEAT_NO_KEY + "=" + v_no + "&" + ApplicationConstants.PP_CD + "=" + gaurd_code_ac.getText().toString(), 0).setListener(BaseInternalReport.this);
    }

    public void request_For_Guard_code() {
        String reg_device_id = br.skymapglobal.ktp_challan.helpers.Utils.getIMEI(context);
        String reg_device_type = "A";
        String PP_CD = br.skymapglobal.ktp_challan.helpers.Utils.getStringPreferences(context, "USERNAME", "").toUpperCase();
        new CallWebServiceAsyncTask(context, ApplicationConstants.GET_GUARD_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.PP_CD + "=" + PP_CD, 0).setListener(BaseInternalReport.this);
    }


    public void get_Location_From_Server() {
        String v_no = beat_et.getText().toString();
        String reg_device_id = br.skymapglobal.ktp_challan.helpers.Utils.getIMEI(context);
        String reg_device_type = "A";
        new CallWebServiceAsyncTask(context, ApplicationConstants.GET_PLACE_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.PP_CD + "=" + gaurd_code_ac.getText().toString() + "&" + ApplicationConstants.BEAT_NO + "=" + v_no, 0).setListener(BaseInternalReport.this);
    }

    public void showConformationGuardChangedDialog(final int msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to change guard code.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        gaurd_code_ac.setSelection(msg);
                        br.skymapglobal.ktp_challan.helpers.Utils.saveStringPreferences(context, "Guard_Code", gaurd_code_ac.getText().toString());
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        gaurd_code_ac.setText(br.skymapglobal.ktp_challan.helpers.Utils.getStringPreferences(context, "Guard_Code", ""));
                        dialog.dismiss();
                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Information");
        alert.show();
    }


    class CreateReportAsyncTask extends AsyncTask<ReportModels, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(ReportModels... params) {
            try {
                ReportModels models = params[0];
                return AppEngineService.getInstance().myApiService.createReport(models.tableName, models.description, models.userId, models.beatNo, models.guardCode, models.status, models.active, models.longitude, models.latitude, models.type, models.place).execute().getData();
            } catch (IOException e) {
                return e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.hide();
            try {
                Integer.parseInt(result);
                String caseNo = result;
                new UploadFileAsyncTask().execute(caseNo);

            } catch (Exception e) {
                Toast.makeText(context, "Error " + result, Toast.LENGTH_SHORT).show();

            }
        }


    }

    class UploadFileAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String caseNo = params[0];
                CloudStorage.shareInstance().uploadFileImage(bucketName, fileUri, caseNo);
                return caseNo;

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();

            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.hide();
            if (result != null) {
                showAlertDialog("Information", "Report has been successfully generated vide case no is " + result);
                edtDescription.setText("");
                if (fileUri.exists()) {
                    fileUri.delete();
                }
                previewImage.setImageDrawable(getResources().getDrawable(R.drawable.no_image));
            } else {
                showAlertDialog("Error", "Upload File Error");
            }

        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (enableGPS() && canGetLocation()) {
                    attemptSubmitProcess();
                }

            } else {
                requestLocationPermission();
            }
        }
    }


}
