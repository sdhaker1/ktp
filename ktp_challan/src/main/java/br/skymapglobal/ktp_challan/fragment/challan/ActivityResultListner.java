package br.skymapglobal.ktp_challan.fragment.challan;

import android.content.Intent;

/**
 * Created by Admin on 26-04-2017.
 */

public interface ActivityResultListner {

    void onResult(int requestCode, int resultCode, Intent data);
}
