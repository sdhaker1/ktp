package br.skymapglobal.ktp_challan.fragment.internalreport;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.ArrayAdapter;

import br.skymapglobal.ktp_challan.R;
import br.skymapglobal.ktp_challan.helpers.BucketConstants;

public class ViolationFragment extends BaseInternalReport {

    @NonNull
    @Override
    String getBucketName() {
        return BucketConstants.REPORT_VOILATION;
    }

    @NonNull
    @Override
    String getTableName() {
        return "tblViolation";
    }

    @Override
    void initialView(View view) {
        view.findViewById(R.id.select_type_view).setVisibility(View.VISIBLE);
        view.findViewById(R.id.title_view).setVisibility(View.GONE);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.violation_type_array));
        AppCompatSpinner spinner = (AppCompatSpinner) view.findViewById(R.id.sp_select_type);
        spinner.setAdapter(adapter);
    }

    @Override
    String getTypeSelected() {
        AppCompatSpinner spinner = (AppCompatSpinner) getView().findViewById(R.id.sp_select_type);
        return getResources().getStringArray(R.array.violation_type_array)[spinner.getSelectedItemPosition()];
    }
}
