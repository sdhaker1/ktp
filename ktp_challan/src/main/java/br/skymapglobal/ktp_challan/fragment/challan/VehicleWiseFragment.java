package br.skymapglobal.ktp_challan.fragment.challan;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;

import br.skymapglobal.ktp_challan.ProsecutionCaseDetailActivity;
import br.skymapglobal.ktp_challan.R;
import br.skymapglobal.ktp_challan.adapter.DividerItemDecoration;
import br.skymapglobal.ktp_challan.adapter.ItemModel;
import br.skymapglobal.ktp_challan.adapter.RecyclerViewType;
import br.skymapglobal.ktp_challan.adapter.VehicleWisePendingRecyclerViewAdapter;
import br.skymapglobal.ktp_challan.asynctasks.CallWebServiceAsyncTask;
import br.skymapglobal.ktp_challan.helpers.ApplicationConstants;
import br.skymapglobal.ktp_challan.helpers.CallbackInterface;
import br.skymapglobal.ktp_challan.helpers.IVehicleWisePendingListener;
import br.skymapglobal.ktp_challan.helpers.Utils;
import br.skymapglobal.ktp_challan.models.ResponseCaseCountList;
import br.skymapglobal.ktp_challan.models.entity.CaseCountInfo;

public class VehicleWiseFragment extends Fragment implements CallbackInterface, IVehicleWisePendingListener {


    private IVehicleWisePendingListener mListener;
    private ArrayList<ItemModel> listModels;
    private VehicleWisePendingRecyclerViewAdapter adapter;
    private EditText edtVehicleNo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listModels = new ArrayList<>();
//        listModels.add(new ItemModel(RecyclerViewType.HEADER, null, null));
        adapter = new VehicleWisePendingRecyclerViewAdapter(listModels, mListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vehicle_wise_pending, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list_total_pending);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        edtVehicleNo = (EditText) view.findViewById(R.id.edt_vehicle_no);
        view.findViewById(R.id.btn_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtVehicleNo.setError(null);
                String vehicleNo = edtVehicleNo.getText().toString();
                if (TextUtils.isEmpty(vehicleNo)) {
                    edtVehicleNo.setError("Field require");
                } else {
                    getCaseCountListRequest(vehicleNo);
                }
            }
        });
        edtVehicleNo.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = this;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void getCaseCountListRequest(String vehicleNo) {
        String param = "&reg_device_id=%s&reg_device_type=%s&Vech_No=%s";
        String reg_device_id = Utils.getIMEI(getActivity());
        String reg_device_type = "A";
        String PP_CD = br.skymapglobal.ktp_challan.helpers.Utils.getStringPreferences(getActivity(), "USERNAME", "");

//        reg_device_id = "355240072233698";
        param = String.format(param, reg_device_id, reg_device_type, vehicleNo);
        new CallWebServiceAsyncTask(getActivity(), ApplicationConstants.GET_CASE_COUNT_METHOD, param, 0).setListener(this);
    }

    @Override
    public void refreshList(String response, String method, int type) {
        if (type == 0 && method.equals(ApplicationConstants.GET_CASE_COUNT_METHOD)) {

            ResponseCaseCountList responseCaseCountList = ResponseCaseCountList.parseFromJSON(response);
            if (responseCaseCountList == null) {
                showAlert("", "Data Empty");
                listModels = new ArrayList<>();
//                listModels.add(new ItemModel(RecyclerViewType.HEADER, null, null));
                adapter.setItems(listModels);
                adapter.notifyDataSetChanged();
                return;
            }
            if (responseCaseCountList.status == 1) {
                if (responseCaseCountList.data != null) {
                    listModels = new ArrayList<>();
                    listModels.add(new ItemModel(RecyclerViewType.HEADER, null, null));
                    for (CaseCountInfo caseCountInfo : responseCaseCountList.data) {
                        ItemModel item = new ItemModel(RecyclerViewType.ITEM_1, null, caseCountInfo);
                        listModels.add(item);

                    }

                    adapter.setItems(listModels);
                    adapter.notifyDataSetChanged();
                }

            } else {
                showAlert("Error", responseCaseCountList.msg);
            }
        }
    }

    void showAlert(String title, String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(msg);
        alertDialog.setTitle(title);
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alertDialog.create().show();
    }

    @Override
    public void onItemClick(CaseCountInfo item) {

        Intent intent = new Intent(getActivity(), ProsecutionCaseDetailActivity.class);
        intent.putExtra(ProsecutionCaseDetailActivity.ARG_TITLE, item.type);
        intent.putExtra(ProsecutionCaseDetailActivity.ARG_CASE, item.type);
        getActivity().startActivity(intent);
//        Toast.makeText(getContext(), item.TOTAL_CASE, Toast.LENGTH_LONG).show();
    }

}
