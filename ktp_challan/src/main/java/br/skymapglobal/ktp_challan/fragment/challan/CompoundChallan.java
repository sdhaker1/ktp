package br.skymapglobal.ktp_challan.fragment.challan;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import br.skymapglobal.ktp_challan.MainActivity;
import br.skymapglobal.ktp_challan.R;
import br.skymapglobal.ktp_challan.asynctasks.CallWebServiceAsyncTask;
import br.skymapglobal.ktp_challan.fragment.ViewPagerFragment;
import br.skymapglobal.ktp_challan.helpers.ApplicationConstants;
import br.skymapglobal.ktp_challan.helpers.CallbackInterface;
import br.skymapglobal.ktp_challan.helpers.EnglishNumberToWords;
import br.skymapglobal.ktp_challan.helpers.PDFHelper;
import br.skymapglobal.ktp_challan.helpers.Utils;
import br.skymapglobal.ktp_challan.models.DL_info;
import br.skymapglobal.ktp_challan.models.Licence_Info_Table;
import br.skymapglobal.ktp_challan.models.OffenceWithVehicle;
import br.skymapglobal.ktp_challan.models.SpotChallanInfo;
import br.skymapglobal.ktp_challan.models.Vehicle_info;

public class CompoundChallan extends Fragment implements View.OnClickListener, CallbackInterface {

    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String TAG = "drive-quickstart";
    private static final int REQUEST_CODE_CAPTURE_IMAGE = 1;
    private static final int REQUEST_CODE_CREATOR = 2;
    private static final int REQUEST_CODE_RESOLUTION = 3;
    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "Hello Camera";
    public static Bitmap bitmap;
    public static String offence_no;
    public static int fine;
    public static String section;
    public static DL_info licence_info_table = new DL_info();
    public static EditText phn_et, beat_et, email_et, current_loc_et;
    public static Vehicle_info vehicle_info = new Vehicle_info();

    public static EditText vehicle_no_et;
    // Activity request codes
    private final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    ArrayList<String> options = new ArrayList<String>();
    int amount;
    private GoogleApiClient mGoogleApiClient;
    private Uri fileUri; // file url to store image/video
    private int num, value;
    private Context mContext;
    private RadioGroup radioGroup, radioGroup_type;
    private LinearLayout linearLayout;
    private Button upload_doc_photo_btn;
    //    private Spinner offence_type_spinner;
    private ImageView imgPreview, doc_imgPreview;
    private TextView upload_photo_tv;
    private Spinner document_type_spinner, vehicle_type_spinner;
    private LinearLayout on_spot_ll, pending_view_ll, info_ll;
    private EditText licence_num;
    private AppCompatButton create_challan_btn, submit_btn;
    private LinearLayout driven_by_other_btn, driven_by_owner_btn;
    private TextView DL_status_tv, name_tv, add_tv, issue_date_tv, expiry_date_tv, vehicle_class_tv, uuid_tv, owner_add_tv;
    private EditText spot_phone, reg_number_tv, vehicle_type_tv, owner_name_tv, document_num_et, case_no_et;
    public static String Fine_Amt_1, Fine_Amt_2, Fine_Amt_3, Fine_Amt_4, Fine_Amt_5;
    private String COMP_CASE_DATE, FINE_AMOUNT, ACCUSED_PERSON, GUARD_NAME, VECH_NO, RC_NO, CASE_NO;
    private TextView acc_tv, rc_no_tv, vehicle_tv, fine_tv, case_tv, amt_tv;
    private ImageView ivSearchVehicleInfo, ivSearchDlInfo, ivSearchBeatInfo, ivSearchcaseno;
    private AutoCompleteTextView actv, gaurd_code_ac;
    private boolean isDoc_type, isError, isFound;
    int mYear;
    int mMonth;
    int mDay;
    String my_var;
    String mGuardCode="";
    boolean isGuardChangedFromList=false;
    boolean isAlertShowFromEdit=false;
    public ArrayList<OffenceWithVehicle> mOffenceWithVehicleList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        mContext = getActivity();
        View view = inflater.inflate(R.layout.upload_challan, container,false);
        final Calendar newCalendar = Calendar.getInstance();
        mYear = newCalendar.get(Calendar.YEAR);
        mMonth = newCalendar.get(Calendar.MONTH);
        mDay = newCalendar.get(Calendar.DAY_OF_MONTH);
        phn_et = (EditText) view.findViewById(R.id.phn_et);
        add_tv = (TextView) view.findViewById(R.id.add_tv);
        name_tv = (TextView) view.findViewById(R.id.name_tv);
        uuid_tv = (TextView) view.findViewById(R.id.uuid_tv);
        email_et = (EditText) view.findViewById(R.id.email_et);
        imgPreview = (ImageView) view.findViewById(R.id.imgPreview);
        radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        radioGroup_type = (RadioGroup) view.findViewById(R.id.radioGroup_type);
        licence_num = (EditText) view.findViewById(R.id.licence_num);
        owner_add_tv = (TextView) view.findViewById(R.id.owner_add_tv);
        DL_status_tv = (TextView) view.findViewById(R.id.DL_status_tv);
        vehicle_no_et = (EditText) view.findViewById(R.id.vehicle_no_et);
        owner_name_tv = (EditText) view.findViewById(R.id.owner_name_tv);
        issue_date_tv = (EditText) view.findViewById(R.id.issue_date_tv);
        reg_number_tv = (EditText) view.findViewById(R.id.reg_number_tv);
        beat_et = (EditText) view.findViewById(R.id.beat_et);
        linearLayout = (LinearLayout) view.findViewById(R.id.other_doc_ll);
        current_loc_et = (EditText) view.findViewById(R.id.current_loc_et);
        expiry_date_tv = (TextView) view.findViewById(R.id.expiry_date_tv);
        doc_imgPreview = (ImageView) view.findViewById(R.id.doc_imgPreview);
        vehicle_type_tv = (EditText) view.findViewById(R.id.vehicle_type_tv);
        spot_phone = (EditText) view.findViewById(R.id.input_phone);
        vehicle_type_spinner = (Spinner) view.findViewById(R.id.vehicle_type_spinner);
        upload_photo_tv = (TextView) view.findViewById(R.id.upload_photo_tv);
//        vehicle_model_tv = (EditText) view.findViewById(R.id.vehicle_model_tv);
        vehicle_class_tv = (TextView) view.findViewById(R.id.vehicle_class_tv);
        amt_tv = (TextView) view.findViewById(R.id.amt_tv);
//        vehicle_color_tv = (EditText) view.findViewById(R.id.vehicle_color_tv);
        create_challan_btn = (AppCompatButton) view.findViewById(R.id.create_challan_btn);
        driven_by_owner_btn = (LinearLayout) view.findViewById(R.id.driven_by_owner_btn);
        driven_by_other_btn = (LinearLayout) view.findViewById(R.id.driven_by_other_btn);
        upload_doc_photo_btn = (Button) view.findViewById(R.id.upload_doc_photo_btn);
//        offence_type_spinner = (Spinner) view.findViewById(R.id.offence_type_spinner);
        document_num_et = (EditText) view.findViewById(R.id.document_num_et);
        document_type_spinner = (Spinner) view.findViewById(R.id.document_type_spinner);
        on_spot_ll = (LinearLayout) view.findViewById(R.id.on_spot);
        pending_view_ll = (LinearLayout) view.findViewById(R.id.pending_view);
        submit_btn = (AppCompatButton) view.findViewById(R.id.submit_btn);
//        print_btn_btn= (LinearLayout) view.findViewById(R.id.print_btn_btn);
        case_no_et = (EditText) view.findViewById(R.id.case_no_et);
        info_ll = (LinearLayout) view.findViewById(R.id.info_ll);

        acc_tv = (TextView) view.findViewById(R.id.acc_tv);
        rc_no_tv = (TextView) view.findViewById(R.id.rc_no_tv);
        vehicle_tv = (TextView) view.findViewById(R.id.vehicle_tv);
        fine_tv = (TextView) view.findViewById(R.id.amt_tv);
        case_tv = (TextView) view.findViewById(R.id.case_no_tv);
        ivSearchcaseno = (ImageView) view.findViewById(R.id.ivSearchcaseno);
        ivSearchVehicleInfo = (ImageView) view.findViewById(R.id.ivSearchVehicleInfo);
        ivSearchDlInfo = (ImageView) view.findViewById(R.id.ivSearchDlInfo);
        ivSearchBeatInfo = (ImageView) view.findViewById(R.id.ivSearchBeatInfo);
//        submit_tv= (TextView) view.findViewById(R.id.submit_tv);
        actv = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView1);
        gaurd_code_ac = (AutoCompleteTextView) view.findViewById(R.id.gaurd_code_ac);
        beat_et.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        vehicle_no_et.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        licence_num.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        current_loc_et.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        owner_name_tv.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        reg_number_tv.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        vehicle_type_tv.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        document_num_et.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        case_no_et.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        DL_status_tv.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        name_tv.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        add_tv.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        issue_date_tv.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        expiry_date_tv.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        vehicle_class_tv.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        uuid_tv.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        owner_add_tv.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        gaurd_code_ac.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        final ArrayAdapter<String> gaurd_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.guard_type_array));
        gaurd_code_ac.setAdapter(gaurd_adapter);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.section_array));
        actv.setAdapter(adapter);
        actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                my_var = adapter.getItem(position).toString();
            }
        });
        /*if (!br.skymapglobal.ktp_challan.helpers.Utils.getStringPreferences(mContext, "Guard_Code", "").equals("")) {
            gaurd_code_ac.setText(br.skymapglobal.ktp_challan.helpers.Utils.getStringPreferences(mContext, "Guard_Code", ""));
            mGuardCode=gaurd_code_ac.getText().toString().trim();
        }
        if (gaurd_code_ac.getText().toString() == null || gaurd_code_ac.getText().toString().equals("")) {

        }*/
        request_For_Guard_code();

        gaurd_code_ac.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showConformationGuardChangedDialog(position);
            }
        });

        gaurd_code_ac.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String newguardcode=gaurd_code_ac.getText().toString().trim();
                    if(false==newguardcode.equalsIgnoreCase(mGuardCode)){
                        if(true==isGuardChangedFromList){
                            isGuardChangedFromList=false;
                        }else{
                            if(false==isAlertShowFromEdit){
                                showConformationGuardChangedDialog();
                                isAlertShowFromEdit=true;
                            }
                        }
                    }
                }
            }
        });

        spot_phone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {
                    if (spot_phone.getText().toString().trim()
                            .length() < 10) {
                        spot_phone.setError("Phone Number must be of 10 digits.");
                        isError = true;
                    } else {
                        isError = false;
                    }
                }
            }


        });
        phn_et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {
                    if (phn_et.getText().toString().trim()
                            .length() < 10) {
                        phn_et.setError("Phone Number must be of 10 digits.");
                        isError = true;
                    } else {
                        isError = false;
                    }
                }
            }


        });
        /**
         * Unset the var whenever the user types. Validation will
         * then fail. This is how we enforce selecting from the list.
         */

        beat_et.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                current_loc_et.setText("");
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        actv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                my_var = null;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        owner_name_tv.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                vehicle_info.setOwner_name("" + s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        uuid_tv.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                vehicle_info.setOwnerf_name("" + s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        owner_add_tv.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                vehicle_info.setOwner_address("" + s);
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        vehicle_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getSelectedItemPosition() != 0) {
                    vehicle_info.setVech_type(vehicle_type_spinner.getSelectedItem().toString());
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        if (!br.skymapglobal.ktp_challan.helpers.Utils.getStringPreferences(mContext, "BEAT_NO", "").equals("")) {
            beat_et.setText(br.skymapglobal.ktp_challan.helpers.Utils.getStringPreferences(mContext, "BEAT_NO", ""));
        }

        ivSearchcaseno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                request_For_Case_NO();

            }
        });
        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = spot_phone.getText().toString().trim();
                if (TextUtils.isEmpty(phone) && phone.length() < 10) {
                    spot_phone.setError("Phone Number must be of 10 digits.");
                } else {
                    spot_phone.setError(null);
                    spotChallanInfo.MOBILE_NO = phone;
                    save_insert_spot_payment();
                }


            }
        });


        if (!gaurd_code_ac.getText().toString().equals("") && !beat_et.getText().toString().equals("")) {
            get_Location_From_Server();
        }
        driven_by_owner_btn.setOnClickListener(this);
        driven_by_other_btn.setOnClickListener(this);
        create_challan_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean isValidOffence = false;
                String vehicle = "";
                String section_no = "";

                if (actv.getText().toString().trim().length()>0) {
                    section_no = actv.getText().toString().trim();
                }

                if(!isFound){
                    //vehicle not found
                    vehicle=vehicle_type_spinner.getSelectedItem().toString();
                    if(vehicle.trim().equalsIgnoreCase("SELECT")){
                        vehicle="";
                    }
                }else{
                    //vehicle found
                    if(vehicle_info.getVech_type()!=null){
                        vehicle=vehicle_info.getVech_type();
                    }
                }

                if (mOffenceWithVehicleList == null ) {
                    BuilOffenceType();
                }
                if (section_no.trim().length() > 0 && vehicle.trim().length() > 0) {
                    isValidOffence = OffenceTypeValidationWithVehicle(vehicle, section_no);
                    if (true == isValidOffence) {
                        if (!isFound) {
                            request_For_Fine_Amt_Vehicle_Not_Found();
                        } else {
                    /*if(sdsdsd)*/
                            request_For_Subsequent_LIST_UNSER_SECTION();
                        }
                    }else {
                        Toast.makeText(mContext, "Please select valid offence type", Toast.LENGTH_LONG).show();
                    }
                }else{
                    if (section_no.trim().length() <= 0) {
                        Toast.makeText(mContext, "Please provide valid offence type", Toast.LENGTH_LONG).show();
                    }
                    if (vehicle.trim().length() <= 0) {
                        if(isFound){
                            Toast.makeText(mContext, "Please provide valid vehicle type", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(mContext, "Please provide valid vehicle information", Toast.LENGTH_LONG).show();
                        }

                    }
                }
            }
        });
        actv.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
//                    if (!vehicle_no_et.getText().toString().equals("")) {
                    section = actv.getText().toString();
//                        request_For_Subsequent_LIST_UNSER_SECTION();
//                    }
                }
            }
        });

        ivSearchVehicleInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!vehicle_no_et.getText().toString().equals("") && !vehicle_no_et.getText().toString().equals("WB")) {
                    request_For_Subsequent_LIST();
                }
            }
        });

        ivSearchDlInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!beat_et.getText().toString().equals("") && !beat_et.getText().toString().equals("WB")) {
                    request_For_DL_LIST();
                }
            }
        });

        ivSearchBeatInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!beat_et.getText().toString().equals("")) {
                    request_Checking_Beat_No();
                }
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                View radioButton = radioGroup.findViewById(checkedId);
                int index = radioGroup.indexOfChild(radioButton);
                switch (index) {
                    case 0:
                        linearLayout.setVisibility(View.GONE);
                        licence_num.setVisibility(View.VISIBLE);

                        isDoc_type = false;
                        break;
                    case 1:
                        licence_num.setText("");
                        issue_date_tv.setText("");
                        expiry_date_tv.setText("");
                        name_tv.setText("");
                        add_tv.setText("");
                        vehicle_class_tv.setText("");
                        licence_info_table.setIssu_dt("");
                        licence_info_table.setValid_upto("");
                        licence_info_table.setOwner_name("");
                        licence_info_table.setOwner_address("");
                        licence_info_table.setLcns_cat("");
                        licence_num.setVisibility(View.GONE);
                        linearLayout.setVisibility(View.VISIBLE);
//                        SEIZED_DOC_ID = document_num_et.getText().toString();

                        isDoc_type = true;
                        break;
                }
            }
        });

        radioGroup_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                View radioButton = radioGroup_type.findViewById(checkedId);
                int index = radioGroup_type.indexOfChild(radioButton);
                switch (index) {
                    case 0:
                        pending_view_ll.setVisibility(View.VISIBLE);
                        on_spot_ll.setVisibility(View.GONE);
                        break;
                    case 1:
                        pending_view_ll.setVisibility(View.GONE);
                        on_spot_ll.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });


        return view;
    }



    public void showConformationGuardChangedDialog(final int msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Are you sure you want to change guard code.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        gaurd_code_ac.setSelection(msg);
                        Utils.saveStringPreferences(mContext, "Raid_Guard_Code", gaurd_code_ac.getText().toString());
                        mGuardCode=gaurd_code_ac.getText().toString().trim();
                        isGuardChangedFromList=true;
                        current_loc_et.setText("");
                        beat_et.setText("");
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        gaurd_code_ac.setText(Utils.getStringPreferences(mContext, "Guard_Code", ""));
                        dialog.dismiss();
                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Information");
        alert.show();
    }

    public void showConformationGuardChangedDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Are you sure you want to change guard code.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //gaurd_code_ac.setSelection(msg);
                        Utils.saveStringPreferences(mContext, "Raid_Guard_Code", gaurd_code_ac.getText().toString());
                        mGuardCode=gaurd_code_ac.getText().toString().trim();
                        current_loc_et.setText("");
                        beat_et.setText("");
                        isAlertShowFromEdit=false;
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        gaurd_code_ac.setText(Utils.getStringPreferences(mContext, "Guard_Code", ""));
                        isAlertShowFromEdit=false;
                        dialog.dismiss();
                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Information");
        alert.show();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.driven_by_owner_btn:
//                driven_by_other_btn.setBackgroundResource(android.R.drawable.btn_default);
//                driven_by_other_btn.setTextColor(getResources().getColor(R.color.black));
//                driven_by_owner_btn.setTextColor(getResources().getColor(R.color.white));
                driven_by_other_btn.setBackgroundColor(getResources().getColor(R.color.zomato_blue));
                driven_by_owner_btn.setBackgroundColor(getResources().getColor(R.color.translucent_dark));
                break;
            case R.id.driven_by_other_btn:
//                driven_by_owner_btn.setBackgroundResource(android.R.drawable.btn_default);
//                driven_by_owner_btn.setTextColor(getResources().getColor(R.color.black));
//                driven_by_other_btn.setTextColor(getResources().getColor(R.color.white));
                driven_by_other_btn.setBackgroundColor(getResources().getColor(R.color.translucent_dark));
                driven_by_owner_btn.setBackgroundColor(getResources().getColor(R.color.zomato_blue));
                break;
        }
    }

    public void populateDriverInfo(Licence_Info_Table licence_info_table) {
        if (licence_info_table.getSTATUS().equalsIgnoreCase("inactive")) {
            DL_status_tv.setTextColor(getResources().getColor(R.color.colorAccent));
        } else {
            DL_status_tv.setTextColor(getResources().getColor(R.color.green_color));
        }
        add_tv.setText(licence_info_table.getADDRESS());
        name_tv.setText(licence_info_table.getOWNER_NAME());
        issue_date_tv.setText(licence_info_table.getISSUE_DATE());
        expiry_date_tv.setText(licence_info_table.getEXPIRY_DATE());
        vehicle_class_tv.setText(licence_info_table.getVEHICAL_CLASS());
        DL_status_tv.setText(licence_info_table.getSTATUS().toUpperCase());
    }


    public void request_For_DL_LIST() {
        String dl_no = licence_num.getText().toString();
        new CallWebServiceAsyncTask(mContext, ApplicationConstants.DL_LIST_METHOD, "&" + ApplicationConstants.DL_NO + "=" + dl_no, 0).setListener(CompoundChallan.this);
    }

    public void request_For_Subsequent_LIST() {
        String v_no = vehicle_no_et.getText().toString();
        String reg_device_id = br.skymapglobal.ktp_challan.helpers.Utils.getIMEI(mContext);
        String reg_device_type = "A";
        String vio_us_cd = "ALL";

        new CallWebServiceAsyncTask(mContext, ApplicationConstants.SUBEQUENT_LIST_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.VECH_NO + "=" + v_no, 0).setListener(CompoundChallan.this);
    }

    public void request_Checking_Beat_No() {
        String v_no = beat_et.getText().toString();
        String reg_device_id = br.skymapglobal.ktp_challan.helpers.Utils.getIMEI(mContext);
        String reg_device_type = "A";
        //((MainActivity)mContext).TIME_ANALYST;
        new CallWebServiceAsyncTask(mContext, ApplicationConstants.BEAT_CHECKING_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.BEAT_NO_KEY + "=" + v_no + "&" + ApplicationConstants.PP_CD + "=" + gaurd_code_ac.getText().toString(), 0).setListener(CompoundChallan.this);
    }

    public void get_Location_From_Server() {
        String v_no = beat_et.getText().toString();
        String reg_device_id = br.skymapglobal.ktp_challan.helpers.Utils.getIMEI(mContext);
        String reg_device_type = "A";
        new CallWebServiceAsyncTask(mContext, ApplicationConstants.GET_PLACE_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.PP_CD + "=" + gaurd_code_ac.getText().toString() + "&" + ApplicationConstants.BEAT_NO + "=" + v_no, 0).setListener(CompoundChallan.this);
    }

    public void request_For_US_LIST() {
        String reg_device_id = br.skymapglobal.ktp_challan.helpers.Utils.getIMEI(mContext);
        String reg_device_type = "A";
        new CallWebServiceAsyncTask(mContext, ApplicationConstants.US_LIST_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type, 0).setListener(CompoundChallan.this);
    }

    public void request_CASE_COUNT_LIST() {
        String v_no = vehicle_no_et.getText().toString();
        String reg_device_id = br.skymapglobal.ktp_challan.helpers.Utils.getIMEI(mContext);
        String reg_device_type = "A";
        new CallWebServiceAsyncTask(mContext, ApplicationConstants.CASE_COUNT_LIST_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.VECH_NO + "=" + v_no, 0);
    }

    public void save_insert_spot_payment() {
        String reg_device_id = Utils.getIMEI(mContext);
        String reg_device_type = "A";
        String comp_case_no = case_no_et.getText().toString();
        String comp_case_dt = case_tv.getText().toString();
        String spot_fine_amt = amt_tv.getText().toString();
        new CallWebServiceAsyncTask(mContext, ApplicationConstants.INSERT_SPOT_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.COMP_CASE_NO + "=" + comp_case_no + "&" + ApplicationConstants.COMP_CASE_DT + "=" + comp_case_dt + "&" + ApplicationConstants.SPOT_FINE_AMT + "=" + spot_fine_amt, 0).setListener(CompoundChallan.this);
    }

    public void request_For_Subsequent_LIST_UNSER_SECTION() {
        String v_no = vehicle_no_et.getText().toString();
        String reg_device_id = Utils.getIMEI(mContext);
        System.out.println("value of device id : " + reg_device_id);
        String reg_device_type = "A";
        String vio_us_cd = actv.getText().toString();
        String inputvehicleno=v_no;
        String fetchedvehicleno=reg_number_tv.getText().toString();
        if(inputvehicleno.equalsIgnoreCase(fetchedvehicleno)){
            new CallWebServiceAsyncTask(mContext, ApplicationConstants.SubsequentList_Skymap_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.VECH_NO + "=" + v_no + "&" + ApplicationConstants.VIO_US_CD_KEY + "=" + vio_us_cd, 1).setListener(CompoundChallan.this);
        }else{
            Toast.makeText(mContext, "Input vehicle no and Registration vehicle no is not same.Please input valid vehicle no and press search button", Toast.LENGTH_LONG).show();
        }
    }

    SpotChallanInfo spotChallanInfo = new SpotChallanInfo();

    public void request_For_Case_NO() {
        spotChallanInfo = new SpotChallanInfo();
        String case_no = case_no_et.getText().toString();
        String reg_device_id = Utils.getIMEI(mContext);
        String reg_device_type = "A";
        spotChallanInfo.case_no = case_no;
        new CallWebServiceAsyncTask(mContext, ApplicationConstants.GET_PENDING_CASE_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.COMP_CASE_NO + "=" + case_no, 0).setListener(CompoundChallan.this);
    }

    public void request_For_Fine_Amt_Vehicle_Not_Found() {
        String case_no = case_no_et.getText().toString();
        String reg_device_id = br.skymapglobal.ktp_challan.helpers.Utils.getIMEI(mContext);
        this.offence_no = "0";
        String reg_device_type = "A";
        String vech_type_cd = vehicle_type_spinner.getSelectedItem().toString();
        String vio_us_cd = actv.getText().toString();
        /*fdfdfd*/

        new CallWebServiceAsyncTask(mContext, ApplicationConstants.GET_FINE_AMT_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.VIO_US_CD + "=" + vio_us_cd + "&" + ApplicationConstants.VECH_TYPE_CD + "=" + vech_type_cd, 0).setListener(CompoundChallan.this);
    }

    public void request_For_Guard_code() {
        String reg_device_id = br.skymapglobal.ktp_challan.helpers.Utils.getIMEI(mContext);
        String reg_device_type = "A";
        String PP_CD = br.skymapglobal.ktp_challan.helpers.Utils.getStringPreferences(mContext, "USERNAME", "").toUpperCase();
        new CallWebServiceAsyncTask(mContext, ApplicationConstants.GET_GUARD_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.PP_CD + "=" + PP_CD, 0).setListener(CompoundChallan.this);
    }

    @Override
    public void refreshList(String response, String method, int num) {
        System.out.println("response" + response);
        Log.e("response==", response);
        if (method == ApplicationConstants.SUBEQUENT_LIST_METHOD) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((!jsonObject.getString("vech_type").equals("null")) && (!jsonObject.getString("vech_type").equals(null)) && (jsonObject.getString("vech_type") != null) && (!jsonObject.getString("vech_type").equals("")) && !jsonObject.getString("vech_type").isEmpty() && (jsonObject.getString("owner_name") != null) && (!jsonObject.getString("owner_name").equals("")) && !jsonObject.getString("owner_name").isEmpty()) {
                    String vech_type = jsonObject.getString("vech_type");
                    String owner_name = jsonObject.getString("owner_name");
                    String ownerf_name = jsonObject.getString("ownerf_name");
                    String owner_address = jsonObject.getString("owner_address");
                    vehicle_type_tv.setVisibility(View.VISIBLE);
                    vehicle_type_spinner.setVisibility(View.GONE);
                    vehicle_type_tv.setText(vech_type);
                    owner_name_tv.setText(owner_name);
                    uuid_tv.setText(ownerf_name);
                    owner_add_tv.setText(owner_address);
                    vehicle_info.setVech_type(vech_type);
                    vehicle_info.setOwner_name(owner_name);
                    vehicle_info.setOwner_address(owner_address);
                    vehicle_info.setOwnerf_name(ownerf_name);
                    vehicle_type_tv.setEnabled(false);
                    owner_name_tv.setEnabled(false);
                    uuid_tv.setEnabled(false);
                    owner_add_tv.setEnabled(false);
                    reg_number_tv.setText(vehicle_no_et.getText().toString());
                    reg_number_tv.setTextColor(getResources().getColor(R.color.black));
                    vehicle_type_tv.setTextColor(getResources().getColor(R.color.black));
                    owner_name_tv.setTextColor(getResources().getColor(R.color.black));
                    uuid_tv.setTextColor(getResources().getColor(R.color.black));
                    owner_add_tv.setTextColor(getResources().getColor(R.color.black));
                    isFound = true;
                } else {
                    reg_number_tv.setTextColor(getResources().getColor(R.color.black));
                    reg_number_tv.setText(vehicle_no_et.getText().toString());
                    vehicle_type_tv.setFocusable(true);
                    vehicle_type_tv.setFocusableInTouchMode(true);
                    vehicle_type_tv.setClickable(true);
                    vehicle_type_tv.setEnabled(true);
                    vehicle_type_tv.setText("");
                    vehicle_type_tv.setVisibility(View.GONE);
                    vehicle_type_spinner.setVisibility(View.VISIBLE);

                    owner_name_tv.setFocusable(true);
                    owner_name_tv.setFocusableInTouchMode(true);
                    owner_name_tv.setClickable(true);
                    owner_name_tv.setEnabled(true);
                    owner_name_tv.setText("");

                    owner_add_tv.setFocusable(true);
                    owner_add_tv.setFocusableInTouchMode(true);
                    owner_add_tv.setClickable(true);
                    owner_add_tv.setEnabled(true);
                    owner_add_tv.setText("");
                    uuid_tv.setFocusable(true);
                    uuid_tv.setFocusableInTouchMode(true);
                    uuid_tv.setClickable(true);
                    uuid_tv.setEnabled(true);
                    uuid_tv.setText("");
                    isFound = false;
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        if (method == ApplicationConstants.INSERT_SPOT_METHOD) {
            System.out.println("response=" + response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1)) {
                    final String msg = jsonObject.getString("msg");
                    spotChallanInfo.case_id = msg;
                    spotChallanInfo.oname = jsonObject.getString("oname");
                    spotChallanInfo.guard = jsonObject.getString("Guard");

                    new AsyncTask<SpotChallanInfo, Void, String>() {
                        ProgressDialog dialog;

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            dialog = new ProgressDialog(mContext);
                            dialog.setCancelable(false);
                            dialog.setMessage("Please wait...");
                            dialog.show();
                        }

                        @Override
                        protected String doInBackground(SpotChallanInfo... spotsInfo) {
                            return PDFHelper.getInstance().generateSpotFile(spotsInfo[0]) ? "Successfully" : null;
                        }

                        @Override
                        protected void onPostExecute(String result) {
                            super.onPostExecute(result);
                            dialog.dismiss();
                            showOnSpotDialog(msg);

                            if (result == null) {
                                Toast.makeText(mContext, "Error Upload PDF File", Toast.LENGTH_LONG).show();
                            }


                        }
                    }.execute(spotChallanInfo);

//                    String msg=jsonObject.getString("msg");
                } else {
                    showOnPaymentDialog(jsonObject.getString("msg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (method == ApplicationConstants.GET_PLACE_METHOD) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1) && (jsonObject.getString("msg").equals("Success"))) {
                    String PLACE_NAME = jsonObject.getString("PLACE_NAME");
                    current_loc_et.setText(PLACE_NAME);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (method == ApplicationConstants.GET_FINE_AMT_METHOD) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1) && (jsonObject.getString("msg").equals("Success"))) {
                    String Fine_Amount = jsonObject.getString("Fine_Amount");
                    fine = Integer.parseInt(Fine_Amount);
                    if (isDoc_type) {
                        if (beat_et.getText().toString().equals("")) {
                            beat_et.setError("Please enter beat number");
                        } else if (vehicle_no_et.getText().toString().equals("")) {
                            vehicle_no_et.setError("Please enter vehicle number");
                        } else if (actv.getText().toString().equals("")) {
                            actv.setError("Please select offence type");
                        } else if (current_loc_et.getText().toString().equals("")) {
                            current_loc_et.setError("Please enter current location");
                        } else if (phn_et.getText().toString().equals("")) {
                            phn_et.setError("Please enter phone number");
                        } else if (document_type_spinner.getSelectedItemPosition() == 0) {
                            TextView errorText = (TextView) document_type_spinner.getSelectedView();
                            errorText.setError("");
                            errorText.setTextColor(Color.RED);//just to highlight that this is an error
                            errorText.setText("Please select document type");
                        } else if (document_num_et.getText().toString().equals("")) {
                            document_num_et.setError("Please enter document number");
                        } else if (fine == 0) {
                            Toast.makeText(mContext, "Please select valid offence type", Toast.LENGTH_LONG).show();
                        } else {
                            FragmentManager frgManager = getActivity().getSupportFragmentManager();
//                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            Fragment fragment = new PrintCompundChallan();
                            frgManager.beginTransaction().replace(R.id.frame_layout_main, fragment)
                                    .commitAllowingStateLoss();
                        }
                    } else {
                        if (beat_et.getText().toString().equals("")) {
                            beat_et.setError("Please enter beat number");
                        } else if (vehicle_no_et.getText().toString().equals("") || vehicle_no_et.getText().toString().equals("WB")) {
                            vehicle_no_et.setError("Please enter vehicle number");
                        } else if (actv.getText().toString().equals("") || my_var == null) {
                            actv.setError("Please select offence type");
                        } else if (isError) {
                            phn_et.setError("Phone Number must be of 10 digits.");
                        } else if (phn_et.getText().toString().equals("")) {
                            phn_et.setError("Please enter phone number");
                        } else if (licence_num.getText().toString().equals("")) {
                            licence_num.setError("Please enter licence number");
                        } else if (current_loc_et.getText().toString().equals("")) {
                            current_loc_et.setError("Please enter current location");
                        } else if (fine == 0) {
                            Toast.makeText(mContext, "Please select valid offence type", Toast.LENGTH_LONG).show();
                        } else {
                            FragmentManager frgManager = getActivity().getSupportFragmentManager();
//                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            Fragment fragment = new PrintCompundChallan();
                            frgManager.beginTransaction().replace(R.id.frame_layout_main, fragment)
                                    .commitAllowingStateLoss();
                        }
                    }
                } else {
                    if (isDoc_type) {
                        if (beat_et.getText().toString().equals("")) {
                            beat_et.setError("Please enter beat number");
                        } else if (vehicle_no_et.getText().toString().equals("")) {
                            vehicle_no_et.setError("Please enter vehicle number");
                        } else if (actv.getText().toString().equals("")) {
                            actv.setError("Please select offence type");
                        } else if (current_loc_et.getText().toString().equals("")) {
                            current_loc_et.setError("Please enter current location");
                        } else if (phn_et.getText().toString().equals("")) {
                            phn_et.setError("Please enter phone number");
                        } else if (document_type_spinner.getSelectedItemPosition() == 0) {
                            TextView errorText = (TextView) document_type_spinner.getSelectedView();
                            errorText.setError("");
                            errorText.setTextColor(Color.RED);//just to highlight that this is an error
                            errorText.setText("Please select document type");
                        } else if (document_num_et.getText().toString().equals("")) {
                            document_num_et.setError("Please enter document number");
                        } else if (fine == 0) {
                            Toast.makeText(mContext, "Please select valid offence type", Toast.LENGTH_LONG).show();
                        } else {
                            FragmentManager frgManager = getActivity().getSupportFragmentManager();
//                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            Fragment fragment = new PrintCompundChallan();
                            frgManager.beginTransaction().replace(R.id.frame_layout_main, fragment)
                                    .commitAllowingStateLoss();
                        }
                    } else {
                        if (beat_et.getText().toString().equals("")) {
                            beat_et.setError("Please enter beat number");
                        } else if (vehicle_no_et.getText().toString().equals("") || vehicle_no_et.getText().toString().equals("WB")) {
                            vehicle_no_et.setError("Please enter vehicle number");
                        } else if (actv.getText().toString().equals("") || my_var == null) {
                            actv.setError("Please select offence type");
                        } else if (isError) {
                            phn_et.setError("Phone Number must be of 10 digits.");
                        } else if (phn_et.getText().toString().equals("")) {
                            phn_et.setError("Please enter phone number");
                        } else if (licence_num.getText().toString().equals("")) {
                            licence_num.setError("Please enter licence number");
                        } else if (current_loc_et.getText().toString().equals("")) {
                            current_loc_et.setError("Please enter current location");
                        } else if (fine == 0) {
                            Toast.makeText(mContext, "Please select valid offence type", Toast.LENGTH_LONG).show();
                        } else {
                            FragmentManager frgManager = getActivity().getSupportFragmentManager();
//                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            Fragment fragment = new PrintCompundChallan();
                            frgManager.beginTransaction().replace(R.id.frame_layout_main, fragment)
                                    .commitAllowingStateLoss();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        if (method == ApplicationConstants.SubsequentList_Skymap_METHOD) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String offence_no = jsonObject.getString("offence_no");
                this.offence_no = offence_no;
                JSONObject jsonObject2 = new JSONObject(jsonObject.getString("data"));
                if (jsonObject2 == null) {
                    fine = 0;
                } else {
                    JSONObject jsonObject3 = new JSONObject(jsonObject2.getString("AmountListCollection"));
                    Fine_Amt_1 = jsonObject3.getString("Fine_Amt_1");
                    Fine_Amt_2 = jsonObject3.getString("Fine_Amt_2");
                    Fine_Amt_3 = jsonObject3.getString("Fine_Amt_3");
                    Fine_Amt_4 = jsonObject3.getString("Fine_Amt_4");
                    Fine_Amt_5 = jsonObject3.getString("Fine_Amt_5");
                    fine = check_offence_total_amount(offence_no);
                }
            } catch (Exception e) {
                e.printStackTrace();
                fine = 0;
            }
            if (isDoc_type) {
                if (beat_et.getText().toString().equals("")) {
                    beat_et.setError("Please enter beat number");
                } else if (vehicle_no_et.getText().toString().equals("")) {
                    vehicle_no_et.setError("Please enter vehicle number");
                } else if (actv.getText().toString().equals("")) {
                    actv.setError("Please select offence type");
                } else if (current_loc_et.getText().toString().equals("")) {
                    current_loc_et.setError("Please enter current location");
                } else if (phn_et.getText().toString().equals("")) {
                    phn_et.setError("Please enter phone number");
                } else if (document_type_spinner.getSelectedItemPosition() == 0) {
                    TextView errorText = (TextView) document_type_spinner.getSelectedView();
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText("Please select document type");
                } else if (document_num_et.getText().toString().equals("")) {
                    document_num_et.setError("Please enter document number");
                } else if (fine == 0) {
                    Toast.makeText(mContext, "Please select valid offence type", Toast.LENGTH_LONG).show();
                } else {
                    FragmentManager frgManager = getActivity().getSupportFragmentManager();
//                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    Fragment fragment = new PrintCompundChallan();
                    frgManager.beginTransaction().replace(R.id.frame_layout_main, fragment)
                            .commitAllowingStateLoss();
                }
            } else {
                if (beat_et.getText().toString().equals("")) {
                    beat_et.setError("Please enter beat number");
                } else if (vehicle_no_et.getText().toString().equals("") || vehicle_no_et.getText().toString().equals("WB")) {
                    vehicle_no_et.setError("Please enter vehicle number");
                } else if (actv.getText().toString().equals("") || my_var == null) {
                    actv.setError("Please select offence type");
                } else if (isError) {
                    phn_et.setError("Phone Number must be of 10 digits.");
                } else if (phn_et.getText().toString().equals("")) {
                    phn_et.setError("Please enter phone number");
                } else if (licence_num.getText().toString().equals("")) {
                    licence_num.setError("Please enter licence number");
                } else if (current_loc_et.getText().toString().equals("")) {
                    current_loc_et.setError("Please enter current location");
                } else if (fine == 0) {
                    Toast.makeText(mContext, "Please select valid offence type", Toast.LENGTH_LONG).show();
                } else {
                    FragmentManager frgManager = getActivity().getSupportFragmentManager();
//                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    Fragment fragment = new PrintCompundChallan();
                    frgManager.beginTransaction().replace(R.id.frame_layout_main, fragment)
                            .commitAllowingStateLoss();
                }
            }
        }
        if (method == ApplicationConstants.BEAT_CHECKING_METHOD) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1) && (jsonObject.getString("msg").equals("Success"))) {
//                    if(current_loc_et.getText().equals("")) {
                    get_Location_From_Server();
//                    }
                } else {
                    beat_et.setText("");
                    Toast.makeText(mContext, "You have entered wrong beat number", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (method == ApplicationConstants.DL_LIST_METHOD) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1) && (jsonObject.getString("msg").equals("Success"))) {
                    String issu_dt = jsonObject.getString("issu_dt");
                    String valid_upto = jsonObject.getString("valid_upto");
                    String owner_name = jsonObject.getString("owner_name");
                    String owner_address = jsonObject.getString("owner_address");
                    String lcns_cat = jsonObject.getString("lcns_cat");
                    issue_date_tv.setText(issu_dt);
                    expiry_date_tv.setText(valid_upto);
                    name_tv.setText(owner_name);
                    add_tv.setText(owner_address);

                    vehicle_class_tv.setText(lcns_cat);
                    licence_info_table.setIssu_dt(issu_dt);
                    licence_info_table.setValid_upto(valid_upto);
                    licence_info_table.setOwner_name(owner_name);
                    licence_info_table.setOwner_address(owner_address);
                    licence_info_table.setLcns_cat(lcns_cat);
                    issue_date_tv.setTextColor(getResources().getColor(R.color.black));
                    expiry_date_tv.setTextColor(getResources().getColor(R.color.black));
                    name_tv.setTextColor(getResources().getColor(R.color.black));
                    add_tv.setTextColor(getResources().getColor(R.color.black));
                    reg_number_tv.setTextColor(getResources().getColor(R.color.black));
                    vehicle_class_tv.setTextColor(getResources().getColor(R.color.black));
                    issue_date_tv.setEnabled(false);
                    expiry_date_tv.setEnabled(false);
                    name_tv.setEnabled(false);
                    add_tv.setEnabled(false);
                    vehicle_class_tv.setEnabled(false);
                } else {
                    issue_date_tv.setFocusable(true);
                    issue_date_tv.setFocusableInTouchMode(true);
                    issue_date_tv.setClickable(true);
                    issue_date_tv.setEnabled(true);
                    issue_date_tv.setText("");
                    issue_date_tv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            DatePickerDialog datePicker = new DatePickerDialog(getActivity(), fromDatePickerDialog, mYear,
                                    mMonth, mDay);
                            datePicker.show();

                        }
                    });

                    expiry_date_tv.setFocusable(true);
                    expiry_date_tv.setFocusableInTouchMode(true);
                    expiry_date_tv.setClickable(true);
                    expiry_date_tv.setEnabled(true);
                    expiry_date_tv.setText("");
                    expiry_date_tv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            DatePickerDialog datePicker = new DatePickerDialog(getActivity(), fromDatePickerDialog2, mYear,
                                    mMonth, mDay);
                            datePicker.show();

                        }
                    });

                    name_tv.setFocusable(true);
                    name_tv.setFocusableInTouchMode(true);
                    name_tv.setClickable(true);
                    name_tv.setEnabled(true);
                    name_tv.setText("");

                    add_tv.setFocusable(true);
                    add_tv.setFocusableInTouchMode(true);
                    add_tv.setClickable(true);
                    add_tv.setEnabled(true);
                    add_tv.setText("");

                    vehicle_class_tv.setFocusable(true);
                    vehicle_class_tv.setFocusableInTouchMode(true);
                    vehicle_class_tv.setClickable(true);
                    vehicle_class_tv.setEnabled(true);
                    vehicle_class_tv.setText("");
                }
            } catch (Exception e) {

            }
        }
        if (method == ApplicationConstants.US_LIST_METHOD) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray jsonArray = jsonObject.getJSONArray("VIO_US_CD");
                options.add("Select");
                for (int i = 0; i < jsonArray.length(); i++) {
                    options.add(jsonArray.get(i).toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (method == ApplicationConstants.GET_PENDING_CASE_METHOD) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1) && (jsonObject.getString("msg").equals("Success"))) {
//                    JSONArray jsonArrayinner = jsonObject.getJSONArray("VIO_US_CD");

                    info_ll.setVisibility(View.VISIBLE);
                    case_tv.setText(jsonObject.getString("Case_Date"));
                    acc_tv.setText(jsonObject.getString("Offence"));
                    rc_no_tv.setText(jsonObject.getString("CourtName"));
                    vehicle_tv.setText(jsonObject.getString("vech_No"));
                    fine_tv.setText(jsonObject.getString("Fineamount"));
//                    submit_tv.setText("Print");
//                        }
                    spotChallanInfo.date = jsonObject.getString("Case_Date");
                    spotChallanInfo.offence_type = jsonObject.getString("Offence");
                    spotChallanInfo.courtName = jsonObject.getString("CourtName");
                    spotChallanInfo.vehicle_no = jsonObject.getString("vech_No");
                    spotChallanInfo.spot_fine_amt = jsonObject.getString("Fineamount");
                    int amount = Integer.parseInt(spotChallanInfo.spot_fine_amt);
                    spotChallanInfo.fineAmountStr = EnglishNumberToWords.convert(amount);

                } else {
                    info_ll.setVisibility(View.GONE);
                    Toast.makeText(mContext, "No Record found! Please check case No.", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (method == ApplicationConstants.GET_GUARD_METHOD) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1) && (jsonObject.getString("msg").equals("Success"))) {
                    gaurd_code_ac.setText(jsonObject.getString("Guard_Code"));
                    mGuardCode=gaurd_code_ac.getText().toString().trim();
                    br.skymapglobal.ktp_challan.helpers.Utils.saveStringPreferences(mContext, "Guard_Code", jsonObject.getString("Guard_Code"));
                    br.skymapglobal.ktp_challan.helpers.Utils.saveStringPreferences(mContext, "Raid_Guard_Code", "");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public int check_offence_total_amount(String off_num) {
        switch (off_num) {
            case ("0"):
                amount = Integer.parseInt(Fine_Amt_1);
                break;
            case ("1"):
                amount = Integer.parseInt(Fine_Amt_2);
                break;
            case ("2"):
                amount = Integer.parseInt(Fine_Amt_3);
                break;
            case ("3"):
                amount = Integer.parseInt(Fine_Amt_4);
                break;
            default:
                amount = Integer.parseInt(Fine_Amt_4);
                break;
        }
        return amount;
    }

    public void showOnPaymentDialog(String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        //Uncomment the below code to Set the message and title from the strings.xml file
        //builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);
        //Setting message manually and performing action on button click
        builder.setMessage("" + msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        FragmentManager frgManager = getActivity().getSupportFragmentManager();
//                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        Fragment fragment = ViewPagerFragment.newInstance(1);
                        frgManager.beginTransaction().replace(R.id.frame_layout_main, fragment)
                                .commitAllowingStateLoss();
                        dialog.dismiss();
                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Information");
        alert.show();
    }

    public void showOnSpotDialog(String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        //Uncomment the below code to Set the message and title from the strings.xml file
        //builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);
        //Setting message manually and performing action on button click
        builder.setMessage("Payment done successfully.\nRC Number is " + msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        FragmentManager frgManager = getActivity().getSupportFragmentManager();
//                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        Fragment fragment = ViewPagerFragment.newInstance(1);
                        frgManager.beginTransaction().replace(R.id.frame_layout_main, fragment)
                                .commitAllowingStateLoss();
                        dialog.dismiss();
                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Information");
        alert.show();
    }

    final DatePickerDialog.OnDateSetListener fromDatePickerDialog = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            updateStartDate(year, monthOfYear, dayOfMonth);

        }
    };
    final DatePickerDialog.OnDateSetListener fromDatePickerDialog2 = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            updateEndDate(year, monthOfYear, dayOfMonth);

        }
    };


    public Dialog onDialog(int id) {
        switch (id) {
            case 1:
                return new DatePickerDialog(getActivity(), fromDatePickerDialog, mYear,
                        mMonth, mDay);

            case 2:
                return new DatePickerDialog(getActivity(), fromDatePickerDialog2, mYear,
                        mMonth, mDay);
        }
        return null;
    }

    private void updateStartDate(int year, int month, int day) {
        Calendar cl = Calendar.getInstance();
        month = Integer.parseInt(checkDigit(month));
        cl.set(year, month + 1, day);

        String startDate = "", endDate = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        if (expiry_date_tv.getText().toString().length() != 0) {
            endDate = expiry_date_tv.getText().toString();
        } else {
            endDate = sdf.format(cl.getTime());
        }
        Calendar strCal = Calendar.getInstance();
        strCal.set(year, month, day);
        startDate = sdf.format(strCal.getTime());
        Date firstDate = null, lastDate = null;
        try {

            firstDate = sdf.parse(startDate);
            lastDate = sdf.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (cl.after(Calendar.getInstance()) || cl.equals(Calendar.getInstance())) {

            if (!firstDate.after(lastDate) || firstDate.equals(lastDate)) {

                issue_date_tv.setText(checkDigit(month + 1) + "/" + checkDigit(day) + "/" + year);
            } else {
                issue_date_tv.setError("Past dates not allowed");
            }
        } else {
            issue_date_tv.setError("Past dates not allowed");
        }
    }

    private void updateEndDate(int year, int month, int day) {
        Calendar cl = Calendar.getInstance();
        month = Integer.parseInt(checkDigit(month));
        cl.set(year, month + 1, day);
        String startDate = "", endDate = "";
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        if (issue_date_tv.getText().toString().length() != 0) {
            startDate = issue_date_tv.getText().toString();
        } else {
            Calendar stCal = Calendar.getInstance();
            startDate = sdf.format(stCal.getTime());
        }
        Calendar enCal = Calendar.getInstance();
        enCal.set(year, month, day);
        endDate = sdf.format(enCal.getTime());
        Date firstDate = null, lastDate = null;
        try {

            firstDate = sdf.parse(startDate);
            lastDate = sdf.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (cl.after(Calendar.getInstance()) || cl.equals(Calendar.getInstance())) {
            if (lastDate.after(firstDate) || lastDate.equals(firstDate)) {
                expiry_date_tv.setText(checkDigit(month + 1) + "/" + checkDigit(day) + "/" + year);
            } else {
                expiry_date_tv.setError("Past dates not allowed");
            }
        } else {
            expiry_date_tv.setError("Past dates not allowed");
        }
    }

    public String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }

    public boolean OffenceTypeValidationWithVehicle(String vehicle_type, String offence_type) {
        boolean isvalid = true;
        for (OffenceWithVehicle obj : mOffenceWithVehicleList) {
            if (obj.getOfencetype().equalsIgnoreCase(offence_type)) {
                if (obj.getStatus().equalsIgnoreCase("YES")) {
                    if (obj.getVehicltype().equalsIgnoreCase(vehicle_type)) {
                        isvalid = true;
                    } else {
                        isvalid = false;
                    }
                } else {
                    if (obj.getVehicltype().equalsIgnoreCase(vehicle_type)) {
                        isvalid = false;
                    } else {
                        isvalid = true;
                    }
                }
            }
        }
        return isvalid;


    }

    private void BuilOffenceType() {
        mOffenceWithVehicleList = new ArrayList<OffenceWithVehicle>();
        String[] section = getResources().getStringArray(R.array.vehicle_type_section_validation);
        if (section != null && section.length > 0) {
            for (String s : section) {
                if (s.contains("#")) {
                    String[] splitsection = s.split("#");
                    if (splitsection.length == 3) {
                        OffenceWithVehicle obj = new OffenceWithVehicle();
                        obj.setVehicltype(splitsection[0].trim());
                        obj.setOfencetype(splitsection[1].trim());
                        obj.setStatus(splitsection[2].trim());
                        mOffenceWithVehicleList.add(obj);
                        obj = null;
                    }
                }
            }
        }
    }

}
