package br.skymapglobal.ktp_challan.fragment.challan;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;
import com.yalantis.ucrop.util.FileUtils;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import br.skymapglobal.ktp_challan.MainActivity;
import br.skymapglobal.ktp_challan.R;
import br.skymapglobal.ktp_challan.asynctasks.CallWebServiceAsyncTask;
import br.skymapglobal.ktp_challan.fragment.ViewPagerFragment;
import br.skymapglobal.ktp_challan.helpers.ApplicationConstants;
import br.skymapglobal.ktp_challan.helpers.CallbackInterface;
import br.skymapglobal.ktp_challan.helpers.CloudStorage;
import br.skymapglobal.ktp_challan.helpers.EnglishNumberToWords;
import br.skymapglobal.ktp_challan.helpers.FileHelper;
import br.skymapglobal.ktp_challan.helpers.PDFHelper;
import br.skymapglobal.ktp_challan.helpers.Utils;
import br.skymapglobal.ktp_challan.models.CaseHistoryResponse;
import br.skymapglobal.ktp_challan.models.CompoundChallanInfo;
import br.skymapglobal.ktp_challan.models.DL_info;
import br.skymapglobal.ktp_challan.models.SpotChallanInfo;
import br.skymapglobal.ktp_challan.models.Vehicle_info;
import br.skymapglobal.ktp_challan.models.entity.CaseInfo;

public class PrintCompundChallan extends Fragment implements CallbackInterface, View.OnClickListener, ActivityResultListner {//,GoogleApiClient.ConnectionCallbacks,
    private static final int REQUEST_WRITE_STORAGE = 5;
    private static final int REQUEST_CAMERA = 0;
    private static final String TAG = "drive-quickstart";

    private final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public String DOC_NAME_TXT;
    public String SEIZED_DL_ID="";
    private int value;
    File image1, image2;
    private boolean is_Doc, is_Vehicle;

    boolean isDLrequired=false;

    private Context mContext;
    private ImageView imgPreview;
    private String case_no;
    private TextView DL_status_tv, name_tv, add_tv, issue_date_tv, expiry_date_tv, vehicle_class_tv, fine_tv, loc_tv;
    private TextView section_tv, date_tv, owner_name1_tv, reg_number_tv, vehicle_type_tv, owner_name_tv, uuid_tv, owner_add_tv;
    private TableLayout tableCaseHistory;
    protected ProgressDialog progressDialog;

    String mImagePath1="";
    String mImagePath2="";

    //boolean isNormalFine=true;

    String fineselect="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getActivity();
        progressDialog = new ProgressDialog(mContext);
        View view = inflater.inflate(R.layout.print_compound_challan, container,
                false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.
                Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        ((MainActivity)getActivity()).setListner(this);
        loc_tv = (TextView) view.findViewById(R.id.loc_tv);
        fine_tv = (TextView) view.findViewById(R.id.fine_tv);
        date_tv = (TextView) view.findViewById(R.id.date_tv);
        uuid_tv = (TextView) view.findViewById(R.id.uuid_tv);
        section_tv = (TextView) view.findViewById(R.id.section_tv);
        imgPreview = (ImageView) view.findViewById(R.id.imgPreview);
        owner_add_tv = (TextView) view.findViewById(R.id.owner_add_tv);
        owner_name_tv = (TextView) view.findViewById(R.id.owner_name_tv);
        reg_number_tv = (TextView) view.findViewById(R.id.reg_number_tv);
        owner_name1_tv = (TextView) view.findViewById(R.id.owner_name1_tv);
        vehicle_type_tv = (TextView) view.findViewById(R.id.vehicle_type_tv);
        add_tv = (TextView) view.findViewById(R.id.add_tv);
        name_tv = (TextView) view.findViewById(R.id.name_tv);
        DL_status_tv = (TextView) view.findViewById(R.id.DL_status_tv);
        issue_date_tv = (TextView) view.findViewById(R.id.issue_date_tv);
        expiry_date_tv = (TextView) view.findViewById(R.id.expiry_date_tv);
        vehicle_class_tv = (TextView) view.findViewById(R.id.vehicle_class_tv);
        tableCaseHistory = (TableLayout) view.findViewById(R.id.table_case_history);

        if (CompoundChallan.vehicle_no_et.getText().toString() != null && !CompoundChallan.vehicle_no_et.getText().toString().equals("")) {
            reg_number_tv.setText(CompoundChallan.vehicle_no_et.getText().toString());
        }
        fine_tv.setText("Fine : " + CompoundChallan.fine + " INR");

        loc_tv.setText(CompoundChallan.current_loc_et.getText().toString());

        if (CompoundChallan.vehicle_info != null) {
            populateVehicleInfo(CompoundChallan.vehicle_info);
        }
        if (CompoundChallan.licence_info_table != null) {
            populateDriverInfo(CompoundChallan.licence_info_table);
        }

        view.findViewById(R.id.print_challan_btn).setOnClickListener(this);
        view.findViewById(R.id.pending_challan_btn).setOnClickListener(this);
        view.findViewById(R.id.show_case_history).setOnClickListener(this);
        view.findViewById(R.id.cancel_ll).setOnClickListener(this);
        view.findViewById(R.id.fine_amount_btn).setOnClickListener(this);
        view.findViewById(R.id.fine_amount_btn).setVisibility(View.GONE);
        tableCaseHistory.setVisibility(View.GONE);
        /*if(CompoundChallan.offence_no!=null){
            if(false==CompoundChallan.offence_no.trim().equalsIgnoreCase("0")){
                ShowFineAmountDialog(CompoundChallan.offence_no, CompoundChallan.Fine_Amt_1,CompoundChallan.Fine_Amt_2,CompoundChallan.Fine_Amt_3,CompoundChallan.Fine_Amt_4);
            }
        }*/
        return view;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.print_challan_btn:
                showConformationDialog();
                break;
            case R.id.pending_challan_btn:
                showDocumentDialog();
                break;
            case R.id.show_case_history:

                requestGetCaseHistory();
                tableCaseHistory.setVisibility(View.VISIBLE);
                break;
            case R.id.cancel_ll:
                FragmentManager frgManager = getActivity().getSupportFragmentManager();
                Fragment fragment = ViewPagerFragment.newInstance(1);
                frgManager.beginTransaction().replace(R.id.frame_layout_main, fragment)
                        .commitAllowingStateLoss();
                break;

            case R.id.fine_amount_btn:
                ShowFineAmountDialog(CompoundChallan.offence_no, CompoundChallan.Fine_Amt_1,CompoundChallan.Fine_Amt_2,CompoundChallan.Fine_Amt_3,CompoundChallan.Fine_Amt_4);
                break;
        }
    }


    public void populateVehicleInfo(Vehicle_info registration_info_table) {

        owner_add_tv.setText(registration_info_table.getOwner_address());
        owner_name_tv.setText(registration_info_table.getOwner_name());
        owner_name1_tv.setText(registration_info_table.getOwner_name());
        vehicle_type_tv.setText(registration_info_table.getVech_type());
        uuid_tv.setText(registration_info_table.getOwnerf_name());
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        String formattedDate = df.format(c.getTime());
        date_tv.setText(formattedDate);
        if (CompoundChallan.section != null) {
            section_tv.setText(CompoundChallan.section);
        }
    }

    public void populateDriverInfo(DL_info licence_info_table) {

        add_tv.setText(licence_info_table.getOwner_address());
        name_tv.setText(licence_info_table.getOwner_name());
        issue_date_tv.setText(licence_info_table.getIssu_dt());
        expiry_date_tv.setText(licence_info_table.getValid_upto());
        vehicle_class_tv.setText(licence_info_table.getLcns_cat());
//        DL_status_tv.setText(licence_info_table.getSTATUS().toUpperCase());
    }

    private ImageView img_preview_vehicle, img_preview_doc;

    public void showDocumentDialog() {
        // custom dialog
        SEIZED_DL_ID="";
        is_Doc = false;
        is_Vehicle = false;
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.ceize_document_dialog);
        dialog.setTitle("Upload Documents");
        final LinearLayout other_doc_ll = (LinearLayout) dialog.findViewById(R.id.other_doc_ll);
        final LinearLayout licence_ll = (LinearLayout) dialog.findViewById(R.id.licence_ll);
        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radioGroup);
        final EditText licence_num = (EditText) dialog.findViewById(R.id.editTextDLNo);
        licence_num.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        final Spinner document_type_spinner = (Spinner) dialog.findViewById(R.id.document_type_spinner);
        img_preview_vehicle = (ImageView) dialog.findViewById(R.id.img_vehicle);
        img_preview_doc = (ImageView) dialog.findViewById(R.id.img_document);


        document_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getSelectedItemPosition() != 0) {
                    DOC_NAME_TXT = shortFormOffinceType(document_type_spinner.getSelectedItemPosition());
                    if(DOC_NAME_TXT.trim().equalsIgnoreCase("DL")){
                        licence_num.setVisibility(View.VISIBLE);
                        isDLrequired=true;
                    }else{
                        licence_num.setVisibility(View.GONE);
                        isDLrequired=false;
                        SEIZED_DL_ID="";
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        dialog.findViewById(R.id.upload_photo_tv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value = 1;
                is_Vehicle = true;
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestWriteStoragePermission();
                    return;
                }

                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestCameraPermission();
                } else {
                    captureImage();
                }

            }
        });

        dialog.findViewById(R.id.upload_doc_photo_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value = 2;
                is_Doc = true;
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestWriteStoragePermission();
                    return;
                }
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestCameraPermission();
                } else {
                    captureImage();
                }

            }
        });

        dialog.findViewById(R.id.create_challan_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (document_type_spinner.getSelectedItemPosition() == 0) {
                    TextView errorText = (TextView) document_type_spinner.getSelectedView();
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText("Please select document type");
                } else {
                    String dlnum=licence_num.getText().toString().trim();
                    if(isDLrequired){
                        if(dlnum.length()>=4){
                            if(true==is_Doc){
                                SEIZED_DL_ID=dlnum;
                                save_Challan_Data(0);
                                dialog.dismiss();
                            }else{
                                Toast.makeText(mContext,"Please capture document photograph",Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(mContext,"Please provide valid license no",Toast.LENGTH_LONG).show();
                        }
                    }else{
                        if(true==is_Doc){
                            save_Challan_Data(0);
                            dialog.dismiss();
                        }else{
                            Toast.makeText(mContext,"Please capture document photograph",Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                View radioButton = radioGroup.findViewById(checkedId);
                int index = radioGroup.indexOfChild(radioButton);
                switch (index) {
                    case 0:
                        other_doc_ll.setVisibility(View.GONE);
                        licence_ll.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        licence_num.setText("");
                        issue_date_tv.setText("");
                        expiry_date_tv.setText("");
                        name_tv.setText("");
                        add_tv.setText("");
                        vehicle_class_tv.setText("");
                        licence_ll.setVisibility(View.GONE);
                        other_doc_ll.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        dialog.show();
    }


    public void ShowFineAmountDialog(String offencno, final String fine1, final String fine2, final String fine3, final String fine4) {

        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.fine_amount_dialog);
        dialog.setCancelable(false);
        dialog.setTitle("Select Fine Amount");

        TextView tv = (TextView) dialog.findViewById(R.id.fineTitle);
        tv.setText("Subsequent Fine Amount is "+CompoundChallan.fine +" INR");
        RadioButton rb1= (RadioButton) dialog.findViewById(R.id.radioButton1);
        RadioButton rb2= (RadioButton) dialog.findViewById(R.id.radioButton2);
        RadioButton rb3= (RadioButton) dialog.findViewById(R.id.radioButton3);
        RadioButton rb4= (RadioButton) dialog.findViewById(R.id.radioButton4);

        if(null==offencno){
            offencno="0";
        }else{
            if(offencno.trim().length()<=0){
                offencno="0";
            }
        }

        switch (offencno) {
            case ("0"):
                rb1.setVisibility(View.VISIBLE);
                rb2.setVisibility(View.GONE);
                rb3.setVisibility(View.GONE);
                rb4.setVisibility(View.GONE);
                break;
            case ("1"):
                rb1.setVisibility(View.VISIBLE);
                rb2.setVisibility(View.VISIBLE);
                rb3.setVisibility(View.GONE);
                rb4.setVisibility(View.GONE);
                break;
            case ("2"):
                rb1.setVisibility(View.VISIBLE);
                rb2.setVisibility(View.VISIBLE);
                rb3.setVisibility(View.VISIBLE);
                rb4.setVisibility(View.GONE);
                break;
            case ("3"):
                rb1.setVisibility(View.VISIBLE);
                rb2.setVisibility(View.VISIBLE);
                rb3.setVisibility(View.VISIBLE);
                rb4.setVisibility(View.VISIBLE);
                break;
            default:
                rb1.setVisibility(View.VISIBLE);
                rb2.setVisibility(View.VISIBLE);
                rb3.setVisibility(View.VISIBLE);
                rb4.setVisibility(View.VISIBLE);
                break;
        }


        rb1.setText("Fine Amount 1     "+fine1+ " INR");
        rb1.setTypeface(null, Typeface.BOLD);
        rb2.setText("Fine Amount 2     "+fine2+ " INR");
        rb2.setTypeface(null, Typeface.BOLD);
        rb3.setText("Fine Amount 3     "+fine3+ " INR");
        rb3.setTypeface(null, Typeface.BOLD);
        rb4.setText("Fine Amount 4     "+fine4+ " INR");
        rb4.setTypeface(null, Typeface.BOLD);


        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radioGroup);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                View radioButton = radioGroup.findViewById(checkedId);
                int index = radioGroup.indexOfChild(radioButton);
                switch (index) {
                    case 0:
                        fine_tv.setText("Fine : " + fine1 + " INR");
                        //CompoundChallan.offence_no="1";
                        fineselect="0";
                        break;
                    case 1:
                        fine_tv.setText("Fine : " + fine2 + " INR");
                        //CompoundChallan.offence_no="2";
                        fineselect="1";
                        break;
                    case 2:
                        fine_tv.setText("Fine : " + fine3 + " INR");
                        //CompoundChallan.offence_no="3";
                        fineselect="2";
                        break;
                    case 3:
                        fine_tv.setText("Fine : " + fine4 + " INR");
                        //CompoundChallan.offence_no="4";
                        fineselect="3";
                        break;
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }



    CompoundChallanInfo compoundChallanInfo;

    public void save_Challan_Data(int num) {
        compoundChallanInfo = new CompoundChallanInfo();
        String v_no = reg_number_tv.getText().toString();
        String reg_device_id = Utils.getIMEI(mContext);
        String reg_device_type = "A";
        String vehicle_type = vehicle_type_tv.getText().toString();
        String Acused_prsn_name = owner_name_tv.getText().toString();
        String Acused_prsn_addr = owner_add_tv.getText().toString();
        String Beat_No = CompoundChallan.beat_et.getText().toString();
        Utils.saveStringPreferences(mContext, "BEAT_NO", Beat_No);
        String Road_Name = loc_tv.getText().toString();
        String us01 = section_tv.getText().toString();
        int on01 = Integer.parseInt(CompoundChallan.offence_no) + 1;
        String us02 = "";
        int on02 = 0;
        String us03 = "";
        int on03 = 0;
        String us04 = "";
        int on04 = 0;
        String us05 = "";
        int on05 = 0;
        int Fine_AMT = 0;


        //For Fine Amount Selection

        /*switch (fineselect) {
            case ("0"):
                Fine_AMT=Integer.parseInt(CompoundChallan.Fine_Amt_1);
                break;

            case ("1"):
                Fine_AMT=Integer.parseInt(CompoundChallan.Fine_Amt_2);
                break;

            case ("2"):
                Fine_AMT=Integer.parseInt(CompoundChallan.Fine_Amt_3);
                break;

            case ("3"):
                Fine_AMT=Integer.parseInt(CompoundChallan.Fine_Amt_4);
                break;

            default:
                Fine_AMT=CompoundChallan.fine;
                break;
        }*/

        /*if(true==isNormalFine){
            Fine_AMT=Integer.parseInt(CompoundChallan.Fine_Amt_1);
        }else{
            Fine_AMT=CompoundChallan.fine;
        }*/



        //Remove this code when Fine Selection Work
        Fine_AMT=CompoundChallan.fine;

        String DOC_NAME = (num == 1) ? "SPOT" : DOC_NAME_TXT;

        String SEIZED_DOC_ID = "";
        String MOBILE_NO = CompoundChallan.phn_et.getText().toString();
        Utils.saveStringPreferences(mContext, "VEHICLE_NO", v_no);
        Utils.saveStringPreferences(mContext, "U/S", us01);
        String mobile = Utils.getStringPreferences(mContext, "PASSWORD", "");
        String PP_CD = Utils.getStringPreferences(mContext, "USERNAME", "").toUpperCase();
        String guard_code = Utils.getStringPreferences(mContext, "Guard_Code", "").toUpperCase();
        String raid_guard_code = Utils.getStringPreferences(mContext, "Raid_Guard_Code", "").toUpperCase();

        Log.e("ORGINAL_GUARD_CODE" ,guard_code);
        Log.e("RAID GUARD CODE PRE",raid_guard_code);

        if(raid_guard_code.length()>0 && raid_guard_code!=null){
            if(raid_guard_code.equalsIgnoreCase(guard_code)){
                raid_guard_code="XXX";
            }
        }else{
            raid_guard_code="XXX";
        }
        Log.e("RAID GUARD CODE POST",raid_guard_code);

        compoundChallanInfo.v_no = v_no;
        compoundChallanInfo.reg_device_id = reg_device_id;
        compoundChallanInfo.reg_device_type = reg_device_type;
        compoundChallanInfo.vehicle_type = vehicle_type;
        compoundChallanInfo.Acused_prsn_name = Acused_prsn_name;
        compoundChallanInfo.Acused_prsn_addr = Acused_prsn_addr;
        compoundChallanInfo.Beat_No = Beat_No;
        compoundChallanInfo.Road_Name = Road_Name;
        compoundChallanInfo.us01 = us01;
        compoundChallanInfo.on01 = on01;
        compoundChallanInfo.us02 = us02;
        compoundChallanInfo.on02 = on02;
        compoundChallanInfo.us03 = us03;
        compoundChallanInfo.on03 = on03;
        compoundChallanInfo.us04 = us04;
        compoundChallanInfo.on04 = on04;
        compoundChallanInfo.us05 = us05;
        compoundChallanInfo.on05 = on05;

        if(num!=1){
            if(DOC_NAME_TXT.equalsIgnoreCase("DL")){
                SEIZED_DOC_ID=SEIZED_DL_ID;
            }
        }


        compoundChallanInfo.SEIZED_DOC_ID = SEIZED_DOC_ID;
        compoundChallanInfo.MOBILE_NO = MOBILE_NO;
        compoundChallanInfo.Fine_AMT = Fine_AMT + "";
        compoundChallanInfo.DOC_NAME = DOC_NAME;
        compoundChallanInfo.omobile = mobile;
        compoundChallanInfo.PP_CD = PP_CD;
        compoundChallanInfo.guard_code = guard_code;

        spotChallanInfo = new SpotChallanInfo();
        spotChallanInfo.spot_fine_amt = Fine_AMT + "";
        spotChallanInfo.fineAmountStr = EnglishNumberToWords.convert(Fine_AMT);
        spotChallanInfo.Acused_prsn_name = Acused_prsn_name;
        spotChallanInfo.offence_type = us01;
        spotChallanInfo.vehicle_no = v_no;
        spotChallanInfo.MOBILE_NO = MOBILE_NO;

        if(num==1){
            new CallWebServiceAsyncTask(mContext, ApplicationConstants.GENRT_MERGED_COMPOUND_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.VEHICLE_NO + "=" + v_no + "&" + ApplicationConstants.VEHICLE_TYPE + "=" + vehicle_type + "&" + ApplicationConstants.ACUSED_PRSN_NAME + "=" + Acused_prsn_name + "&" + ApplicationConstants.ACUSED_PRSN_ADDR + "=" + Acused_prsn_addr + "&" + ApplicationConstants.BEAT_NO + "=" + Beat_No + "&" + ApplicationConstants.ROAD_NAME + "=" + Road_Name + "&" + ApplicationConstants.US_01 + "=" + us01 + "&" + ApplicationConstants.ON_01 + "=" + on01 + "&" + ApplicationConstants.US_02 + "=" + us02 + "&" + ApplicationConstants.ON_02 + "=" + on02 + "&" + ApplicationConstants.US_03 + "=" + us03 + "&" + ApplicationConstants.ON_03 + "=" + on03 + "&" + ApplicationConstants.US_04 + "=" + us04 + "&" + ApplicationConstants.ON_04 + "=" + on04 + "&" + ApplicationConstants.US_05 + "=" + us05 + "&" + ApplicationConstants.ON_05 + "=" + on05 + "&" + ApplicationConstants.Fine_AMT + "=" + Fine_AMT + "&" + ApplicationConstants.DOC_NAME + "=" + DOC_NAME + "&" + ApplicationConstants.SEIZED_DOC_ID + "=" + SEIZED_DOC_ID + "&" + ApplicationConstants.MOBILE_NO + "=" + MOBILE_NO + "&" + ApplicationConstants.PP_CD + "=" + PP_CD + "&" + ApplicationConstants.OMOBILE + "=" + mobile + "&" + ApplicationConstants.GUARD_CODE + "=" + guard_code+ "&" + ApplicationConstants.SRAID_GUARD_CODE + "=" + raid_guard_code, num).setListener(PrintCompundChallan.this);
        }else{
            new CallWebServiceAsyncTask(mContext, ApplicationConstants.GENRT_COMPOUND_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.VEHICLE_NO + "=" + v_no + "&" + ApplicationConstants.VEHICLE_TYPE + "=" + vehicle_type + "&" + ApplicationConstants.ACUSED_PRSN_NAME + "=" + Acused_prsn_name + "&" + ApplicationConstants.ACUSED_PRSN_ADDR + "=" + Acused_prsn_addr + "&" + ApplicationConstants.BEAT_NO + "=" + Beat_No + "&" + ApplicationConstants.ROAD_NAME + "=" + Road_Name + "&" + ApplicationConstants.US_01 + "=" + us01 + "&" + ApplicationConstants.ON_01 + "=" + on01 + "&" + ApplicationConstants.US_02 + "=" + us02 + "&" + ApplicationConstants.ON_02 + "=" + on02 + "&" + ApplicationConstants.US_03 + "=" + us03 + "&" + ApplicationConstants.ON_03 + "=" + on03 + "&" + ApplicationConstants.US_04 + "=" + us04 + "&" + ApplicationConstants.ON_04 + "=" + on04 + "&" + ApplicationConstants.US_05 + "=" + us05 + "&" + ApplicationConstants.ON_05 + "=" + on05 + "&" + ApplicationConstants.Fine_AMT + "=" + Fine_AMT + "&" + ApplicationConstants.DOC_NAME + "=" + DOC_NAME + "&" + ApplicationConstants.SEIZED_DOC_ID + "=" + SEIZED_DOC_ID + "&" + ApplicationConstants.MOBILE_NO + "=" + MOBILE_NO + "&" + ApplicationConstants.PP_CD + "=" + PP_CD + "&" + ApplicationConstants.OMOBILE + "=" + mobile + "&" + ApplicationConstants.GUARD_CODE + "=" + guard_code, num).setListener(PrintCompundChallan.this);
        }

    }

    SpotChallanInfo spotChallanInfo = new SpotChallanInfo();

    public void save_insert_spot_payment(String comp_case_no_str) {
        String reg_device_id = Utils.getIMEI(mContext);
        String reg_device_type = "A";
        String[] parts = comp_case_no_str.split(",");
        String comp_case_no = parts[0];
        String comp_case_dt = date_tv.getText().toString();
        String spot_fine_amt = "" + CompoundChallan.fine;
        spotChallanInfo.date = comp_case_dt;
        spotChallanInfo.case_no = parts[0];
        new CallWebServiceAsyncTask(mContext, ApplicationConstants.INSERT_SPOT_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.COMP_CASE_NO + "=" + comp_case_no + "&" + ApplicationConstants.COMP_CASE_DT + "=" + comp_case_dt + "&" + ApplicationConstants.SPOT_FINE_AMT + "=" + spot_fine_amt, 0).setListener(PrintCompundChallan.this);
    }


    private void requestGetCaseHistory() {
        String param = "&reg_device_type=A&reg_device_id=%s&vech_no=%s&vio_us_cd=%s";
        String deviceId = Utils.getIMEI(getActivity());
        String vehicleNo = reg_number_tv.getText().toString();
        String vio_us_cd = section_tv.getText().toString();
        param = String.format(param, deviceId, vehicleNo, vio_us_cd);
        new CallWebServiceAsyncTask(mContext, ApplicationConstants.CASE_HISTORY, param, 0).setListener(PrintCompundChallan.this);
    }


    @Override
    public void refreshList(String response, String method, final int type) {
        if (method == ApplicationConstants.GENRT_COMPOUND_METHOD) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1)) {
                    case_no = jsonObject.getString("msg");
                    String courtName = jsonObject.getString("CourtName");
                    String oname = jsonObject.getString("oname");
                    compoundChallanInfo.oname = oname;
                    compoundChallanInfo.courtName = courtName;
                    compoundChallanInfo.case_no = case_no.split(",")[0];

                    spotChallanInfo.case_no = case_no.split(",")[0];
                    spotChallanInfo.courtName = courtName;
                    spotChallanInfo.oname = oname;

                        new AsyncTask<Void, Void, String>() {
                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                progressDialog.setMessage("Please wait...");
                                progressDialog.setCancelable(false);
                                progressDialog.show();
                            }

                            @Override
                            protected String doInBackground(Void... voids) {
                                try {
                                    if (is_Vehicle) {
                                        CloudStorage.shareInstance().uploadFileImage("pendingvehicle", image1, case_no);
                                    }
                                    if (is_Doc) {
                                        CloudStorage.shareInstance().uploadFileImage("pendingdoc", image2, case_no);
                                    }
                                    if (is_Doc) {
                                        compoundChallanInfo.imageUrl = image2.getPath();
                                    }

                                    return PDFHelper.getInstance().generateCompoundFile(compoundChallanInfo) ? "Successfully" : null;

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    return null;
                                }
                            }

                            @Override
                            protected void onPostExecute(String result) {
                                super.onPostExecute(result);
                                progressDialog.dismiss();
                                if(case_no!=null && case_no.trim().length()>0){
                                    showDialog(case_no, type,"");
                                }else{
                                    showDialog(case_no, type,"");
                                    Toast.makeText(mContext, "False Upload Image!", Toast.LENGTH_LONG).show();
                                }
                            }
                        }.execute();

                } else {
                    Toast.makeText(mContext,"No response from server.", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (method == ApplicationConstants.INSERT_SPOT_METHOD) {
            System.out.println("response=" + response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1)) {
                    final String msg = jsonObject.getString("msg");
                    spotChallanInfo.case_id = jsonObject.getString("msg");
                    spotChallanInfo.oname = jsonObject.getString("oname");
                    spotChallanInfo.guard = jsonObject.getString("Guard");
                    new AsyncTask<SpotChallanInfo, Void, String>() {
                        ProgressDialog dialog;
                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            dialog = new ProgressDialog(mContext);
                            dialog.setCancelable(false);
                            dialog.setMessage("Please wait...");
                            dialog.show();
                        }

                        @Override
                        protected String doInBackground(SpotChallanInfo... spotsInfo) {
                            return PDFHelper.getInstance().generateSpotFile(spotsInfo[0]) ? "Successfully" : null;
                        }

                        @Override
                        protected void onPostExecute(String result) {
                            super.onPostExecute(result);
                            dialog.dismiss();
                            showOnSpotDialog(msg);
                            if (result == null) {
                                Toast.makeText(mContext, "Error Upload PDF File", Toast.LENGTH_LONG).show();
                            }
                        }
                    }.execute(spotChallanInfo);
                } else {
                    Toast.makeText(mContext,"Payment Not Done.", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (method == ApplicationConstants.CASE_HISTORY) {
            System.out.println("response=" + response);
            CaseHistoryResponse caseHistoryResponse = new Gson().fromJson(response, CaseHistoryResponse.class);
            if (caseHistoryResponse == null) {
                Toast.makeText(mContext, "Error", Toast.LENGTH_SHORT).show();
            } else {
                if (caseHistoryResponse.status == 1) {
                    if (caseHistoryResponse.data != null && caseHistoryResponse.data.CaseListCollection != null)
                        addDataTable(caseHistoryResponse.data.CaseListCollection);

                } else {
                    if (TextUtils.isEmpty(caseHistoryResponse.msg)) {
                        caseHistoryResponse.msg = "No case Found";
                    }
                    Toast.makeText(mContext, caseHistoryResponse.msg, Toast.LENGTH_SHORT).show();
                }
            }
        } else if (method == ApplicationConstants.GENRT_MERGED_COMPOUND_METHOD) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1)) {
                    case_no = jsonObject.getString("msg");
                    String courtName = jsonObject.getString("CourtName");
                    String oname = jsonObject.getString("oname");
                    final String rc_no= jsonObject.getString("RC");
                    compoundChallanInfo.oname = oname;
                    compoundChallanInfo.courtName = courtName;
                    compoundChallanInfo.case_no = case_no.split(",")[0];

                    spotChallanInfo.case_no = case_no.split(",")[0];
                    spotChallanInfo.courtName = courtName;
                    spotChallanInfo.oname = oname;

                    spotChallanInfo.case_id = rc_no;
                    spotChallanInfo.guard = jsonObject.getString("Guard");

                    new AsyncTask<Void, Void, String>() {
                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            progressDialog.setMessage("Please wait...");
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                        }

                        @Override
                        protected String doInBackground(Void... voids) {
                            try {
                                if (is_Vehicle) {
                                    CloudStorage.shareInstance().uploadFileImage("pendingvehicle", image1, case_no);
                                }
                                if (is_Doc) {
                                    CloudStorage.shareInstance().uploadFileImage("pendingdoc", image2, case_no);
                                }
                                if (is_Doc)
                                    compoundChallanInfo.imageUrl = image2.getPath();

                                long starttime=(new Date()).getTime();
                                ApplicationConstants.TIME_ANALYST+=("&CompundChallanPDF_start_time="+starttime+"");
                                String isSucess= PDFHelper.getInstance().generateCompoundFile(compoundChallanInfo) ? "Successfully" : null;
                                long endtime=(new Date()).getTime();
                                long executintime=endtime-starttime;
                                ApplicationConstants.TIME_ANALYST+=("&CompundChallanPDF_end_time="+endtime+"&CompundChallanPDF_exc_time="+executintime);
                                return isSucess;
                            } catch (Exception e) {
                                e.printStackTrace();
                                return null;
                            }
                        }

                        @Override
                        protected void onPostExecute(String result) {
                            super.onPostExecute(result);
                            progressDialog.dismiss();
                            if(case_no!=null && case_no.trim().length()>0){
                                SaveSpotPdfToCloud();
                            }else{
                                showDialog(case_no, 2,rc_no);
                                Toast.makeText(mContext, "False Upload Image!", Toast.LENGTH_LONG).show();
                            }
                        }
                    }.execute();

                } else {
                    Toast.makeText(mContext,"No response from server.", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void SaveSpotPdfToCloud() {
        String comp_case_dt = date_tv.getText().toString();
        //String spot_fine_amt = "" + CompoundChallan.fine;
        spotChallanInfo.date = comp_case_dt;
        //spotChallanInfo.case_no = parts[0];
        /*spotChallanInfo.case_id = jsonObject.getString("msg");
        spotChallanInfo.oname = jsonObject.getString("oname");
        spotChallanInfo.guard = jsonObject.getString("Guard");*/
        //new CallWebServiceAsyncTask(mContext, ApplicationConstants.INSERT_SPOT_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.COMP_CASE_NO + "=" + comp_case_no + "&" + ApplicationConstants.COMP_CASE_DT + "=" + comp_case_dt + "&" + ApplicationConstants.SPOT_FINE_AMT + "=" + spot_fine_amt, 0).setListener(PrintCompundChallan.this);
        new AsyncTask<SpotChallanInfo, Void, String>() {
            ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new ProgressDialog(mContext);
                dialog.setCancelable(false);
                dialog.setMessage("Please wait...");
                dialog.show();
            }

            @Override
            protected String doInBackground(SpotChallanInfo... spotsInfo) {
                long starttime=(new Date()).getTime();
                ApplicationConstants.TIME_ANALYST+=("&SPOTChallanPDF_start_time="+starttime+"");
                String isSucess= PDFHelper.getInstance().generateSpotFile(spotsInfo[0]) ? "Successfully" : null;
                long endtime=(new Date()).getTime();
                long executintime=endtime-starttime;
                ApplicationConstants.TIME_ANALYST+=("&SPOTChallanPDF_end_time="+endtime+"&SPOTChallanPDF_exc_time="+executintime);
                return isSucess;
                //return PDFHelper.getInstance().generateSpotFile(spotsInfo[0]) ? "Successfully" : null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                dialog.dismiss();
                String reg_device_id = Utils.getIMEI(mContext);
                ApplicationConstants.TIME_ANALYST+=("&IMEI_NO="+reg_device_id);
                ApplicationConstants.TIME_ANALYST+=("&NETWORK_TYPE="+Utils.getNetworkClass(mContext));
                ApplicationConstants.TIME_ANALYST+=("&MODEL="+Utils.getModel());
                ApplicationConstants.TIME_ANALYST+=("&VERSION="+Utils.getSdkversion());
                ApplicationConstants.TIME_ANALYST+=("&PRODUCT="+Utils.getProduct());
                (new CallWebServiceAsyncTask(mContext,ApplicationConstants.TIME,"",-1)).setListener(null);
                showDialog(case_no, 2,spotChallanInfo.case_id);
            }
        }.execute(spotChallanInfo);

    }

    private void addDataTable(CaseInfo[] caseListCollection) {
        String rowColor = "#F5F5F5";
        View view = tableCaseHistory.getChildAt(0);
        tableCaseHistory.removeAllViews();
        tableCaseHistory.addView(view);
        for (CaseInfo caseInfo : caseListCollection) {
            rowColor = (rowColor.equals("#F5F5F5")) ? "#E3F2FD" : "#F5F5F5";
            View rowView = getActivity().getLayoutInflater().inflate(R.layout.case_history_row, null);
            TextView column1 = (TextView) rowView.findViewById(R.id.case_history_column_1);
            TextView column2 = (TextView) rowView.findViewById(R.id.case_history_column_2);
            TextView column3 = (TextView) rowView.findViewById(R.id.case_history_column_3);
            TextView column4 = (TextView) rowView.findViewById(R.id.case_history_column_4);
            column1.setText(caseInfo.case_date);
            column2.setText(caseInfo.case_no);
            column3.setText(caseInfo.fine_amt);
            column4.setText(caseInfo.case_status);
            rowView.setBackgroundColor(Color.parseColor(rowColor));
            tableCaseHistory.addView(rowView);
        }
        Rect textRect = new Rect(); //coordinates to scroll to
        tableCaseHistory.getHitRect(textRect);
        ScrollView scrollView = (ScrollView) getView().findViewById(R.id.scroll_content);
        scrollView.requestChildRectangleOnScreen(tableCaseHistory, textRect, false);
    }

    public void showDialog(final String caseNo, final int type,final String rc_no) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        String msg="";
        if(type==2){
            if(caseNo.contains(",")){
                String caseno=caseNo.split(",")[0];
                String fineamount=caseNo.split(",")[1];
                msg="Challan has been successfully generated vide Case No " + caseno+", RC No "+rc_no+" & "+fineamount;
            }else{
                msg="Challan has been successfully generated vide Case No " + caseNo+", RC No "+rc_no;
            }
        }else{
            msg="Challan has been successfully generated vide Case No " + caseNo;
        }
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (type == 1) {
                            save_insert_spot_payment(caseNo);
                            dialog.dismiss();
                        } else {

                            FragmentManager frgManager = getActivity().getSupportFragmentManager();
                            Fragment fragment = ViewPagerFragment.newInstance(1);
                            frgManager.beginTransaction().replace(R.id.frame_layout_main, fragment).commitAllowingStateLoss();
                            dialog.dismiss();
                        }
                    }
                });

        AlertDialog alert = builder.create();
        alert.setTitle("Information");
        alert.show();
    }

   /* public void ShowConformationFineDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        is_Doc=false;
        is_Vehicle=false;
        SEIZED_DL_ID="";
        builder.setMessage("Subsequent Fine Amount is "+CompoundChallan.fine+"\nDo you want to proceed for the normal fine.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        fine_tv.setText("Fine : " + CompoundChallan.Fine_Amt_1 + " INR");
                        isNormalFine=true;
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        isNormalFine=false;
                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Information");
        alert.show();
    }*/

    public void showConformationDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        is_Doc=false;
        is_Vehicle=false;
        SEIZED_DL_ID="";
        builder.setMessage("Do you want to proceed for the spot fine.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        save_Challan_Data(1);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        FragmentManager frgManager = getActivity().getSupportFragmentManager();
//                        Fragment fragment = new CompoundChallan();
//                        frgManager.beginTransaction().replace(R.id.container, fragment)
//                                .commitAllowingStateLoss();
                        dialog.dismiss();
                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Information");
        alert.show();
    }

    public void showOnSpotDialog(String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Payment done successfully.\nRC Number is " + msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        FragmentManager frgManager = getActivity().getSupportFragmentManager();
//                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        Fragment fragment = ViewPagerFragment.newInstance(1);
                        frgManager.beginTransaction().replace(R.id.frame_layout_main, fragment)
                                .commitAllowingStateLoss();
                        dialog.dismiss();
                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Information");
        alert.show();
    }

    public String shortFormOffinceType(int pos) {
        String str = null;
        switch (pos) {
            case 1:
                str = "DL";
                break;
            case 2:
                str = "TT";
                break;
            case 3:
                str = "RC";
                break;
            case 4:
                str = "PERMIT";
                break;
            case 5:
                str = "IC";
                break;
            case 6:
                str = "CF";
                break;
            case 7:
                str = "OTHERS";
                break;

        }
        return str;
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        // if the result is capturing Image
//        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
//            if (resultCode == -1) {
//                data.getStringExtra(mImagePath);
//                previewCapturedImage();
//
//            } else if (resultCode == 0) {
//                if(2==value){
//                    is_Doc = false;
//                }else if(1==value){
//                    is_Vehicle = false;
//                }
//                Toast.makeText(mContext,"Cancel capture image", Toast.LENGTH_SHORT).show();
//            } else {
//                // failed to capture image
//                if(2==value){
//                    is_Doc = false;
//                }else if(1==value){
//                    is_Vehicle = false;
//                }
//                Toast.makeText(mContext,"Sorry! Failed to capture image", Toast.LENGTH_SHORT).show();
//            }
//        }
//
//    }

    private void captureImage() {
        /*String img_name = null;
        if (value == 1) {
            img_name = "image_001.png";
        } else if (value == 2) {
            img_name = "image_002.png";
        }
        Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File imagesFolder = FileHelper.getInstance().getCompoundDir();

        File img = new File(imagesFolder, img_name);
        if (img.exists()) {
            img.delete();
        }

        if (value == 1) {
            image1 = img;
        } else if (value == 2) {
            image2 = img;
        }

        mImagePath1=img.getPath();*/
        //imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(img));
        //startActivityForResult(imageIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        new SandriosCamera(getActivity(), CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
                .setShowPicker(false)
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO)
                .enableImageCropping(false)
                .launchCamera();

    }


    /*
     * Display image from a path to ImageView
     */
    private void previewCapturedImage(String filename) {
        try {
            imgPreview.setVisibility(View.VISIBLE);
            BitmapFactory.Options options = new BitmapFactory.Options();
            FileHelper.getInstance().compressImage(filename);
            Bitmap bitmap = BitmapFactory.decodeFile(filename,options);
            if (value == 1) {
                img_preview_vehicle.setImageBitmap(bitmap);
                image1= new File(filename);
                //Copyfile(image1);
            } else {
                /*FileHelper.getInstance().compressImage(image2.getPath());
                Bitmap bitmap = BitmapFactory.decodeFile(image2.getPath(),options);*/
                img_preview_doc.setImageBitmap(bitmap);
                image2=new File(filename);
                //Copyfile(image2);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void requestWriteStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        }
    }


    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA);

        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        }
    }


    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CAMERA) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.
            Log.i(TAG, "Received response for Camera permission request.");

            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Camera permission has been granted, preview can be displayed
                Log.i(TAG, "CAMERA permission has now been granted. Showing preview.");
                captureImage();
            } else {
                Log.i(TAG, "CAMERA permission was NOT granted.");

            }

        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == -1) {
                String mImagePath = data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH);
                Log.d("", mImagePath);
                previewCapturedImage(mImagePath);

            } else if (resultCode == 0) {
                if (2 == value) {
                    is_Doc = false;
                } else if (1 == value) {
                    is_Vehicle = false;
                }
                Toast.makeText(mContext, "Cancel capture image", Toast.LENGTH_SHORT).show();
            } else {
                // failed to capture image
                if (2 == value) {
                    is_Doc = false;
                } else if (1 == value) {
                    is_Vehicle = false;
                }
                Toast.makeText(mContext, "Sorry! Failed to capture image", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void Copyfile(File source){
        InputStream is = null;
        OutputStream os = null;
        String img_name = null;
        if (value == 1) {
            img_name = "image_001.png";
        } else if (value == 2) {
            img_name = "image_002.png";
        }
        File imagesFolder = FileHelper.getInstance().getCompoundDir();
        File img = new File(imagesFolder, img_name);
        if (img.exists()) {
            img.delete();
        }
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(img);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }

            is.close();
            os.close();
        } catch(Exception e){

        }finally {

        }
    }
}

