package br.skymapglobal.ktp_challan.fragment.challan;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Calendar;

import br.skymapglobal.ktp_challan.ProsecutionCaseDetailActivity;
import br.skymapglobal.ktp_challan.R;
import br.skymapglobal.ktp_challan.adapter.DividerItemDecoration;
import br.skymapglobal.ktp_challan.adapter.ItemModel;
import br.skymapglobal.ktp_challan.adapter.ProsecutionRecyclerViewAdapter;
import br.skymapglobal.ktp_challan.adapter.RecyclerViewType;
import br.skymapglobal.ktp_challan.asynctasks.CallWebServiceAsyncTask;
import br.skymapglobal.ktp_challan.helpers.ApplicationConstants;
import br.skymapglobal.ktp_challan.helpers.CallbackInterface;
import br.skymapglobal.ktp_challan.helpers.IProsecutionListener;
import br.skymapglobal.ktp_challan.helpers.Utils;
import br.skymapglobal.ktp_challan.models.ResponseGetSummery;
import br.skymapglobal.ktp_challan.models.entity.ProsecutionInfo;

public class ProsecutionFragment extends Fragment implements CallbackInterface, IProsecutionListener {


    private IProsecutionListener mListener;
    private ArrayList<ItemModel> listModels;
    private ProsecutionRecyclerViewAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listModels = new ArrayList<>();
        listModels.add(new ItemModel(RecyclerViewType.HEADER, null, null));
        listModels.add(new ItemModel(RecyclerViewType.FOOTER, "0", null));
        adapter = new ProsecutionRecyclerViewAdapter(listModels, mListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_prosecution, container, false);


        Context context = view.getContext();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list_prosecution);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        getSummeryRequest();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = this;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void getSummeryRequest() {
        String param = "&reg_device_id=%s&reg_device_type=%s&PP_CD=%s&date=%s";
        String reg_device_id = br.skymapglobal.ktp_challan.helpers.Utils.getIMEI(getActivity());
        String reg_device_type = "A";
        String PP_CD = br.skymapglobal.ktp_challan.helpers.Utils.getStringPreferences(getActivity(), "USERNAME", "");
        String date = Utils.convertDateToString(Calendar.getInstance().getTime(), "dd-MMM-yyyy").toUpperCase();

//        reg_device_id = "355240072233698";
//        PP_CD = "CAL/WB9560";
        param = String.format(param, reg_device_id, reg_device_type, PP_CD, date);
        new CallWebServiceAsyncTask(getActivity(), ApplicationConstants.GET_SUMMERY_METHOD, param, 0).setListener(this);
    }

    @Override
    public void refreshList(String response, String method, int type) {
        if (type == 0 && method.equals(ApplicationConstants.GET_SUMMERY_METHOD)) {
            ResponseGetSummery responseGetSummery = ResponseGetSummery.parseFromJSON(response);
            if (responseGetSummery == null) {
                showAlert("", "Data Empty");
                listModels = new ArrayList<>();
                listModels.add(new ItemModel(RecyclerViewType.HEADER, null, null));
                listModels.add(new ItemModel(RecyclerViewType.FOOTER, "0", null));
                adapter.setItems(listModels);
                adapter.notifyDataSetChanged();
                return;
            }
            if (responseGetSummery.status == 1) {
                if (responseGetSummery.data != null) {
                    listModels = new ArrayList<>();
                    listModels.add(new ItemModel(RecyclerViewType.HEADER, null, null));
                    int sum = 0;
                    for (ProsecutionInfo prosecutionInfo : responseGetSummery.data) {
                        ItemModel item = new ItemModel(RecyclerViewType.ITEM_1, null, prosecutionInfo);
                        listModels.add(item);
                        try {
                            sum += Double.parseDouble(prosecutionInfo.TOTAL_FINE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    listModels.add(new ItemModel(RecyclerViewType.FOOTER, String.valueOf(sum), null));
                    adapter.setItems(listModels);
                    adapter.notifyDataSetChanged();
                }

            } else {
                showAlert("Error", responseGetSummery.msg);
            }
        }
    }

    void showAlert(String title, String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(msg);
        alertDialog.setTitle(title);
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alertDialog.create().show();
    }

    @Override
    public void onItemClick(ProsecutionInfo item) {

        Intent intent = new Intent(getActivity(), ProsecutionCaseDetailActivity.class);
        intent.putExtra(ProsecutionCaseDetailActivity.ARG_TITLE, item.type);
        intent.putExtra(ProsecutionCaseDetailActivity.ARG_CASE, item.type);
        getActivity().startActivity(intent);
//        Toast.makeText(getContext(), item.TOTAL_CASE, Toast.LENGTH_LONG).show();
    }

}
