
/**
 * Created by sl5 on 9/6/16.
 */
package br.skymapglobal.ktp_challan.fragment.challan;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import br.skymapglobal.ktp_challan.R;
import br.skymapglobal.ktp_challan.asynctasks.CallWebServiceAsyncTask;
import br.skymapglobal.ktp_challan.fragment.ViewPagerFragment;
import br.skymapglobal.ktp_challan.helpers.ApplicationConstants;
import br.skymapglobal.ktp_challan.helpers.CallbackInterface;
import br.skymapglobal.ktp_challan.helpers.CloudStorage;
import br.skymapglobal.ktp_challan.helpers.EnglishNumberToWords;
import br.skymapglobal.ktp_challan.helpers.FileHelper;
import br.skymapglobal.ktp_challan.helpers.PDFHelper;
import br.skymapglobal.ktp_challan.helpers.Utils;
import br.skymapglobal.ktp_challan.models.DL_info;
import br.skymapglobal.ktp_challan.models.SeizureChallanInfo;
import br.skymapglobal.ktp_challan.models.Vehicle_info;

public class PrintSeizureChallan extends Fragment implements CallbackInterface {

    public static Bitmap bitmap;
    private Context mContext;
    private ImageView imgPreview_vehicle, imgPreview_document;
    private LinearLayout cancel_btn, pending_challan_btn;
    private String case_id;
    private TextView DL_status_tv, name_tv, add_tv, issue_date_tv, expiry_date_tv, vehicle_class_tv, fine_tv, loc_tv;
    private TextView section_tv, date_tv, owner_name1_tv, reg_number_tv, vehicle_type_tv, owner_name_tv, uuid_tv, owner_add_tv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getActivity();
        View view = inflater.inflate(R.layout.print_seizure_challan, container,
                false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.
                Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        loc_tv = (TextView) view.findViewById(R.id.loc_tv);
        fine_tv = (TextView) view.findViewById(R.id.fine_tv);
        date_tv = (TextView) view.findViewById(R.id.date_tv);
        uuid_tv = (TextView) view.findViewById(R.id.uuid_tv);
        section_tv = (TextView) view.findViewById(R.id.section_tv);
        imgPreview_vehicle = (ImageView) view.findViewById(R.id.imgPreview_vehicle);
        owner_add_tv = (TextView) view.findViewById(R.id.owner_add_tv);
        owner_name_tv = (TextView) view.findViewById(R.id.owner_name_tv);
        reg_number_tv = (TextView) view.findViewById(R.id.reg_number_tv);
        owner_name1_tv = (TextView) view.findViewById(R.id.owner_name1_tv);
        vehicle_type_tv = (TextView) view.findViewById(R.id.vehicle_type_tv);
//        print_challan_btn = (LinearLayout) view.findViewById(R.id.print_challan_btn);
        cancel_btn = (LinearLayout) view.findViewById(R.id.cancel_ll);
        add_tv = (TextView) view.findViewById(R.id.add_tv);
        name_tv = (TextView) view.findViewById(R.id.name_tv);
        DL_status_tv = (TextView) view.findViewById(R.id.DL_status_tv);
        issue_date_tv = (TextView) view.findViewById(R.id.issue_date_tv);
        expiry_date_tv = (TextView) view.findViewById(R.id.expiry_date_tv);
        vehicle_class_tv = (TextView) view.findViewById(R.id.vehicle_class_tv);
        pending_challan_btn = (LinearLayout) view.findViewById(R.id.pending_challan_btn);
        imgPreview_document = (ImageView) view.findViewById(R.id.imgPreview_document);
        if (!SeizureChallan.vehicle_no_et.getText().toString().equals("")) {
            reg_number_tv.setText(SeizureChallan.vehicle_no_et.getText().toString());
        }
        fine_tv.setText("Fine : " + SeizureChallan.fine + " INR");

        loc_tv.setText(SeizureChallan.current_loc_et.getText().toString());


        BitmapFactory.Options options = new BitmapFactory.Options();

        if (SeizureChallan.is_Vehicle) {
            FileHelper.getInstance().compressImage(SeizureChallan.vehicleImage.getPath());
            imgPreview_vehicle.setImageBitmap(BitmapFactory.decodeFile(SeizureChallan.vehicleImage.getPath(),
                    options));
        }

        if (SeizureChallan.is_Doc) {
            FileHelper.getInstance().compressImage(SeizureChallan.docImage.getPath());
            imgPreview_document.setImageBitmap(BitmapFactory.decodeFile(SeizureChallan.docImage.getPath(),
                    options));
        }

        if (SeizureChallan.vehicle_info != null) {
            populateVehicleInfo(SeizureChallan.vehicle_info);
        }
        if (SeizureChallan.licence_info_table != null) {
            populateDriverInfo(SeizureChallan.licence_info_table);
        }

        pending_challan_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save_Challan_Data(0);
            }
        });
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager frgManager = getActivity().getSupportFragmentManager();
                Fragment fragment = ViewPagerFragment.newInstance(2);
                frgManager.beginTransaction().replace(R.id.frame_layout_main, fragment)
                        .commitAllowingStateLoss();
            }
        });

        return view;
    }

    public void populateVehicleInfo(Vehicle_info registration_info_table) {

        owner_add_tv.setText(registration_info_table.getOwner_address());
        owner_name_tv.setText(registration_info_table.getOwner_name());
        owner_name1_tv.setText(registration_info_table.getOwner_name());
        vehicle_type_tv.setText(registration_info_table.getVech_type());
        uuid_tv.setText(registration_info_table.getOwnerf_name());
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        date_tv.setText(formattedDate);
        if (SeizureChallan.section != null) {
            section_tv.setText(SeizureChallan.section);
        }
    }

    public void populateDriverInfo(DL_info licence_info_table) {

        add_tv.setText(licence_info_table.getOwner_address());
        name_tv.setText(licence_info_table.getOwner_name());
        issue_date_tv.setText(licence_info_table.getIssu_dt());
        expiry_date_tv.setText(licence_info_table.getValid_upto());
        vehicle_class_tv.setText(licence_info_table.getLcns_cat());
    }

    SeizureChallanInfo seizureChallanInfo = new SeizureChallanInfo();

    public void save_Challan_Data(int num) {
        seizureChallanInfo = new SeizureChallanInfo();

        String v_no = reg_number_tv.getText().toString();
        String reg_device_id = Utils.getIMEI(mContext);
        String reg_device_type = "A";
        String vehicle_type = vehicle_type_tv.getText().toString();
        String Acused_prsn_name = owner_name_tv.getText().toString();
        String Acused_prsn_addr = owner_add_tv.getText().toString();
        String Beat_No = SeizureChallan.beat_et.getText().toString();
        Utils.saveStringPreferences(mContext, "BEAT_NO", Beat_No);
        String Road_Name = loc_tv.getText().toString();
        String us01 = section_tv.getText().toString();
        int on01 = Integer.parseInt(SeizureChallan.offence_no) + 1;
        String us02 = "";
        int on02 = 0;
        String us03 = "";
        int on03 = 0;
        String us04 = "";
        int on04 = 0;
        String us05 = "";
        int on05 = 0;
        int Fine_AMT = SeizureChallan.fine;
        String DOC_NAME = SeizureChallan.DOC_NAME;
        String SEIZED_DOC_ID = SeizureChallan.SEIZED_DOC_ID;
        String MOBILE_NO = SeizureChallan.phn_et.getText().toString();
        Utils.saveStringPreferences(mContext, "VEHICLE_NO", v_no);
        Utils.saveStringPreferences(mContext, "U/S", us01);
        String omobile = Utils.getStringPreferences(mContext, "PASSWORD", "");
        String PP_CD = Utils.getStringPreferences(mContext, "USERNAME", "").toUpperCase();
        String category = "S";
        String guard_code = Utils.getStringPreferences(mContext, "Guard_Code", "").toUpperCase();

        seizureChallanInfo.MOBILE_NO = MOBILE_NO;
        seizureChallanInfo.Acused_prsn_name = Acused_prsn_name;
        seizureChallanInfo.vehicle_no = v_no;
        seizureChallanInfo.offenceType = us01;
        seizureChallanInfo.fineAmount = Fine_AMT + "";
        seizureChallanInfo.place = Road_Name;
        seizureChallanInfo.fineAmountStr = EnglishNumberToWords.convert(Fine_AMT);

        new CallWebServiceAsyncTask(mContext, ApplicationConstants.GENRT_SEIZURE_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.VEHICLE_NO + "=" + v_no + "&" + ApplicationConstants.VEHICLE_TYPE + "=" + vehicle_type + "&" + ApplicationConstants.ACUSED_PRSN_NAME + "=" + Acused_prsn_name + "&" + ApplicationConstants.ACUSED_PRSN_ADDR + "=" + Acused_prsn_addr + "&" + ApplicationConstants.BEAT_NO + "=" + Beat_No + "&" + ApplicationConstants.ROAD_NAME + "=" + Road_Name + "&" + ApplicationConstants.US_01 + "=" + us01 + "&" + ApplicationConstants.ON_01 + "=" + on01 + "&" + ApplicationConstants.US_02 + "=" + us02 + "&" + ApplicationConstants.ON_02 + "=" + on02 + "&" + ApplicationConstants.US_03 + "=" + us03 + "&" + ApplicationConstants.ON_03 + "=" + on03 + "&" + ApplicationConstants.US_04 + "=" + us04 + "&" + ApplicationConstants.ON_04 + "=" + on04 + "&" + ApplicationConstants.US_05 + "=" + us05 + "&" + ApplicationConstants.ON_05 + "=" + on05 + "&" + ApplicationConstants.Fine_AMT + "=" + Fine_AMT + "&" + ApplicationConstants.DOC_NAME + "=" + DOC_NAME + "&" + ApplicationConstants.SEIZED_DOC_ID + "=" + SEIZED_DOC_ID + "&" + ApplicationConstants.MOBILE_NO + "=" + MOBILE_NO + "&" + ApplicationConstants.PP_CD + "=" + PP_CD + "&" + ApplicationConstants.OMOBILE + "=" + omobile + "&" + ApplicationConstants.CATEGORY + "=" + category + "&" + ApplicationConstants.GUARD_CODE + "=" + guard_code, num).setListener(PrintSeizureChallan.this);
    }

    public void save_insert_spot_payment(String comp_case_no_str) {
        String reg_device_id = Utils.getIMEI(mContext);
        String reg_device_type = "A";
        String comp_case_no = comp_case_no_str;
        String comp_case_dt = date_tv.getText().toString();
        String spot_fine_amt = "" + SeizureChallan.fine;
        new CallWebServiceAsyncTask(mContext, ApplicationConstants.INSERT_SPOT_METHOD, "&" + ApplicationConstants.REG_DEVICE_ID + "=" + reg_device_id + "&" + ApplicationConstants.REG_DEVICE_TYPE + "=" + reg_device_type + "&" + ApplicationConstants.COMP_CASE_NO + "=" + comp_case_no + "&" + ApplicationConstants.COMP_CASE_DT + "=" + comp_case_dt + "&" + ApplicationConstants.SPOT_FINE_AMT + "=" + spot_fine_amt, 0).setListener(PrintSeizureChallan.this);
    }

    @Override
    public void refreshList(String response, final String method, final int type) {
        if (method == ApplicationConstants.GENRT_SEIZURE_METHOD) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1)) {
                    case_id = jsonObject.getString("msg");
                    this.seizureChallanInfo.case_id = case_id;
                    this.seizureChallanInfo.oname = jsonObject.getString("oname");
                    this.seizureChallanInfo.userName = Utils.getStringPreferences(getActivity(), "USERNAME", "");
                    this.seizureChallanInfo.bitmap = getBitmapFromAsset(mContext, "checker.png");
                    this.seizureChallanInfo.itemCheck = SeizureChallan.selectType;
                    new AsyncTask<Void, Void, String>() {
                        ProgressDialog progressDialog;

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            progressDialog = new ProgressDialog(mContext);
                            progressDialog.setMessage("Please wait...");
                            progressDialog.show();
                        }

                        @Override
                        protected String doInBackground(Void... voids) {
                            try {
                                if (SeizureChallan.is_Doc) {
                                    CloudStorage.shareInstance().uploadFileImage("seizuredoc", SeizureChallan.docImage, case_id);
                                    seizureChallanInfo.urlImage = SeizureChallan.docImage.getPath();
                                }
                                if (SeizureChallan.is_Vehicle) {
                                    CloudStorage.shareInstance().uploadFileImage("seizurevehicle", SeizureChallan.vehicleImage, case_id);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return PDFHelper.getInstance().generateSeizureFile(seizureChallanInfo) ? "Successfully" : null;
                        }

                        @Override
                        protected void onPostExecute(String result) {
                            super.onPostExecute(result);
                            if (result == null) {
                                Toast.makeText(mContext, "False Upload PDF File!", Toast.LENGTH_LONG).show();
                            }
                            progressDialog.dismiss();
                            showDialog(case_id, type);
                        }
                    }.execute();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (method == ApplicationConstants.INSERT_SPOT_METHOD) {
            System.out.println("response=" + response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1) && (jsonObject.getString("msg").equals("Success"))) {
                    showOnSpotDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap getBitmapFromAsset(Context context, String paramString) {
        try {
            return BitmapFactory.decodeStream(context.getAssets().open(paramString));
        } catch (IOException paramContext) {
        }
        return null;
    }

    public void showDialog(final String msg, final int type) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Challan has been successfully generated vide case no is " + msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (type == 1) {
                            save_insert_spot_payment(msg);
                            dialog.dismiss();
                        } else {

                            FragmentManager frgManager = getActivity().getSupportFragmentManager();
                            Fragment fragment = ViewPagerFragment.newInstance(2);
                            frgManager.beginTransaction().replace(R.id.frame_layout_main, fragment)
                                    .commitAllowingStateLoss();
                            dialog.dismiss();
                        }
                    }
                });

        AlertDialog alert = builder.create();
        alert.setTitle("Information");
        alert.show();
    }


    public void showOnSpotDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Payment done successfully.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        FragmentManager frgManager = getActivity().getSupportFragmentManager();
                        Fragment fragment = ViewPagerFragment.newInstance(2);
                        frgManager.beginTransaction().replace(R.id.frame_layout_main, fragment)
                                .commitAllowingStateLoss();
                        dialog.dismiss();
                    }
                });

        AlertDialog alert = builder.create();
        alert.setTitle("Information");
        alert.show();
    }

}

