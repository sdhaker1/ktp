package br.skymapglobal.ktp_challan.fragment.internalreport;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import br.skymapglobal.ktp_challan.R;
import br.skymapglobal.ktp_challan.helpers.BucketConstants;

public class OthersFragment extends BaseInternalReport {
    private EditText edtTitle;

    @NonNull
    @Override
    String getBucketName() {
        return BucketConstants.REPORT_OTHERS;
    }

    @NonNull
    @Override
    String getTableName() {
        return "tblOthers";
    }

    @Override
    void initialView(View view) {
        view.findViewById(R.id.select_type_view).setVisibility(View.GONE);
        view.findViewById(R.id.title_view).setVisibility(View.VISIBLE);
        edtTitle = (EditText) view.findViewById(R.id.edt_title);
    }

    @Override
    String getTypeSelected() {
        if(TextUtils.isEmpty(edtTitle.getText().toString().trim())){
            edtTitle.setError("Field require!");
            return null;
        }
        return edtTitle.getText().toString();
    }

}
