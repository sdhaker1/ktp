package br.skymapglobal.ktp_challan.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.skymapglobal.ktp_challan.R;
import br.skymapglobal.ktp_challan.adapter.TabPagerItem;
import br.skymapglobal.ktp_challan.adapter.ViewPagerAdapter;
import br.skymapglobal.ktp_challan.fragment.challan.CompoundChallan;
import br.skymapglobal.ktp_challan.fragment.challan.SeizureChallan;

public class ViewPagerFragment extends Fragment {
	private List<TabPagerItem> mTabs = new ArrayList<>();
    private int type;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createTabPagerItem();
    }
    public static ViewPagerFragment newInstance(int text){
        ViewPagerFragment mFragment = new ViewPagerFragment();
        Bundle mBundle = new Bundle();
        mBundle.putInt("VAlUE", text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }

    private void createTabPagerItem(){
        Fragment fragment=null;
        fragment = new CompoundChallan();
        mTabs.add(new TabPagerItem(getString(R.string.starred), fragment));
        fragment = new SeizureChallan();
        mTabs.add(new TabPagerItem(getString(R.string.important), fragment));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        type= bundle.getInt("VAlUE", 0);
        View rootView = inflater.inflate(R.layout.fragment_viewpager, container, false);
        rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT ));
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
    	ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
    	mViewPager.setOffscreenPageLimit(mTabs.size());
        mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), mTabs));
        TabLayout mSlidingTabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mSlidingTabLayout.setElevation(15);
        }
        mSlidingTabLayout.setupWithViewPager(mViewPager);
        TabLayout.Tab tab = mSlidingTabLayout.getTabAt(type-1);
        tab.select();
    }
}