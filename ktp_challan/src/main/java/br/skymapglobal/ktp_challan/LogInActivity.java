package br.skymapglobal.ktp_challan;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresPermission;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.regex.Pattern;

import br.skymapglobal.ktp_challan.asynctasks.CallWebServiceAsyncTask;
import br.skymapglobal.ktp_challan.helpers.ApplicationConstants;
import br.skymapglobal.ktp_challan.helpers.CallbackInterface;
import br.skymapglobal.ktp_challan.helpers.Utils;

public class LogInActivity extends AppCompatActivity implements CallbackInterface, View.OnClickListener {

    private Context context;
    private AppCompatEditText user_name, password;
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 0;
    private static View mLoginForm;
    private static String devicetoken="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_actvity);
        password = (AppCompatEditText) findViewById(R.id.editText_password);
        user_name = (AppCompatEditText) findViewById(R.id.editText_username);
        user_name.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mLoginForm = findViewById(R.id.login_form);
        AppCompatCheckBox checkbox_showpassword = (AppCompatCheckBox) findViewById(R.id.checkbox_showpassword);
        checkbox_showpassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    password.setInputType(129);
                }
            }
        });
        findViewById(R.id.btn_login).setOnClickListener(this);
        findViewById(R.id.ivHelp1).setOnClickListener(this);
        findViewById(R.id.ivHelp2).setOnClickListener(this);

        context = LogInActivity.this;
        loadIMEI();
        mLoginForm.setVisibility(View.INVISIBLE);
        initLoginView();
    }

    private void initLoginView() {
        if (Utils.getBoolPreferences(context, "IS_LOGIN")) {
            Intent intent = new Intent(context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } else {
            mLoginForm.setVisibility(View.VISIBLE);
        }
    }

    public void requestLogin() {
        devicetoken=Utils.getStringPreferences(context,"DEVICE_TOKEN","");
        if(devicetoken.trim().length()>0){
            requestRegister(devicetoken);
        }else{
            attemptRegister();
        }
    }

    public void requestRegister(@NonNull String deviceToken) {
        String param = "&reg_mobile_no=%s&reg_device_id=%s&reg_device_type=A&version=1&reg_device_token=%s&reg_email=%s&gpf=%s";
        String mob_no = password.getText().toString().trim();
        String reg_device_id = Utils.getIMEI(context);
        String email_id=getAccountName();
        String gpf = user_name.getText().toString().trim();
        param = String.format(param, mob_no, reg_device_id, deviceToken,email_id, gpf);
        new CallWebServiceAsyncTask(context, ApplicationConstants.REGISTER_METHOD, param, 0).setListener(LogInActivity.this);
    }

    public void requestDeviceStatus() {
        String param = "&reg_mobile_no=%s&reg_device_type=A&reg_device_id=%s";
        String mob_no = password.getText().toString().trim();
        String reg_device_id = Utils.getIMEI(context);
        param = String.format(param, mob_no, reg_device_id);
        new CallWebServiceAsyncTask(context, ApplicationConstants.DEVICE_STATUS, param, 0).setListener(LogInActivity.this);
    }


    @Override
    public void refreshList(String response, String method, int num) {

        if (method.equals(ApplicationConstants.REGISTER_METHOD)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1) && (jsonObject.getString("msg").equals("Success"))) {
                    Utils.saveBoolPreferences(context, "LOGIN_REGISTER", true);
                    Utils.saveStringPreferences(context, "USERNAME", user_name.getText().toString().trim());
                    Utils.saveStringPreferences(context, "PASSWORD", password.getText().toString().trim());
                    requestDeviceStatus();
                } else {
                    showAlert("Login", jsonObject.getString("msg"));
                }
            } catch (Exception e) {
                Toast.makeText(context, "Server Error!", Toast.LENGTH_LONG).show();
            }
        }
        if (method.equals(ApplicationConstants.DEVICE_STATUS)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if ((jsonObject.getInt("status") == 1) && (jsonObject.getString("msg").equals("Success"))) {
                    Utils.saveBoolPreferences(context, "IS_LOGIN", true);
                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    showAlert("Login", jsonObject.getString("msg"));
                }
            } catch (Exception e) {
                Toast.makeText(context, "Server Error!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void ShowToast(String message){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) findViewById(R.id.custom_toast_container));

        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(message);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    void showAlert(String title, String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(msg);
        alertDialog.setTitle(title);
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alertDialog.create().show();
    }

    void showConfirmRegister(String title, String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(msg);
        alertDialog.setTitle(title);
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                attemptRegister();
                dialog.dismiss();
            }
        });
        alertDialog.create().show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.exit(0);
        finishAffinity();
    }

    private void attemptRegister() {
        new AsyncTask<Void, Void, String>() {

            ProgressDialog progressDialog = new ProgressDialog(context);

            @Override
            protected void onPreExecute(){
                progressDialog.setMessage("Please Wait....");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
                try {
                    return instanceID.getToken(getString(R.string.gcm_defaultSenderId),GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String token) {
                progressDialog.dismiss();
                if (token != null) {
                    devicetoken=token;
                    Utils.saveStringPreferences(context, "DEVICE_TOKEN", token);
                    requestRegister(token);
                } else {
                    requestRegister("");
                    //Toast.makeText(context, "GSM service Error!", Toast.LENGTH_LONG).show();
                }
            }
        }.execute();

    }

    public void loadIMEI() {
        // Check if the READ_PHONE_STATE permission is already available.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // READ_PHONE_STATE permission has not been granted.
            requestReadPhoneStatePermission();
        } else {
            // READ_PHONE_STATE permission is already been granted.
            doPermissionGrantedStuffs();
        }
    }

    private String getAccountName(){
        String accountName="";
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        try{
            Account[] accounts = AccountManager.get(context).getAccounts();
            for (Account account : accounts) {
                if (emailPattern.matcher(account.name).matches()) {
                    accountName = account.name;
                    break;
                }
            }
        }catch (SecurityException ex){}
        return accountName;
    }
    /**
     * Requests the READ_PHONE_STATE permission.
     * If the permission has been denied previously, a dialog will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    private void requestReadPhoneStatePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_PHONE_STATE)) {
            ActivityCompat.requestPermissions(LogInActivity.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,@NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    doPermissionGrantedStuffs();
                }
        }
    }

    public void doPermissionGrantedStuffs() {
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        Utils.IMEI_No = tm.getDeviceId();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                if (user_name.getText().toString().equals("")) {
                    Toast.makeText(context, "Please enter username", Toast.LENGTH_LONG).show();
                } else if (password.getText().toString().equals("")) {
                    Toast.makeText(context, "Please enter password", Toast.LENGTH_LONG).show();
                } else {
                    requestLogin();
                }
                break;
            case R.id.ivHelp1:
                ShowToast("User Name is your GPF No. followed by CAl/WB \nExample CAL/WBXX123");
                break;
            case R.id.ivHelp2:
                ShowToast("Please provide your 10 digit mobile no which was registered at the time of registration");
                break;
            default:
                break;
        }
    }
}