package br.skymapglobal.ktp_challan;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.jsoup.Jsoup;

import br.skymapglobal.ktp_challan.dao.DatabaseHelper;
import br.skymapglobal.ktp_challan.fragment.ViewPagerFragment;
import br.skymapglobal.ktp_challan.fragment.challan.ActivityResultListner;
import br.skymapglobal.ktp_challan.fragment.challan.ProsecutionFragment;
import br.skymapglobal.ktp_challan.fragment.challan.VehicleWiseFragment;
import br.skymapglobal.ktp_challan.fragment.internalreport.OthersFragment;
import br.skymapglobal.ktp_challan.fragment.internalreport.RoadConditionFragment;
import br.skymapglobal.ktp_challan.fragment.internalreport.RoadFurnitureFragment;
import br.skymapglobal.ktp_challan.fragment.internalreport.RoadMarkingFragment;
import br.skymapglobal.ktp_challan.fragment.internalreport.SignalFragment;
import br.skymapglobal.ktp_challan.fragment.internalreport.TrafficCongestionFragment;
import br.skymapglobal.ktp_challan.fragment.internalreport.ViolationFragment;
import br.skymapglobal.ktp_challan.helpers.PDFHelper;
import br.skymapglobal.ktp_challan.helpers.Utils;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Fragment mMainFragment;
    private DatabaseHelper databaseHelper;
    private Context context;
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 0;
    private static final int REQUEST_WRITE_STORAGE = 1;

    private ActivityResultListner myResultListner ;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        new AsyncTask<String, String, String>() {
//            @Override
//            protected String doInBackground(String... strings) {
//                SpotChallanInfo spotChallanInfo =new SpotChallanInfo();
//                spotChallanInfo.case_id="1";
//                spotChallanInfo.case_no="1";
//                spotChallanInfo.vehicle_no="1";
//                spotChallanInfo.offence_type="1";
//                spotChallanInfo.date="1";
//                spotChallanInfo.spot_fine_amt="1";
//                spotChallanInfo.courtName="1";
//                spotChallanInfo.fineAmountStr="1";
//                PDFHelper.getInstance().generateSpotFile(spotChallanInfo);
//                return null;
//            }
//        }.execute();
        isGrantedPermission();
        loadIMEI();
        PDFHelper.initial(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if (mMainFragment == null) {
            mMainFragment = ViewPagerFragment.newInstance(1);
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout_main, mMainFragment).commit();
            navigationView.setCheckedItem(R.id.nav_upload_challan);
        }
        databaseHelper = new DatabaseHelper(this);
        context = this;

        View headerLayout = navigationView.getHeaderView(0);
        TextView source = (TextView) headerLayout.findViewById(R.id.user_source);
        TextView memberId = (TextView) headerLayout.findViewById(R.id.user_member_id);
        ImageView avatar = (ImageView) headerLayout.findViewById(R.id.user_avatar);
        source.setText("SOURCE : KTP");
        memberId.setText("MEMBER ID : " + Utils.getStringPreferences(context, "USERNAME", ""));
        avatar.setImageResource(R.drawable.ic_no_user);
        new GetVersionCode().execute();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            System.exit(0);
            finishAffinity();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        int id = item.getItemId();
        switch (id) {
//            case R.id.nav_register:
//                getSupportActionBar().setTitle("Register with Police");
//                if (mMainFragment instanceof RegisterFragment) return true;
//                mMainFragment = new RegisterFragment();
//                break;
            case R.id.nav_upload_challan:
                getSupportActionBar().setTitle("Challan Generate");
                if (mMainFragment instanceof ViewPagerFragment) return true;
                mMainFragment = ViewPagerFragment.newInstance(1);
                break;

            case R.id.nav_wise_total_pending:
                getSupportActionBar().setTitle("Vehicle Wise Total Pending");
                if (mMainFragment instanceof VehicleWiseFragment) return true;
                mMainFragment = new VehicleWiseFragment();
                break;

            case R.id.nav_prosecution:
                getSupportActionBar().setTitle("Individual Prosecution Detail");
                if (mMainFragment instanceof ProsecutionFragment) return true;
                mMainFragment = new ProsecutionFragment();
                break;

            case R.id.nav_signal:
                getSupportActionBar().setTitle("Report Signal");
                if (mMainFragment instanceof SignalFragment) return true;
                mMainFragment = new SignalFragment();
                break;

            case R.id.nav_traffic_congestion:
                getSupportActionBar().setTitle("Report Traffic Congestion");
                if (mMainFragment instanceof TrafficCongestionFragment) return true;
                mMainFragment = new TrafficCongestionFragment();
                break;

            case R.id.nav_violation:
                getSupportActionBar().setTitle("Report Violation");
                if (mMainFragment instanceof ViolationFragment) return true;
                mMainFragment = new ViolationFragment();
                break;

            case R.id.nav_road_condition:
                getSupportActionBar().setTitle("Report Road Condition");
                if (mMainFragment instanceof RoadConditionFragment) return true;
                mMainFragment = new RoadConditionFragment();
                break;

            case R.id.nav_road_furniture:
                getSupportActionBar().setTitle("Report Road Furniture");
                if (mMainFragment instanceof RoadFurnitureFragment) return true;
                mMainFragment = new RoadFurnitureFragment();
                break;

            case R.id.nav_road_marking:
                getSupportActionBar().setTitle("Report Road Marking");
                if (mMainFragment instanceof RoadMarkingFragment) return true;
                mMainFragment = new RoadMarkingFragment();
                break;

            case R.id.nav_other:
                getSupportActionBar().setTitle("Report Others");
                if (mMainFragment instanceof OthersFragment) return true;
                mMainFragment = new OthersFragment();
                break;

            case R.id.nav_version:
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:"));
                startActivity(sendIntent);
                break;

            case R.id.nav_logout:
                Utils.saveBoolPreferences(context, "IS_LOGIN", false);
                Intent intent = new Intent(context, LogInActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout_main, mMainFragment).commit();
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


    public void loadIMEI() {
        // Check if the READ_PHONE_STATE permission is already available.
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // READ_PHONE_STATE permission has not been granted.
            requestReadPhoneStatePermission();
        } else {
            // READ_PHONE_STATE permission is already been granted.
            doPermissionGrantedStuffs();
        }
    }


    private void requestReadPhoneStatePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,android.Manifest.permission.READ_PHONE_STATE)) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{android.Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
            // READ_PHONE_STATE permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,@NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_PHONE_STATE) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                doPermissionGrantedStuffs();
            }
        }
    }

    public void doPermissionGrantedStuffs() {
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        Utils.IMEI_No = tm.getDeviceId();

    }


    private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=com.skymapglobal.ktp_challan_generation&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                e.printStackTrace();
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null) {
                try {
                    String currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                    if (!onlineVersion.equals(currentVersion)) {
                        showConfirmUpdate("Kolkata Police", "Please update new version!");
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        void showConfirmUpdate(String title, String msg) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setMessage(msg);
            alertDialog.setTitle(title);
            alertDialog.setCancelable(false);
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    onBackPressed();
                }
            });
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    Intent intent;
                    Utils.saveBoolPreferences(context, "IS_LOGIN", false);
                    Utils.saveBoolPreferences(context, "LOGIN_REGISTER", false);
                    try {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } catch (android.content.ActivityNotFoundException _e) {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName));
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                    onBackPressed();
//                dialog.dismiss();
                }
            });
            alertDialog.create().show();
        }
    }


    protected boolean isGrantedPermission() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestWriteStoragePermission();
            return false;
        } else {
            return true;
        }
    }

    protected void requestWriteStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(myResultListner!=null){
            myResultListner.onResult(requestCode , resultCode , data);
        }
    }

    public void setListner(ActivityResultListner listner){
        this.myResultListner = listner ;
    }
}
