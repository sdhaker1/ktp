package br.skymapglobal.ktp_challan;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void mainActivityTest() {
        ViewInteraction appCompatAutoCompleteTextView = onView(
                withId(R.id.gaurd_code_ac));
        appCompatAutoCompleteTextView.perform(scrollTo(), replaceText("TA"), closeSoftKeyboard());

        ViewInteraction appCompatTextView = onView(
                allOf(withId(android.R.id.text1), withText("TA"), isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatButton = onView(
                allOf(withId(android.R.id.button1), withText("Yes"),
                        withParent(allOf(withClassName(is("android.widget.LinearLayout")),
                                withParent(withClassName(is("android.widget.LinearLayout"))))),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction appCompatEditText = onView(
                withId(R.id.beat_et));
        appCompatEditText.perform(scrollTo(), replaceText("1"), closeSoftKeyboard());

        ViewInteraction appCompatImageView = onView(
                withId(R.id.ivSearchBeatInfo));
        appCompatImageView.perform(scrollTo(), click());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.vehicle_no_et), withText("WB")));
        appCompatEditText2.perform(scrollTo(), replaceText("WBTEST01"), closeSoftKeyboard());

        ViewInteraction appCompatImageView2 = onView(
                withId(R.id.ivSearchVehicleInfo));
        appCompatImageView2.perform(scrollTo(), click());

        ViewInteraction appCompatAutoCompleteTextView2 = onView(
                withId(R.id.autoCompleteTextView1));
        appCompatAutoCompleteTextView2.perform(scrollTo(), replaceText("129"), closeSoftKeyboard());

        ViewInteraction appCompatTextView2 = onView(
                allOf(withId(android.R.id.text1), withText("129/177MVA"), isDisplayed()));
        appCompatTextView2.perform(click());

        ViewInteraction appCompatEditText3 = onView(
                withId(R.id.phn_et));
        appCompatEditText3.perform(scrollTo(), replaceText("9038244639"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.phn_et), withText("9038244639")));
        appCompatEditText4.perform(pressImeActionButton());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.create_challan_btn), withText("Submit"),
                        withParent(withId(R.id.pending_view))));
        appCompatButton2.perform(scrollTo(), click());

        ViewInteraction appCompatButton3 = onView(
                allOf(withId(R.id.print_challan_btn), withText("On-spot")));
        appCompatButton3.perform(scrollTo(), click());

        ViewInteraction appCompatButton4 = onView(
                allOf(withId(android.R.id.button1), withText("Yes"),
                        withParent(allOf(withClassName(is("android.widget.LinearLayout")),
                                withParent(withClassName(is("android.widget.LinearLayout"))))),
                        isDisplayed()));
        appCompatButton4.perform(click());

        ViewInteraction appCompatButton5 = onView(
                allOf(withId(R.id.print_challan_btn), withText("On-spot")));
        appCompatButton5.perform(scrollTo(), click());

        ViewInteraction appCompatButton6 = onView(
                allOf(withId(android.R.id.button1), withText("Yes"),
                        withParent(allOf(withClassName(is("android.widget.LinearLayout")),
                                withParent(withClassName(is("android.widget.LinearLayout"))))),
                        isDisplayed()));
        appCompatButton6.perform(click());

        ViewInteraction appCompatButton7 = onView(
                allOf(withId(R.id.print_challan_btn), withText("On-spot")));
        appCompatButton7.perform(scrollTo(), click());

        ViewInteraction appCompatButton8 = onView(
                allOf(withId(android.R.id.button1), withText("Yes"),
                        withParent(allOf(withClassName(is("android.widget.LinearLayout")),
                                withParent(withClassName(is("android.widget.LinearLayout"))))),
                        isDisplayed()));
        appCompatButton8.perform(click());

        ViewInteraction appCompatButton9 = onView(
                allOf(withId(R.id.pending_challan_btn), withText("Pending")));
        appCompatButton9.perform(scrollTo(), click());

        ViewInteraction appCompatTextView3 = onView(
                allOf(withId(R.id.upload_doc_photo_btn), withText("Upload Document Photograph"),
                        withParent(withId(R.id.other_doc_ll)),
                        isDisplayed()));
        appCompatTextView3.perform(click());

        ViewInteraction recordButton = onView(
                allOf(withId(R.id.record_button),
                        withParent(withId(R.id.record_panel)),
                        isDisplayed()));
        recordButton.perform(click());

        ViewInteraction appCompatTextView4 = onView(
                allOf(withId(R.id.confirm_media_result), withText("Save"),
                        withParent(allOf(withId(R.id.preview_control_panel),
                                withParent(withId(R.id.preview_activity_container)))),
                        isDisplayed()));
        appCompatTextView4.perform(click());

    }

}
