package br.skymapglobal.ktp_backend.models;

import java.util.ArrayList;

/**
 * Created by thaibui on 8/15/16.
 */
public class ReportCountResponse {
    public String tableName;
    public ArrayList<CountOfReport> listData;
}
