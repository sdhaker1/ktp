package br.skymapglobal.ktp_backend.models;

import com.google.gson.Gson;

/**
 * Created by thaibui on 8/19/16.
 */
public class AccessToken {
    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public long getExp() {
        return exp;
    }

    public void setExp(long exp) {
        this.exp = exp;
    }

    private UserModel user;
    private long exp;

    public String toJSONString() {
        return new Gson().toJson(this);
    }

    public static AccessToken parseFromJSON(String json) {
        return new Gson().fromJson(json, AccessToken.class);
    }
}
