package br.skymapglobal.ktp_backend.models;

/**
 * Created by pham tuan anh on 31/08/2016.
 */
public class Register {
    public int id;
    public String type;
    public String image;
    public String tenantName;
    public String fatherName;
    public String aadhaarCardNo;
    public int age;
    public String occupation;
    public String maritalStatus;
    public String tenantAddress;
    public String placeOfWork;
    public String detailsKnownPerson;
    public String knownPersonPhone;
    public String currentLocation;
    public String rentedHouseAddress;
    public String reportdatetime;
}
