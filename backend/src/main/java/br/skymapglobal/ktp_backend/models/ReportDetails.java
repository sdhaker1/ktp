package br.skymapglobal.ktp_backend.models;

/**
 * Created by thaibui on 8/16/16.
 */
public class ReportDetails {
   public String id;
    public String reportdatetime;
    public String description;
    public String userid;
    public String beatno;
    public String status;
    public String imagelocation;
    public boolean active;
    public String latitude;
    public String longitude;
    public String address;
    public String guardcode;
    public String type;

}
