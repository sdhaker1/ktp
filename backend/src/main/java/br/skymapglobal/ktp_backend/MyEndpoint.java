/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Endpoints Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloEndpoints
*/

package br.skymapglobal.ktp_backend;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.appengine.api.utils.SystemProperty;
import com.google.appengine.repackaged.com.google.gson.Gson;

import org.jose4j.lang.JoseException;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Properties;

import javax.inject.Named;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import br.skymapglobal.ktp_backend.global.AppConstants;
import br.skymapglobal.ktp_backend.global.Config;
import br.skymapglobal.ktp_backend.helpers.AppUtils;
import br.skymapglobal.ktp_backend.helpers.JWTHelper;
import br.skymapglobal.ktp_backend.helpers.SQLConnector;
import br.skymapglobal.ktp_backend.models.AccessToken;
import br.skymapglobal.ktp_backend.models.CountOfReport;
import br.skymapglobal.ktp_backend.models.DeptDetail;
import br.skymapglobal.ktp_backend.models.PoliceRegisterModel;
import br.skymapglobal.ktp_backend.models.Register;
import br.skymapglobal.ktp_backend.models.ReportCountResponse;
import br.skymapglobal.ktp_backend.models.ReportDetails;
import br.skymapglobal.ktp_backend.models.ResponseModel;
import br.skymapglobal.ktp_backend.models.UserModel;

/**
 * An endpoint class we are exposing
 */
@Api(
        name = "myApi",
        version = "v1",
        namespace = @ApiNamespace(
                ownerDomain = "ktp_backend.skymapglobal.br",
                ownerName = "ktp_backend.skymapglobal.br",
                packagePath = ""
        )
)
public class MyEndpoint {


    public MyEndpoint() {
    }

    /**
     * A simple endpoint method that takes a name and says Hi back
     */
    @ApiMethod(name = "sendEmail")
    public MyBean sendEmail(@Named("email") String email, @Named("table") String table, @Named("reportID") String reportID, @Named("description") String description, @Named("place") String place, @Named("datetime") String datetime) {


        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        try {
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress("octcc.cloud@gmail.com", "Admin"));
            msg.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(email, email));
            String title = "Report " + getReportName(table) + " Report ID: " + reportID;
            msg.setSubject(title);
            String urlImage = SQLConnector.getInstance().getImageUrl(table, reportID);
            String htmlBody = "<body>\n" +
                    "<div style=\"width:300px;\">\n" +
                    "    <div style=\"width:100%\">\n" +
                    "        <h4>Report Detail </h4><hr/><br/>" +
                    "        <div style=\"width:50%;float:left;display:inline\">\n" +
                    "            <span >Guard Code: </span>\n" +
                    "            <b>TN</b>\n" +
                    "        </div>\n" +
                    "        <div style=\"width:50%;float:left;display:inline\">\n" +
                    "            <span>Beat Number: </span>\n" +
                    "            <b>01</b>\n" +
                    "        </div>\n" +
                    "    </div><br/>\n" +
                    "    <p>Place: " + place + " </p><br/>" +
                    "    <p>Datetime: " + datetime + "</p><br/>" +
                    "    <img style=\"width:100%\" src=\"" + urlImage + "\">\n" +
                    "    <br/><p style=\"width:100%\">Description: " + description + "</p>\n" +
                    "</div>\n" +
                    "</body>";

            Multipart mp = new MimeMultipart();

            MimeBodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(htmlBody, "text/html");
            mp.addBodyPart(htmlPart);
            msg.setContent(mp);
            Transport.send(msg);
        } catch (AddressException e) {
            e.printStackTrace();
            // ...
        } catch (MessagingException e) {
            e.printStackTrace();
            // ...
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            // ...
        }
        MyBean myBean = new MyBean();
        myBean.setData("Email has been forwarded.");
        return myBean;
    }

    @ApiMethod(name = "getUsers")
    public MyBean getUsers(@Named("token") String token) {
        MyBean myBean = new MyBean();
        ResponseModel response = new ResponseModel();
        response.setType(AppConstants.REFRESH_TOKEN_TYPE);
        if (AppUtils.isEmpty(token)) {
            response.setCode(-1);
            response.setMsg("Token Empty!");
        } else {
            if (!JWTHelper.getInstance().isValidToken(token)) {
                response.setCode(-2);
                response.setMsg("Invalid Token!");
            } else {

                try {
                    String data = JWTHelper.getInstance().getData(token);
                    AccessToken accessToken = AccessToken.parseFromJSON(data);
                    if (accessToken != null && accessToken.getUser() != null) {
                        if (System.currentTimeMillis() > accessToken.getExp()) {
                            response.setCode(-10);
                            response.setMsg("Token Expired");
                        } else {
                            UserModel userModel = accessToken.getUser();
                            if (userModel.deptId == 1005) {
                                ArrayList<UserModel> users = SQLConnector.getInstance().queryGetAllUser();
                                if (users != null) {
                                    response.setCode(1);
                                    response.setData(users);
                                } else {
                                    response.setCode(-100);
                                    response.setMsg("SQL server error");
                                }

                            } else {
                                response.setCode(-11);
                                response.setMsg("User not grand permission");
                            }

                        }
                    } else {
                        response.setCode(-3);
                        response.setMsg("Token Error");
                    }

                } catch (JoseException e) {
                    response.setCode(-4);
                    response.setMsg("Token Error!");
                    e.printStackTrace();
                }

            }

        }

        myBean.setData(response.toJSONString());
        return myBean;
    }

    @ApiMethod(name = "users")
    public MyBean users(@Named("token") String token) {
        MyBean myBean = new MyBean();
        ResponseModel response = new ResponseModel();
        ArrayList<UserModel> users = SQLConnector.getInstance().queryGetAllUser();
        if (users != null) {
            response.setCode(1);
            response.setData(users);
        } else {
            response.setCode(-100);
            response.setMsg("SQL server error");
        }
        myBean.setData(response.toJSONString());
        return myBean;
    }

    @ApiMethod(name = "departments")
    public MyBean departments() {
        MyBean myBean = new MyBean();
        ResponseModel response = new ResponseModel();
        ArrayList<DeptDetail> dept = SQLConnector.getInstance().queryAllDept();
        if (dept != null) {
            response.setCode(1);
            response.setData(dept);
        } else {
            response.setCode(-100);
            response.setMsg("SQL server error");
        }
        myBean.setData(response.toJSONString());
        return myBean;
    }

    @ApiMethod(name = "logout")
    public MyBean logout(@Named("token") String token) {
        MyBean myBean = new MyBean();
        ResponseModel response = new ResponseModel();
        response.setType(AppConstants.LOGOUT_TYPE);
        try {
            if (token != null)
                JWTHelper.getInstance().clearToken(token);
            response.setCode(1);
        } catch (Exception e) {
            response.setCode(-100);
            response.setMsg("Service Error!");
        }
        myBean.setData(response.toJSONString());
        return myBean;
    }

    @ApiMethod(name = "profileUser")
    public MyBean profileUser(@Named("token") String token) {
        MyBean myBean = new MyBean();
        ResponseModel response = new ResponseModel();
        response.setType(AppConstants.REFRESH_TOKEN_TYPE);

        if (!JWTHelper.getInstance().isValidToken(token)) {
            response.setCode(-1);
            response.setMsg("Invalid Token!");
        } else {
            try {
                String data = JWTHelper.getInstance().getData(token);
                AccessToken accessToken = AccessToken.parseFromJSON(data);
                if (accessToken != null && accessToken.getUser() != null) {
                    if (accessToken.getExp() < System.currentTimeMillis()) {
                        response.setCode(-10);
                        response.setMsg("Token Expired");
                    } else {
                        response.setCode(1);
                        response.setData(accessToken.getUser());
                    }
                } else {
                    response.setCode(-3);
                    response.setMsg("Token Error");
                }

            } catch (JoseException e) {
                response.setCode(-4);
                response.setMsg("Token Error!");
                e.printStackTrace();
            }

        }

        myBean.setData(response.toJSONString());
        return myBean;
    }

    @ApiMethod(name = "refreshToken")
    public MyBean refreshToken(@Named("token") String token) {
        MyBean myBean = new MyBean();
        ResponseModel response = new ResponseModel();
        response.setType(AppConstants.REFRESH_TOKEN_TYPE);

        if (!JWTHelper.getInstance().isValidToken(token)) {
            response.setCode(-1);
            response.setMsg("Invalid Token!");
        } else {
            try {
                String data = JWTHelper.getInstance().getData(token);
                AccessToken accessToken = AccessToken.parseFromJSON(data);
                if (accessToken != null && accessToken.getUser() != null) {
                    if (accessToken.getExp() < System.currentTimeMillis()) {
                        response.setCode(-10);
                        response.setMsg("Token Expired");
                    } else {
                        Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.DAY_OF_MONTH, Config.TOKEN_TIME_EXP);
                        accessToken.setExp(cal.getTimeInMillis());
                        JWTHelper.getInstance().clearToken(token);
                        token = JWTHelper.getInstance().genToken(accessToken.toJSONString());
                        response.setCode(1);
                        response.setData(token);
                    }
                } else {
                    response.setCode(-3);
                    response.setMsg("Token Error");
                }

            } catch (JoseException e) {
                response.setCode(-4);
                response.setMsg("Token Error!");
                e.printStackTrace();
            }

        }


        myBean.setData(response.toJSONString());
        return myBean;
    }

    @ApiMethod(name = "login")
    public MyBean login(@Named("userName") String userName, @Named("password") String password) {
        MyBean myBean = new MyBean();
        ResponseModel response = new ResponseModel();
        response.setType(AppConstants.LOGIN_TYPE);

        if (AppUtils.isEmpty(userName) || AppUtils.isEmpty(password)) {
            response.setCode(-1);
            response.setMsg("Invalid user or password!");
        } else {
            try {
                AccessToken accessToken = new AccessToken();

                UserModel userModel = SQLConnector.getInstance().getUserLogin(userName, password);
                if (userModel != null) {
                    accessToken.setUser(userModel);
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DAY_OF_MONTH, Config.TOKEN_TIME_EXP);
                    accessToken.setExp(cal.getTimeInMillis());
                    response.setCode(1);
                    response.setData(JWTHelper.getInstance().genToken(accessToken.toJSONString()));
                } else {
                    response.setCode(-1);
                    response.setMsg("Invalid user or password!");
                }

            } catch (JoseException e) {
                response.setCode(-100);
                response.setData("Service Error");
                e.printStackTrace();
            }

        }

        myBean.setData(response.toJSONString());
        return myBean;
    }

    @ApiMethod(name = "resolveReport")
    public MyBean resolveReport(@Named("tableName") String tableName, @Named("reportID") String reportID) {
        MyBean response = new MyBean();
        String result = SQLConnector.getInstance().queryResolveReport(tableName, reportID) ? " Status Already Resolved" : "Error";
        response.setData(result);
        return response;
    }

    @ApiMethod(name = "countReport")
    public MyBean countReport(@Named("listData") String listData, @Named("fromDate") String fromDate, @Named("toDate") String toDate) {
        ResponseModel responseModel = new ResponseModel();
        MyBean response = new MyBean();

        ArrayList<ReportCountResponse> responseArrayList = new ArrayList<>();

        String[] tables = new Gson().fromJson(listData, String[].class);

        Calendar start = AppUtils.calendarFromStringDate(fromDate);
        Calendar end = AppUtils.calendarFromStringDate(toDate);
        if (start == null || end == null || tables == null || tables.length == 0) {
            responseModel.setCode(-1);
            responseModel.setMsg("Input invalid");
            response.setData(responseModel.toJSONString());
            return response;
        }
        for (String tableName : tables) {

            HashMap<String, String> map = createMap(start, end);

            ReportCountResponse reportCountResponse = SQLConnector.getInstance().queryCountReport(tableName, fromDate, toDate, map);

            if (reportCountResponse == null) {
                responseModel.setCode(-2);
                responseModel.setMsg("SQL service error");

                response.setData(responseModel.toJSONString());
                return response;
            } else {
                responseArrayList.add(reportCountResponse);

            }
        }

        responseModel.setCode(1);
        responseModel.setData(new Gson().toJson(responseArrayList));

        response.setData(responseModel.toJSONString());
        return response;
    }

    @ApiMethod(name = "countReportOfWeek")
    public MyBean countReportOfWeek(@Named("listData") String listData) {

        ArrayList<ReportCountResponse> responseArrayList = new ArrayList<>();

        String[] tables = new Gson().fromJson(listData, String[].class);
        MyBean response = new MyBean();
        String url;
        try {
            if (SystemProperty.environment.value() ==
                    SystemProperty.Environment.Value.Production) {
                Class.forName("com.mysql.jdbc.GoogleDriver");
                url = "jdbc:google:mysql://ktpcloud-1470216325260:ktpinstance/ktpreportdb?user=root";
            } else {
                Class.forName("com.mysql.jdbc.Driver");
                url = "jdbc:mysql://173.194.83.210/ktpreportdb?user=ktpuser&password=ktpuser";
            }

            Connection conn = DriverManager.getConnection(url);
            for (String tableName : tables) {
                ReportCountResponse reportCountResponse = new ReportCountResponse();
                reportCountResponse.tableName = tableName;
                reportCountResponse.listData = new ArrayList<>();

                String query = "SELECT thedays.theday, IFNULL(summary.`count`,0) `count`\n" +
                        "   FROM (\n" +
                        "        SELECT DATE(NOW())-INTERVAL seq.seq DAY theday FROM (\n" +
                        "                   SELECT 0 AS seq \n" +
                        "                     UNION ALL SELECT 1  UNION ALL SELECT 2 \n" +
                        "                     UNION ALL SELECT 3  UNION ALL SELECT 4\n" +
                        "                     UNION ALL SELECT 5  UNION ALL SELECT 6\n" +
                        "                ) seq \n" +
                        "          ) thedays\n" +
                        "   LEFT JOIN (\n" +
                        "        SELECT date(reportdatetime) as theday, COUNT(*) as count\n" +
                        "          FROM " + tableName + " WHERE status='Pending' and date(reportdatetime) >= DATE_SUB(NOW(),  INTERVAL 7 DAY)\n" +
                        "         GROUP BY DATE(reportdatetime)\n" +
                        "       ) summary USING (theday)\n" +
                        "   ORDER BY thedays.theday desc";

                Statement statement = conn.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                if (!resultSet.first()) continue;
                do {
                    String cout = resultSet.getString("count");
                    String date = resultSet.getString("theday");
                    reportCountResponse.listData.add(new CountOfReport(date, cout));
                } while (resultSet.next());
                responseArrayList.add(reportCountResponse);

            }
            response.setData(new Gson().toJson(responseArrayList));
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(e.getMessage());
        }
        return response;
    }

    @ApiMethod(name = "countReportOfDay")
    public MyBean countReportOfDay(@Named("listData") String listData) {

        ArrayList<ReportCountResponse> responseArrayList = new ArrayList<>();

        String[] tables = new Gson().fromJson(listData, String[].class);
        MyBean response = new MyBean();
        String url;
        try {
            if (SystemProperty.environment.value() ==
                    SystemProperty.Environment.Value.Production) {
                Class.forName("com.mysql.jdbc.GoogleDriver");
                url = "jdbc:google:mysql://ktpcloud-1470216325260:ktpinstance/ktpreportdb?user=root";
            } else {
                Class.forName("com.mysql.jdbc.Driver");
                url = "jdbc:mysql://173.194.83.210/ktpreportdb?user=ktpuser&password=ktpuser";
            }
            Connection conn = DriverManager.getConnection(url);
            for (String tableName : tables) {
                ReportCountResponse reportCountResponse = new ReportCountResponse();
                reportCountResponse.tableName = tableName;
                reportCountResponse.listData = new ArrayList<>();

                String query = "SELECT curdate() as theday, COUNT(*) as count FROM " + tableName + " WHERE date(reportdatetime) = curdate() and status = 'Pending'";

                Statement statement = conn.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                if (!resultSet.first()) continue;
                do {
                    String cout = resultSet.getString("count");
                    String date = resultSet.getString("theday");
                    reportCountResponse.listData.add(new CountOfReport(date, cout));
                } while (resultSet.next());
                responseArrayList.add(reportCountResponse);

            }
            response.setData(new Gson().toJson(responseArrayList));
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(e.getMessage());
        }
        return response;
    }

    @ApiMethod(name = "countRegister")
    public MyBean countRegister(@Named("fromDate") String fromDate, @Named("toDate") String toDate) {
        ResponseModel responseModel = new ResponseModel();
        MyBean response = new MyBean();

        ArrayList<ReportCountResponse> responseArrayList = new ArrayList<>();

        String tableName = "tblRegister";

        Calendar start = AppUtils.calendarFromStringDate(fromDate);
        Calendar end = AppUtils.calendarFromStringDate(toDate);
        if (start == null || end == null) {
            responseModel.setCode(-1);
            responseModel.setMsg("Input invalid");
            response.setData(responseModel.toJSONString());
            return response;
        }
        HashMap<String, String> map = createMap(start, end);

        ReportCountResponse reportCountResponse = SQLConnector.getInstance().queryCountReport(tableName, fromDate, toDate, map);

        if (reportCountResponse == null) {
            responseModel.setCode(-2);
            responseModel.setMsg("SQL service error");

            response.setData(responseModel.toJSONString());
            return response;
        } else {
            responseArrayList.add(reportCountResponse);

        }

        responseModel.setCode(1);
        responseModel.setData(new Gson().toJson(responseArrayList));

        response.setData(responseModel.toJSONString());
        return response;
    }

    @ApiMethod(name = "getReports")
    public MyBean getReports(@Named("tableName") String tableName, @Named("fromDate") String fromDate, @Named("toDate") String toDate, @Named("status") String status) {
        MyBean response = new MyBean();
        String url;
        String whereStatus = "";
        switch (status) {
            case "All":
                break;
            case "Resolved":
                whereStatus = " and status = 'Resolved' ";
                break;
            case "Pending":
                whereStatus = " and status = 'Pending' ";
                break;
        }
        ArrayList<ReportDetails> reports = SQLConnector.getInstance().queryGetReports(tableName, fromDate, toDate, whereStatus);

        ResponseModel responseModel = new ResponseModel();

        if (reports == null) {
            responseModel.setCode(-1);
            responseModel.setMsg("Error");
        } else {
            responseModel.setCode(1);
            responseModel.setData(reports);
        }

        response.setData(responseModel.toJSONString());

        return response;
    }

    @ApiMethod(name = "getRegisters")
    public MyBean getRegisters(@Named("fromDate") String fromDate, @Named("toDate") String toDate, @Named("type") String type) {
        MyBean response = new MyBean();
        String url;
        String whereStatus = " Type in ( " + type + " )";
        String tableName = "tblRegister";
        ArrayList<Register> registers = SQLConnector.getInstance().queryGetRegisters(fromDate, toDate, whereStatus);

        ResponseModel responseModel = new ResponseModel();

        if (registers == null) {
            responseModel.setCode(-1);
            responseModel.setMsg("Error");
        } else {
            responseModel.setCode(1);
            responseModel.setData(registers);
        }

        response.setData(responseModel.toJSONString());

        return response;
    }

    @ApiMethod(name = "createReport")
    public MyBean createReport(@Named("tableName") String tableName,
                               @Named("description") String description,
                               @Named("userid") String userid,
                               @Named("beatno") String beatno,
                               @Named("guardcode") String guardcode,
                               @Named("status") String status,
                               @Named("active") boolean active,
                               @Named("longitude") String longitude,
                               @Named("latitude") String latitude,
                               @Named("type") String type,
                               @Named("place") String place) {

        MyBean response = new MyBean();
        String id = SQLConnector.getInstance().queryInsertReport(tableName, description, userid, beatno, guardcode, status, active, longitude, latitude, type, place);
        if (id != null) {
            response.setData(id);
        } else {
            response.setData("Service Error");
        }
        return response;
    }

    @ApiMethod(name = "ktpPublishCreateReport")
    public MyBean ktpPublishCreateReport(@Named("tableName") String tableName,
                                         @Named("deviceId") String deviceId,
                                         @Named("description") String description,
                                         @Named("place") String place,
                                         @Named("category") String category,
                                         @Named("city") String city,
                                         @Named("lat") String lat,
                                         @Named("lng") String lng,
                                         @Named("mobile") String mobile,
                                         @Named("ps") String ps) {

        MyBean response = new MyBean();
        String id = SQLConnector.getInstance().queryInsertReportKTPPublish(tableName, deviceId, category, description, place, city, lat, lng, mobile, ps);

        if (id != null) {
            response.setData(id);
        } else {
            response.setData("Service Error");
        }
        return response;
    }

    @ApiMethod(name = "ktpPublishCreateRegistration")
    public MyBean ktpPublishCreateRegistration(@Named("type") String type,
                                               @Named("deviceId") String deviceId,
                                               @Named("data") String data,
                                               @Named("place") String place,
                                               @Named("lat") String lat,
                                               @Named("lng") String lng) {
        MyBean response = new MyBean();
        String id = SQLConnector.getInstance().queryInsertRegistrationKTPPublic(type, deviceId, data, place, lat, lng);

        if (id != null) {
            response.setData(id);
        } else {
            response.setData("Service Error");
        }
        return response;

    }

    @ApiMethod(name = "createUser")
    public MyBean createUser(@Named("deptid") int deptid,
                             @Named("username") String username,
                             @Named("password") String password,
                             @Named("fullname") String fullname,
                             @Named("email") String email,
                             @Named("contactno") String contactno,
                             @Named("active") int active) {

        MyBean response = new MyBean();
        UserModel userModel = new UserModel(deptid, username, password, fullname, email, contactno, active);
        int id = SQLConnector.getInstance().queryInsertUser(userModel);
        if (id != 0) {
            response.setData("User has been created.");
        } else {
            response.setData("Service Error");
        }
        return response;
    }

    @ApiMethod(name = "insertPanic")
    public MyBean insertPanic(@Named("insertDate") String date,
                             @Named("deviceId") String deviceId,
                             @Named("Lat") String Lat,
                             @Named("Lng") String Lng) {

        MyBean response = new MyBean();
         int id = SQLConnector.getInstance().queryInsertPanic(date, deviceId, Lat, Lng);
        if (id != 0) {
            response.setData("Sucess");
        } else {
            response.setData("Service Error");
        }
        return response;
    }

    @ApiMethod(name = "updateUser")
    public MyBean updateUser(@Named("userid") String userid,
                             @Named("deptid") int deptid,
                             @Named("username") String username,
                             @Named("password") String password,
                             @Named("fullname") String fullname,
                             @Named("email") String email,
                             @Named("contactno") String contactno,
                             @Named("active") int active) {

        MyBean response = new MyBean();
        UserModel userModel = new UserModel(deptid, username, password, fullname, email, contactno, active);
        userModel.userId = userid;
        int id = SQLConnector.getInstance().queryUpdateUser(userModel);
        if (id != 0) {
            response.setData("User have updated.");
        } else {
            response.setData("Service Error");
        }
        return response;
    }


    @ApiMethod(name = "disableUser")
    public MyBean disableUser(@Named("userID") String userID) {
        MyBean response = new MyBean();
        String result = SQLConnector.getInstance().queryDisableUser(userID) ? "Updated" : "Error";
        response.setData(result);
        return response;
    }


    @ApiMethod(name = "createDept")
    public MyBean createDept(
            @Named("departmentName") String departmentName,
            @Named("email") String email,
            @Named("contactno") String contactno,
            @Named("description") String description,
            @Named("listReport") String listReport) {

        MyBean response = new MyBean();
        DeptDetail deptDetail = new DeptDetail(departmentName, email, contactno, description, listReport);
        int id = SQLConnector.getInstance().queryInsertDept(deptDetail);
        if (id != 0) {
            response.setData("Department have created.");
        } else {
            response.setData("Service Error");
        }
        return response;
    }

    @ApiMethod(name = "registerPolice")
    public MyBean registerPolice(@Named("data") String data) {
        PoliceRegisterModel model = PoliceRegisterModel.parseFromJSON(data);
        MyBean response = new MyBean();
        if (model != null) {
            int id = SQLConnector.getInstance().queryInsertPoliceRegister(model);
            if (id != 0) {
                response.setData("" + id);
            } else {
                response.setData("Service Error");
            }
        } else {
            response.setData("Input invalid");
        }

        return response;
    }

    @ApiMethod(name = "updateDept")
    public MyBean updateDept(
            @Named("deptid") String deptid,
            @Named("departmentName") String departmentName,
            @Named("email") String email,
            @Named("contactno") String contactno,
            @Named("description") String description,
            @Named("listReport") String listReport) {

        MyBean response = new MyBean();
        DeptDetail deptDetail = new DeptDetail(departmentName, email, contactno, description, listReport);
        deptDetail.deptid = Integer.valueOf(deptid);
        int id = SQLConnector.getInstance().queryUpdateDept(deptDetail);
        if (id != 0) {
            response.setData("Department have updated.");
        } else {
            response.setData("Service Error");
        }
        return response;
    }

    private HashMap<String, String> createMap(Calendar start, Calendar end) {
        String endDate = AppUtils.convertDateToString(end.getTime(), "yyyy-MM-dd");
        HashMap<String, String> result = new HashMap<>();
        String temp;
        do {

            temp = AppUtils.convertDateToString(start.getTime(), "yyyy-MM-dd");
            result.put(temp, "0");
            start.add(Calendar.DATE, 1);

        } while (!temp.endsWith(endDate));
        result.put(endDate, "0");
        return result;
    }

    private String getReportName(String tableName) {

        switch (tableName) {
            case "tblSignal":
                return "Signal";
            case "tblTrafficCongestion":
                return "Traffic Congestion";
            case "tblViolation":
                return "Violation";
            case "tblRoadMarking":
                return "Road Marking";
            case "tblRoadCondition":
                return "Road Condition";
            case "tblRoadFurniture":
                return "Road Furniture";
            case "tblOthers":
                return "Others";
            default:
                return "Others";
        }
    }
}
