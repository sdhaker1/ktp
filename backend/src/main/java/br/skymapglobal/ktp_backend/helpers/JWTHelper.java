package br.skymapglobal.ktp_backend.helpers;


import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.Expiration;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;

import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.lang.JoseException;

import java.util.Calendar;
import java.util.logging.Level;

import br.skymapglobal.ktp_backend.global.Config;

public class JWTHelper {

    private static final JWTHelper instance = new JWTHelper();

    public static JWTHelper getInstance() {
        return instance;
    }

    public String genToken(String payload) throws JoseException {
        JsonWebEncryption jwe = new JsonWebEncryption();
        jwe.setPayload(payload);
        jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.A128KW);
        jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
        jwe.setKey(Config.JWT_KEY);
        String serializedJwe = jwe.getCompactSerialization();
        System.out.println("Serialized Encrypted JWE: " + serializedJwe);
        cacheToken(serializedJwe);
        return serializedJwe;
    }

    private void cacheToken(String token) {
        MemcacheService syncCache = MemcacheServiceFactory.getMemcacheService();
        syncCache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, Config.TOKEN_TIME_EXP);
        Expiration expiration = Expiration.onDate(calendar.getTime());
        syncCache.put(token, true, expiration);

    }

    public boolean isValidToken(String token) {
        MemcacheService syncCache = MemcacheServiceFactory.getMemcacheService();
        syncCache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
        Object object = syncCache.get(token);
        if (object == null) return false;
        return (boolean) object;
    }

    public void clearToken(String token) {
        MemcacheService syncCache = MemcacheServiceFactory.getMemcacheService();
        syncCache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
        syncCache.delete(token);
    }

    public String getData(String token) throws JoseException {
        JsonWebEncryption jwe = new JsonWebEncryption();
        jwe.setKey(Config.JWT_KEY);
        jwe.setCompactSerialization(token);
        System.out.println("Payload: " + jwe.getPayload());
        return jwe.getPayload();
    }


}
