function login() {
    event.preventDefault(); // prevent PageReLoad

    $('.error').css('display', 'none'); // hide error msg
    $("#login-form :input").prop("disabled", true);
    enableloading();
    var ValidEmail = $('#username').val()
    var ValidPassword = $('#password').val()

   gapi.client.myApi.login({'userName': ValidEmail, 'password': ValidPassword}).execute(
        function(response) {
            $("#login-form :input").prop("disabled", false);
            disableLoading();
            if (!response.error) {
                obj = JSON.parse(response.result.data);
                if (obj.code==1) {
                        window.localStorage.accessToken = obj.data;
                        window.location = "pages/main.html"; // go to home.html
                    }
                    else {
                        $('#error').html(obj.msg);
                        $('#error').css('display', 'block'); // show error msg
                    }
            }
            else if (response.error) {
                alert(response.error.code);
            }
        }
    );
}


function refreshToken()
{
    if (window.localStorage.accessToken === null) return;

    gapi.client.myApi.refreshToken({'token': window.localStorage.accessToken}).execute(
            function(response) {

                if (!response.error) {
                    obj = JSON.parse(response.result.data);

                    if (obj.code == '1') {
                            window.localStorage.accessToken = obj.data;
                            window.location = "pages/main.html"; // go to home.html
                        }
                        else {
                            return;
                        }
                }
                else if (response.error) {
                    alert(response.error.code);
                }
            }
        );
}


function init() {

      var apiName = 'myApi';
      var apiVersion = 'v1';
      var apiRoot = 'https://ktpcloud-1470216325260.appspot.com/_ah/api';
      var callback = function() {
        refreshToken();
      }
      gapi.client.load(apiName, apiVersion, callback, apiRoot);
    }
