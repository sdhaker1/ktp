function logout(){
        gapi.client.myApi.logout({'token' : window.localStorage.accessToken}).execute(
            function(response){
                 if(response.error)
                    console.log(response.error.code);
                 var  obj = JSON.parse(response.result.data);
                 if (obj.code=='1') {
                     window.localStorage.accessToken = null;
                     window.location = "../index.html"; // go to home.html
                 }
                 else {
                     return;
                 }
            });
    }