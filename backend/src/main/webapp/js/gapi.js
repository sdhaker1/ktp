var allTableName = ["tblSignal","tblTrafficCongestion","tblViolation","tblRoadMarking","tblRoadCondition","tblOthers","tblRoadFurniture"];

function getReportName(tableName){
    switch(tableName) {
        case 'tblSignal':
            return 'Signal';
        case 'tblTrafficCongestion':
            return 'Traffic Congestion';
        case 'tblViolation':
            return 'Violation';
        case 'tblRoadMarking':
            return 'Road Marking';
        case 'tblRoadCondition':
            return 'Road Condition';
        case 'tblRoadFurniture':
            return 'Road Furniture';
        case 'tblOthers':
            return 'Others';
    }
}

function loadLastWeekCountReport() {
    gapi.client.myApi.countReportOfWeek({'listData': JSON.stringify(allTableName)}).execute(
        function(response){
            if(response.error)
                console.log(response.error.code);
            objs = JSON.parse(response.result.data);

            objs.forEach(function(obj){
                var table_tag = "#table-report-" + obj.tableName;
                var table_content = "<thead>\
                                        <tr>\
                                            <th>Date</td>\
                                            <th>Count of Report</td>\
                                        </tr>\
                                    </thead>\
                                    <tbody>";
                obj.listData.forEach(function(data){
                    table_content += "<tr><td>" + data.date + "</td><td><a class=\"cursor-pointer\" onclick=\"showReportOfDay('" + obj.tableName + "','" + data.date +"')\">" + data.count + "</a></td></tr>";
                })
                $(table_tag).html(table_content);
            })
        }
    )
}

    function loadCountOfAllReport() {
           gapi.client.myApi.countReportOfDay({'listData': JSON.stringify(allTableName)}).execute(
                function(response) {
                    if (!response.error) {
                        objs = JSON.parse(response.result.data);
                        objs.forEach(function(obj){
                            var p_tag = "#count-"+obj.tableName;
                            if (obj.listData.length == 0) count = 0
                            else count = obj.listData[0].count;
                            $(p_tag).html(count);
                        })
                    }
                    else if (response.error) {
                        console.log(response.error.code);
                    }
                }
            );

           gapi.client.myApi.countRegister({'fromDate': moment().format('YYYY-MM-DD'), 'toDate' : moment().format('YYYY-MM-DD')}).execute(
                           function(response) {
                               if (!response.error) {
                                   objs = JSON.parse(response.result.data);
                                   objs.data.forEach(function(obj){
                                       var p_tag = "#count-"+obj.tableName;
                                       if (obj.listData.length == 0) count = 0
                                       else count = obj.listData[0].count;
                                       $(p_tag).html(count);
                                   })
                               }
                               else if (response.error) {
                                   console.log(response.error.code);
                               }
                           }
                       );
    }

    function fillDate()
    {
        loadCountOfAllReport();
        loadLastWeekCountReport();
    }

    var i = 0;

    function myLoop () {           //  create a loop function
        setTimeout(function () {    //  call a 3s setTimeout when the loop is called
            fillDate();
            loadUserInfo();//  ..  setTimeout()        //  your code here
            myLoop();
        }, 3000)
        }


    function init() {

      var apiName = 'myApi';
      var apiVersion = 'v1';
      var apiRoot = 'https://ktpcloud-1470216325260.appspot.com/_ah/api';
      var callback = function() {
        //refreshToken();
        loadUserInfo();
        fillDate();
        myLoop();
      }
      gapi.client.load(apiName, apiVersion, callback, apiRoot);
    }

    function initForTest() {
      var apiName = 'myApi';
      var apiVersion = 'v1';
      var apiRoot = 'https://' + window.location.host + '/_ah/api';
      if (window.location.hostname == 'localhost'
          || window.location.hostname == '127.0.0.1'
          || ((window.location.port != "") && (window.location.port > 1023))) {
            // We're probably running against the DevAppServer
            apiRoot = 'http://' + window.location.host + '/_ah/api';
      }
      var callback = function() {
            refreshToken();
            loadUserInfo();
            fillDate();
            myLoop();
      }
      gapi.client.load(apiName, apiVersion, callback, apiRoot);
    }


    function showReportOfDay(tableName,date) {
        $('#reportrange').data('fromDate',date);
        $('#reportrange').data('toDate',date);
        $('#reportrange span').html(date + ' - ' + date);
        $('#report-detail-table-name').data('tableName',tableName);
        $('#select-status').val("pending");
        $('#select-status').select2();
        showReport(tableName,date,date,'Pending')
    }

    function showReport(tableName,fromDate,toDate,status) {

        gapi.client.myApi.getReports({'tableName':tableName , 'fromDate':fromDate, 'toDate':toDate , 'status' :status}).execute(

            function(response){
                var table;
                if(response.error)
                    console.log(response.error.code);
                objs = JSON.parse(response.result.data);
                var table_content = "<thead class=\"w3-blue\"\>\
                                        <tr>\
                                            <th>ID</th>\
                                            <th>User</th>\
                                            <th>Date Time</th>\
                                            <th>Description</th>\
                                            <th>Location</th>\
                                            <th>Status</th>\
                                            <th>Image</th>\
                                            <th><button class=\"cursor-pointer w3-blue w3-btn w3-round\"><i class=\"fa fa-share\"></i></button></th>\
                                            <th><button class=\"cursor-pointer w3-blue w3-btn w3-round\"><i class=\"fa fa-envelope\"></i></button></th>\
                                        </tr>\
                                    </thead>\
                                    <tbody>";
                var location = "";
                objs.data.forEach(function(obj){
                    location = obj.latitude+" , "+obj.longitude
                    table_content += "<tr><td>" + obj.id +"</td><td>" + obj.userid +"</td><td>" + obj.reportdatetime +"</td><td>";
                    table_content += obj.description.toUpperCase() +"</td><td>"
                    table_content += obj.address.toUpperCase() +"</td><td>" + obj.status.toUpperCase() +"</td>"
                    table_content += "<td><image style=\"height:45px\" onclick=\"showImage(this)\" src=\""+obj.imagelocation +"\"></td>";

                    if (obj.active)
                        table_content += "<td><button class=\"cursor-pointer w3-btn w3-white w3-ripple w3-round w3-text-theme\" onclick=\"resolveReport('" + tableName + "', " + obj.id +")\"><i class=\"fa fa-share\"></i></button></td><td><button onclick=\"openEmail('" + tableName + "', " + obj.id +" ,'"+obj.description +"','"+obj.address+"','"+obj.reportdatetime+"')\" class=\"cursor-pointer w3-btn w3-white w3-ripple w3-round w3-text-theme\"><i class=\"fa fa-envelope\"></i></button></td>";
                    else
                        table_content += "<td><button type=\"button\" disabled class=\"cursor-pointer w3-btn w3-white w3-ripple w3-round w3-text-theme\" onclick=\"resolveReport('" + tableName + "'," + obj.id + ")\"><i class=\"fa fa-share\"></i></button></td><td><button onclick=\"openEmail('" + tableName + "', " + obj.id +" ,'"+obj.description +"','"+obj.address+"','"+obj.reportdatetime+"')\" class=\"cursor-pointer w3-btn w3-white w3-ripple w3-round w3-text-theme\"><i class=\"fa fa-envelope\"></i></button></td>";
                    table_content +="</tr>";
                })
                table_content += "</tbody>"
                title = getReportName(tableName)+" Report Detail.";
                $('#report-detail-table-name').html(title);
                $('#table-report-detail').html(table_content);

                table = $('#table-report-detail').DataTable({
                "destroy": true,
                "paging": true,
                "lengthChange": true,
                "aLengthMenu": [[5, 10,-1], [5,10,"All"]],
                "iDisplayLength": 5,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false
                });
            }
        )

        $('#report-detail').modal('show');
    }

    function resolveReport(tableName,reportId){

        if (confirm('Status will be update to Resolved...'))
            {
                enableloading();
                gapi.client.myApi.resolveReport({'tableName':tableName , 'reportID':reportId}).execute(
                    function(response){
                         disableLoading();
                         if(response.error)
                            console.log(response.error.code);
                         alert(response.result.data);
                         showReportByFilter();
                    });
            }
        else return;
    }

    function logout(){
        gapi.client.myApi.logout({'token' : window.localStorage.accessToken}).execute(
            function(response){
                 if(response.error)
                    console.log(response.error.code);
                 var  obj = JSON.parse(response.result.data);
                 if (obj.code=='1') {
                     window.localStorage.accessToken = null;
                     window.location = "../index.html"; // go to home.html
                 }
                 else {
                     return;
                 }
            });
    }

    function loadUserInfo() {
        if (window.localStorage.accessToken === null) return;

        gapi.client.myApi.profileUser({'token': window.localStorage.accessToken}).execute(
                function(response) {

                    if (!response.error) {
                        obj = JSON.parse(response.result.data);

                        if (obj.code == '1') {

                                 var reports = obj.data.reports.split(",");
                                reports.forEach(function (report){
                                    $("#"+report).css('display', 'block');
                                })
                                if (obj.data.deptId == "1005") $(".admin").css('display', 'block');
                                window.localStorage.email = obj.data.email;
                                window.localStorage.username = obj.data.userName;
                                $(".username").html(obj.data.userName);
                            }
                    }
                    else if (response.error) {
                        alert(response.error.code);
                    }
                }
            );

    }

    function openEmail(tableName,reportID,reportDesceiption,address,datetime) {
        $('#sendEmailDialog').modal('show');
        $('#email').val(window.localStorage.email);
        $('#sendEmailForm').unbind('submit');
        $('#sendEmailForm').on('submit', function(e){
                    e.preventDefault();
                   if ($('#email').val()== "") {
                               $('#error').html("You can't leave this empty.")
                               $('#error').css('display', 'block');
                               $('#email').focus();
                               return;
                           }
                   if (confirm('Forward Report ....'))
                        {
                            enableloading();
                            var email = $('#email').val()
                            gapi.client.myApi.sendEmail({"email": email,"table": tableName, "reportID": reportID, "description": reportDesceiption,"place":address,"datetime":datetime }).execute(
                            function(response){
                               disableLoading();
                               if(response.error)
                                   console.log(response.error.code);
                               $('#sendEmailDialog').modal('toggle');
                               alert(response.data);
                               }
                            );
                        }
                   else return;

                });
    }


