function showUser() {
    enableloading();
    gapi.client.myApi.users({'token':window.localStorage.accessToken}).execute(
        function(response){
            var table;
            disableLoading();
            if(response.error)
                console.log(response.error.code);
            objs = JSON.parse(response.result.data);
            var table_content = "<thead class=\"w3-blue\"\>\
                                    <tr>\
                                        <th>UserName</th>\
                                        <th>Name</th>\
                                        <th>Email</th>\
                                        <th>Contact No</th>\
                                        <th>Department</th>\
                                        <th>Active</th>\
                                        <th><button class=\"cursor-pointer w3-btn w3-blue\"><i class=\"fa fa-pencil\"></i></button></i></th>\
                                        <th><button class=\"cursor-pointer w3-btn w3-blue\"><i class=\"fa fa-remove\"></i></button></i></th>\
                                    </tr>\
                                </thead>\
                                <tbody>";

            objs.data.forEach(function(obj){
                table_content += "<tr>";
                table_content += "<td>" + obj.userName +"</td>";
                table_content += "<td>" + obj.fullName +"</td>";
                table_content += "<td>" + obj.email +"</td>";
                table_content += "<td>" + obj.contactNo +"</td>";
                table_content += "<td>" + obj.deptName +"</td>";
                table_content += "<td>" + obj.active +"</td>";
                table_content += "<td><button data-id=\""+obj.userId+"\" data-deptid=\""+obj.deptId+"\" class=\"update-user cursor-pointer w3-btn w3-white w3-ripple w3-round w3-text-theme\"><i class=\"fa fa-pencil\"></i></button></td>";
                table_content += "<td><button onclick=\"disableUser("+obj.userId+")\" class=\"cursor-pointer w3-btn w3-white w3-ripple w3-round w3-text-theme\"><i class=\"fa fa-remove\"></i></button></td>";
                table_content +="</tr>";
            })
            table_content += "</tbody>"

            $('#table-user').html(table_content);

            table = $('#table-user').DataTable({
            "destroy": true,
            "paging": true,
            "lengthChange": true,
            "aLengthMenu": [[10,20,-1], [10,20,"All"]],
            "iDisplayLength": 5,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false
            });

            $("#table-user").on("click", ".update-user", function(){
               var $row = $(this).closest("tr");    // Find the row
                                           var $tds = $row.find("td");
                                           var data = [];
                                           data[0] = $(this).data("id");
                                           data[1] = $(this).data("deptid");
                                           $.each($tds, function() {
                                               data.push($(this).text());
                                           });
                                           openUpdateUser(data);
            });
            getAllDept();
        }
    )
}


function disableUser(userID) {

     if (confirm('Disable User...'))
         {
             gapi.client.myApi.disableUser({'userID':userID}).execute(
                 function(response){
                      if(response.error)
                         console.log(response.error.code);
                      alert(response.result.data);
                      showUser();
             });
         }
     else  {
         return;
     }
}


function getAllDept() {

    enableloading()
   gapi.client.myApi.departments().execute(
           function(response){
               disableLoading();
               if(response.error)
                   console.log(response.error.code);
               objs = JSON.parse(response.result.data);

               objs.data.forEach(function(obj){
                    $('#select-department')
                        .append($("<option></option>")
                                   .attr("value",obj.deptid)
                                   .text(obj.departmentName));
               })
           }
       )

}

function init() {

      var apiName = 'myApi';
      var apiVersion = 'v1';
      var apiRoot = 'https://ktpcloud-1470216325260.appspot.com/_ah/api';
      var callback = function() {
         showUser();
      }
      gapi.client.load(apiName, apiVersion, callback, apiRoot);

    }

function initForTest() {
      var apiName = 'myApi';
      var apiVersion = 'v1';
      var apiRoot = 'https://' + window.location.host + '/_ah/api';
      if (window.location.hostname == 'localhost'
          || window.location.hostname == '127.0.0.1'
          || ((window.location.port != "") && (window.location.port > 1023))) {
            // We're probably running against the DevAppServer
            apiRoot = 'http://' + window.location.host + '/_ah/api';
      }
      var callback = function() {
        showUser();
      }
      gapi.client.load(apiName, apiVersion, callback, apiRoot);
    }

function openUpdateUser(data) {
//    $('#select-department').find('option').remove()
//    getAllDept();
    $('#user-detail-title').html("Update User");
    $('#select-department').val(data[1]);
    $('#username').val(data[2]);
    $("#username").prop("readonly", true);
    $('#fullname').val(data[3])
    $('#email').val(data[4]);
    $('#contactno').val(data[5]);

    $('#user-detail').modal('show');

    $('#new-user-form').unbind('submit');

    $('#new-user-form').on('submit', function(e){
            e.preventDefault();
            if(!checkCreateUser()) {
               return;
            }
            if (confirm('Update User.'))
                {
                    var userId = data[0];
                    var deptid = $('#select-department').val();
                    var username = $('#username').val();
                    var password = $('#password').val();

                    var fullname = $('#fullname').val();
                    var email = $('#email').val();
                    var contactno = $('#contactno').val();
                    updateUser(userId, deptid, username, password, fullname,email,contactno);
                }
            else  {
                return;
            }
        });
}


function updateUser(userId, deptid, username, password, fullname,email,contactno){
    $('#new-user-form :input').prop("disabled", true);
    enableloading();
    gapi.client.myApi.updateUser({'userid':userId, 'deptid':deptid, 'username':username,'password':password, 'fullname':fullname,'email':email,'contactno':contactno,'active':'1' }).execute(
                   function(response){
                       $('#new-user-form :input').prop("disabled", false);
                       disableLoading()
                       if(response.error)
                           console.log(response.error.code);
                       $('#user-detail').modal('toggle');
                       location.reload();
                       alert(response.data);
                   }
               )
}

function openCreteUser(){

//    $('#select-department').find('option').remove()
//    getAllDept();

    $('#username').val('');
    $("#username").prop("readonly", false);
    $('#password').val('');
    $('#fullname').val('');
    $('#email').val('');
    $('#contactno').val('');
    $('#user-detail-title').html("Create New User")
    $('#user-detail').modal('show');

    $('#new-user-form').unbind('submit');

    $('#new-user-form').on('submit', function(e){
            e.preventDefault();
            if(!checkCreateUser()) {
               return;
            }
            if (confirm('Create New User...'))
                {
                    var deptid = $('#select-department').val();
                    var username = $('#username').val();
                    var password = $('#password').val();

                    var fullname = $('#fullname').val();
                    var email = $('#email').val();
                    var contactno = $('#contactno').val();
                    createNewUser(deptid, username, password, fullname,email,contactno);
                }
            else  {
                return;
            }
        });
}

function showDialog(title,messenger,callback) {
    event.preventDefault();
    $('#alert-title').html(title);
    $('#alert-messenger').html(messenger);
    $('#alert-dialog').modal('show');
    $('#alert-submit').click = callback;
}

function checkCreateUser() {

    if ($('#username').val()== "") {
            $('#error').html("You can't leave this empty.")
            $('#error').css('display', 'block');
            $('#username').focus();
            return false;
        }

    if ($('#password').val()== "") {
        $('#error').html("You can't leave this empty.")
        $('#error').css('display', 'block');
        $('#password').focus();
        return false;
    }

    if ($('#rePass').val()== "") {
            $('#error').html("You can't leave this empty.")
            $('#error').css('display', 'block');
            $('#password').focus();
            return false;
        }

    if ($("#password").val() !== $("#rePass").val())  {
                $('#error').html("These passwords don't match")
                $('#error').css('display', 'block');
                $('#rePass').focus();
                return false;
            }

     if ($('#fullname').val()== "") {
                 $('#error').html("You can't leave this empty.")
                 $('#error').css('display', 'block');
                 $('#fullname').focus();
                 return false;
             }

     if ($('#email').val()== "") {
                 $('#error').html("You can't leave this empty.")
                 $('#error').css('display', 'block');
                 $('#email').focus();
                 return false;
             }

     if ($('#contactno').val()== "") {
                 $('#error').html("You can't leave this empty.")
                 $('#error').css('display', 'block');
                 $('#contactno').focus();
                 return false;
             }
    return true;
}

function createNewUser(deptid, username, password, fullname,email,contactno){
    $('#new-user-form :input').prop("disabled", true);
    enableloading();

    gapi.client.myApi.createUser({'deptid':deptid, 'username':username,'password':password, 'fullname':fullname,'email':email,'contactno':contactno,'active':'1' }).execute(
               function(response){
                    disableLoading();
                    $('#new-user-form :input').prop("disabled", false);
                   if(response.error)
                       console.log(response.error.code);
                   $('#user-detail').modal('toggle');
                   location.reload();
                   alert(response.data);
               }
           )
}

$(document).ready(function() {
    $("#contactno").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});


$(window).load(function(){
    $(".username").html(window.localStorage.username);
});
