function refreshToken()
{
if (window.localStorage.accessToken === null) return;

gapi.client.myApi.refreshToken({'token': window.localStorage.accessToken}).execute(
        function(response) {

            if (!response.error) {
                obj = JSON.parse(response.result.data);

                if (obj.code == '1') {
                        window.localStorage.accessToken = obj.data;
                    }
                    else {

                        window.location = "../index.html"; // go to home.html
                    }
            }
            else if (response.error) {
                alert(response.error.code);
            }
        }
    );
}

$(function() {

    var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#reportrange').data('fromDate',start.format('YYYY-MM-DD'));
        $('#reportrange').data('toDate',end.format('YYYY-MM-DD'));
        $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        if(typeof gapi.client != 'undefined') showReportByFilter();
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});

function showReportByFilter(){
    var filter = filterReportDetail();
    showReport(filter.tableName,filter.fromDate,filter.toDate,filter.status);
}

function filterReportDetail(){

    var filter = new Object();
    var statusArray = [];
    var statusString;
    $("#select-status option:selected").each(function () {
                    statusArray.push($(this).text());
                  });
    if (statusArray.length === 2)  statusString="All";
    else statusString= statusArray[0];

    var fromDate = $('#reportrange').data('fromDate');
    var toDate = $('#reportrange').data('toDate');

    var tableName = $('#report-detail-table-name').data('tableName');

    filter.tableName = tableName;
    filter.fromDate = fromDate;
    filter.toDate = toDate;
    filter.status = statusString;

    return filter;
}

$("#select-status").change(function () {
  showReportByFilter()
});
