package br.skymapglobal.ktp_backend.models;

//import com.google.appengine.repackaged.com.google.gson.Gson;
import com.google.gson.Gson;

/**
 * Created by thaibui on 8/19/16.
 */
public class ResponseModel {

    private int type;
    private Object data;
    private int code;
    private String msg;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    public String toJSONString() {
        return new Gson().toJson(this);
    }
}
