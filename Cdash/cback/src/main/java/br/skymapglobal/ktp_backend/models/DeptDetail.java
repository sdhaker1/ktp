package br.skymapglobal.ktp_backend.models;

/**
 * Created by pham tuan anh on 24/08/2016.
 */
public class DeptDetail {
    public int deptid;
    public String departmentName;
    public String email;
    public String contactNo;
    public String description;
    public int active;
    public String listReport;

    public DeptDetail(String departmentName,String email,String contactNo,String description,String listReport ){
        this.departmentName = departmentName;
        this.email = email;
        this.contactNo = contactNo ;
        this.description = description;
        this.active = 1;
        this.listReport = listReport;
    }

    public DeptDetail(){};
}
