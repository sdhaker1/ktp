package br.skymapglobal.ktp_backend.global;

import org.jose4j.keys.AesKey;
import org.jose4j.lang.ByteUtil;

import java.security.Key;

/**
 * Created by thaibui on 8/19/16.
 */
public class Config {
    public static final Key JWT_KEY = new AesKey(ByteUtil.randomBytes(16));
    public static final int TOKEN_TIME_EXP = 7; //Day
}
