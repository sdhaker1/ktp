package br.skymapglobal.ktp_backend.helpers;

import com.google.appengine.api.utils.SystemProperty;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.PrintWriter;
import java.io.IOException;
import br.skymapglobal.ktp_backend.models.CountOfReport;
import br.skymapglobal.ktp_backend.models.DeptDetail;
import br.skymapglobal.ktp_backend.models.PoliceRegisterModel;
import br.skymapglobal.ktp_backend.models.Register;
import br.skymapglobal.ktp_backend.models.ReportCountResponse;
import br.skymapglobal.ktp_backend.models.GridCountResponse;
import br.skymapglobal.ktp_backend.models.ReportDetails;
import br.skymapglobal.ktp_backend.models.UserModel;
import br.skymapglobal.ktp_backend.models.PoliceStation;
import java.util.logging.Logger;

/**
 * Created by thaibui on 8/23/16.
 */
public class SQLConnector {
    //private static final Logger log = Logger.getLogger(MyClass.class.getName());
    private static SQLConnector ourInstance = new SQLConnector();

    public static SQLConnector getInstance() {
        return ourInstance;
    }

    private Connection connection;
    private String url = null;

    private SQLConnector() {

        try {

            /*Class.forName("com.mysql.jdbc.GoogleDriver");
            url = "jdbc:google:mysql://ktpcloud-1470216325260:ktpinstance/ktpreportdb?user=root";*/
            if (SystemProperty.environment.value() == SystemProperty.Environment.Value.Production) {
                Class.forName("com.mysql.jdbc.GoogleDriver");
                url = "jdbc:google:mysql://ktpcloud-1470216325260:ktpinstance/ktpreportdb?user=root";
            } else {
                Class.forName("com.mysql.jdbc.Driver");
                url = "jdbc:mysql://173.194.83.210/ktpreportdb?user=ktpuser&password=ktpuser";
            }
            connection = DriverManager.getConnection(url);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void log(String s) {

       // System.console().writer().println(s);

    }

    private boolean reConnect() {
        try {
            if (connection != null) {

                connection.close();

                connection = null;
            }
            connection = DriverManager.getConnection(url);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;

        }
    }

    private boolean isConnection() {
        try {
            if (connection == null) {

                return reConnect();
            } else {
                return connection.isReadOnly() || reConnect();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public UserModel getUserLogin(String user, String password) {
        if (isConnection()) {
            try {
                String query = "select * from vwUserDetail where username = ? and password = ? and active = 1";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, user);
                preparedStatement.setString(2, password);
                ResultSet resultSet = preparedStatement.executeQuery();
                if (!resultSet.first()) {
                    preparedStatement.close();
                    return null;
                } else {

                    UserModel userModel = new UserModel();
                    userModel.userId = resultSet.getString("pkumid");
                    userModel.userName = resultSet.getString("username");
                    userModel.deptId = resultSet.getInt("deptid");
                    userModel.email = resultSet.getString("email");
                    userModel.contactNo = resultSet.getString("contactno");
                    userModel.active = resultSet.getInt("active");
                    userModel.deptName = resultSet.getString("departmentname");
                    userModel.reports = resultSet.getString("reports");
                    preparedStatement.close();
                    return userModel;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    public ArrayList<UserModel> queryGetAllUser() {

        if (isConnection()) {
            try {
                String query = "select * from vwUserDetail";
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                ArrayList<UserModel> users = new ArrayList<>();
                if (resultSet.first()) {
                    do {
                        UserModel userModel = new UserModel();
                        userModel.userId = resultSet.getString("pkumid");
                        userModel.userName = resultSet.getString("username");
                        userModel.fullName = resultSet.getString("fullname");
                        userModel.contactNo = resultSet.getString("contactno");
                        userModel.email = resultSet.getString("email");
                        userModel.deptId = resultSet.getInt("deptid");
                        userModel.deptName = resultSet.getString("departmentname");
                        userModel.active = resultSet.getInt("active");
                        users.add(userModel);
                    } while (resultSet.next());
                }
                statement.close();
                return users;

            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;

        }

    }

    public ArrayList<DeptDetail> queryAllDept() {
        if (isConnection()) {
            try {
                String query = "select * from tblDepartmentMaster";
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                ArrayList<DeptDetail> department = new ArrayList<>();
                if (resultSet.first()) {
                    do {
                        DeptDetail deptDetail = new DeptDetail();
                        deptDetail.deptid = resultSet.getInt("id");
                        deptDetail.departmentName = resultSet.getString("departmentname");
                        deptDetail.contactNo = resultSet.getString("contactno");
                        deptDetail.email = resultSet.getString("email");
                        deptDetail.description = resultSet.getString("description");
                        deptDetail.active = resultSet.getInt("active");
                        deptDetail.listReport = resultSet.getString("listreport");
                        department.add(deptDetail);
                    } while (resultSet.next());
                }
                statement.close();
                return department;

            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;

        }
    }

    public ArrayList<PoliceStation> queryAllPoliceStation() {
        if (isConnection()) {
            try {
                String query = "select * from PoliceStation";
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                ArrayList<PoliceStation> ps_list = new ArrayList<>();
                if (resultSet.first()) {
                    do {
                        PoliceStation ps = new PoliceStation();
                        ps.code = resultSet.getString("Code");
                        ps.name = resultSet.getString("Name");

                        ps_list.add(ps);
                    } while (resultSet.next());
                }
                statement.close();
                return ps_list;

            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;

        }
    }

    public boolean queryResolveReport(String tableName, String reportID) {

        if (isConnection()) {
            try {
                String query = "Update " + tableName + " Set  status = ? where id = " + reportID;
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, "Resolved");
                int result = preparedStatement.executeUpdate();
                return (result == 1);
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    public String queryInsertReport(String tableName, String description, String userid, String beatno, String guardcode, String status, boolean active, String longitude, String latitude, String type, String place) {
        String query;
        if (isConnection()) {
            try {
                switch (tableName) {
                    case "tblRoadFurniture":
                    case "tblPublicViolation":
                        query = "insert into " + tableName + " (description, userid, beatno, guardcode, active ,status, lon, lat, place, type ) value (?,?,?,?,?,?,?,?,?,?);";
                        break;
                    case "tblOthers":
                        query = "insert into " + tableName + " (description, userid, beatno, guardcode, active ,status, lon, lat, place, subject ) value (?,?,?,?,?,?,?,?,?,?);";
                        break;
                    default:
                        query = "insert into " + tableName + " (description, userid, beatno, guardcode, active ,status, lon, lat, place ) value (?,?,?,?,?,?,?,?,?);";
                        break;
                }

                PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, description);
                preparedStatement.setString(2, userid);
                preparedStatement.setString(3, beatno);
                preparedStatement.setString(4, guardcode);
                preparedStatement.setBoolean(5, active);
                preparedStatement.setString(6, status);
                preparedStatement.setString(7, longitude);
                preparedStatement.setString(8, latitude);
                preparedStatement.setString(9, place);
                if (tableName.equals("tblRoadFurniture") || tableName.equals("tblPublicViolation") || tableName.equals("tblOthers")) {
                    preparedStatement.setString(10, type);
                }
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();

                if (!resultSet.first()) {
                    return null;
                } else {
                    String id = resultSet.getInt(1) + "";
                    String query3 = "update " + tableName + " set imagelocation = ? where id = ?";
                    preparedStatement.clearParameters();
                    preparedStatement.close();
                    preparedStatement = connection.prepareStatement(query3);
                    preparedStatement.setString(1, getImageUrl(tableName, id));
                    preparedStatement.setString(2, id);
                    preparedStatement.executeUpdate();
                    preparedStatement.clearParameters();
                    preparedStatement.close();
                    return id;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }

    }

    public String queryInsertReportKTPPublish(String tableName, String deviceId, String category, String description, String location, String city, String lat, String lng,String mobile, String ps) {
        String query;
        if (isConnection()) {
            try {
                query = "insert into " + tableName + " (DeviceId , Category, Description, Place, City, Lat, Lng, Mobile, PoliceStation) value (?,?,?,?,?,?,?,?,?);";
                PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, deviceId);
                preparedStatement.setString(2, category);
                preparedStatement.setString(3, description);
                preparedStatement.setString(4, location);
                preparedStatement.setString(5, city);
                preparedStatement.setString(6, lat);
                preparedStatement.setString(7, lng);
                preparedStatement.setString(8, mobile);
                preparedStatement.setString(9, ps);
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();

                if (!resultSet.first()) {
                    return null;
                } else {
                    String id = resultSet.getInt(1) + "";
                    String query3 = "update " + tableName + " set ImageUrl = ? where ID = ?";
                    preparedStatement.clearParameters();
                    preparedStatement.close();
                    preparedStatement = connection.prepareStatement(query3);
                    preparedStatement.setString(1, getImageUrl(tableName, id));
                    preparedStatement.setString(2, id);
                    preparedStatement.executeUpdate();
                    preparedStatement.clearParameters();
                    preparedStatement.close();
                    return id;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }

    }

    public String queryInsertRegistrationKTPPublic(String type, String deviceId, String data, String place, String lat, String lng) {
        String query;
        if (isConnection()) {
            try {
                query = "insert into tblPublicRegistration" + " (Type, DeviceId , Data, Place, Lat, Lng) value (?,?,?,?,?,?);";
                PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, type);
                preparedStatement.setString(2, deviceId);
                preparedStatement.setString(3, data);
                preparedStatement.setString(4, place);
                preparedStatement.setString(5, lat);
                preparedStatement.setString(6, lng);
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();

                if (!resultSet.first()) {
                    return null;
                } else {
                    String id = resultSet.getInt(1) + "";
                    String query3 = "update tblPublicRegistration set ImageUrl = ? where ID = ?";
                    preparedStatement.clearParameters();
                    preparedStatement.close();
                    preparedStatement = connection.prepareStatement(query3);
                    preparedStatement.setString(1, getImageUrl("tblPublicRegistration", id));
                    preparedStatement.setString(2, id);
                    preparedStatement.executeUpdate();
                    preparedStatement.clearParameters();
                    preparedStatement.close();
                    return id;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }

    }

    public int queryInsertPoliceRegister(PoliceRegisterModel model) {
        if (isConnection()) {
            try {
                String query = "INSERT INTO tblRegister"
                        + " (Type, TenantName, AadhaarCardNo, Age, Occupation, MaritalStatus, TenantAddress, PlaceOfWork, DetailsKnownPerson, KnownPersonPhone, CurrentLocation, RentedHouseAddress, FatherName)"
                        + " VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')";
                Object[] param = new Object[13];
                param[0] = model.type;
                param[1] = model.tenantName;
                param[2] = model.cardNo;
                param[3] = model.age;
                param[4] = model.occupation;
                param[5] = model.marital;
                param[6] = model.tenantAddress;
                param[7] = model.placeWork;
                param[8] = model.detailsPerson;
                param[9] = model.personPhone;
                param[10] = model.currentLocation;
                param[11] = model.houseAddress;
                param[12] = model.fatherName;
                query = String.format(query, param);

                Statement statement = connection.createStatement();
                statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
                ResultSet keys = statement.getGeneratedKeys();
                int result = 0;
                if (keys.first()) {
                    result = keys.getInt(1);
                    String urlImage = "https://storage.googleapis.com/registerwithpolice/" + result;
                    statement.clearBatch();
                    String query3 = "update tblRegister set Image = '" + urlImage + "' where id = " + result;
                    statement.execute(query3);
                }
                statement.close();
                return result;

            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
        } else {
            return 0;
        }
    }


    public boolean queryDisableUser(String userID) {

        if (isConnection()) {
            try {
                String query = "Update tblUserMaster Set active = ? where pkumid = " + userID;
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setBoolean(1, false);
                int result = preparedStatement.executeUpdate();
                return (result == 1);
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    public int queryInsertUser(UserModel user) {
        if (isConnection()) {
            try {
                String query = "insert into tblUserMaster (deptid, username, password, fullname, email ,contactno, active) value (?,?,?,?,?,?,?);";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setInt(1, user.deptId);
                preparedStatement.setString(2, user.userName);
                preparedStatement.setString(3, user.password);
                preparedStatement.setString(4, user.fullName);
                preparedStatement.setString(5, user.email);
                preparedStatement.setString(6, user.contactNo);
                preparedStatement.setInt(7, user.active);
                int result = preparedStatement.executeUpdate();
                preparedStatement.close();
                return result;

            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
        } else {
            return 0;
        }

    }


    public int queryUpdateUser(UserModel user) {
        if (isConnection()) {
            try {
                String query = "update tblUserMaster set deptid = ? , username = ?, password = ? , fullname = ?, email = ? ,contactno= ? , active = ? where pkumid= ?;";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setInt(1, user.deptId);
                preparedStatement.setString(2, user.userName);
                preparedStatement.setString(3, user.password);
                preparedStatement.setString(4, user.fullName);
                preparedStatement.setString(5, user.email);
                preparedStatement.setString(6, user.contactNo);
                preparedStatement.setInt(7, user.active);
                preparedStatement.setString(8, user.userId);
                int result = preparedStatement.executeUpdate();
                preparedStatement.close();
                return result;

            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
        } else {
            return 0;
        }

    }

    public int queryInsertDept(DeptDetail deptDetail) {
        if (isConnection()) {
            try {
                String query = "insert into tblDepartmentMaster (departmentname, email ,contactno, description, active, listreport) value (?,?,?,?,?,?);";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, deptDetail.departmentName);
                preparedStatement.setString(2, deptDetail.email);
                preparedStatement.setString(3, deptDetail.contactNo);
                preparedStatement.setString(4, deptDetail.description);
                preparedStatement.setInt(5, deptDetail.active);
                preparedStatement.setString(6, deptDetail.listReport);
                int result = preparedStatement.executeUpdate();
                preparedStatement.close();
                return result;

            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
        } else {
            return 0;
        }
    }

    public int queryUpdateDept(DeptDetail deptDetail) {
        if (isConnection()) {
            try {
                String query = "update tblDepartmentMaster set departmentname = ? , email = ? ,contactno = ?, description = ? , listreport = ? where id = ? ;";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, deptDetail.departmentName);
                preparedStatement.setString(2, deptDetail.email);
                preparedStatement.setString(3, deptDetail.contactNo);
                preparedStatement.setString(4, deptDetail.description);
                preparedStatement.setString(5, deptDetail.listReport);
                preparedStatement.setInt(6, deptDetail.deptid);
                int result = preparedStatement.executeUpdate();
                preparedStatement.close();
                return result;

            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    public String getImageUrl(String tableName, String id) {
        String bucketName;
        if (tableName.equals("tblSignal")) {
            bucketName = "reportsignal";
        } else if (tableName.equals("tblTrafficCongestion")) {
            bucketName = "reporttrafficcongestion";
        } else if (tableName.equals("tblViolation")) {
            bucketName = "reportviolation";
        } else if (tableName.equals("tblRoadMarking")) {
            bucketName = "reportmarking";
        } else if (tableName.equals("tblRoadCondition")) {
            bucketName = "reportcondition";
        } else if (tableName.equals("tblRoadFurniture")) {
            bucketName = "reportfurniture";
        } else if (tableName.equals("tblOthers")) {
            bucketName = "reportothers";
        } else if (tableName.equals("tblPublicCrime")) {
            bucketName = "ktppublicreportcrime";
        } else if (tableName.equals("tblPublicViolation")) {
            bucketName = "ktppublicreportviolation";
        } else if (tableName.equals("tblPublicRegistration")) {
            bucketName = "ktppublicregistration";
        } else {
            bucketName = "";
        }

        String BASE_URL = "https://storage.googleapis.com/%s/%s";
        return String.format(BASE_URL, bucketName, id);

    }
    
  public ArrayList<ReportDetails> queryGridReports(String fromDate, String toDate, String where) {

        if (isConnection()) {
            try {
                ArrayList<ReportDetails> reports = new ArrayList<>();
                String query = "";
                                   
                    query = "select * from tblPublicRegistration where lat is not null and lng is not null and DATE(DateTime) >= '" + fromDate + "' and DATE(DateTime) <= '" + toDate + "' and " + where + " order by DateTime desc";
               // String query = "select * from " + tableName + " where lat is not null and lng is not null " + wheretype + " order by DateTime desc";
                Statement statement = connection.createStatement();


                ResultSet resultSet = statement.executeQuery(query);
                if (resultSet.first()) {
                    do {
                        ReportDetails reportDetails = new ReportDetails();
                        reportDetails.id = resultSet.getString("id");
                      reportDetails.devid = resultSet.getString("deviceid");

                        reportDetails.datetime = resultSet.getString("DateTime");

                            reportDetails.type = resultSet.getString("type");
                            reportDetails.data = resultSet.getString("data");


                        reportDetails.latitude = resultSet.getString("lat");
                        reportDetails.longitude = resultSet.getString("lng");
                        reportDetails.address = resultSet.getString("place");


                        reportDetails.imagelocation = resultSet.getString("imageurl");



                        reports.add(reportDetails);
                    } while (resultSet.next());
                }
                statement.close();
                return reports;
            } catch (SQLException e) {

                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }
    

    public ArrayList<ReportDetails> queryGetReports(String tableName, String fromDate, String toDate, String whereStatus) {

        if (isConnection()) {
            try {
                ArrayList<ReportDetails> reports = new ArrayList<>();
                String query = "";
                if (tableName.contains("tblPublicRegistration"))
                    whereStatus = whereStatus.replace("status","type");

                    query = "select * from " + tableName + " where lat is not null and lng is not null and DATE(DateTime) >= '" + fromDate + "' and DATE(DateTime) <= '" + toDate + "'" + whereStatus + " order by DateTime desc";
               // String query = "select * from " + tableName + " where lat is not null and lng is not null " + whereStatus + " order by DateTime desc";
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                if (resultSet.first()) {
                    do {
                        ReportDetails reportDetails = new ReportDetails();
                        reportDetails.id = resultSet.getString("id");
                      reportDetails.devid = resultSet.getString("deviceid");

                        reportDetails.datetime = resultSet.getString("DateTime");
                        reportDetails.latitude = resultSet.getString("lat");
                        reportDetails.longitude = resultSet.getString("lng");

                        if (!tableName.contains("tblPanicHits")) {
                            if (tableName.contains("tblPublicRegistration")) {
                                reportDetails.type = resultSet.getString("type");
                                reportDetails.data = resultSet.getString("data");

                            } else {
                                reportDetails.description = resultSet.getString("description");
                                reportDetails.mobile = resultSet.getString("mobile");
                                reportDetails.pstn = resultSet.getString("policestation");
                                reportDetails.city = resultSet.getString("city");
                                reportDetails.status = resultSet.getString("status");
                            }

                            reportDetails.address = resultSet.getString("place");


                            reportDetails.imagelocation = resultSet.getString("imageurl");

                        }

                        reports.add(reportDetails);
                    } while (resultSet.next());
                }
                statement.close();
                return reports;
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }


    public ArrayList<Register> queryGetRegisters(String fromDate, String toDate, String whereStatus) {

        if (isConnection()) {
            try {
                ArrayList<Register> registers = new ArrayList<>();
                String query = "select * from tblRegister where DATE(DateTime) >= '" + fromDate + "' and DATE(DateTime) <= '" + toDate + "'" + whereStatus + " order by DateTime desc";
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                if (resultSet.first()) {
                    do {
                        Register register = new Register();
                        register.id = resultSet.getInt("ID");
                        register.type = resultSet.getString("Type");
                        register.image = resultSet.getString("Image");
                        register.tenantName = resultSet.getString("TenantName");
                        register.fatherName = resultSet.getString("FatherName");
                        register.aadhaarCardNo = resultSet.getString("AadhaarCardNo");
                        register.age = resultSet.getInt("Age");
                        register.occupation = resultSet.getString("Occupation");
                        register.maritalStatus = resultSet.getString("MaritalStatus");
                        register.tenantAddress = resultSet.getString("TenantAddress");
                        register.placeOfWork = resultSet.getString("PlaceOfWork");
                        register.detailsKnownPerson = resultSet.getString("DetailsKnownPerson");
                        register.knownPersonPhone = resultSet.getString("KnownPersonPhone");
                        register.currentLocation = resultSet.getString("CurrentLocation");
                        register.rentedHouseAddress = resultSet.getString("RentedHouseAddress");
                       // register.DateTime = resultSet.getString("DateTime");

                    } while (resultSet.next());
                }
                statement.close();
                return registers;
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }


    }

    public ReportCountResponse queryCountReport(String tableName, String fromDate, String toDate, HashMap<String, String> map) {
        ReportCountResponse reportCountResponse = new ReportCountResponse();
        reportCountResponse.tableName = tableName;
        reportCountResponse.listData = new ArrayList<>();

        if (isConnection()) {
            try {
                String query = "SELECT date(DateTime) as mDay, COUNT(*) as mCount FROM " + tableName + " WHERE date(DateTime) >= ? AND date(DateTime) <= ? GROUP BY DATE(DateTime) ORDER BY mDay desc";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, fromDate);
                statement.setString(2, toDate);

    ResultSet resultSet = statement.executeQuery();
    if (resultSet.first())
        do {
            String count = resultSet.getString("mCount");
            String date = resultSet.getString("mDay");
            map.put(date, count);
        } while (resultSet.next());

    Object[] keys = map.keySet().toArray();
    Object[] values = map.values().toArray();
    for (int i = 0; i < map.size(); i++) {
        String date = (String) keys[i];
        String count = (String) values[i];
        reportCountResponse.listData.add(new CountOfReport(date, count));
    }
    statement.close();

                return reportCountResponse;
            } catch (SQLException e) {
               // e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }
    
    public GridCountResponse queryGridCountReport(String where, String fromDate, String toDate, HashMap<String, String> map) {
        GridCountResponse gridCountResponse = new GridCountResponse();
        //gridCountResponse.tableName = tableName;
        gridCountResponse.listData = new ArrayList<>();

        if (isConnection()) {
            try {
               // String query = "SELECT DISTINCT concat(name,',',id) as mDay, (SELECT COUNT(*) FROM tblPublicRegistration b WHERE b.ps_id = a.id and ('All' = 'All' or type = 'All') and date(DateTime) >= '2017-03-27' AND date(DateTime) <= '2017-03-27') AS mCount FROM PoliceStation a order by name";
                String query = "SELECT DISTINCT concat(name,',',id) as mDay, (SELECT COUNT(*) FROM tblPublicRegistration b WHERE b.ps_id = a.id and " + where + " and date(DateTime) >= '" + fromDate + "' AND date(DateTime) <= '" + toDate + "' ) AS mCount FROM PoliceStation a order by name";
                PreparedStatement statement = connection.prepareStatement(query);
/*
                statement.setString(1, type);
                statement.setString(2, type);

                statement.setString(3, fromDate);
                statement.setString(4, toDate);
*/

    ResultSet resultSet = statement.executeQuery();
    if (resultSet.first())
        do {
            String count = resultSet.getString("mCount");
            String date = resultSet.getString("mDay");
            gridCountResponse.listData.add(new CountOfReport(date, count));
           // map.put(date, count);
        } while (resultSet.next());

    statement.close();

                return gridCountResponse;
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }
    
}
