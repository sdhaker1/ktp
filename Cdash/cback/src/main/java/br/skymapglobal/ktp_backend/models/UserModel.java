package br.skymapglobal.ktp_backend.models;

/**
 * Created by thaibui on 8/19/16.
 */
public class UserModel {

    public String userId;
    public String userName;
    public String fullName;
    public int deptId;
    public String deptName;
    public String contactNo;
    public String email;
    public int active;
    public String password;
    public String reports;

    public UserModel(int deptId, String userName,String password,String fullName ,String email,String contactNo,int active) {
        this.deptId = deptId;
        this.userName = userName;
        this.password = password;
        this.fullName = fullName;
        this.email = email;
        this.contactNo = contactNo;
        this.active = active;

    }

    public UserModel(){};

}
