package br.skymapglobal.ktp_backend.models;

/**
 * Created by thaibui on 8/16/16.
 */
public class ReportDetails {
   public String id;
    public String datetime;
    public String description;
    public String city;
    public String devid;
    public String mobile;
    public String pstn;
    public String imagelocation;
    public String status;
    public String latitude;
    public String longitude;
    public String address;
    public String type;
    public String data;

}
