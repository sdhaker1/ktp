package br.skymapglobal.ktp_backend.global;

/**
 * Created by thaibui on 8/19/16.
 */
public class AppConstants {
    public static final int LOGIN_TYPE = 0;
    public static final int LOGOUT_TYPE = 1;
    public static final int REFRESH_TOKEN_TYPE = 2;

}
