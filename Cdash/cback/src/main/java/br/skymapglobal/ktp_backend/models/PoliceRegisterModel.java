package br.skymapglobal.ktp_backend.models;

import com.google.appengine.repackaged.com.google.gson.Gson;

/**
 * Created by thaibui on 8/27/16.
 */
public class PoliceRegisterModel {

    public String type = "aaaaa";
    public String marital = "aaaaa";
    public String rented = "aaaaa";
    public String tenantName = "aaaaa";
    public String fatherName = "aaaaa";
    public String cardNo = "aaaaa";
    public String age = "aaaaa";
    public String occupation = "aaaaa";
    public String mobile = "aaaaa";
    public String tenantAddress = "aaaaa";
    public String placeWork = "aaaaa";
    public String detailsPerson = "aaaaa";
    public String personPhone = "aaaaa";
    public String area = "aaaaa";
    public String currentLocation = "aaaaa";
    public String houseAddress = "aaaaa";

    public static PoliceRegisterModel parseFromJSON(String json) {
        return new Gson().fromJson(json, PoliceRegisterModel.class);
    }
}
