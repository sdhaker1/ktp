package br.skymapglobal.ktp_backend.models;

/**
 * Created by thaibui on 8/15/16.
 */
public class CountOfReport {
    String date;
    String count;

    public CountOfReport(String date, String count) {
        this.date = date;
        this.count = count;
    }
}
