var allTableName = ["tblPublicCrime","tblPublicViolation","tblPublicRegistration"];

var jsonhash = {};
var objs;
var dt1;
var dt2;
var where;
var ps_id;
var ps_name;

function show_datax(id)
{
$('#reformed').reform(jsonhash[id], {'editor':'data'});

$('#data-details').modal('show');

}

function fmtdate(d)
{
  var month = d.getMonth()+1;
    var day = d.getDate();

return d.getFullYear() + '-' +
        (month<10 ? '0' : '') + month + '-' +
        (day<10 ? '0' : '') + day;
}

function getReportName(tableName){
    switch(tableName) {
     case 'TenantRegister':
            return 'Tenant Registration';
    case 'SecurityGuard':
            return 'Security Guard Registration';
    case 'DomesticHelp':
            return 'Domestic Help Registration';
    case 'ServiceProvider':
            return 'Service Provider Registration';
        case 'tblPublicCrime':
            return 'Crime';
        case 'tblTrafficCongestion':
            return 'Traffic Congestion';
        case 'tblPublicViolation':
            return 'Violation';
        case 'tblRoadMarking':
            return 'Road Marking';
        case 'tblRoadCondition':
            return 'Road Condition';
        case 'tblRoadFurniture':
            return 'Road Furniture';
        case 'tblOthers':
            return 'Others';
    }
}

function loadLastWeekCountReport() {
    gapi.client.myApi.countReportOfWeek({'listData': JSON.stringify(allTableName)}).execute(
        function(response){
            if(response.error)
                console.log(response.error.code);
            objs = JSON.parse(response.result.data);

            objs.forEach(function(obj){
                var table_tag = "#table-report-" + obj.tableName;
                var table_content = "<thead>\
                                        <tr>\
                                            <th>Date</td>\
                                            <th>Count of Report</td>\
                                        </tr>\
                                    </thead>\
                                    <tbody>";
                obj.listData.forEach(function(data){
                    table_content += "<tr><td>" + data.date + "</td><td><a class=\"cursor-pointer\" onclick=\"showReportOfDay('" + obj.tableName + "','" + data.date +"')\">" + data.count + "</a></td></tr>";
                })
                $(table_tag).html(table_content);
            })
        }
    )
}

   function loadCountOfGridReport(where,dt1,dt2) {
            gapi.client.myApi.countGrid({'where': where,'fromDate':dt1, 'toDate':dt2}).execute(
                function(response) {
                    if (!response.error) {
                       var json =JSON.parse(response.result.data);
                       var js = JSON.parse(json.data);
                       objs = js[0].listData;
                       show_ps();
                                 }
                    else if (response.error) {
                        console.log(response.error.code);
                        
                    }
                }
            );
}

    function loadCountOfAllReport() {
           gapi.client.myApi.countReportOfDay({'listData': JSON.stringify(allTableName)}).execute(
                function(response) {
                    if (!response.error) {
                        objs = JSON.parse(response.result.data);
                        objs.forEach(function(obj){
                            var p_tag = "#count-"+obj.tableName;
                            if (obj.listData.length == 0) count = 0
                            else count = obj.listData[0].count;
                            $(p_tag).html(count);
                        })
                    }
                    else if (response.error) {
                        console.log(response.error.code);
                    }
                }
            );
/*
           gapi.client.myApi.countRegister({'fromDate': moment().format('YYYY-MM-DD'), 'toDate' : moment().format('YYYY-MM-DD')}).execute(
                           function(response) {
                               if (!response.error) {
                                   objs = JSON.parse(response.result.data);
                                   objs.data.forEach(function(obj){
                                       var p_tag = "#count-"+obj.tableName;
                                       if (obj.listData.length == 0) count = 0
                                       else count = obj.listData[0].count;
                                       $(p_tag).html(count);
                                   })
                               }
                               else if (response.error) {
                                   console.log(response.error.code);
                               }
                           }
                       );
                       */
    }

    function fillDate()
    {

        loadCountOfAllReport();
        loadLastWeekCountReport();
    }

    var i = 0;

    function myLoop () {           //  create a loop function
        setTimeout(function () {    //  call a 3s setTimeout when the loop is called
            fillDate();
            loadUserInfo();//  ..  setTimeout()        //  your code here
            myLoop();
        }, 3000)
        }


    function init() {

      var apiName = 'myApi';
      var apiVersion = 'v1';
      var apiRoot = 'https://18-dot-citizen-dot-ktpcloud-1470216325260.appspot.com/_ah/api';
     // var apiRoot = 'http://localhost:8080/_ah/api';
     
      if (window.location.hostname == 'localhost' || window.location.hostname == '127.0.0.1')
	  apiRoot = 'http://localhost:8080/_ah/api';

      var callback = function() {
             init_ps();
      }
      gapi.client.load(apiName, apiVersion, callback, apiRoot);
    }
    
    function initForTest() {
      var apiName = 'myApi';
      var apiVersion = 'v1';
    var apiRoot = 'http://localhost:8080/_ah/api';
  
        var callback = function() {
            refreshToken();
            loadUserInfo();
            fillDate();
            myLoop();
            
 
      }
      gapi.client.load(apiName, apiVersion, callback, apiRoot);
    }


    function showReportOfDay(tableName,date) {
        $('#reportrange').data('fromDate',date);
        $('#reportrange').data('toDate',date);
        $('#reportrange span').html(date + ' - ' + date);
        $('#report-detail-table-name').data('tableName',tableName);
        $('#select-status').val("pending");
        $('#select-status').select2();
        showReport(tableName,date,date,'pending')
    }

    function showGridReport(idx) {
   
 ps_id = keyhash[idx].id;
 ps_name = keyhash[idx].name;
 

        gapi.client.myApi.gridReports({'fromDate':dt1, 'toDate':dt2 , 'where' :where +  " and ps_id = " + ps_id + " "}).execute(

            function(response){
                var table;
                if(response.error)
                    console.log(response.error.code);
                    
                objs = JSON.parse(response.result.data);
                            
                var table_content = "<thead class=\"w3-blue\"\>\
                                        <tr>\
                                         <th>Type</th>\
                                            <th>ID</th>\
                                            <th>Name</th>\
                                            <th>Date Time</th>\
                                            <th>Location</th>\
                                            <th>Mobile</th>\
                                            <th>Image</th>\
                                              <th>Detail</th>\
                                             <th><button class=\"cursor-pointer w3-blue w3-btn w3-round\"><i class=\"fa fa-envelope\"></i></button></th>\
                                        </tr>\
                                    </thead>\
                                    <tbody>";
                var location = "";
                
           var tableName = 'tblPublicRegistration';
                
                objs.data.forEach(function(obj){
                
                
                
                     var json = JSON.parse(obj.data);
                     
                     jsonhash[obj.id] = obj.data;
                     
                    location = obj.latitude+" , "+obj.longitude
                    table_content += "<tr><td>" + getReportName(obj.type) + "</td><td>" + obj.id +"</td><td>" + json.name +"</td><td>" + obj.datetime +"</td><td>";
                                      
                    table_content += obj.address.toUpperCase() +"</td><td>" + json.mobile +"</td>"
                    table_content += "<td><image style=\"height:45px\" onclick=\"showImage(this)\" src=\""+obj.imagelocation +"\"></td>";
			table_content += "<td><button class=\"cursor-pointer w3-btn w3-white w3-ripple w3-round w3-text-theme\" onclick=\"show_datax(" + obj.id +")\"><i class=\"fa fa-list-alt\"></i></button></td>";
                    if (obj.active)
                        table_content += "<td><button onclick=\"openEmail('" + tableName + "', " + obj.id +" ,'"+obj.description +"','"+obj.address+"','"+obj.datetime+"')\" class=\"cursor-pointer w3-btn w3-white w3-ripple w3-round w3-text-theme\"><i class=\"fa fa-envelope\"></i></button></td>";
                    else
                        table_content += "<td><button onclick=\"openEmail('" + tableName + "', " + obj.id +" ,'"+obj.description +"','"+obj.address+"','"+obj.datetime+"')\" class=\"cursor-pointer w3-btn w3-white w3-ripple w3-round w3-text-theme\"><i class=\"fa fa-envelope\"></i></button></td>";
                    table_content +="</tr>";
                })
                
                
                    title = "Public Registration Report for " + ps_name;
                            table_content += "</tbody>"
            
                $('#report-detail-table-name').html(title);
                $('#table-report-detail').html(table_content);

                table = $('#table-report-detail').DataTable({
                "destroy": true,
                "paging": true,
                "lengthChange": true,
                "aLengthMenu": [[5, 10,-1], [5,10,"All"]],
                "iDisplayLength": 5,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false
                });
            }
        )

        $('#report-detail').modal('show');
    }

    function resolveReport(tableName,reportId){

        if (confirm('Status will be update to Resolved...'))
            {
                enableloading();
                gapi.client.myApi.resolveReport({'tableName':tableName , 'reportID':reportId}).execute(
                    function(response){
                         disableLoading();
                         if(response.error)
                            console.log(response.error.code);
                         alert(response.result.data);
                         showReportByFilter();
                    });
            }
        else return;
    }

    function logout(){
        gapi.client.myApi.logout({'token' : window.localStorage.accessToken}).execute(
            function(response){
                 if(response.error)
                    console.log(response.error.code);
                 var  obj = JSON.parse(response.result.data);
                 if (obj.code=='1') {
                     window.localStorage.accessToken = null;
                     window.location = "../index.html"; // go to home.html
                 }
                 else {
                     return;
                 }
            });
    }

    function loadUserInfo() {
        if (window.localStorage.accessToken === null) return;

        gapi.client.myApi.profileUser({'token': window.localStorage.accessToken}).execute(
                function(response) {

                    if (!response.error) {
                        obj = JSON.parse(response.result.data);

                        if (obj.code == '1') {

                                 var reports = obj.data.reports.split(",");
                                reports.forEach(function (report){
                                    $("#"+report).css('display', 'block');
                                })
                                if (obj.data.deptId == "1005") $(".admin").css('display', 'block');
                                window.localStorage.email = obj.data.email;
                                window.localStorage.username = obj.data.userName;
                                $(".username").html(obj.data.userName);
                            }
                    }
                    else if (response.error) {
                        alert(response.error.code);
                    }
                }
            );

    }

    function openEmail(tableName,reportID,reportDesceiption,address,datetime) {
        $('#sendEmailDialog').modal('show');
        $('#email').val(window.localStorage.email);
        $('#sendEmailForm').unbind('submit');
        $('#sendEmailForm').on('submit', function(e){
                    e.preventDefault();
                   if ($('#email').val()== "") {
                               $('#error').html("You can't leave this empty.")
                               $('#error').css('display', 'block');
                               $('#email').focus();
                               return;
                           }
                   if (confirm('Forward Report ....'))
                        {
                            enableloading();
                            var email = $('#email').val()
                            gapi.client.myApi.sendEmail({"email": email,"table": tableName, "reportID": reportID, "description": reportDesceiption,"place":address,"datetime":datetime }).execute(
                            function(response){
                               disableLoading();
                               if(response.error)
                                   console.log(response.error.code);
                               $('#sendEmailDialog').modal('toggle');
                               alert(response.data);
                               }
                            );
                        }
                   else return;

                });
    }


