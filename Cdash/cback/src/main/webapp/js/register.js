function showRegister(){

}

function init() {

      var apiName = 'myApi';
      var apiVersion = 'v1';
      var apiRoot = 'https://ktpcloud-1470216325260.appspot.com/_ah/api';
      var callback = function() {
         showRegister();
      }
      gapi.client.load(apiName, apiVersion, callback, apiRoot);

    }

function initForTest() {
      var apiName = 'myApi';
      var apiVersion = 'v1';
      var apiRoot = 'https://' + window.location.host + '/_ah/api';
      if (window.location.hostname == 'localhost'
          || window.location.hostname == '127.0.0.1'
          || ((window.location.port != "") && (window.location.port > 1023))) {
            // We're probably running against the DevAppServer
            apiRoot = 'http://' + window.location.host + '/_ah/api';
      }
      var callback = function() {
        showRegister();
      }
      gapi.client.load(apiName, apiVersion, callback, apiRoot);
    }