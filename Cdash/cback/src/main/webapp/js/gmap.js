var map;
var objdata;  

function show_data()
{
$('#reformed').reform(objdata, {'editor':'data'});

$('#data-details').modal('show');

}

function showOnMapPS(){

   title = document.getElementById('report-detail-table-name').innerText;
    
 document.getElementById('map').style.display = 'none';
    $('#report-map-title').html(title);
    $('#report-map').modal('show');

 
         gapi.client.myApi.gridReports({'fromDate':dt1, 'toDate':dt2 , 'where' :where +  " and ps_id = " + ps_id + " "}).execute(

            function(response){
          document.getElementById('map').style.display = 'block';   
                map = new google.maps.Map(document.getElementById('map'), {
                                  center: {lat: 22.595212, lng: 88.403940},
                                  zoom: 10
                                });
                                
                var table;
                var markers = [];
                if(response.error)
                    console.log(response.error.code);
                objs = JSON.parse(response.result.data);

                if (objs.data.length == 0) return;

                var gm = google.maps;
                var iw = new gm.InfoWindow();
              var oms = new OverlappingMarkerSpiderfier(map,
                {markersWontMove: true, markersWontHide: true,circleFootSeparation: 100, spiralFootSeparation: 100});

                var shadow = new gm.MarkerImage(
                        'https://www.google.com/intl/en_ALL/mapfiles/shadow50.png',
                        new gm.Size(1000, 1420),  // size   - for sprite clipping
                        new gm.Point(0, 0),   // origin - ditto
                        new gm.Point(800, 900)  // anchor - where to meet map location
                      );

              oms.addListener('click', function(marker) {
                iw.setContent(marker.desc);
                iw.open(map, marker);
              });
              oms.addListener('spiderfy', function(markers) {
                for(var i = 0; i < markers.length; i ++) {
                  markers[i].setShadow(null);
                }
                iw.close();
              });
              oms.addListener('unspiderfy', function(markers) {
                for(var i = 0; i < markers.length; i ++) {
                  markers[i].setShadow(shadow);
                }
              });

                var bounds = new gm.LatLngBounds();

                objs.data.forEach(function(obj){
                    var myLatlng = new google.maps.LatLng(obj.latitude,obj.longitude);
                    bounds.extend(myLatlng);
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        title:obj.description,
                        shadow: shadow
                    });

                    var content = "<img width=\"50%\" style=\"float:left; margin: 0 1em 1em 0; height:auto;\" src=\"" + obj.imagelocation + "\">";
                    content += "<h4>Report Detail </h4><hr/><br/>";
                    content += "<p><b>Type: </b>"+getReportName(obj.type)+"</p>";
                     if (obj.data)
                    {
			var json = JSON.parse(obj.data);
	            content += "<p><b>Name: </b>"+json.name+"</p>";
	             content += "<p><b>Contact: </b>"+json.mobile+"</p>";

	            }

                    content += "<p><b>Location: </b>"+obj.address+"</p>";
                    content+= "<p><b>Datetime: </b>: "+obj.datetime+"</p>";
                    if (obj.data)
                    {
                    objdata = obj.data;
                       content+= "<p><button onclick='show_data()'>Details</button></p>";

                    }
                    else
                    content+= "<p><b>Description: </b>: "+obj.description.toUpperCase() +"</p>"
                    ;
                    //content += "<p><b>Status: </b>: "+obj.status.toUpperCase()+"</p>"
                    marker.info = new google.maps.InfoWindow({
                      content: content
                    });

                    marker.desc = content;

                    if (obj.active)  marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
                    else marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');

                    marker.setMap(map);
                    oms.addMarker(marker);
                })
                map.fitBounds(bounds);
             

            }
        )
}

 
function addMarkerForReport(){

    var filter = filterReportDetail();
    showOnMap(filter.tableName ,filter.fromDate,filter.toDate ,filter.status);
}

function showOnMap(tableName,fromDate,toDate,status){

    title = getReportName(tableName)+" Report.";
    
    if (status == 'TenantRegister')
    {
    title = 'Tenant Registration Report.';
    }
    
    if (status == 'SecurityGuard')
    {
    title = 'Security Guard Registration Report.';
    }

  if (status == 'DomesticHelp')
    {
    title = 'Domestic Help Registration Report.';
    }
    
if (status == 'ServiceProvider')
    {
    title = 'Service Provider Registration Report.';
    }
document.getElementById('map').style.display = 'none';
    $('#report-map-title').html(title);
    $('#report-map').modal('show');


    gapi.client.myApi.getReports({'tableName':tableName , 'fromDate':fromDate, 'toDate':toDate , 'status' :status}).execute(
            function(response){
            document.getElementById('map').style.display = 'block';   
                map = new google.maps.Map(document.getElementById('map'), {
                                  center: {lat: 22.595212, lng: 88.403940},
                                  zoom: 10
                                });
                                
                var table;
                var markers = [];
                if(response.error)
                    console.log(response.error.code);
                objs = JSON.parse(response.result.data);

                if (objs.data.length == 0) return;

                var gm = google.maps;
                var iw = new gm.InfoWindow();
              var oms = new OverlappingMarkerSpiderfier(map,
                {markersWontMove: true, markersWontHide: true,circleFootSeparation: 100, spiralFootSeparation: 100});

                var shadow = new gm.MarkerImage(
                        'https://www.google.com/intl/en_ALL/mapfiles/shadow50.png',
                        new gm.Size(1000, 1420),  // size   - for sprite clipping
                        new gm.Point(0, 0),   // origin - ditto
                        new gm.Point(800, 900)  // anchor - where to meet map location
                      );

              oms.addListener('click', function(marker) {
                iw.setContent(marker.desc);
                iw.open(map, marker);
              });
              oms.addListener('spiderfy', function(markers) {
                for(var i = 0; i < markers.length; i ++) {
                  markers[i].setShadow(null);
                }
                iw.close();
              });
              oms.addListener('unspiderfy', function(markers) {
                for(var i = 0; i < markers.length; i ++) {
                  markers[i].setShadow(shadow);
                }
              });

                var bounds = new gm.LatLngBounds();

                objs.data.forEach(function(obj){
                if (!obj.DeviceId) 
                obj.DeviceId = obj.devid;
                
                if (!obj.imagelocation)
                obj.imagelocation = '../dist/img/blank.png';
                 
                    var myLatlng = new google.maps.LatLng(obj.latitude,obj.longitude);
                    bounds.extend(myLatlng);
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        title:obj.DeviceId,
                        shadow: shadow
                    });

                    var content = "<img width=\"50%\" style=\"float:left; margin: 0 1em 1em 0; height:auto;\" src=\"" + obj.imagelocation + "\">";
                    content += "<h4>Report Detail </h4><hr/><br/>";
                     if (obj.address)
                    content += "<p><b>Location: </b>"+obj.address+"</p>";
                    content+= "<p><b>Datetime: </b>: "+obj.datetime+"</p>";
                    if (obj.data)
                    {
                    objdata = obj.data;
                       content+= "<p><button onclick='show_data()'>Details</button></p>";

                    }
                    else
                    {
                    if (obj.description)
                    content+= "<p><b>Description: </b>: "+obj.description.toUpperCase() +"</p>";
                    else                   
                    content+= "<p><b>Device Id: </b>: "+obj.DeviceId +"</p>";

                    }
                    //content += "<p><b>Status: </b>: "+obj.status.toUpperCase()+"</p>"
                    marker.info = new google.maps.InfoWindow({
                      content: content
                    });

                    marker.desc = content;

                    if (obj.active)  marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
                    else marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');

                    marker.setMap(map);
                    oms.addMarker(marker);
                })
                map.fitBounds(bounds);
             

            }
        )
}

function addMarkerToday(tableName,type){
    var d = new Date();

    var month = d.getMonth()+1;
    var day = d.getDate();

    var date = d.getFullYear() + '-' +
        (month<10 ? '0' : '') + month + '-' +
        (day<10 ? '0' : '') + day;

    showOnMap(tableName,date,date,type);
}

