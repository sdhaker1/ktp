
function showDept() {
    enableloading();
    gapi.client.myApi.departments().execute(
        function(response){
            disableLoading();
            var table;
            if(response.error)
                console.log(response.error.code);
            objs = JSON.parse(response.result.data);
            var table_content = "<thead class=\"w3-blue\"\>\
                                    <tr>\
                                        <th>Department Name</th>\
                                        <th>Email</th>\
                                        <th>Contact No</th>\
                                        <th>Description</th>\
                                        <th>Reports</th>\
                                        <th>Active</th>\
                                        <th><button class=\"cursor-pointer w3-btn w3-blue\"><i class=\"fa fa-pencil\"></i></button></i></th>\
                                    </tr>\
                                </thead>\
                                <tbody>";

            objs.data.forEach(function(obj){
                table_content += "<tr>";
                table_content += "<td>" + obj.departmentName +"</td>";
                table_content += "<td>" + obj.email +"</td>";
                table_content += "<td>" + obj.contactNo +"</td>";
                table_content += "<td>" + obj.description +"</td>";
                table_content += "<td>" + obj.listReport +"</td>";
                table_content += "<td>" + obj.active +"</td>";
                table_content += "<td><button data-id=\""+obj.deptid+"\" class=\"department-button cursor-pointer w3-btn w3-white w3-ripple w3-round w3-text-theme\"><i class=\"fa  fa-pencil\"></i></button></td>";
                table_content +="</tr>";
            })
            table_content += "</tbody>"

            $('#table-dept').html(table_content);

            table = $('#table-user').DataTable({
            "destroy": true,
            "paging": true,
            "lengthChange": true,
            "aLengthMenu": [[10,20,-1], [10,20,"All"]],
            "iDisplayLength": 5,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false
            });

            $(".department-button").click(function() {
                var $row = $(this).closest("tr");    // Find the row
                var $tds = $row.find("td");
                var data = [];
                data[0] = $(this).data("id");
                $.each($tds, function() {
                    data.push($(this).text());
                });
                fillToForm(data);
            });
        }
    )

}

function fillToForm(data) {
   $('#deptname').val(data[1]);
   $('#email').val(data[2]);
   $('#contactno').val(data[3]) ;
   $('#description').val(data[4]) ;
   $("#select-department option").prop("selected", false);
   var values = data[5];
   $.each(values.split(","), function(i,e){
       $("#select-department option[value='" + e + "']").prop("selected", true);
   });
   $("#select-department").select2();
   $('#dept-detail').modal('show');
   $('#new-dept-form').unbind('submit');
   $('#new-dept-form').on('submit', function(e){
               e.preventDefault();
               if(!checkCreateDept()) {
                  return;
               }
               if (confirm('Department updated...'))
                   {
                       var deptid = data[0];
                       var departmentName = $('#deptname').val();
                       var email = $('#email').val();
                       var description = $('#description').val();
                       var contactno = $('#contactno').val();
                       var listReport = $('#select-department').val().toString();
                       updateDept(deptid,departmentName,email,contactno,description,listReport);
                   }
               else  {
                   return;
               }
           });
}

function updateDept(deptid,departmentName,email,contactno,description,listReport) {
      $('#new-dept-form :input').prop("disabled", true);
      enableloading();
      gapi.client.myApi.updateDept({'deptid':deptid, 'departmentName':departmentName, 'email':email,'contactno':contactno, 'description':description,'listReport':listReport}).execute(
                     function(response){
                         $('#new-dept-form :input').prop("disabled", false);
                         disableLoading();
                         if(response.error)
                             console.log(response.error.code);
                         $('#dept-detail').modal('toggle');
                         alert(response.data);
                         location.reload();
                     }
                 )
}

function init() {

      var apiName = 'myApi';
      var apiVersion = 'v1';
    var apiRoot = 'https://18-dot-citizen-dot-ktpcloud-1470216325260.appspot.com/_ah/api';
      var callback = function() {
         showDept();
      }
      gapi.client.load(apiName, apiVersion, callback, apiRoot);

    }

function initForTest() {
  var apiName = 'myApi';
  var apiVersion = 'v1';
  var apiRoot = 'https://' + window.location.host + '/_ah/api';
  if (window.location.hostname == 'localhost'
      || window.location.hostname == '127.0.0.1'
      || ((window.location.port != "") && (window.location.port > 1023))) {
        // We're probably running against the DevAppServer
        apiRoot = 'http://' + window.location.host + '/_ah/api';
  }
  var callback = function() {
    showDept();
  }
  gapi.client.load(apiName, apiVersion, callback, apiRoot);
}


function openCreteDept(){

    $('#deptname').val('');
    $('#email').val('');
    $('#description').val('');
    $('#contactno').val('');
    $('#select-department').val('');
    $('#dept-modal-title').html("Create New Department");
    $('#select-department').select2();
    $('#dept-detail').modal('show');
    $('#new-dept-form').unbind('submit');
    $('#new-dept-form').on('submit', function(e){
            e.preventDefault();
            if(!checkCreateDept()) {
               return;
            }
            if (confirm('Create new department...'))
                {
                    var departmentName = $('#deptname').val();
                    var email = $('#email').val();
                    var description = $('#description').val();
                    var contactno = $('#contactno').val();
                    var listReport = $('#select-department').val().toString();
                    createNewDept(departmentName,email,contactno,description,listReport);
                }
            else  {
                return;
            }
        });
}
function checkCreateDept() {

    if ($('#deptname').val()== "") {
            $('#error').html("You can't leave this empty.")
            $('#error').css('display', 'block');
            $('#deptname').focus();
            return false;
        }

     if ($('#email').val()== "") {
                 $('#error').html("You can't leave this empty.")
                 $('#error').css('display', 'block');
                 $('#email').focus();
                 return false;
             }

     if ($('#contactno').val()== "") {
                 $('#error').html("You can't leave this empty.")
                 $('#error').css('display', 'block');
                 $('#contactno').focus();
                 return false;
             }
     if ($('#description').val()== "") {
                      $('#error').html("You can't leave this empty.")
                      $('#error').css('display', 'block');
                      $('#description').focus();
                      return false;
                  }
    return true;
}

function createNewDept(departmentName,email,contactno,description,listReport){
    $('#new-dept-form :input').prop("disabled", true);
    enableloading();
    gapi.client.myApi.createDept({'departmentName':departmentName, 'email':email,'contactno':contactno, 'description':description,'listReport':listReport}).execute(
               function(response){
                   disableLoading();
                   $('#new-dept-form :input').prop("disabled", false);
                   if(response.error)
                       console.log(response.error.code);
                   $('#dept-detail').modal('toggle');
                   alert(response.data);
                   location.reload();
               }
           )
}

$(document).ready(function() {
    $("#contactno").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

$(window).load(function(){
    $(".username").html(window.localStorage.username);
});
